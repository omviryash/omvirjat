<?php 
	session_start();
	include "etc/om_config.inc";
  	include "etc/functions.inc";
	
	$final_ary = array();
	
	$newrows = '';
	
	$sql_com = "SELECT CONCAT(itemid,' ',expirydate) AS item, SUM(CASE buySell WHEN  'Buy' THEN qty ELSE 0 END) AS buy, SUM(CASE `buySell`
    WHEN 'Sell' THEN `qty` ELSE 0 END) AS sell, SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) - SUM(CASE `buySell` WHEN 'Sell' THEN `qty`
    ELSE 0 END) AS difference FROM tradetxtcx  where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."' GROUP BY itemid, expirydate";
	
	$item_rs = mysql_query($sql_com);
		
	$newrows = '<table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse; width:100%">';
	$newrows .= '<tr><th>Item</th><th>Buy</th><th>Sell</th><th>Balance</th></tr>';
	while($item_row = mysql_fetch_array($item_rs)){
		$newrows .= '<tr>';
		$newrows .= '<td>'.$item_row['item'].'</td>';	
		$newrows .= '<td align="right" class="com_profit">'.$item_row['buy'].'</td>';
		$newrows .= '<td align="right" class="com_loss">'.$item_row['sell'].'</td>';
		$newrows .= '<td align="right" class="'.($item_row['difference'] >= 0?"mcx_profit":"mcx_loss").'">'.$item_row['difference'].'</td>';
		$newrows .= '</tr>';
	}
	$newrows .= '</table>';
	
	if($newrows != ""){
		$final_ary['hasrow'] = mysql_num_rows($item_rs);
		$final_ary['rows'] = $newrows;
	}
	
	echo json_encode($final_ary);
?>