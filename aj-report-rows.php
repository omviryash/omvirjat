<?php 
	session_start();
	include "etc/om_config.inc";
  	include "etc/functions.inc";
	
	$final_ary = array();
	
	$newrows = '';
	
	if(isset($_POST['tradetype']) && trim($_POST['tradetype']) != "" && isset($_POST['last_mcxid']) && trim($_POST['last_mcxid']) != "" && isset($_POST['last_comid']) && trim($_POST['last_comid']) != ""){
		$where = '';
	
		if(trim($_POST['clientId']) != "" && strtoupper($_POST['clientId']) != 'ALL'){
			$where .= " and clientId='".$_POST['clientId']."'";
		}
		
		if(trim($_POST['itemId']) != "" && strtoupper($_POST['itemId']) != 'ALL'){
			$where .= " and itemId='".$_POST['itemId']."'";
		}
	
		$sql_mcx = "select tradeId, buySell, qty, price, tradeDate, tradeTime, itemId, clientId, expiryDate, vendor, 'MCX' as tradetype, ipaddress from tradetxt where tradeId > '".$_POST['last_mcxid']."' and tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'".$where;
		$sql_com = "select tradeId, buySell, qty, price, tradeDate, tradeTime, itemId, clientId, expiryDate, vendor, 'Commex' as tradetype, ipaddress from tradetxtcx where tradeId > '".$_POST['last_comid']."' and tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'".$where;
		
		/*ADDED ON 27-MAY-2015*/  
		/*$data_mcx_tot_buy = mysql_query("SELECT SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) AS buy FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
		$row_mcx_tot_buy = mysql_fetch_array($data_mcx_tot_buy);
		$mcx_tot_buy = $row_mcx_tot_buy['buy'];*/
		
		/*ADDED ON 27-MAY-2015*/   
		/*$data_mcx_tot_sell = mysql_query("SELECT SUM(CASE buySell WHEN 'Sell' THEN qty ELSE 0 END) AS sell FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
		$row_mcx_tot_sell = mysql_fetch_array($data_mcx_tot_sell);
		$mcx_tot_sell = $row_mcx_tot_sell['sell'];*/
		
		/*ADDED ON 27-MAY-2015*/  
		/*$data_com_tot_buy = mysql_query("SELECT SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) AS buy FROM tradetxtcx where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
		$row_com_tot_buy = mysql_fetch_array($data_com_tot_buy);
		$com_tot_buy = $row_com_tot_buy['buy'];*/
			  
		/*ADDED ON 27-MAY-2015*/  		  
		/*$data_com_tot_sell = mysql_query("SELECT SUM(CASE buySell WHEN 'Sell' THEN qty ELSE 0 END) AS sell FROM tradetxtcx where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
		$row_com_tot_sell = mysql_fetch_array($data_com_tot_sell);
		$com_tot_sell = $row_com_tot_sell['sell']; */
  
		if($_POST['tradetype'] == "all"){
			$trade_rs = mysql_query($sql_mcx.' UNION ALL '.$sql_com);
			$tot_buy = $mcx_tot_buy + $com_tot_buy;
			$tot_sell = $mcx_tot_sell + $com_tot_sell;
		}
		elseif($_POST['tradetype'] == "mcx"){
			$trade_rs = mysql_query($sql_mcx);
			$tot_buy = $mcx_tot_buy;
			$tot_sell = $mcx_tot_sell;
		}
		elseif($_POST['tradetype'] == "com"){
			$trade_rs = mysql_query($sql_com);
			$tot_buy = $com_tot_buy;
			$tot_sell = $com_tot_sell;
		}
		
		$has_new_row = false;
		
		while($trade_row = mysql_fetch_array($trade_rs)){
			$has_new_row = true;
			
			if($trade_row['tradetype'] == "MCX"){
				$last_mcxid = $trade_row['tradeId'];
			}
			else{
				$last_comid = $trade_row['tradeId'];
			}
			
			if($trade_row['tradetype'] == "MCX"){
				$newrows .= '<tr class="'.(strtoupper($trade_row['buySell']) == "BUY"?"mcx_profit":"mcx_loss").'">';
			}
			else{
				$newrows .= '<tr class="'.(strtoupper($trade_row['buySell']) == "BUY"?"com_profit":"com_loss").'">';
			}
			
			$newrows .= '<td>'.$trade_row['tradetype'].'</td>';	
			
			$clientId = $trade_row['clientId'] != "0" ? $trade_row['clientId'] : ""; 
			$newrows .= '<td>'.$trade_row['clientId'].'</td>';
					
			$newrows .= '<td>'.$trade_row['itemId'].'</td>';
			
				$newrows .= '<td align="right">'.$trade_row['qty'].'</td>';
				$newrows .= '<td align="right">'.$trade_row['price'].'</td>';
			
			$newrows .= '<td>'.date('d-m-y', strtotime($trade_row['tradeDate'])).'</td>';
			$newrows .= '<td>'.$trade_row['tradeTime'].'</td>';
			
			$newrows .= '<td>'.$trade_row['expiryDate'].'</td>';
			$newrows .= '<td>'.$trade_row['ipaddress'].'</td>';
			
			
			
			$newrows .= '</tr>';
		}
		
		/*ADDED ON 27-MAY-2015*/  
		/*if($has_new_row){
			$newrows .= '<tr id="tot_row"><td><strong>TOTAL</strong></td><td></td><td>'.$tot_buy.'</td><td></td><td>'.$tot_sell.'</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>';
		}*/
		
		if($has_new_row){
			$final_ary['hasrow'] = 1;
			$final_ary['rows'] = $newrows;
			
			if($last_mcxid != ""){
				$final_ary['last_mcxid'] = $last_mcxid;
			}
			else{
				$final_ary['last_mcxid'] = -1;
			}
			
			if($last_comid != ""){
				$final_ary['last_comid'] = $last_comid;
			}
			else{
				$final_ary['last_comid'] = -1;
			}
		}
		else{
			$final_ary['hasrow'] = 0;
		}
	}
	else{
		$final_ary['hasrow'] = 0;
	}
	
	echo json_encode($final_ary);
?>