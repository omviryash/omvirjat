<?php
session_start();
//$host = "localhost"; //database location
//$user = "omvirco_om2"; //database username
//$pass = "omvir159951"; //database password
//$dbName = "omvirco_mhare"; //database name

include "./etc/om_config.inc";
$message = "";
if(!isset($_SESSION['user'])) {
	header("Location:login.php");
	exit;
}
if(isset($_GET['id']))
{
	$id = $_GET['id'];
	
	$q = "select * from user where id=".$id;

	$r = mysql_query($q) or print mysql_error();
	$u = mysql_result($r,0,1);
}


if(isset($_POST['newPassword'])) {
	$checkLogin = "UPDATE user SET username = '".$_POST['username']."',password = '".md5($_POST['newPassword'])."' WHERE id = ".$_POST['id_']."";
	$checkLoginresult = mysql_query($checkLogin);
	
	header("Location:signout.php");
	exit;
}


?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>J & M Company</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/admin.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
	
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">J & M Company</a>
          </div>
          <!--<div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="menu.php">Home</a></li>
              <li><a href="clientTrades.php">Mcx Trades</a></li>
              <li><a href="clientTradesComex.php">Comex Trades</a></li>
              <li><a href="clientTrades.php?display=gross">Mcx Gross</a></li>
              <li><a href="clientTradesComex.php?display=gross">Comex Gross</a></li>
            </ul>
			<ul class="nav navbar-nav navbar-right">
                <li><a href="signout.php">Signout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

        <div class="form-box" id="login-box">
            <div class="header">J & M Company</div>
            <form action="changePass.php" method="post" name="changePass" id="changePass" >
                <div class="body bg-gray" >
					<div id="message"><?php echo $message; ?></div>
                    <div class="form-group">
						<input type="text" name="username" id="username" class="form-control" placeholder="User Name" value="<?php if(isset($u)) { echo $u; } ?>"/>
						<input type="hidden" name="id_" id="id_" value="<?php if(isset($id)) { echo $id; } ?>"/>
                    </div>
					<div class="form-group">
                        <input type="password" name="newPassword" id="newPassword" class="form-control" placeholder="New Password"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="confPassword" id="confPassword" class="form-control" placeholder="Conform Password"/>
                    </div>
					
                </div>
                <div class="footer">                                                               
                    <button type="submit" name="cp" id="cp" class="btn bg-olive btn-block">Change Password</button>  
                </div>
            </form>
        </div>
        <script src="js/admin/jquery.min.js"></script>
        <script type="text/javascript" >
			$(document).ready(function() {
				$("#confPassword, #newPassword").focusout(function() {
					$("#message").html("");
					if($("#confPassword").val() == "" ||  $("#newPassword").val() == "") {
						$("#message").html("New password and Confirm Password are required").css("color","red");
					}
					if($("#confPassword").val() != $("#newPassword").val()) {
						$("#message").html("New password and Conform password must be same").css("color","red");
					}
				});
				$("#cp").on("click",function(e) {
					e.preventDefault();
					$("#message").html("");
					if($("#confPassword").val() == "" ||  $("#newPassword").val() == "") {
						$("#message").html("New password and Confirm Password are required").css("color","red");
					} else if($("#confPassword").val() != $("#newPassword").val()) {
						$("#message").html("New password and Conform password must be same").css("color","red");
					} else {
						document.changePass.submit();
					}
				});
			});
		</script>
        <script src="js/admin/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>								