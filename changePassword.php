<?php
session_start();

include "./etc/om_config.inc";

if(!isset($_SESSION['user'])) {
	header("Location:login.php");
	exit;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>J & M Company</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/admin.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
	
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">J & M Company</a>
          </div>
          <!--<div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="menu.php">Home</a></li>
              <li><a href="clientTrades.php">Mcx Trades</a></li>
              <li><a href="clientTradesComex.php">Comex Trades</a></li>
              <li><a href="clientTrades.php?display=gross">Mcx Gross</a></li>
              <li><a href="clientTradesComex.php?display=gross">Comex Gross</a></li>
            </ul>
			<ul class="nav navbar-nav navbar-right">
                <li><a href="signout.php">Signout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
		</nav>
	                <div class="form-group">
				       
					      

<table class="table-responsive" align="center" border="2" width="500px">
		<tr>
			<th>UserName</th>
			<th>Type</th>
			<th>Action</th>
		</tr>
       	<?php
			$query = "select * from user";
			$rs = mysql_query($query);
			
			while($row = mysql_fetch_array($rs))
			{
			?>
				<tr>
					<td>
						<?php echo $row['username']; ?>
					</td>
					<td>
						<?php echo $row['type']; ?>
					</td>
					<td>
						<a href="changePass.php?id=<?php echo $row['id']; ?>">Edit</a>
					</td>
				</tr>					
											
			<?php
			}								
			?>
</table>									
</div>
			</body>
<html>
