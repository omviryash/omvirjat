<?php
session_start();
if(!isset($_SESSION['user'])) {
    header("Location:login.php?goTo=clientTradesComex");
	exit;
}

include "./etc/om_config.inc";
if(!isset($_SESSION['toDate'])) {
  header("Location: selectDtSession.php?goTo=clientTradesComex");
  exit;
} else {
  include "./etc/functions.inc";
  $smarty = new SmartyWWW();
  
  $message = "";
  $clientTotNetProfitLossInRs = 0;
  $clientIdIn = "0";
  
//if profitBankRate / lossBankRate NOT set, then set :Start
  if(!isset($_SESSION['profitBankRate']))
    $_SESSION['profitBankRate'] = 0;
  if(!isset($_SESSION['lossBankRate']))
    $_SESSION['lossBankRate']   = 0;
//if profitBankRate / lossBankRate NOT set, then set :End
////Request parameters, if passed : transfer to proper variable :Start
  if(isset($_REQUEST['display']))
    $display = $_REQUEST['display'];
  else
    $display = 'trades';
  if(isset($_REQUEST['itemId']))
    $currentItemId = $_REQUEST['itemId'];
  else
    $currentItemId = "All";
////Request parameters, if passed : transfer to proper variable :End
  applyConfirm();
  
	////Group Selection for Combo : Start
	$groupIdSelected  = isset($_REQUEST['groupName']) ? $_REQUEST['groupName'] : "";
	$groupId          = array();
	$groupName        = array();
	
		
	$groupQuery = "SELECT groupId,groupName
	                 FROM groupList
	                ORDER BY groupName";
	$groupQueryResult = mysql_query($groupQuery);
	$groupCount             = 0;
  $groupId[$groupCount]   = 0;
  $groupName[$groupCount] = 'All';	
  $groupCount++;
	while($groupRow = mysql_fetch_array($groupQueryResult))
	{
	  $groupId[$groupCount]     = $groupRow['groupId'];
	  $groupName[$groupCount]   = $groupRow['groupName'];
	  $groupCount++;
	}
	////Group Selection for Combo : End
	  
  //Client records :Start
  $clientIdSelected = isset($_REQUEST['clientId'])?$_REQUEST['clientId']:0;
  $clientIdValues = array();
  $clientIdOptions = array();
  $i = 0;
  $clientIdValues[0]  = 0;
  $clientIdOptions[0] = 'All';
  $i++;

  $clientQuery1  = "SELECT * FROM client WHERE clientId IN ( ";
  $clientQuery1.= "SELECT clientId FROM tradetxtcx WHERE 1 = 1 ";
  if(isset($_SESSION['fromDate']))
  {
     $clientQuery1 .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
  }
  $clientQuery  .= $clientQuery1." ) ";
  if(isset($_REQUEST['groupName']) && $_REQUEST['groupName'] != 'All')
	{
	  $clientQuery    .= " AND groupName = '".$groupIdSelected."' ";
	}
  $clientQuery .= " ORDER BY clientId";
  $clientResult = mysql_query($clientQuery);
  while($clientRow = mysql_fetch_array($clientResult))
  {
    $temp_set = 1;
    $clientIdValues[$i] = $clientRow['clientId'];
	$clientIdOptions[$i] =$clientRow['clientId'];
    //$clientIdOptions[$i] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];

    $clientProfitBankRate[$clientRow['clientId']] = $clientRow['profitBankRate'];
    $clientLossBankRate[$clientRow['clientId']]   = $clientRow['lossBankRate'];

    $i++;
  }

  //Client records :End
  //Item records :Start
  $itemIdSelected = $currentItemId;
  $itemIdValues = array();
  $itemIdOptions = array();
  $itemCount = 0;
  $itemIdValues[0]  = "All";
  $itemIdOptions[0] = "All";
  $itemCount++;

  $itemRecords = array();
  $clientBrok  = array();
  $itemQuery = "SELECT * FROM item WHERE exchange LIKE 'Comex' ORDER BY itemId";
  $itemResult = mysql_query($itemQuery);
  while($itemRow = mysql_fetch_array($itemResult))
  {
    $itemRecords[$itemRow['itemId']]['priceOn']   = $itemRow['priceOn'];
    $itemRecords[$itemRow['itemId']]['min']       = $itemRow['min'];
    $itemRecords[$itemRow['itemId']]['mulAmount'] = $itemRow['mulAmount'];
    $itemRecords[$itemRow['itemId']]['high']    = $itemRow['high'];
    $itemRecords[$itemRow['itemId']]['low']     = $itemRow['low'];
    
    //ClientBrok :Start
    for($i=0;$i<count($clientIdValues);$i++)
      $clientBrok[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
    //ClientBrok :End
    
    $itemIdValues[$itemCount]  = $itemRow['itemId'];
    $itemIdOptions[$itemCount] = $itemRow['itemId'];
    $itemCount++;
  }
  //Item records :End
  //Override clientBrok from clientBrok table :Start
  $clientBrokQuery = "SELECT * FROM clientbrok ORDER BY clientId";
  $clientBrokResult = mysql_query($clientBrokQuery);
  while($clientBrokRow = mysql_fetch_array($clientBrokResult))
  {
    $clientBrok[$clientBrokRow['clientId']][$clientBrokRow['itemId']] = $clientBrokRow['oneSideBrok'];
  }
  //Override clientBrok from clientBrok table :Start
  
  //Expiry records :Start
  if(isset($_REQUEST['expiryDate']))
  {
    if($_REQUEST['itemIdChanged']==1 || $currentItemId=="All")
      $expiryDateSelected = 0;
    else
      $expiryDateSelected = $_REQUEST['expiryDate'];
  }
  else
    $expiryDateSelected = 0;
    
  $expiryDateValues = array();
  $expiryDateOptions = array();
  $i = 0;
  $expiryDateValues[0]  = 0;
  $expiryDateOptions[0] = 'All';
  $i++;

  if($currentItemId!="All")
  {
    $expiryQuery = "SELECT * FROM expiry
                      ORDER BY itemId, expiryDate";
    $expiryResult = mysql_query($expiryQuery);
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      if($expiryRow['itemId'] == $currentItemId)
      {
        $expiryDateValues[$i]  = $expiryRow['expiryDate'];
        $expiryDateOptions[$i] = $expiryRow['expiryDate'];
        $i++;
      }
    }
  }
  //Expiry records :End

  $trades = array();
  $prevClientId = 0;
  $prevItemId = '';
  $prevExpiryDate = '';
  $valuesForGrossLine = array();
  $wholeItemArr       = array();
  $wholeItemArrCount  = -1;  //-1, because we do ++ when we store 0

  $wholeBuyQty        = 0;
  $wholeTotBuyAmount  = 0;
  $wholeBuyRash       = 0;
  $wholeSellQty       = 0;
  $wholeTotSellAmount = 0;
  $wholeSellRash      = 0;
  $wholeProfitLoss    = 0;
  $wholeOneSideBrok   = 0;
  $wholeNetProfitLoss = 0;
  $wholeNetLossOnly   = 0;
  $wholeNetProfitOnly = 0;
	$singleClientOnly   = 0;

  $valuesForGrossLine['openBuyQty'] = 0;
  $valuesForGrossLine['openSellQty'] = 0;
  $valuesForGrossLine['totBuyQty'] = 0;
  $valuesForGrossLine['totSellQty'] = 0;
  $valuesForGrossLine['totBuyAmount'] = 0;
  $valuesForGrossLine['totSellAmount'] = 0;
  $i = 0;
  $tradesQuery = "SELECT * FROM tradetxtcx";
/////////////////////////////////////////////Where Condition :Start
  $whereGiven = false;
  if(isset($_REQUEST['clientId']) && $_REQUEST['clientId']!=0)
  {
  	$singleClientOnly = 1;
    $tradesQuery .= " WHERE clientId = ".$_REQUEST['clientId'];
    $whereGiven = true;
  }
  if($whereGiven == false)
  {
    $tradesQuery .= " WHERE 1 = 1 ";
    $whereGiven = true;
  }
  if(isset($_REQUEST['groupName']) && $_REQUEST['groupName'] != 'All')
  {
  	$tradesQuery .= " AND clientId IN (SELECT clientId 
  	                                     FROM client 
  	                                    WHERE groupName = '".$_REQUEST['groupName']."')";
  }
  if(isset($_REQUEST['buySellOnly']) && $_REQUEST['buySellOnly'] != 'All')
  	  $tradesQuery .= " AND buySell LIKE '".$_REQUEST['buySellOnly']."'";
  
  if($currentItemId!="All")
  {
    if($whereGiven)
      $tradesQuery .= " AND   itemId LIKE '".$currentItemId."'";
    else
      $tradesQuery .= " WHERE itemId LIKE '".$currentItemId."'";
    $whereGiven = true;
  }
  if(isset($_REQUEST['expiryDate']) && $_REQUEST['expiryDate']!='0' && $_REQUEST['itemIdChanged']!=1 && $currentItemId!="All")
  {
    if($whereGiven)
      $tradesQuery .= " AND   expiryDate LIKE '".$_REQUEST['expiryDate']."'";
    else
      $tradesQuery .= " WHERE expiryDate LIKE '".$_REQUEST['expiryDate']."'";
    $whereGiven = true;
  }

  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
    if($whereGiven)
     $tradesQuery .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
    else
    {
      $tradesQuery .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
      $whereGiven = true;
    }
  }

  if($display == 'openStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND standing = -1";
    else
    {
      $tradesQuery .= " WHERE standing = -1";
      $whereGiven = true;
    }
  }
  if($display == 'closeStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND standing = 1";
    else
    {
      $tradesQuery .= " WHERE standing = 1";
      $whereGiven = true;
    }
  }
  if($display == 'openCloseStand')
  {
    if($whereGiven)
     $tradesQuery .= " AND (standing = -1 OR standing = 1)";
    else
    {
      $tradesQuery .= " WHERE (standing = -1 OR standing = 1)";
      $whereGiven = true;
    }
  }
  
  if($display == 'noInnerStand')
  {
    if($whereGiven)
      $tradesQuery .= " AND NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
    else
    {
      $tradesQuery .= " WHERE NOT ((standing = -1 OR standing = 1) 
                       AND tradeDate != '".$_SESSION['fromDate']."'
                       AND tradeDate != '".$_SESSION['toDate']."')";
      $whereGiven = true;
    }
  }
/////////////////////////////////////////////Where Condition :End
  $tradesQuery .= " ORDER BY clientId, itemId ASC, expiryDate, 
                     tradeDate ASC, standing, tradeTime, tradeId";
  $tradesResult = mysql_query($tradesQuery);
  if(mysql_num_rows($tradesResult) == 0)
    $message = "No records!";
  else
  {
    while($tradesRow = mysql_fetch_array($tradesResult))
    {
    	$clientIdIn                   .= ",".$tradesRow['clientId'];
      $trades[$i]['tradeId']         = $tradesRow['tradeId'];
      $trades[$i]['confirmed']       = $tradesRow['confirmed'];
      $trades[$i]['highLowConf']     = $tradesRow['highLowConf'];
      $trades[$i]['dispGross']       = 0;
      $trades[$i]['dispClientWhole'] = 0;
      
      if($i==0)
      { $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;  }
  
      //For Gross line :Start //Gross section comes initially in while loop, but it is used after displaying trades ... it is here because when client or item or expiry change, first we store data to display gross for previous matter
      if($tradesRow['clientId'] != $prevClientId || $tradesRow['itemId'] != $prevItemId || $tradesRow['expiryDate'] != $prevExpiryDate)
      {
        if($prevClientId != 0)
        {
          $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
          $valuesForGrossLine['clientPrevProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'];
          $valuesForGrossLine['clientPrevBrok']       = $trades[$i-1]['clientTotBrok'];
          
          $valuesForGrossLine['openBuyQty'] = 0;
          $valuesForGrossLine['openSellQty'] = 0;
          $valuesForGrossLine['totBuyQty']  = 0;
          $valuesForGrossLine['totSellQty'] = 0;
          $valuesForGrossLine['totBuyAmount'] = 0;
          $valuesForGrossLine['totSellAmount'] = 0;
          
          if($tradesRow['clientId'] != $prevClientId)
          {
            $trades[$i-1]['dispClientWhole'] = 1;
            $valuesForGrossLine['clientPrevProfitLoss'] = 0;  $valuesForGrossLine['clientPrevBrok'] = 0;
          }
        }
      }
      //For Gross line :End
      //priceOn and min are here because we need to assign value after gross line, and also, at first time : we take first priceOn and min and then we use it in gross line
      $priceOn = $itemRecords[$tradesRow['itemId']]['priceOn'];
      $valuesForGrossLine['min'] = $itemRecords[$tradesRow['itemId']]['min'];
  
      //We take first oneSideBrok and then use in Gross line
      $valuesForGrossLine['oneSideBrok'] = $clientBrok[$tradesRow['clientId']][$tradesRow['itemId']];
      
      $trades[$i]['clientId']     = $tradesRow['clientId'];
      $trades[$i]['prevClientId'] = $prevClientId;
      $prevClientId               = $tradesRow['clientId'];
      $trades[$i]['itemId']       = $tradesRow['itemId'];
      $trades[$i]['prevItemId']   = $prevItemId;
      $prevItemId                 = $tradesRow['itemId'];
      $trades[$i]['expiryDate']   = $tradesRow['expiryDate'];
      $trades[$i]['prevExpiryDate']   = $prevExpiryDate;
      $prevExpiryDate             = $tradesRow['expiryDate'];
      
      $trades[$i]['mulAmount']    = $itemRecords[$tradesRow['itemId']]['mulAmount'];
  
      $trades[$i]['clientName'] = $tradesRow['firstName']." ".$tradesRow['middleName']." ".$tradesRow['lastName'];
      $trades[$i]['vendor']    = $tradesRow['vendor'];
      $trades[$i]['tradeDate'] = mysqlToDDMMYY($tradesRow['tradeDate']);
      $trades[$i]['tradeTime'] = $tradesRow['tradeTime'];
      $trades[$i]['standing']  = standToDisplay($tradesRow['standing']);
      $trades[$i]['itemIdExpiry'] = $tradesRow['itemId']."-".substr($tradesRow['expiryDate'],2,3);
      $trades[$i]['buySell']   = $tradesRow['buySell'];
  
      $trades[$i]['tradeRefNo']  = $tradesRow['tradeRefNo'];
      $trades[$i]['userRemarks'] = $tradesRow['userRemarks'];
      $trades[$i]['ownClient']   = $tradesRow['ownClient'];
  
      if($tradesRow['buySell'] == 'Buy')
      {
        $trades[$i]['fontColor'] = "blue";
        $trades[$i]['buyQty']    = $tradesRow['qty'];
        $trades[$i]['price']     = $tradesRow['price'];
        $trades[$i]['sellQty']   = '&nbsp;';
        $trades[$i]['sellPrice'] = '&nbsp;';
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openBuyQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totBuyQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totBuyAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      else
      {
        $trades[$i]['fontColor'] = "red";
        $trades[$i]['buyQty'] = '&nbsp;';
        $trades[$i]['price'] = '&nbsp;';
        $trades[$i]['sellQty'] = $tradesRow['qty'];
        $trades[$i]['sellPrice'] = $tradesRow['price'];
        
        if($tradesRow['standing'] == -1)
          $valuesForGrossLine['openSellQty'] += $tradesRow['qty'];
          
        $valuesForGrossLine['totSellQty'] += $tradesRow['qty'];
        $valuesForGrossLine['totSellAmount'] += $tradesRow['price']*$tradesRow['qty'];
      }
      $i++;
    }
    $totTrades = $i;
    $trades = updateForGrossLine($trades, $i, $valuesForGrossLine);
    $trades[$i-1]['dispClientWhole'] = 1;

    //number_format :Start
    for($i=0;$i<$totTrades;$i++)
    {
      $trades[$i]['totBuyAmount']  = number_format(isset($trades[$i]['totBuyAmount'])?$trades[$i]['totBuyAmount']:0,4,'.','');
      $trades[$i]['totSellAmount'] = number_format(isset($trades[$i]['totSellAmount'])?$trades[$i]['totSellAmount']:0,4,'.','');
      $trades[$i]['buyRash']       = number_format(isset($trades[$i]['buyRash'])?$trades[$i]['buyRash']:0,4,'.','');
      $trades[$i]['sellRash']      = number_format(isset($trades[$i]['sellRash'])?$trades[$i]['sellRash']:0,4,'.','');
      $trades[$i]['profitLoss']    = number_format(isset($trades[$i]['profitLoss'])?$trades[$i]['profitLoss']:0,4,'.','');
      $trades[$i]['oneSideBrok']   = number_format(isset($trades[$i]['oneSideBrok'])?$trades[$i]['oneSideBrok']:0,4,'.','');
      $trades[$i]['netProfitLossNotFormatted'] = isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0;
      $trades[$i]['netProfitLoss'] = number_format(isset($trades[$i]['netProfitLoss'])?$trades[$i]['netProfitLoss']:0,4,'.','');
      if(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] >= 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$clientProfitBankRate[$trades[$i]['clientId']],4,'.','');
      elseif(isset($trades[$i]['netProfitLoss']) && $trades[$i]['netProfitLoss'] < 0)
        $trades[$i]['netProfitLossInRs'] = number_format($trades[$i]['netProfitLoss']*$clientLossBankRate[$trades[$i]['clientId']],4,'.','');
      else
        $trades[$i]['netProfitLossInRs'] = 0;
        
      $trades[$i]['clientTotProfitLoss'] = number_format(isset($trades[$i]['clientTotProfitLoss'])?$trades[$i]['clientTotProfitLoss']:0,4,'.','');
      $trades[$i]['clientTotBrok']       = number_format(isset($trades[$i]['clientTotBrok'])?$trades[$i]['clientTotBrok']:0,4,'.','');

//////////////////////////
      if(isset($trades[$i]['clientTotNetProfitLoss']) && $trades[$i]['clientTotNetProfitLoss'] >= 0)
      {
        $trades[$i]['clientTotNetProfitLossInRsNoFormat'] = $trades[$i]['clientTotNetProfitLoss']*$clientProfitBankRate[$trades[$i]['clientId']];
        $trades[$i]['clientTotNetProfitLossInRs'] = number_format($trades[$i]['clientTotNetProfitLossInRsNoFormat'],4,'.','');
      }
      elseif(isset($trades[$i]['clientTotNetProfitLoss']) && $trades[$i]['clientTotNetProfitLoss'] < 0)
      {
        $trades[$i]['clientTotNetProfitLossInRsNoFormat'] = $trades[$i]['clientTotNetProfitLoss']*$clientLossBankRate[$trades[$i]['clientId']];
        $trades[$i]['clientTotNetProfitLossInRs'] = number_format($trades[$i]['clientTotNetProfitLossInRsNoFormat'],4,'.','');
      }
      else
        $trades[$i]['clientTotNetProfitLossInRs'] = 0;
      $clientTotNetProfitLossInRs = $trades[$i]['clientTotNetProfitLossInRs'];
//////////////////////////
      $trades[$i]['clientTotNetProfitLoss'] = number_format(isset($trades[$i]['clientTotNetProfitLoss'])?$trades[$i]['clientTotNetProfitLoss']:0,4,'.','');
    }
    for($i=0;$i<=$wholeItemArrCount;$i++)
    {
  //////////////For whole total :Start
      $wholeBuyQty        += $wholeItemArr[$i]['buyQty'];
      $wholeTotBuyAmount  += $wholeItemArr[$i]['totBuyAmount'];
      $wholeSellQty       += $wholeItemArr[$i]['sellQty'];
      $wholeTotSellAmount += $wholeItemArr[$i]['totSellAmount'];
      $wholeProfitLoss    += $wholeItemArr[$i]['profitLoss'];
      $wholeOneSideBrok   += $wholeItemArr[$i]['oneSideBrok'];
      $wholeNetProfitLoss += $wholeItemArr[$i]['netProfitLoss'];
      if($wholeItemArr[$i]['netProfitLoss'] >= 0) $wholeNetProfitOnly += $wholeItemArr[$i]['netProfitLoss'];
      else $wholeNetLossOnly += $wholeItemArr[$i]['netProfitLoss'];

  //////////////For whole total :End
  //////////////For number_format :Start
      $wholeItemArr[$i]['totBuyAmount']  = number_format($wholeItemArr[$i]['totBuyAmount'],4,'.','');
      $wholeItemArr[$i]['totSellAmount'] = number_format($wholeItemArr[$i]['totSellAmount'],4,'.','');
      $wholeItemArr[$i]['buyRash']       = number_format($wholeItemArr[$i]['buyRash'],4,'.','');
      $wholeItemArr[$i]['sellRash']      = number_format($wholeItemArr[$i]['sellRash'],4,'.','');
      $wholeItemArr[$i]['profitLoss']    = number_format($wholeItemArr[$i]['profitLoss'],4,'.','');
      $wholeItemArr[$i]['oneSideBrok']   = number_format($wholeItemArr[$i]['oneSideBrok'],4,'.','');
      $wholeItemArr[$i]['netProfitLoss'] = number_format($wholeItemArr[$i]['netProfitLoss'],4,'.','');
  //////////////For number_format :End
    }
    $wholeBuyRash  = ($wholeBuyQty!=0)?($wholeTotBuyAmount/$wholeBuyQty):0;
    $wholeSellRash = ($wholeSellQty!=0)?($wholeTotSellAmount/$wholeSellQty):0;
  }
  //number_format :End
  
  ///////////////// buySellOnly :Start
  $buySellOnlySelected = "All";
  if(isset($_REQUEST['buySellOnly']))
  {
  	$buySellOnlySelected = $_REQUEST['buySellOnly'];
  	
    if($_REQUEST['buySellOnly'] == 'All')
    {
    	$buySellOnlyAll  = "SELECTED";
    	$buySellOnlyBuy  = "";
    	$buySellOnlySell = "";
    }
    elseif($_REQUEST['buySellOnly'] == 'Buy')
    {
    	$buySellOnlyAll  = "";
    	$buySellOnlyBuy  = "SELECTED";
    	$buySellOnlySell = "";
    }
    else
    {
    	$buySellOnlyAll  = "";
    	$buySellOnlyBuy  = "";
    	$buySellOnlySell = "SELECTED";
    }
  }
  else
  {
  	$buySellOnlyAll  = "SELECTED";
  	$buySellOnlyBuy  = "";
  	$buySellOnlySell = "";
  }
  ///////////////// buySellOnly :End

  ///////////////// exchangeOnly :Start
  $exchangeSelected = "Comex";
  if(isset($_REQUEST['exchange']))
  {
  	$exchangeSelected = $_REQUEST['exchange'];
  	
    if($_REQUEST['exchange'] == 'All')
    {
    	$exchangeOnlyAll  = "SELECTED";
    	$exchangeOnlyMCX  = "";
    	$exchangeOnlyComex = "";
		$exchangeOnlyShare = "";
    }
    elseif($_REQUEST['exchange'] == 'MCX')
    {
    	$exchangeOnlyAll  = "";
    	$exchangeOnlyMCX  = "SELECTED";
    	$exchangeOnlyComex = "";
		$exchangeOnlyShare = "";
    }
    elseif($_REQUEST['exchange'] == 'Comex')
	{
		$exchangeOnlyAll  = "";
    	$exchangeOnlyMCX  = "";
    	$exchangeOnlyComex = "SELECTED";
		$exchangeOnlyShare = "";
	}
	else
    {
    	$exchangeOnlyAll  = "";
    	$exchangeOnlyMCX  = "";
    	$exchangeOnlyComex = "";
		$exchangeOnlyShare = "SELECTED";
    }
  }
  else
  {
  	$exchangeOnlyAll  = "";
  	$exchangeOnlyMCX  = "";
  	$exchangeOnlyComex = "SELECTED";
	$exchangeOnlyShare = "";
  }
  ///////////////// exchangeOnly :End
  ///////////////// Client Overwrite : Start
  $clientOverwriteQuery  = "SELECT * FROM client WHERE clientId IN ( ";
///////////////////////////////////////////////////
  $tradesQuery = "SELECT clientId FROM tradetxtcx WHERE 1 = 1 ";
  if(isset($_SESSION['fromDate']))
  {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
     $tradesQuery .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
  }
  $clientOverwriteQuery  .= $tradesQuery." ) ";
///////////////////////////////////////////////////

  $clientOverwriteQuery .= " ORDER BY clientId";
  $clientOverwriteResult = mysql_query($clientOverwriteQuery);
  if($groupIdSelected == "")
  {
    $clientIdValues = NULL;
    $clientIdOptions = NULL;
    $clientIdValues = array();
    $clientIdOptions = array();
    $i = 0;
    $clientIdValues[0]  = 0;
    $clientIdOptions[0] = 'All';
    $i++;
    while($clientOverwriteRow = mysql_fetch_array($clientOverwriteResult))
    {
      $clientIdValues[$i] = $clientOverwriteRow['clientId'];
	  $clientIdOptions[$i] = $clientOverwriteRow['clientId'];
      //$clientIdOptions[$i] = $clientOverwriteRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
      $i++;
    }
  }  
  ///////////////// Client Overwrite :End

  $smarty->assign("singleClientOnly",$singleClientOnly);
  $smarty->assign("groupIdSelected",$groupIdSelected);
  $smarty->assign("groupId",$groupId);
  $smarty->assign("groupName",$groupName);
  $smarty->assign("buySellOnlyAll",$buySellOnlyAll);
  $smarty->assign("buySellOnlyBuy",$buySellOnlyBuy);
  $smarty->assign("buySellOnlySell",$buySellOnlySell);
  $smarty->assign("buySellOnlySelected",$buySellOnlySelected);
  
  $smarty->assign("exchangeOnlyAll",$exchangeOnlyAll);
  $smarty->assign("exchangeOnlyMCX",$exchangeOnlyMCX);
  $smarty->assign("exchangeOnlyComex",$exchangeOnlyComex);
  $smarty->assign("exchangeOnlyShare",$exchangeOnlyShare);
  $smarty->assign("exchangeSelected",$exchangeSelected);
 

  $smarty->assign("PHP_SELF", $_SERVER['PHP_SELF']);
  $smarty->assign("goTo", "clientTradesComex");
  $smarty->assign("display", $display);
  $smarty->assign("message", $message);
  $smarty->assign("clientIdSelected", $clientIdSelected);
  $smarty->assign("clientIdValues",   $clientIdValues);
  $smarty->assign("clientIdOptions",  $clientIdOptions);
  $smarty->assign("itemIdSelected",   $itemIdSelected);
  $smarty->assign("itemIdValues",     $itemIdValues);
  $smarty->assign("itemIdOptions",    $itemIdOptions);
  $smarty->assign("expiryDateSelected", $expiryDateSelected);
  $smarty->assign("expiryDateValues",   $expiryDateValues);
  $smarty->assign("expiryDateOptions",  $expiryDateOptions);
  
  $smarty->assign("fromDate", substr($_SESSION['fromDate'],8,2)."-".substr($_SESSION['fromDate'],5,2)."-".substr($_SESSION['fromDate'],2,2));
  $smarty->assign("toDate",   substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],2,2));
  $smarty->assign("profitLossDate", substr($_SESSION['toDate'],8,2)."-".substr($_SESSION['toDate'],5,2)."-".substr($_SESSION['toDate'],0,4));
  
  $smarty->assign("trades",           $trades);
  $smarty->assign("wholeItemArr",     $wholeItemArr);
  $smarty->assign("clientTotNetProfitLossInRs",     $clientTotNetProfitLossInRs);

  $smarty->assign("wholeBuyQty",        $wholeBuyQty);
  $smarty->assign("wholeTotBuyAmount",  number_format($wholeTotBuyAmount,4,'.',''));
  $smarty->assign("wholeBuyRash",       number_format($wholeBuyRash,4,'.',''));
  $smarty->assign("wholeSellQty",       $wholeSellQty);
  $smarty->assign("wholeTotSellAmount", number_format($wholeTotSellAmount,4,'.',''));
  $smarty->assign("wholeSellRash",      number_format($wholeSellRash,4,'.',''));
  $smarty->assign("wholeProfitLoss",    number_format($wholeProfitLoss,4,'.',''));
  $smarty->assign("wholeOneSideBrok",   number_format($wholeOneSideBrok,4,'.',''));
  $smarty->assign("wholeNetProfitLoss", number_format($wholeNetProfitLoss,4,'.',''));
  $smarty->assign("wholeNetProfitOnly", number_format($wholeNetProfitOnly,4,'.',''));
  $smarty->assign("wholeNetLossOnly",   number_format($wholeNetLossOnly,4,'.',''));
  $smarty->assign("isFrom","COMEX");

/////////////////Use tpl as per 'display' parameter :Start
  if($display == 'trades')
    $smarty->display("clientTradesComex.tpl");
  elseif($display == 'detailed')
    $smarty->display("clientTradesComex.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'gross')
    $smarty->display("clientGrossComex.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("clientItemWiseGross.tpl");
  elseif($display == 'itemPending')
    $smarty->display("clientItemPending.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("clientStand.tpl");
  elseif($display == 'PLToAccount')
    $smarty->display("clientPLToAccount.tpl");
  elseif($display == 'tradesPrint')
    $smarty->display("clientTradesPrint.tpl");
  elseif($display == 'detailed')
    $smarty->display("clientTradesPrint.tpl");//We display detailed view from same file for minimum view
  elseif($display == 'grossPrint')
    $smarty->display("clientGrossPrint.tpl");
  elseif($display == 'itemWiseGross')
    $smarty->display("clientItemWiseGrossPrint.tpl");
  elseif($display == 'openStand' || $display == 'closeStand' || $display == 'openCloseStand')
    $smarty->display("clientStandPrint.tpl");
  else
    $smarty->display("clientTradesComex.tpl");
/////////////////Use tpl as per 'display' parameter :End
}
///////////////////////////////
function updateForGrossLine($trades, $i, $valuesForGrossLine)
{
  global $wholeItemArr, $wholeItemArrCount;
  $trades[$i-1]['dispGross']     = 1;
  $trades[$i-1]['totBuyQty']     = $valuesForGrossLine['totBuyQty'];
  $trades[$i-1]['totSellQty']    = $valuesForGrossLine['totSellQty'];
  $trades[$i-1]['totBuyAmount']  = $valuesForGrossLine['totBuyAmount'];
  $trades[$i-1]['totSellAmount'] = $valuesForGrossLine['totSellAmount'];
  $trades[$i-1]['buyRash']       = ($valuesForGrossLine['totBuyQty']!=0)?($valuesForGrossLine['totBuyAmount']/$valuesForGrossLine['totBuyQty']):0;
  $trades[$i-1]['sellRash']      = ($valuesForGrossLine['totSellQty']!=0)?($valuesForGrossLine['totSellAmount']/$valuesForGrossLine['totSellQty']):0;
  
  if($trades[$i-1]['totBuyQty'] == $trades[$i-1]['totSellQty'])
  {
    $trades[$i-1]['profitLoss'] = $valuesForGrossLine['totSellAmount'] - $valuesForGrossLine['totBuyAmount'];
    $trades[$i-1]['profitLoss'] = $trades[$i-1]['profitLoss']*$trades[$i-1]['mulAmount'];
    
    //We subtract openBuyQty and openSellQty... but actually there will be only 1 open qty : buy or sell ...so result will be ok
    $trades[$i-1]['oneSideBrok'] 
     = $valuesForGrossLine['oneSideBrok']
       * ($valuesForGrossLine['totBuyQty'] - $valuesForGrossLine['openBuyQty']
          - $valuesForGrossLine['openSellQty'])
       / $valuesForGrossLine['min'];
    
    $trades[$i-1]['netProfitLoss']= $trades[$i-1]['profitLoss'] - $trades[$i-1]['oneSideBrok'];
  }
  else
  {
    $trades[$i-1]['profitLoss']   = 0;
    $trades[$i-1]['oneSideBrok']  = 0;
    $trades[$i-1]['netProfitLoss']= 0;
  }
  
  $trades[$i-1]['clientTotProfitLoss'] = $valuesForGrossLine['clientPrevProfitLoss'] + $trades[$i-1]['profitLoss'];

  $trades[$i-1]['clientTotBrok']       = $valuesForGrossLine['clientPrevBrok'] + $trades[$i-1]['oneSideBrok'];
  $trades[$i-1]['clientTotNetProfitLoss'] = $trades[$i-1]['clientTotProfitLoss'] - $trades[$i-1]['clientTotBrok'];

  if(!array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr))
  {
    $wholeItemArrCount++;
    $wholeItemArr[$wholeItemArrCount]['itemIdExpiry']  = $trades[$i-1]['itemIdExpiry'];
    $wholeItemArr[$wholeItemArrCount]['buyQty']        = 0;
    $wholeItemArr[$wholeItemArrCount]['sellQty']       = 0;
    $wholeItemArr[$wholeItemArrCount]['totBuyAmount']  = 0;
    $wholeItemArr[$wholeItemArrCount]['totSellAmount'] = 0;
    $wholeItemArr[$wholeItemArrCount]['profitLoss']    = 0;
    $wholeItemArr[$wholeItemArrCount]['oneSideBrok']   = 0;
    $wholeItemArr[$wholeItemArrCount]['netProfitLoss'] = 0;
    
    $elementNoToUse = $wholeItemArrCount;
  }
  else
  {
    $foundArray = array_search_recursive($trades[$i-1]['itemIdExpiry'], $wholeItemArr);
    $elementNoToUse = $foundArray[0];
  }
    
  $wholeItemArr[$elementNoToUse]['buyQty']        += $trades[$i-1]['totBuyQty'];
  $wholeItemArr[$elementNoToUse]['sellQty']       += $trades[$i-1]['totSellQty'];
  $wholeItemArr[$elementNoToUse]['totBuyAmount']  += $trades[$i-1]['totBuyAmount'];
  $wholeItemArr[$elementNoToUse]['totSellAmount'] += $trades[$i-1]['totSellAmount'];
  $wholeItemArr[$elementNoToUse]['buyRash']       = ($wholeItemArr[$wholeItemArrCount]['buyQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totBuyAmount']/$wholeItemArr[$wholeItemArrCount]['buyQty']):0;
  $wholeItemArr[$elementNoToUse]['sellRash']      = ($wholeItemArr[$wholeItemArrCount]['sellQty']!=0)?($wholeItemArr[$wholeItemArrCount]['totSellAmount']/$wholeItemArr[$wholeItemArrCount]['sellQty']):0;
  $wholeItemArr[$elementNoToUse]['profitLoss']    += $trades[$i-1]['profitLoss'];
  $wholeItemArr[$elementNoToUse]['oneSideBrok']   += $trades[$i-1]['oneSideBrok'];
  $wholeItemArr[$elementNoToUse]['netProfitLoss'] += $trades[$i-1]['netProfitLoss'];

  return $trades;
}
function applyConfirm()
{
	global $whereCondition;
  if(isset($_REQUEST['confirmBtn']))
  {
  	$updateQuery  = "UPDATE tradetxtcx SET confirmed = 0";
  	$updateQuery .= $whereCondition;
  	
  	mysql_query($updateQuery);
  	
  	if(isset($_REQUEST['confirmed']))
  	{
  		$confirmedArray = implode(",", $_REQUEST['confirmed']);
	  	$updateQuery  = "UPDATE tradetxtcx SET confirmed = 1
	  	                  WHERE tradeId IN (".$confirmedArray.")";
  	
    	mysql_query($updateQuery);
    }
    
    $headerText  = isset($_REQUEST['display'])        ? "display=".$_REQUEST['display'] : "";
    $headerText .= isset($_REQUEST['itemIdChanged'])  ? "&itemIdChanged=".$_REQUEST['itemIdChanged'] : "";
    $headerText .= isset($_REQUEST['clientId'])       ? "&clientId=".$_REQUEST['clientId'] : "";
    $headerText .= isset($_REQUEST['itemId'])         ? "&itemId=".$_REQUEST['itemId'] : "";
    $headerText .= isset($_REQUEST['buySellOnly'])    ? "&buySellOnly=".$_REQUEST['buySellOnly'] : "";
    $headerText .= (isset($_REQUEST['groupName']) && strlen($_REQUEST['groupName']) > 0) ? "&groupName=".$_REQUEST['groupName'] : "";
    $headerText .= isset($_REQUEST['expiryDate'])     ? "&expiryDate=".$_REQUEST['expiryDate'] : "";
    $headerText .= isset($_REQUEST['exchange'])       ? "&exchange=".$_REQUEST['exchange'] : "";
    header("Location: ./clientTradesComex.php?".$headerText);
    exit();
  }
}
?>