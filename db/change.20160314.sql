-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 14, 2016 at 05:41 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omvirjat`
--

-- --------------------------------------------------------

--
-- Table structure for table `tradecolor`
--

CREATE TABLE `tradecolor` (
  `id` int(11) NOT NULL,
  `tradetype` varchar(55) DEFAULT NULL,
  `sodatype` varchar(55) DEFAULT NULL,
  `color` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tradecolor`
--

INSERT INTO `tradecolor` (`id`, `tradetype`, `sodatype`, `color`) VALUES
(1, 'MCX', 'Buy', '5EBFFF'),
(2, 'MCX', 'Sell', 'FF2643'),
(3, 'COMEX', 'Buy', '1F26FF'),
(4, 'COMEX', 'Sell', 'FF2643');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tradecolor`
--
ALTER TABLE `tradecolor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tradecolor`
--
ALTER TABLE `tradecolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
