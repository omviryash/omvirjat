ALTER TABLE `tradetxt` ADD `ipaddress` VARCHAR(15) NOT NULL ;
ALTER TABLE `tradetxtcx` ADD `ipaddress` VARCHAR(15) NOT NULL ;

INSERT INTO `user_type` (`id`, `type`) VALUES (NULL, 'ENTRY_USER');
ALTER TABLE `user` CHANGE `type` `type` ENUM('SUPER_ADMIN','READ_ONLY','ENTRY_USER') NOT NULL DEFAULT 'READ_ONLY';
INSERT INTO `user` (`id`, `username`, `password`, `type`) VALUES (NULL, 'trade', MD5('trade'), 'ENTRY_USER');
