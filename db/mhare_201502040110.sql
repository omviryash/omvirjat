-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2015 at 08:42 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `vipul_mhare`
--

-- --------------------------------------------------------

--
-- Table structure for table `dongel`
--

CREATE TABLE IF NOT EXISTS `dongel` (
`dongle_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dongel`
--

INSERT INTO `dongel` (`dongle_id`, `date_time`) VALUES
(1, '2014-11-19 07:46:26'),
(2, '2014-11-19 07:48:16'),
(3, '2014-11-19 07:48:19'),
(4, '2014-11-19 07:48:22'),
(5, '2014-11-19 07:48:28'),
(6, '2014-11-19 07:48:49'),
(7, '2014-11-19 07:48:51'),
(8, '2014-11-19 07:51:06'),
(9, '2014-11-19 08:44:56'),
(10, '2014-11-19 08:45:32'),
(11, '2014-11-19 09:07:03'),
(12, '2014-11-19 09:08:32'),
(13, '2014-11-19 09:15:10'),
(14, '2014-11-19 09:15:19'),
(15, '2014-11-19 09:29:38'),
(16, '2014-11-23 08:15:14');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`ItemID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`ItemID`, `Name`) VALUES
(1, 'Bali'),
(8, 'gssdd');

-- --------------------------------------------------------

--
-- Table structure for table `kaarigar`
--

CREATE TABLE IF NOT EXISTS `kaarigar` (
`ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Wastage` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kaarigar`
--

INSERT INTO `kaarigar` (`ID`, `Name`, `Phone`, `Wastage`) VALUES
(1, 'Kishor', '1234567890', '2.50'),
(21, 'hggh', '454545', '3'),
(22, 'K1', '', ''),
(23, 'fsd', 'fsd', 'fsd'),
(24, 'lkjk', 'ghfhg', '3123'),
(26, 'das', 'das', 'dasda'),
(27, 'fsd', 'fsd', 'fs'),
(28, 'hardeep', 'pandya', '1.5');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
`ID` int(11) NOT NULL,
  `userId` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `userId`, `password`, `email`) VALUES
(1, 'admin', 'admin', 'admin@mhare.com');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE IF NOT EXISTS `transaction` (
`TransactionID` int(11) NOT NULL,
  `KaarigarID` int(11) NOT NULL,
  `createdDate` date NOT NULL,
  `Type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactiondetail`
--

CREATE TABLE IF NOT EXISTS `transactiondetail` (
`transactiondetail_id` int(11) NOT NULL,
  `TransactionID` int(11) NOT NULL,
  `Item` varchar(100) NOT NULL,
  `GrossWeight` double(9,3) NOT NULL,
  `Less` double(9,3) NOT NULL,
  `InTouch` double(9,3) NOT NULL,
  `NetWeight` double(9,3) NOT NULL,
  `Touch` double(8,2) NOT NULL,
  `Wastage` double(5,2) NOT NULL,
  `Fine` double(9,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vepaari`
--

CREATE TABLE IF NOT EXISTS `vepaari` (
`ID` int(11) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Phone` varchar(15) NOT NULL,
  `Wastage` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vepaari`
--

INSERT INTO `vepaari` (`ID`, `Name`, `Phone`, `Wastage`) VALUES
(1, 'test', '455454', '2.3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dongel`
--
ALTER TABLE `dongel`
 ADD PRIMARY KEY (`dongle_id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`ItemID`);

--
-- Indexes for table `kaarigar`
--
ALTER TABLE `kaarigar`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
 ADD PRIMARY KEY (`TransactionID`), ADD KEY `KaarigarID` (`KaarigarID`);

--
-- Indexes for table `transactiondetail`
--
ALTER TABLE `transactiondetail`
 ADD PRIMARY KEY (`transactiondetail_id`), ADD KEY `TransactionID` (`TransactionID`);

--
-- Indexes for table `vepaari`
--
ALTER TABLE `vepaari`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dongel`
--
ALTER TABLE `dongel`
MODIFY `dongle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `ItemID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kaarigar`
--
ALTER TABLE `kaarigar`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
MODIFY `TransactionID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactiondetail`
--
ALTER TABLE `transactiondetail`
MODIFY `transactiondetail_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vepaari`
--
ALTER TABLE `vepaari`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
