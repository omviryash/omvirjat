-- phpMyAdmin SQL Dump
-- version 2.6.1-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 02, 2015 at 07:45 AM
-- Server version: 5.0.67
-- PHP Version: 5.2.8
-- 
-- Database: `omvirjat`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bankmaster`
-- 

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL auto_increment,
  `bankName` varchar(60) character set utf8 NOT NULL default '',
  `phone1` varchar(12) character set utf8 NOT NULL default '',
  `phone2` varchar(12) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`bankId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bankmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `bhavcopy`
-- 

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL auto_increment,
  `exchange` varchar(30) NOT NULL default '',
  `bhavcopyDate` date NOT NULL default '0000-00-00',
  `sessionId` varchar(15) NOT NULL default '',
  `marketType` varchar(15) NOT NULL default '',
  `instrumentId` int(10) NOT NULL default '0',
  `instrumentName` varchar(15) NOT NULL default '',
  `scriptCode` int(10) NOT NULL default '0',
  `contractCode` varchar(20) NOT NULL default '',
  `scriptGroup` varchar(5) NOT NULL default '',
  `scriptType` varchar(5) NOT NULL default '',
  `expiryDate` date NOT NULL default '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL default '',
  `strikePrice` float NOT NULL default '0',
  `optionType` varchar(4) NOT NULL default '',
  `previousClosePrice` float NOT NULL default '0',
  `openPrice` float NOT NULL default '0',
  `highPrice` float NOT NULL default '0',
  `lowPrice` float NOT NULL default '0',
  `closePrice` float NOT NULL default '0',
  `totalQtyTrade` int(10) NOT NULL default '0',
  `totalValueTrade` double NOT NULL default '0',
  `lifeHigh` float NOT NULL default '0',
  `lifeLow` float NOT NULL default '0',
  `quoteUnits` varchar(10) NOT NULL default '',
  `settlementPrice` float NOT NULL default '0',
  `noOfTrades` int(6) NOT NULL default '0',
  `openInterest` double NOT NULL default '0',
  `avgTradePrice` float NOT NULL default '0',
  `tdcl` float NOT NULL default '0',
  `lstTradePrice` float NOT NULL default '0',
  `remarks` text NOT NULL,
  PRIMARY KEY  (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bhavcopy`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) default '',
  `dwAmount` float default '0',
  `plStatus` char(2) default '',
  `plAmount` float default '0',
  `transactionDate` date default NULL,
  `transType` varchar(20) default NULL,
  `transMode` varchar(30) character set utf8 default NULL,
  `tradeRefNo` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `clientBroker` int(2) default '0',
  `groupName` varchar(100) default '0',
  `profitBankRate` double default NULL,
  `lossBankRate` double default NULL,
  `commission` double default NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `client`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(10) default NULL,
  `exchange` varchar(30) character set utf8 default NULL,
  `oneSideBrok` float default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `clientbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `exchange`
-- 

CREATE TABLE `exchange` (
  `exchangeId` int(6) unsigned NOT NULL auto_increment,
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `multiply` tinyint(1) NOT NULL default '0',
  `profitBankRate` float default NULL,
  `lossBankRate` float default NULL,
  PRIMARY KEY  (`exchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `exchange`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `expensemaster`
-- 

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL auto_increment,
  `expenseName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`expensemasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `expensemaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(10) character set utf8 default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `expiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `general`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `grouplist`
-- 

CREATE TABLE `grouplist` (
  `groupId` int(6) unsigned NOT NULL auto_increment,
  `groupName` varchar(100) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `grouplist`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` float default NULL,
  `brok2` float default NULL,
  `oneSideBrok` float default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchange` varchar(20) default 'MCX',
  `high` double NOT NULL default '0',
  `low` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `itemdatewise`
-- 

CREATE TABLE `itemdatewise` (
  `itemId` varchar(250) character set utf8 NOT NULL default '',
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `itemDate` date NOT NULL default '0000-00-00',
  `high` double NOT NULL default '0',
  `low` double NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `itemdatewise`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `newexpmaster`
-- 

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL auto_increment,
  `newExpName` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `newexpmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orderscx`
-- 

CREATE TABLE `orderscx` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orderscx`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `otherexp`
-- 

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL auto_increment,
  `otherExpName` varchar(50) character set utf8 default NULL,
  `otherExpDate` date default NULL,
  `otherExpAmount` float default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherExpMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `otherexp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `key` varchar(30) default NULL,
  `value` varchar(60) default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `settings`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `standing`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  `highLowConf` char(1) character set utf8 default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxt`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtcx`
-- 

CREATE TABLE `tradetxtcx` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) character set utf8 default NULL,
  `vendor` varchar(50) character set utf8 default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default '0',
  `highLowConf` char(1) character set utf8 default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtcx`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtv1`
-- 

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtv1`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendor`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `itemId` varchar(50) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendorbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

