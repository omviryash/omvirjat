-- 
-- Database: `omvirjat`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bankmaster`
-- 

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL auto_increment,
  `bankName` varchar(60) character set utf8 NOT NULL default '',
  `phone1` varchar(12) character set utf8 NOT NULL default '',
  `phone2` varchar(12) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`bankId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bankmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `bhavcopy`
-- 

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL auto_increment,
  `exchange` varchar(30) NOT NULL default '',
  `bhavcopyDate` date NOT NULL default '0000-00-00',
  `sessionId` varchar(15) NOT NULL default '',
  `marketType` varchar(15) NOT NULL default '',
  `instrumentId` int(10) NOT NULL default '0',
  `instrumentName` varchar(15) NOT NULL default '',
  `scriptCode` int(10) NOT NULL default '0',
  `contractCode` varchar(20) NOT NULL default '',
  `scriptGroup` varchar(5) NOT NULL default '',
  `scriptType` varchar(5) NOT NULL default '',
  `expiryDate` date NOT NULL default '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL default '',
  `strikePrice` float NOT NULL default '0',
  `optionType` varchar(4) NOT NULL default '',
  `previousClosePrice` float NOT NULL default '0',
  `openPrice` float NOT NULL default '0',
  `highPrice` float NOT NULL default '0',
  `lowPrice` float NOT NULL default '0',
  `closePrice` float NOT NULL default '0',
  `totalQtyTrade` int(10) NOT NULL default '0',
  `totalValueTrade` double NOT NULL default '0',
  `lifeHigh` float NOT NULL default '0',
  `lifeLow` float NOT NULL default '0',
  `quoteUnits` varchar(10) NOT NULL default '',
  `settlementPrice` float NOT NULL default '0',
  `noOfTrades` int(6) NOT NULL default '0',
  `openInterest` double NOT NULL default '0',
  `avgTradePrice` float NOT NULL default '0',
  `tdcl` float NOT NULL default '0',
  `lstTradePrice` float NOT NULL default '0',
  `remarks` text NOT NULL,
  PRIMARY KEY  (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bhavcopy`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) default '',
  `dwAmount` float default '0',
  `plStatus` char(2) default '',
  `plAmount` float default '0',
  `transactionDate` date default NULL,
  `transType` varchar(20) default NULL,
  `transMode` varchar(30) character set utf8 default NULL,
  `tradeRefNo` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `clientBroker` int(2) default '0',
  `groupName` varchar(100) default '0',
  `profitBankRate` double default NULL,
  `lossBankRate` double default NULL,
  `commission` double default NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=620 ;

-- 
-- Dumping data for table `client`
-- 

INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (139, '139', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (15, '15', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (21, '21', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (22, '22', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (222, '222', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (23, '23', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (24, '24', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (26, '26', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (29, '29', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '72 V', 55, 55.5, 0);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (30, '30', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (31, '31', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (33, '33', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (34, '34', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (421, '421', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (45, '45', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (5, '5', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (52, '52', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (55, '55', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (6, '6', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (60, '60', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (608, '608', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (609, '609', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (619, '619', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (69, '69', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (73, '73', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', 50, 50.5, 0);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (75, '75', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (81, '81', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (82, '82', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (83, '83', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (84, '85', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(10) default NULL,
  `exchange` varchar(30) character set utf8 default NULL,
  `oneSideBrok` float default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=541 ;

-- 
-- Table structure for table `exchange`
-- 

CREATE TABLE `exchange` (
  `exchangeId` int(6) unsigned NOT NULL auto_increment,
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `multiply` tinyint(1) NOT NULL default '0',
  `profitBankRate` float default NULL,
  `lossBankRate` float default NULL,
  PRIMARY KEY  (`exchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `exchange`
-- 

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES (1, 'MCX', 0, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `expensemaster`
-- 

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL auto_increment,
  `expenseName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`expensemasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `expensemaster`
-- 

INSERT INTO `expensemaster` (`expensemasterId`, `expenseName`) VALUES (1, 'ggg');

-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(10) character set utf8 default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

-- 
-- Dumping data for table `expiry`
-- 

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (1, 'GOLD', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (3, 'CXGOLD', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (4, 'CRUDEOIL', '15NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (5, 'COPPER', '30NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (6, 'GOLDM', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (7, 'LEAD', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (8, 'ZINC', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (9, 'NICKEL', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (10, 'NATURGAS', '26OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (11, 'CXSILVER', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (12, 'CXCRUDE', '31NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (13, 'CXEURO', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (14, 'CXGOLDMINI', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (15, 'CXCOPPER', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (20, 'SILVER', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (24, 'MINICRUDECX', '30NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (21, 'SILVERM', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (25, 'SILVERMINICX', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (26, 'GOLD', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (27, 'CXGOLD', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (28, 'MINICRUDECX', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (29, 'CXSILVER', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (30, 'CXCRUDE', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (31, 'CXEURO', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (32, 'CXGOLDMINI', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (33, 'CXCOPPER', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (34, 'SILVERMINICX', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (35, 'SILVER', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (36, 'CRUDEOIL', '15NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (37, 'COPPER', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (38, 'GOLDM', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (39, 'LEAD', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (40, 'ZINC', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (41, 'NICKEL', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (43, 'NATURGAS', '27NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (44, 'SILVERM', '30NOV2012', 'MCX');

-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `general`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `grouplist`
-- 

CREATE TABLE `grouplist` (
  `groupId` int(6) unsigned NOT NULL auto_increment,
  `groupName` varchar(100) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `grouplist`
-- 

INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES (2, '31');
INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES (3, '72 V');

-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` float default NULL,
  `brok2` float default NULL,
  `oneSideBrok` float default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchange` varchar(20) default 'MCX',
  `high` double NOT NULL default '0',
  `low` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('GOLD', 'GOLD', 'GOLD', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVER', 'SILVER', 'SILVER', NULL, NULL, 500, 30, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXGOLD', 'CXGOLD', 'CXGOLD', NULL, NULL, 40, 1, 1, 100, 1000, 2000, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('COPPER', 'COPPER', 'COPPER', NULL, NULL, 0, 1000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('GOLDM', 'GOLDM', 'GOLDM', NULL, NULL, 0, 10, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('MINICRUDECX', 'MINICRUDECX', 'MINICRUDECX', NULL, NULL, 40, 1, NULL, 500, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('LEAD', 'LEAD', 'LEAD', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('ZINC', 'ZINC', 'ZINC', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('NICKEL', 'NICKEL', 'NICKEL', NULL, NULL, 500, 250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('NATURGAS', 'NATURGAS', 'NATURGAS', NULL, NULL, 500, 1250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXSILVER', 'CXSILVER', 'CXSILVER', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXCRUDE', 'CXCRUDE', 'CXCRUDE', NULL, NULL, 40, 1, 1, 1000, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXEURO', 'CXEURO', 'CXEURO', NULL, NULL, 40, 1, 1, 125000, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXGOLDMINI', 'CXGOLDMINI', 'CXGOLDMINI', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXCOPPER', 'CXCOPPER', 'CXCOPPER', NULL, NULL, 30, 1, 1, 250, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVERMINICX', 'SILVERMINICX', 'SILVERMINICX', NULL, NULL, 40, 1, NULL, 25, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVERM', 'SILVERM', 'SILVERM', NULL, NULL, 100, 5, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `itemdatewise`
-- 

CREATE TABLE `itemdatewise` (
  `itemId` varchar(250) character set utf8 NOT NULL default '',
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `itemDate` date NOT NULL default '0000-00-00',
  `high` double NOT NULL default '0',
  `low` double NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `itemdatewise`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `newexpmaster`
-- 

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL auto_increment,
  `newExpName` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `newexpmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orderscx`
-- 

CREATE TABLE `orderscx` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orderscx`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `otherexp`
-- 

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL auto_increment,
  `otherExpName` varchar(50) character set utf8 default NULL,
  `otherExpDate` date default NULL,
  `otherExpAmount` float default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherExpMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `otherexp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `key` varchar(30) default NULL,
  `value` varchar(60) default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `settings`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `standing`
-- 

INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (3, '2012-12-26', '2012-12-27', 'GOLD05DEC2012', 15500);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (4, '2012-12-26', '2012-12-27', 'SILVER05DEC2012', 28000);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (5, '2012-12-26', '2012-12-27', 'CXEURO10DEC2012', 1.291);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (6, '2012-12-26', '2012-12-27', 'CXEURO30NOV2012', 1.291);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (7, '2012-12-26', '2012-12-27', 'CXGOLD10DEC2012', 1560);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (8, '2012-12-26', '2012-12-27', 'CXSILVER10DEC2012', 3100);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (9, '2013-02-28', '2013-03-01', 'CXGOLD10DEC2012', 1695);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (10, '2013-02-28', '2013-03-01', 'CXSILVER10DEC2012', 3100);

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  `highLowConf` char(1) character set utf8 default '0',
  `ipaddress` VARCHAR(15) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- 
-- Dumping data for table `tradetxt`
-- 

INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (5, 0, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-02', '13:56:01', 100, 31104, NULL, NULL, NULL, '05DEC2012', '_SELF', '6', NULL, 0, '0');
INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (13, -1, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-04', NULL, 100, 31500, 0, NULL, NULL, '05DEC2012', '_SELF', NULL, NULL, 0, '0');
INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (15, -1, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-04', NULL, 100, 0, 0, NULL, NULL, '05DEC2012', '_SELF', NULL, NULL, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtcx`
-- 

CREATE TABLE `tradetxtcx` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) character set utf8 default NULL,
  `vendor` varchar(50) character set utf8 default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default '0',
  `highLowConf` char(1) character set utf8 default '0',
  `ipaddress` VARCHAR(15) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- 
-- Dumping data for table `tradetxtcx`
-- 

INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (1, 0, 73, '73', '', '', 'Buy', 'CXGOLD', '2012-11-01', '17:03:02', 1, 1690, NULL, NULL, NULL, '10DEC2012', '_SELF', '73', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (5, -1, 73, '73', '', '', 'Buy', 'CXGOLD', '2012-11-04', NULL, 1, 0, 0, NULL, NULL, '10DEC2012', '_SELF', NULL, NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (6, 0, 29, '29', '', '', 'Buy', 'CXGOLD', '2012-12-27', '18:46:09', 1, 1660, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (7, 0, 29, '29', '', '', 'Sell', 'CXGOLD', '2012-12-27', '18:46:16', 1, 1665, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (8, 0, 29, '29', '', '', 'Sell', 'CXSILVER', '2012-12-27', '18:46:31', 1, 3050, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (9, 0, 29, '29', '', '', 'Buy', 'CXSILVER', '2012-12-27', '18:46:41', 1, 3080, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtv1`
-- 

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtv1`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendor`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `itemId` varchar(50) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `vendorbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

-- --------------------------------------------------------

--
-- Table structure for table `tradecolor`
--

CREATE TABLE `tradecolor` (
  `id` int(11) NOT NULL,
  `tradetype` varchar(55) DEFAULT NULL,
  `sodatype` varchar(55) DEFAULT NULL,
  `color` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tradecolor`
--

INSERT INTO `tradecolor` (`id`, `tradetype`, `sodatype`, `color`) VALUES
(1, 'MCX', 'Buy', '5EBFFF'),
(2, 'MCX', 'Sell', 'FF2643'),
(3, 'COMEX', 'Buy', '1F26FF'),
(4, 'COMEX', 'Sell', 'FF2643');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tradecolor`
--
ALTER TABLE `tradecolor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tradecolor`
--
ALTER TABLE `tradecolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

CREATE TABLE  `user` (
`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`username` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ,
`password` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;

INSERT INTO `user` (`id`, `username`, `password`) VALUES (NULL, 'admin', MD5('admin12ka4'));

CREATE TABLE IF NOT EXISTS `log` (
  `logId` int(11) NOT NULL AUTO_INCREMENT,
  `tradeId` int(11) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logType` varchar(80) NOT NULL,
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB