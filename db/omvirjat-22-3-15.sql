-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 22, 2015 at 12:00 PM
-- Server version: 5.5.40-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omvirjat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE IF NOT EXISTS `bankmaster` (
  `bankId` int(6) NOT NULL AUTO_INCREMENT,
  `bankName` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone1` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone2` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`bankId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE IF NOT EXISTS `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT,
  `exchange` varchar(30) NOT NULL DEFAULT '',
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) NOT NULL DEFAULT '',
  `marketType` varchar(15) NOT NULL DEFAULT '',
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) NOT NULL DEFAULT '',
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) NOT NULL DEFAULT '',
  `scriptGroup` varchar(5) NOT NULL DEFAULT '',
  `scriptType` varchar(5) NOT NULL DEFAULT '',
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL DEFAULT '',
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) NOT NULL DEFAULT '',
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) NOT NULL DEFAULT '',
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text NOT NULL,
  PRIMARY KEY (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE IF NOT EXISTS `cashflow` (
  `cashFlowId` int(6) NOT NULL AUTO_INCREMENT,
  `clientId` int(10) DEFAULT '0',
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `dwStatus` char(2) DEFAULT '',
  `dwAmount` float DEFAULT '0',
  `plStatus` char(2) DEFAULT '',
  `plAmount` float DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) DEFAULT NULL,
  `transMode` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tradeRefNo` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`cashFlowId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `clientId` int(6) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `clientBroker` int(2) DEFAULT '0',
  `groupName` varchar(100) DEFAULT '0',
  `profitBankRate` double DEFAULT NULL,
  `lossBankRate` double DEFAULT NULL,
  `commission` double DEFAULT NULL,
  PRIMARY KEY (`clientId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES
(1, 'Hari', 'abc', 'sdkf', NULL, NULL, 200, NULL, 'xdgdfg', '65565', '6565', '565', 'b@x.com', 0, 0, '0', 200, 10, 10);

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE IF NOT EXISTS `clientbrok` (
  `clientBrokId` int(6) NOT NULL AUTO_INCREMENT,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(10) DEFAULT NULL,
  `exchange` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  PRIMARY KEY (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE IF NOT EXISTS `exchange` (
  `exchangeId` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL,
  PRIMARY KEY (`exchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE IF NOT EXISTS `expensemaster` (
  `expensemasterId` int(6) NOT NULL AUTO_INCREMENT,
  `expenseName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`expensemasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE IF NOT EXISTS `expiry` (
  `expiryId` int(6) NOT NULL AUTO_INCREMENT,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `exchange` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE IF NOT EXISTS `general` (
  `generalId` int(6) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(250) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`generalId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grouplist`
--

CREATE TABLE IF NOT EXISTS `grouplist` (
  `groupId` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `groupName` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `itemShort` varchar(50) DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchange` varchar(20) DEFAULT 'MCX',
  `high` double NOT NULL DEFAULT '0',
  `low` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `itemdatewise`
--

CREATE TABLE IF NOT EXISTS `itemdatewise` (
  `itemId` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itemDate` date NOT NULL DEFAULT '0000-00-00',
  `high` double NOT NULL DEFAULT '0',
  `low` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE IF NOT EXISTS `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT,
  `newExpName` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `orderId` int(6) NOT NULL AUTO_INCREMENT,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `orderscx`
--

CREATE TABLE IF NOT EXISTS `orderscx` (
  `orderId` int(6) NOT NULL AUTO_INCREMENT,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL,
  PRIMARY KEY (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE IF NOT EXISTS `otherexp` (
  `otherexpId` int(6) NOT NULL AUTO_INCREMENT,
  `otherExpName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpMode` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `settingsId` int(6) NOT NULL AUTO_INCREMENT,
  `key` varchar(30) DEFAULT NULL,
  `value` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`settingsId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE IF NOT EXISTS `standing` (
  `standingId` int(6) NOT NULL AUTO_INCREMENT,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE IF NOT EXISTS `tradetxt` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  PRIMARY KEY (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtcx`
--

CREATE TABLE IF NOT EXISTS `tradetxtcx` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `vendor` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  PRIMARY KEY (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE IF NOT EXISTS `tradetxtv1` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` enum('SUPER_ADMIN','READ_ONLY') NOT NULL DEFAULT 'READ_ONLY',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'SUPER_ADMIN'),
(2, 'k', '8ce4b16b22b58894aa86c421e8759df3', 'READ_ONLY'),
(4, 'abc', '900150983cd24fb0d6963f7d28e17f72', 'SUPER_ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `page` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_type` (`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `type`) VALUES
(1, 'ADMIN'),
(2, 'READ_ONLY');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
  `vendorId` int(6) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(50) DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  PRIMARY KEY (`vendorId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE IF NOT EXISTS `vendorbrok` (
  `clientBrokId` int(6) NOT NULL AUTO_INCREMENT,
  `vendor` varchar(50) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  PRIMARY KEY (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendortemp`
--

CREATE TABLE IF NOT EXISTS `vendortemp` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE IF NOT EXISTS `vendortrades` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE IF NOT EXISTS `zcxexpiry` (
  `expiryId` int(6) NOT NULL AUTO_INCREMENT,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE IF NOT EXISTS `zcxitem` (
  `itemId` varchar(10) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1',
  PRIMARY KEY (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE IF NOT EXISTS `zcxmember` (
  `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT,
  `userId` varchar(10) DEFAULT NULL,
  `memberId` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`zCxMemberId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zcxstanding`
--

CREATE TABLE IF NOT EXISTS `zcxstanding` (
  `standingId` int(6) NOT NULL AUTO_INCREMENT,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE IF NOT EXISTS `zcxtrades` (
  `tradeId` int(6) NOT NULL AUTO_INCREMENT,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(10) DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
