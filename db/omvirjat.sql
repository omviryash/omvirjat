-- phpMyAdmin SQL Dump
-- version 2.6.1-pl3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Feb 28, 2013 at 09:13 PM
-- Server version: 4.1.14
-- PHP Version: 4.4.0
-- 
-- Database: `omvirjat`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `bankmaster`
-- 

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL auto_increment,
  `bankName` varchar(60) character set utf8 NOT NULL default '',
  `phone1` varchar(12) character set utf8 NOT NULL default '',
  `phone2` varchar(12) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`bankId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bankmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `bhavcopy`
-- 

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL auto_increment,
  `exchange` varchar(30) NOT NULL default '',
  `bhavcopyDate` date NOT NULL default '0000-00-00',
  `sessionId` varchar(15) NOT NULL default '',
  `marketType` varchar(15) NOT NULL default '',
  `instrumentId` int(10) NOT NULL default '0',
  `instrumentName` varchar(15) NOT NULL default '',
  `scriptCode` int(10) NOT NULL default '0',
  `contractCode` varchar(20) NOT NULL default '',
  `scriptGroup` varchar(5) NOT NULL default '',
  `scriptType` varchar(5) NOT NULL default '',
  `expiryDate` date NOT NULL default '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL default '',
  `strikePrice` float NOT NULL default '0',
  `optionType` varchar(4) NOT NULL default '',
  `previousClosePrice` float NOT NULL default '0',
  `openPrice` float NOT NULL default '0',
  `highPrice` float NOT NULL default '0',
  `lowPrice` float NOT NULL default '0',
  `closePrice` float NOT NULL default '0',
  `totalQtyTrade` int(10) NOT NULL default '0',
  `totalValueTrade` double NOT NULL default '0',
  `lifeHigh` float NOT NULL default '0',
  `lifeLow` float NOT NULL default '0',
  `quoteUnits` varchar(10) NOT NULL default '',
  `settlementPrice` float NOT NULL default '0',
  `noOfTrades` int(6) NOT NULL default '0',
  `openInterest` double NOT NULL default '0',
  `avgTradePrice` float NOT NULL default '0',
  `tdcl` float NOT NULL default '0',
  `lstTradePrice` float NOT NULL default '0',
  `remarks` text NOT NULL,
  PRIMARY KEY  (`bhavcopyid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `bhavcopy`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `cashflow`
-- 

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL auto_increment,
  `clientId` int(10) default '0',
  `itemIdExpiryDate` varchar(50) default NULL,
  `dwStatus` char(2) default '',
  `dwAmount` float default '0',
  `plStatus` char(2) default '',
  `plAmount` float default '0',
  `transactionDate` date default NULL,
  `transType` varchar(20) default NULL,
  `transMode` varchar(30) character set utf8 default NULL,
  `tradeRefNo` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`cashFlowId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `cashflow`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `client`
-- 

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL auto_increment,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `openingDate` date default NULL,
  `opening` float default NULL,
  `deposit` int(6) default NULL,
  `currentBal` float default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(10) default NULL,
  `email` varchar(60) default NULL,
  `oneSide` tinyint(1) default '0',
  `clientBroker` int(2) default '0',
  `groupName` varchar(100) default '0',
  `profitBankRate` double default NULL,
  `lossBankRate` double default NULL,
  `commission` double default NULL,
  PRIMARY KEY  (`clientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=620 ;

-- 
-- Dumping data for table `client`
-- 

INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (139, '139', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (15, '15', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (21, '21', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (22, '22', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (222, '222', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (23, '23', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (24, '24', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (26, '26', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (29, '29', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '72 V', 55, 55.5, 0);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (30, '30', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (31, '31', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (33, '33', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (34, '34', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (421, '421', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (45, '45', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (5, '5', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (52, '52', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (55, '55', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (6, '6', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (60, '60', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (608, '608', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (609, '609', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (619, '619', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (69, '69', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (73, '73', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', 50, 50.5, 0);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (75, '75', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (81, '81', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (82, '82', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (83, '83', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);
INSERT INTO `client` (`clientId`, `firstName`, `middleName`, `lastName`, `openingDate`, `opening`, `deposit`, `currentBal`, `address`, `phone`, `mobile`, `fax`, `email`, `oneSide`, `clientBroker`, `groupName`, `profitBankRate`, `lossBankRate`, `commission`) VALUES (84, '85', '', '', NULL, NULL, 0, NULL, '', '', '', '', '', 0, 0, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `clientbrok`
-- 

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default NULL,
  `itemId` varchar(10) default NULL,
  `exchange` varchar(30) character set utf8 default NULL,
  `oneSideBrok` float default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=541 ;

-- 
-- Dumping data for table `clientbrok`
-- 

INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (1, 139, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (2, 139, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (3, 139, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (4, 139, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (5, 139, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (6, 139, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (7, 139, 'COPPER', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (8, 139, 'CRUDEOIL', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (9, 139, 'GOLD', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (10, 139, 'GOLDM', 'MCX', 50, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (11, 139, 'LEAD', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (12, 139, 'NATURGAS', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (13, 139, 'NICKEL', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (14, 139, 'SILVER', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (15, 139, 'ZINC', 'MCX', 200, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (16, 15, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (17, 15, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (18, 15, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (19, 15, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (20, 15, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (21, 15, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (22, 15, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (23, 15, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (24, 15, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (25, 15, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (26, 15, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (27, 15, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (28, 15, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (29, 15, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (30, 15, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (31, 21, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (32, 21, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (33, 21, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (34, 21, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (35, 21, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (36, 21, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (37, 21, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (38, 21, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (39, 21, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (40, 21, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (41, 21, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (42, 21, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (43, 21, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (44, 21, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (45, 21, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (46, 22, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (47, 22, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (48, 22, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (49, 22, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (50, 22, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (51, 22, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (52, 22, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (53, 22, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (54, 22, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (55, 22, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (56, 22, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (57, 22, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (58, 22, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (59, 22, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (60, 22, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (61, 222, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (62, 222, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (63, 222, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (64, 222, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (65, 222, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (66, 222, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (67, 222, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (68, 222, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (69, 222, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (70, 222, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (71, 222, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (72, 222, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (73, 222, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (74, 222, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (75, 222, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (76, 23, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (77, 23, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (78, 23, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (79, 23, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (80, 23, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (81, 23, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (82, 23, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (83, 23, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (84, 23, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (85, 23, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (86, 23, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (87, 23, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (88, 23, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (89, 23, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (90, 23, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (91, 24, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (92, 24, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (93, 24, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (94, 24, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (95, 24, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (96, 24, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (97, 24, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (98, 24, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (99, 24, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (100, 24, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (101, 24, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (102, 24, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (103, 24, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (104, 24, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (105, 24, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (106, 26, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (107, 26, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (108, 26, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (109, 26, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (110, 26, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (111, 26, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (112, 26, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (113, 26, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (114, 26, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (115, 26, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (116, 26, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (117, 26, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (118, 26, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (119, 26, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (120, 26, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (121, 29, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (122, 29, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (123, 29, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (124, 29, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (125, 29, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (126, 29, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (127, 29, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (128, 29, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (129, 29, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (130, 29, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (131, 29, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (132, 29, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (133, 29, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (134, 29, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (135, 29, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (136, 30, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (137, 30, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (138, 30, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (139, 30, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (140, 30, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (141, 30, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (142, 30, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (143, 30, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (144, 30, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (145, 30, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (146, 30, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (147, 30, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (148, 30, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (149, 30, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (150, 30, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (151, 31, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (152, 31, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (153, 31, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (154, 31, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (155, 31, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (156, 31, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (157, 31, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (158, 31, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (159, 31, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (160, 31, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (161, 31, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (162, 31, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (163, 31, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (164, 31, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (165, 31, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (166, 33, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (167, 33, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (168, 33, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (169, 33, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (170, 33, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (171, 33, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (172, 33, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (173, 33, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (174, 33, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (175, 33, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (176, 33, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (177, 33, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (178, 33, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (179, 33, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (180, 33, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (181, 34, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (182, 34, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (183, 34, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (184, 34, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (185, 34, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (186, 34, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (187, 34, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (188, 34, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (189, 34, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (190, 34, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (191, 34, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (192, 34, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (193, 34, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (194, 34, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (195, 34, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (196, 421, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (197, 421, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (198, 421, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (199, 421, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (200, 421, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (201, 421, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (202, 421, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (203, 421, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (204, 421, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (205, 421, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (206, 421, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (207, 421, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (208, 421, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (209, 421, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (210, 421, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (211, 45, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (212, 45, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (213, 45, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (214, 45, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (215, 45, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (216, 45, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (217, 45, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (218, 45, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (219, 45, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (220, 45, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (221, 45, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (222, 45, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (223, 45, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (224, 45, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (225, 45, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (226, 5, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (227, 5, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (228, 5, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (229, 5, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (230, 5, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (231, 5, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (232, 5, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (233, 5, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (234, 5, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (235, 5, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (236, 5, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (237, 5, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (238, 5, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (239, 5, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (240, 5, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (241, 52, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (242, 52, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (243, 52, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (244, 52, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (245, 52, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (246, 52, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (247, 52, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (248, 52, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (249, 52, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (250, 52, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (251, 52, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (252, 52, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (253, 52, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (254, 52, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (255, 52, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (256, 55, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (257, 55, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (258, 55, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (259, 55, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (260, 55, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (261, 55, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (262, 55, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (263, 55, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (264, 55, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (265, 55, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (266, 55, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (267, 55, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (268, 55, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (269, 55, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (270, 55, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (271, 6, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (272, 6, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (273, 6, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (274, 6, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (275, 6, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (276, 6, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (277, 6, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (278, 6, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (279, 6, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (280, 6, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (281, 6, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (282, 6, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (283, 6, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (284, 6, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (285, 6, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (286, 60, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (287, 60, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (288, 60, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (289, 60, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (290, 60, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (291, 60, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (292, 60, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (293, 60, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (294, 60, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (295, 60, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (296, 60, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (297, 60, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (298, 60, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (299, 60, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (300, 60, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (301, 608, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (302, 608, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (303, 608, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (304, 608, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (305, 608, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (306, 608, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (307, 608, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (308, 608, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (309, 608, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (310, 608, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (311, 608, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (312, 608, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (313, 608, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (314, 608, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (315, 608, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (316, 609, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (317, 609, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (318, 609, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (319, 609, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (320, 609, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (321, 609, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (322, 609, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (323, 609, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (324, 609, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (325, 609, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (326, 609, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (327, 609, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (328, 609, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (329, 609, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (330, 609, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (331, 619, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (332, 619, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (333, 619, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (334, 619, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (335, 619, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (336, 619, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (337, 619, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (338, 619, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (339, 619, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (340, 619, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (341, 619, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (342, 619, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (343, 619, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (344, 619, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (345, 619, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (346, 69, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (347, 69, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (348, 69, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (349, 69, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (350, 69, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (351, 69, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (352, 69, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (353, 69, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (354, 69, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (355, 69, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (356, 69, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (357, 69, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (358, 69, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (359, 69, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (360, 69, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (361, 73, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (362, 73, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (363, 73, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (364, 73, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (365, 73, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (366, 73, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (367, 73, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (368, 73, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (369, 73, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (370, 73, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (371, 73, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (372, 73, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (373, 73, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (374, 73, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (375, 73, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (376, 75, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (377, 75, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (378, 75, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (379, 75, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (380, 75, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (381, 75, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (382, 75, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (383, 75, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (384, 75, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (385, 75, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (386, 75, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (387, 75, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (388, 75, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (389, 75, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (390, 75, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (391, 81, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (392, 81, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (393, 81, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (394, 81, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (395, 81, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (396, 81, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (397, 81, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (398, 81, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (399, 81, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (400, 81, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (401, 81, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (402, 81, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (403, 81, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (404, 81, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (405, 81, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (406, 82, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (407, 82, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (408, 82, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (409, 82, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (410, 82, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (411, 82, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (412, 82, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (413, 82, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (414, 82, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (415, 82, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (416, 82, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (417, 82, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (418, 82, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (419, 82, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (420, 82, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (421, 83, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (422, 83, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (423, 83, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (424, 83, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (425, 83, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (426, 83, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (427, 83, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (428, 83, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (429, 83, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (430, 83, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (431, 83, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (432, 83, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (433, 83, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (434, 83, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (435, 83, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (436, 84, 'CXCOPPER', 'COMEX', 30, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (437, 84, 'CXCRUDE', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (438, 84, 'CXEURO', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (439, 84, 'CXGOLD', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (440, 84, 'CXGOLDMINI', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (441, 84, 'CXSILVER', 'COMEX', 40, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (442, 84, 'COPPER', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (443, 84, 'CRUDEOIL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (444, 84, 'GOLD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (445, 84, 'GOLDM', 'MCX', 0, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (446, 84, 'LEAD', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (447, 84, 'NATURGAS', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (448, 84, 'NICKEL', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (449, 84, 'SILVER', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (450, 84, 'ZINC', 'MCX', 500, 0, 0);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (451, 139, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (452, 15, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (453, 21, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (454, 22, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (455, 222, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (456, 23, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (457, 24, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (458, 26, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (459, 29, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (460, 30, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (461, 31, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (462, 33, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (463, 34, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (464, 421, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (465, 45, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (466, 5, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (467, 52, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (468, 55, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (469, 6, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (470, 60, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (471, 608, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (472, 609, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (473, 619, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (474, 69, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (475, 73, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (476, 75, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (477, 81, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (478, 82, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (479, 83, 'SILVERM', 'MCX', 100, NULL, NULL);
INSERT INTO `clientbrok` (`clientBrokId`, `clientId`, `itemId`, `exchange`, `oneSideBrok`, `brok1`, `brok2`) VALUES (480, 84, 'SILVERM', 'MCX', 100, NULL, NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `exchange`
-- 

CREATE TABLE `exchange` (
  `exchangeId` int(6) unsigned NOT NULL auto_increment,
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `multiply` tinyint(1) NOT NULL default '0',
  `profitBankRate` float default NULL,
  `lossBankRate` float default NULL,
  PRIMARY KEY  (`exchangeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `exchange`
-- 

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES (1, 'MCX', 0, 1, 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `expensemaster`
-- 

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL auto_increment,
  `expenseName` varchar(50) character set utf8 default NULL,
  PRIMARY KEY  (`expensemasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `expensemaster`
-- 

INSERT INTO `expensemaster` (`expensemasterId`, `expenseName`) VALUES (1, 'ggg');

-- --------------------------------------------------------

-- 
-- Table structure for table `expiry`
-- 

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  `exchange` varchar(10) character set utf8 default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

-- 
-- Dumping data for table `expiry`
-- 

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (1, 'GOLD', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (3, 'CXGOLD', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (4, 'CRUDEOIL', '15NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (5, 'COPPER', '30NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (6, 'GOLDM', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (7, 'LEAD', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (8, 'ZINC', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (9, 'NICKEL', '31OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (10, 'NATURGAS', '26OCT2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (11, 'CXSILVER', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (12, 'CXCRUDE', '31NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (13, 'CXEURO', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (14, 'CXGOLDMINI', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (15, 'CXCOPPER', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (20, 'SILVER', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (24, 'MINICRUDECX', '30NOV2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (21, 'SILVERM', '05DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (25, 'SILVERMINICX', '10DEC2012', NULL);
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (26, 'GOLD', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (27, 'CXGOLD', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (28, 'MINICRUDECX', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (29, 'CXSILVER', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (30, 'CXCRUDE', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (31, 'CXEURO', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (32, 'CXGOLDMINI', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (33, 'CXCOPPER', '30NOV2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (34, 'SILVERMINICX', '10DEC2012', 'COMEX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (35, 'SILVER', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (36, 'CRUDEOIL', '15NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (37, 'COPPER', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (38, 'GOLDM', '05DEC2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (39, 'LEAD', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (40, 'ZINC', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (41, 'NICKEL', '30NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (43, 'NATURGAS', '27NOV2012', 'MCX');
INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES (44, 'SILVERM', '30NOV2012', 'MCX');

-- --------------------------------------------------------

-- 
-- Table structure for table `general`
-- 

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL auto_increment,
  `filePath` varchar(250) default NULL,
  `fileName` varchar(200) default NULL,
  PRIMARY KEY  (`generalId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- 
-- Dumping data for table `general`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `grouplist`
-- 

CREATE TABLE `grouplist` (
  `groupId` int(6) unsigned NOT NULL auto_increment,
  `groupName` varchar(100) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`groupId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- 
-- Dumping data for table `grouplist`
-- 

INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES (2, '31');
INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES (3, '72 V');

-- --------------------------------------------------------

-- 
-- Table structure for table `item`
-- 

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL default '',
  `item` varchar(50) default NULL,
  `itemShort` varchar(50) default NULL,
  `brok` float default NULL,
  `brok2` float default NULL,
  `oneSideBrok` float default NULL,
  `min` int(6) default NULL,
  `priceOn` int(6) default NULL,
  `mulAmount` float default '1',
  `rangeStart` float default NULL,
  `rangeEnd` float default NULL,
  `qtyInLots` tinyint(1) default NULL,
  `exchange` varchar(20) default 'MCX',
  `high` double NOT NULL default '0',
  `low` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `item`
-- 

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('GOLD', 'GOLD', 'GOLD', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVER', 'SILVER', 'SILVER', NULL, NULL, 500, 30, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXGOLD', 'CXGOLD', 'CXGOLD', NULL, NULL, 40, 1, 1, 100, 1000, 2000, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('COPPER', 'COPPER', 'COPPER', NULL, NULL, 0, 1000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('GOLDM', 'GOLDM', 'GOLDM', NULL, NULL, 0, 10, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('MINICRUDECX', 'MINICRUDECX', 'MINICRUDECX', NULL, NULL, 40, 1, NULL, 500, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('LEAD', 'LEAD', 'LEAD', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('ZINC', 'ZINC', 'ZINC', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('NICKEL', 'NICKEL', 'NICKEL', NULL, NULL, 500, 250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('NATURGAS', 'NATURGAS', 'NATURGAS', NULL, NULL, 500, 1250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXSILVER', 'CXSILVER', 'CXSILVER', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXCRUDE', 'CXCRUDE', 'CXCRUDE', NULL, NULL, 40, 1, 1, 1000, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXEURO', 'CXEURO', 'CXEURO', NULL, NULL, 40, 1, 1, 125000, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXGOLDMINI', 'CXGOLDMINI', 'CXGOLDMINI', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('CXCOPPER', 'CXCOPPER', 'CXCOPPER', NULL, NULL, 30, 1, 1, 250, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVERMINICX', 'SILVERMINICX', 'SILVERMINICX', NULL, NULL, 40, 1, NULL, 25, 0, 0, NULL, 'COMEX', 0, 0);
INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES ('SILVERM', 'SILVERM', 'SILVERM', NULL, NULL, 100, 5, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `itemdatewise`
-- 

CREATE TABLE `itemdatewise` (
  `itemId` varchar(250) character set utf8 NOT NULL default '',
  `exchange` varchar(20) character set utf8 NOT NULL default '',
  `itemDate` date NOT NULL default '0000-00-00',
  `high` double NOT NULL default '0',
  `low` double NOT NULL default '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `itemdatewise`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `newexpmaster`
-- 

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL auto_increment,
  `newExpName` varchar(30) character set utf8 NOT NULL default '',
  PRIMARY KEY  (`newExpMasterId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `newexpmaster`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orders`
-- 

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orders`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `orderscx`
-- 

CREATE TABLE `orderscx` (
  `orderId` int(6) NOT NULL auto_increment,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `orderDate` date default NULL,
  `orderTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `orderRefNo` varchar(60) default NULL,
  `orderNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `orderType` varchar(30) NOT NULL default '',
  `orderValidity` varchar(15) NOT NULL default '',
  `orderValidTillDate` date NOT NULL default '0000-00-00',
  `orderStatus` varchar(30) NOT NULL default '',
  `triggerPrice` float default NULL,
  PRIMARY KEY  (`orderId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `orderscx`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `otherexp`
-- 

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL auto_increment,
  `otherExpName` varchar(50) character set utf8 default NULL,
  `otherExpDate` date default NULL,
  `otherExpAmount` float default NULL,
  `note` varchar(60) character set utf8 default NULL,
  `otherExpMode` varchar(60) character set utf8 default NULL,
  PRIMARY KEY  (`otherexpId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `otherexp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `settings`
-- 

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL auto_increment,
  `key` varchar(30) default NULL,
  `value` varchar(60) default NULL,
  PRIMARY KEY  (`settingsId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `settings`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `standing`
-- 

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `standing`
-- 

INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (3, '2012-12-26', '2012-12-27', 'GOLD05DEC2012', 15500);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (4, '2012-12-26', '2012-12-27', 'SILVER05DEC2012', 28000);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (5, '2012-12-26', '2012-12-27', 'CXEURO10DEC2012', 1.291);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (6, '2012-12-26', '2012-12-27', 'CXEURO30NOV2012', 1.291);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (7, '2012-12-26', '2012-12-27', 'CXGOLD10DEC2012', 1560);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (8, '2012-12-26', '2012-12-27', 'CXSILVER10DEC2012', 3100);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (9, '2013-02-28', '2013-03-01', 'CXGOLD10DEC2012', 1695);
INSERT INTO `standing` (`standingId`, `standingDtCurrent`, `standingDtNext`, `itemIdExpiryDate`, `standingPrice`) VALUES (10, '2013-02-28', '2013-03-01', 'CXSILVER10DEC2012', 3100);

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxt`
-- 

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  `highLowConf` char(1) character set utf8 default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

-- 
-- Dumping data for table `tradetxt`
-- 

INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (5, 0, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-02', '13:56:01', 100, 31104, NULL, NULL, NULL, '05DEC2012', '_SELF', '6', NULL, 0, '0');
INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (13, -1, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-04', NULL, 100, 31500, 0, NULL, NULL, '05DEC2012', '_SELF', NULL, NULL, 0, '0');
INSERT INTO `tradetxt` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (15, -1, 6, '6', '', '', 'Buy', 'GOLD', '2012-11-04', NULL, 100, 0, 0, NULL, NULL, '05DEC2012', '_SELF', NULL, NULL, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtcx`
-- 

CREATE TABLE `tradetxtcx` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) character set utf8 default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) character set utf8 default NULL,
  `vendor` varchar(50) character set utf8 default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) default '0',
  `highLowConf` char(1) character set utf8 default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

-- 
-- Dumping data for table `tradetxtcx`
-- 

INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (1, 0, 73, '73', '', '', 'Buy', 'CXGOLD', '2012-11-01', '17:03:02', 1, 1690, NULL, NULL, NULL, '10DEC2012', '_SELF', '73', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (5, -1, 73, '73', '', '', 'Buy', 'CXGOLD', '2012-11-04', NULL, 1, 0, 0, NULL, NULL, '10DEC2012', '_SELF', NULL, NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (6, 0, 29, '29', '', '', 'Buy', 'CXGOLD', '2012-12-27', '18:46:09', 1, 1660, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (7, 0, 29, '29', '', '', 'Sell', 'CXGOLD', '2012-12-27', '18:46:16', 1, 1665, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (8, 0, 29, '29', '', '', 'Sell', 'CXSILVER', '2012-12-27', '18:46:31', 1, 3050, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');
INSERT INTO `tradetxtcx` (`tradeId`, `standing`, `clientId`, `firstName`, `middleName`, `lastName`, `buySell`, `itemId`, `tradeDate`, `tradeTime`, `qty`, `price`, `brok`, `tradeRefNo`, `tradeNote`, `expiryDate`, `vendor`, `userRemarks`, `ownClient`, `confirmed`, `highLowConf`) VALUES (9, 0, 29, '29', '', '', 'Buy', 'CXSILVER', '2012-12-27', '18:46:41', 1, 3080, NULL, NULL, NULL, '10DEC2012', '_SELF', '29', NULL, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `tradetxtv1`
-- 

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tradetxtv1`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendor`
-- 

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `address` text,
  `phone` varchar(30) default NULL,
  `mobile` varchar(22) default NULL,
  `fax` varchar(30) default NULL,
  `email` varchar(60) default NULL,
  `deposit` float default NULL,
  `currentBal` float default NULL,
  PRIMARY KEY  (`vendorId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendor`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendorbrok`
-- 

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL auto_increment,
  `vendor` varchar(50) default NULL,
  `itemId` varchar(50) default NULL,
  `oneSideBrok` int(6) default NULL,
  `brok1` float default NULL,
  `brok2` float default NULL,
  PRIMARY KEY  (`clientBrokId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `vendorbrok`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortemp`
-- 

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `vendortemp`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `vendortrades`
-- 

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default '0',
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(50) default NULL,
  `userRemarks` varchar(50) default NULL,
  `ownClient` varchar(50) default NULL,
  `confirmed` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `vendortrades`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxexpiry`
-- 

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL auto_increment,
  `itemId` varchar(50) default NULL,
  `expiryDate` varchar(20) default NULL,
  PRIMARY KEY  (`expiryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxexpiry`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxitem`
-- 

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL default '',
  `item` varchar(50) default NULL,
  `oneSideBrok` float default '0',
  `mulAmount` float default '0',
  `minQty` float default NULL,
  `brok` int(6) default '1',
  `brok2` int(6) default '1',
  `per` int(6) default '1',
  `unit` int(6) default '1',
  `min` int(6) default '1',
  `priceOn` int(6) default '1',
  `priceUnit` int(6) default '1',
  PRIMARY KEY  (`itemId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `zcxitem`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxmember`
-- 

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL auto_increment,
  `userId` varchar(10) default NULL,
  `memberId` varchar(10) default NULL,
  PRIMARY KEY  (`zCxMemberId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxmember`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxstanding`
-- 

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL auto_increment,
  `standingDtCurrent` date default NULL,
  `standingDtNext` date default NULL,
  `itemIdExpiryDate` varchar(50) default NULL,
  `standingPrice` float default NULL,
  UNIQUE KEY `standingId` (`standingId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxstanding`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `zcxtrades`
-- 

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL auto_increment,
  `standing` tinyint(1) default NULL,
  `clientId` int(6) default '0',
  `firstName` varchar(35) default NULL,
  `middleName` varchar(35) default NULL,
  `lastName` varchar(35) default NULL,
  `buySell` varchar(10) default NULL,
  `itemId` varchar(50) default NULL,
  `tradeDate` date default NULL,
  `tradeTime` varchar(20) default NULL,
  `qty` int(6) default NULL,
  `price` float default NULL,
  `brok` int(6) default NULL,
  `tradeRefNo` varchar(60) default NULL,
  `tradeNote` varchar(200) default NULL,
  `expiryDate` varchar(20) default NULL,
  `vendor` varchar(10) default NULL,
  `removeFromAccount` tinyint(4) default NULL,
  PRIMARY KEY  (`tradeId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `zcxtrades`
-- 

