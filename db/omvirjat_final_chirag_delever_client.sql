-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 02:08 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omvirjat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL,
  `bankName` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone1` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone2` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL,
  `exchange` varchar(30) NOT NULL DEFAULT '',
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) NOT NULL DEFAULT '',
  `marketType` varchar(15) NOT NULL DEFAULT '',
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) NOT NULL DEFAULT '',
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) NOT NULL DEFAULT '',
  `scriptGroup` varchar(5) NOT NULL DEFAULT '',
  `scriptType` varchar(5) NOT NULL DEFAULT '',
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL DEFAULT '',
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) NOT NULL DEFAULT '',
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) NOT NULL DEFAULT '',
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL,
  `clientId` int(10) DEFAULT '0',
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `dwStatus` char(2) DEFAULT '',
  `dwAmount` float DEFAULT '0',
  `plStatus` char(2) DEFAULT '',
  `plAmount` float DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) DEFAULT NULL,
  `transMode` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tradeRefNo` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `clientBroker` int(2) DEFAULT '0',
  `groupName` varchar(100) DEFAULT '0',
  `profitBankRate` double DEFAULT NULL,
  `lossBankRate` double DEFAULT NULL,
  `commission` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(10) DEFAULT NULL,
  `exchange` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `exchangeId` int(6) UNSIGNED NOT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL,
  `expenseName` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `exchange` varchar(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expiry`
--

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES
(36, 'CXGOLDMINI', '15AUG2015', 'COMEX'),
(30, 'SILVER', '03JUL2015', 'MCX'),
(34, 'CRUDEOIL', '19JUN2015', 'MCX'),
(9, 'NATURGAS', '27APR2015', 'MCX'),
(35, 'CXGOLD', '15AUG2015', 'COMEX'),
(20, 'CXCRUDEMINI', '15JUN2015', 'COMEX'),
(13, 'CXSILVER', '15JUN2015', 'COMEX'),
(14, 'CXCRUDE', '15JUN2015', 'COMEX'),
(15, 'CXEURO', '15JUN2015', 'COMEX'),
(17, 'CXCOPPER', '15JUN2015', 'COMEX'),
(21, 'CXSILVERMINI', '15JUN2015', 'COMEX'),
(19, 'CXEUROHALF', '15JUN2015', 'COMEX'),
(31, 'SILVERM', '03JUL2015', 'MCX'),
(33, 'COPPER', '30JUN2015', 'MCX'),
(37, 'GOLD', '05AUG2015', 'MCX'),
(38, 'ZINC', '30JUN2015', 'MCX'),
(39, 'LEAD', '30JUN2015', 'MCX'),
(40, 'NICKEL', '30JUN2015', 'MCX'),
(41, 'GOLDM', '05AUG2015', 'MCX');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grouplist`
--

CREATE TABLE `grouplist` (
  `groupId` int(6) UNSIGNED NOT NULL,
  `groupName` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grouplist`
--

INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES
(1, '28'),
(2, '6');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `itemShort` varchar(50) DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchange` varchar(20) DEFAULT 'MCX',
  `high` double NOT NULL DEFAULT '0',
  `low` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES
('GOLD', 'GOLD', 'GOLD', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('SILVER', 'SILVER', 'SILVER', NULL, NULL, 500, 30, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('CXGOLD', 'CXGOLD', 'CXGOLD', NULL, NULL, 40, 1, 1, 100, 1000, 2000, NULL, 'COMEX', 0, 0),
('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('COPPER', 'COPPER', 'COPPER', NULL, NULL, 0, 1000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('GOLDM', 'GOLDM', 'GOLDM', NULL, NULL, 0, 10, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('LEAD', 'LEAD', 'LEAD', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('ZINC', 'ZINC', 'ZINC', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('NICKEL', 'NICKEL', 'NICKEL', NULL, NULL, 500, 250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('NATURGAS', 'NATURGAS', 'NATURGAS', NULL, NULL, 500, 1250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('CXSILVER', 'CXSILVER', 'CXSILVER', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0),
('CXCRUDE', 'CXCRUDE', 'CXCRUDE', NULL, NULL, 40, 1, 1, 1000, 0, 0, NULL, 'COMEX', 0, 0),
('CXEURO', 'CXEURO', 'CXEURO', NULL, NULL, 40, 1, 1, 125000, 0, 0, NULL, 'COMEX', 0, 0),
('CXGOLDMINI', 'CXGOLDMINI', 'CXGOLDMINI', NULL, NULL, 10, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0),
('CXCOPPER', 'CXCOPPER', 'CXCOPPER', NULL, NULL, 30, 1, 1, 250, 0, 0, NULL, 'COMEX', 0, 0),
('CXSILVERMINI', 'CXSILVERMINI', 'CXSILVERMINI', NULL, NULL, 10, 1, NULL, 25, 1000, 2000, NULL, 'COMEX', 0, 0),
('SILVERM', 'SILVERM', 'SILVERM', NULL, NULL, 100, 5, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('CXCRUDEMINI', 'CXCRUDEMINI', 'CXCRUDEMINI', NULL, NULL, 10, 1, NULL, 500, 40, 100, NULL, 'COMEX', 0, 0),
('CXEUROHALF', 'CXEUROHALF', 'CXEUROHALF', NULL, NULL, 20, 1, NULL, 62500, 1, 5, NULL, 'COMEX', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `itemdatewise`
--

CREATE TABLE `itemdatewise` (
  `itemId` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itemDate` date NOT NULL DEFAULT '0000-00-00',
  `high` double NOT NULL DEFAULT '0',
  `low` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `itemdatewise`
--

INSERT INTO `itemdatewise` (`itemId`, `exchange`, `itemDate`, `high`, `low`) VALUES
('COPPER', 'MCX', '2015-04-01', 382.8, 374.45),
('CRUDEOIL', 'MCX', '2015-04-01', 3150, 2952),
('GOLD', 'MCX', '2015-04-01', 26760, 26238),
('GOLDM', 'MCX', '2015-04-01', 26760, 26238),
('LEAD', 'MCX', '2015-04-01', 116.05, 113.15),
('NATURGAS', 'MCX', '2015-04-01', 167.8, 162.2),
('NICKEL', 'MCX', '2015-04-01', 797.9, 775.6),
('SILVER', 'MCX', '2015-04-01', 38080, 37030),
('SILVERM', 'MCX', '2015-04-01', 38080, 37030),
('ZINC', 'MCX', '2015-04-01', 131.2, 128.85),
('CXCOPPER', 'COMEX', '2015-04-01', 275.9, 272.1),
('CXCRUDE', 'COMEX', '2015-04-01', 50.45, 47.05),
('CXCRUDEMINI', 'COMEX', '2015-04-01', 50.45, 47.05),
('CXEURO', 'COMEX', '2015-04-01', 1.0811, 1.0729),
('CXEUROHALF', 'COMEX', '2015-04-01', 1.0811, 1.0729),
('CXGOLD', 'COMEX', '2015-04-01', 1208.7, 1180.5),
('CXGOLDMINI', 'COMEX', '2015-04-01', 1208.7, 1180.5),
('CXSILVER', 'COMEX', '2015-04-01', 1707.5, 1650),
('CXSILVERMINI', 'COMEX', '2015-04-01', 1707.5, 1650),
('COPPER', 'MCX', '2015-04-02', 377.95, 375.05),
('CRUDEOIL', 'MCX', '2015-04-02', 3117, 3037),
('GOLD', 'MCX', '2015-04-02', 26728, 26500),
('GOLDM', 'MCX', '2015-04-02', 26728, 26500),
('LEAD', 'MCX', '2015-04-02', 117.3, 116.1),
('NATURGAS', 'MCX', '2015-04-02', 170.2, 162.9),
('NICKEL', 'MCX', '2015-04-02', 816.7, 800),
('SILVER', 'MCX', '2015-04-02', 37729, 37107),
('SILVERM', 'MCX', '2015-04-02', 37729, 37107),
('ZINC', 'MCX', '2015-04-02', 132.8, 130.8),
('CXCOPPER', 'COMEX', '2015-04-02', 274.8, 273.4),
('CXCRUDE', 'COMEX', '2015-04-02', 50.27, 48.38),
('CXCRUDEMINI', 'COMEX', '2015-04-02', 50.27, 48.38),
('CXEURO', 'COMEX', '2015-04-02', 1.0916, 1.076),
('CXEUROHALF', 'COMEX', '2015-04-02', 1.0916, 1.076),
('CXGOLD', 'COMEX', '2015-04-02', 1207.4, 1194.8),
('CXGOLDMINI', 'COMEX', '2015-04-02', 1207.4, 1194.8),
('CXSILVER', 'COMEX', '2015-04-02', 1700, 1657),
('CXSILVERMINI', 'COMEX', '2015-04-02', 1700, 1657),
('CXCOPPER', 'COMEX', '2015-04-06', 281, 270.95),
('CXCRUDE', 'COMEX', '2015-04-06', 51.61, 49.47),
('CXCRUDEMINI', 'COMEX', '2015-04-06', 51.61, 49.47),
('CXEURO', 'COMEX', '2015-04-06', 1.1047, 1.0977),
('CXEUROHALF', 'COMEX', '2015-04-06', 1.1047, 1.0977),
('CXGOLD', 'COMEX', '2015-04-06', 1224.5, 1212.6),
('CXGOLDMINI', 'COMEX', '2015-04-06', 1224.5, 1212.6),
('CXSILVER', 'COMEX', '2015-04-06', 1731, 1696),
('CXSILVERMINI', 'COMEX', '2015-04-06', 1731, 1696),
('COPPER', 'MCX', '2015-04-09', 380.3, 374.85),
('CRUDEOIL', 'MCX', '2015-04-09', 3250, 3162),
('GOLD', 'MCX', '2015-04-09', 26700, 26462),
('GOLDM', 'MCX', '2015-04-09', 26700, 26462),
('LEAD', 'MCX', '2015-04-09', 122.85, 119.6),
('NATURGAS', 'MCX', '2015-04-09', 165.5, 158.2),
('NICKEL', 'MCX', '2015-04-09', 795.4, 780.2),
('SILVER', 'MCX', '2015-04-09', 36777, 36085),
('SILVERM', 'MCX', '2015-04-09', 36777, 36085),
('ZINC', 'MCX', '2015-04-09', 135.8, 133.35),
('COPPER', 'MCX', '2015-04-06', 389.8, 371.45),
('CRUDEOIL', 'MCX', '2015-04-06', 3263, 3075),
('GOLD', 'MCX', '2015-04-06', 27095, 26645),
('GOLDM', 'MCX', '2015-04-06', 27095, 26645),
('LEAD', 'MCX', '2015-04-06', 118.35, 116.7),
('NATURGAS', 'MCX', '2015-04-06', 168.6, 165),
('NICKEL', 'MCX', '2015-04-06', 821.5, 803.7),
('SILVER', 'MCX', '2015-04-06', 38588, 37635),
('SILVERM', 'MCX', '2015-04-06', 38588, 37635),
('ZINC', 'MCX', '2015-04-06', 134.5, 132.1),
('COPPER', 'MCX', '2015-04-07', 383.15, 375.5),
('CRUDEOIL', 'MCX', '2015-04-07', 3365, 3203),
('GOLD', 'MCX', '2015-04-07', 26980, 26852),
('GOLDM', 'MCX', '2015-04-07', 26980, 26852),
('LEAD', 'MCX', '2015-04-07', 119.15, 117.3),
('NATURGAS', 'MCX', '2015-04-07', 169.9, 166.1),
('NICKEL', 'MCX', '2015-04-07', 816.5, 785.5),
('SILVER', 'MCX', '2015-04-07', 38140, 37563),
('SILVERM', 'MCX', '2015-04-07', 38140, 37563),
('ZINC', 'MCX', '2015-04-07', 134.45, 132.65),
('COPPER', 'MCX', '2015-04-08', 382.4, 375.9),
('CRUDEOIL', 'MCX', '2015-04-08', 3321, 3166),
('GOLD', 'MCX', '2015-04-08', 26929, 26671),
('GOLDM', 'MCX', '2015-04-08', 26929, 26671),
('LEAD', 'MCX', '2015-04-08', 120.2, 118.2),
('NATURGAS', 'MCX', '2015-04-08', 167.3, 163.4),
('NICKEL', 'MCX', '2015-04-08', 802.8, 785.7),
('SILVER', 'MCX', '2015-04-08', 37840, 36761),
('SILVERM', 'MCX', '2015-04-08', 37840, 36761),
('ZINC', 'MCX', '2015-04-08', 134.5, 133.3),
('COPPER', 'MCX', '2015-04-10', 382.55, 376.6),
('CRUDEOIL', 'MCX', '2015-04-10', 3240, 3132),
('GOLD', 'MCX', '2015-04-10', 26840, 26534),
('GOLDM', 'MCX', '2015-04-10', 26840, 26534),
('LEAD', 'MCX', '2015-04-10', 125.15, 122.45),
('NATURGAS', 'MCX', '2015-04-10', 160, 157.6),
('NICKEL', 'MCX', '2015-04-10', 802.6, 787.4),
('SILVER', 'MCX', '2015-04-10', 37164, 36317),
('SILVERM', 'MCX', '2015-04-10', 37164, 36317),
('ZINC', 'MCX', '2015-04-10', 137.85, 135.75),
('COPPER', 'MCX', '2015-04-13', 381.25, 375.35),
('CRUDEOIL', 'MCX', '2015-04-13', 3319, 3222),
('GOLD', 'MCX', '2015-04-13', 26815, 26629),
('GOLDM', 'MCX', '2015-04-13', 26815, 26629),
('LEAD', 'MCX', '2015-04-13', 125.9, 123.05),
('NATURGAS', 'MCX', '2015-04-13', 159.4, 155.7),
('NICKEL', 'MCX', '2015-04-13', 801.7, 778.2),
('SILVER', 'MCX', '2015-04-13', 36820, 36423),
('SILVERM', 'MCX', '2015-04-13', 36820, 36423),
('ZINC', 'MCX', '2015-04-13', 138.85, 136.9),
('COPPER', 'MCX', '2015-04-14', 375, 369.15),
('CRUDEOIL', 'MCX', '2015-04-14', 3354, 3261),
('GOLD', 'MCX', '2015-04-14', 26622, 26387),
('GOLDM', 'MCX', '2015-04-14', 26622, 26387),
('LEAD', 'MCX', '2015-04-14', 123.55, 122.3),
('NATURGAS', 'MCX', '2015-04-14', 159.7, 157.8),
('NICKEL', 'MCX', '2015-04-14', 787, 765.7),
('SILVER', 'MCX', '2015-04-14', 36570, 35900),
('SILVERM', 'MCX', '2015-04-14', 36570, 35900),
('ZINC', 'MCX', '2015-04-14', 136.8, 135.8),
('CXCOPPER', 'COMEX', '2015-04-14', 272.6, 267.5),
('CXCRUDE', 'COMEX', '2015-04-14', 53.72, 51.83),
('CXCRUDEMINI', 'COMEX', '2015-04-14', 53.72, 51.83),
('CXEURO', 'COMEX', '2015-04-14', 1.0717, 1.0539),
('CXEUROHALF', 'COMEX', '2015-04-14', 1.0717, 1.0539),
('CXGOLD', 'COMEX', '2015-04-14', 1201.3, 1183.5),
('CXGOLDMINI', 'COMEX', '2015-04-14', 1201.3, 1183.5),
('CXSILVER', 'COMEX', '2015-04-14', 1634, 1595.5),
('CXSILVERMINI', 'COMEX', '2015-04-14', 1634, 1595.5),
('COPPER', 'MCX', '2015-04-15', 375.75, 368.8),
('CRUDEOIL', 'MCX', '2015-04-15', 3504, 3342),
('GOLD', 'MCX', '2015-04-15', 26745, 26476),
('GOLDM', 'MCX', '2015-04-15', 26745, 26476),
('LEAD', 'MCX', '2015-04-15', 124.95, 123.15),
('NATURGAS', 'MCX', '2015-04-15', 164, 156.2),
('NICKEL', 'MCX', '2015-04-15', 794.7, 785.1),
('SILVER', 'MCX', '2015-04-15', 36590, 36066),
('SILVERM', 'MCX', '2015-04-15', 36590, 36066),
('ZINC', 'MCX', '2015-04-15', 137.85, 136.15),
('CXCOPPER', 'COMEX', '2015-04-15', 273, 269.2),
('CXCRUDE', 'COMEX', '2015-04-15', 56.14, 53.39),
('CXCRUDEMINI', 'COMEX', '2015-04-15', 56.14, 53.39),
('CXEURO', 'COMEX', '2015-04-15', 1.0708, 1.0579),
('CXEUROHALF', 'COMEX', '2015-04-15', 1.0708, 1.0579),
('CXGOLD', 'COMEX', '2015-04-15', 1202.9, 1188.3),
('CXGOLDMINI', 'COMEX', '2015-04-15', 1202.9, 1188.3),
('CXSILVER', 'COMEX', '2015-04-15', 1632.5, 1604.5),
('CXSILVERMINI', 'COMEX', '2015-04-15', 1632.5, 1604.5),
('COPPER', 'MCX', '2015-04-16', 383.75, 376.1),
('CRUDEOIL', 'MCX', '2015-04-16', 3532, 3435),
('GOLD', 'MCX', '2015-04-16', 26845, 26568),
('GOLDM', 'MCX', '2015-04-16', 26845, 26568),
('LEAD', 'MCX', '2015-04-16', 127.65, 124.8),
('NATURGAS', 'MCX', '2015-04-16', 165.5, 159.3),
('NICKEL', 'MCX', '2015-04-16', 807, 794.1),
('SILVER', 'MCX', '2015-04-16', 36850, 36125),
('SILVERM', 'MCX', '2015-04-16', 36850, 36125),
('ZINC', 'MCX', '2015-04-16', 139.4, 137.4),
('CXCOPPER', 'COMEX', '2015-04-16', 278.75, 271.9),
('CXCRUDE', 'COMEX', '2015-04-16', 56.54, 55.07),
('CXCRUDEMINI', 'COMEX', '2015-04-16', 56.54, 55.07),
('CXEURO', 'COMEX', '2015-04-16', 1.0777, 1.0633),
('CXEUROHALF', 'COMEX', '2015-04-16', 1.0777, 1.0633),
('CXGOLD', 'COMEX', '2015-04-16', 1208.8, 1194.3),
('CXGOLDMINI', 'COMEX', '2015-04-16', 1208.8, 1194.3),
('CXSILVER', 'COMEX', '2015-04-16', 1647.5, 1610),
('CXSILVERMINI', 'COMEX', '2015-04-16', 1647.5, 1610),
('COPPER', 'MCX', '2015-04-17', 386.8, 381.55),
('CRUDEOIL', 'MCX', '2015-04-17', 3554, 3485),
('GOLD', 'MCX', '2015-04-17', 26840, 26617),
('GOLDM', 'MCX', '2015-04-17', 26840, 26617),
('LEAD', 'MCX', '2015-04-17', 128.2, 126.4),
('NATURGAS', 'MCX', '2015-04-17', 168.3, 164.7),
('NICKEL', 'MCX', '2015-04-17', 801.9, 786.1),
('SILVER', 'MCX', '2015-04-17', 36890, 36369),
('SILVERM', 'MCX', '2015-04-17', 36890, 36369),
('ZINC', 'MCX', '2015-04-17', 139.75, 138.15),
('COPPER', 'MCX', '2015-04-20', 387, 379.2),
('CRUDEOIL', 'MCX', '2015-04-20', 3577, 3426),
('GOLD', 'MCX', '2015-04-20', 26967, 26768),
('GOLDM', 'MCX', '2015-04-20', 26967, 26768),
('LEAD', 'MCX', '2015-04-20', 128.8, 127.45),
('NATURGAS', 'MCX', '2015-04-20', 164.2, 160.3),
('NICKEL', 'MCX', '2015-04-20', 803.5, 780.7),
('SILVER', 'MCX', '2015-04-20', 36777, 35950),
('SILVERM', 'MCX', '2015-04-20', 36777, 35950),
('ZINC', 'MCX', '2015-04-20', 140.4, 138.3),
('COPPER', 'MCX', '2015-04-21', 381.6, 375.35),
('CRUDEOIL', 'MCX', '2015-04-21', 3668, 3618),
('GOLD', 'MCX', '2015-04-21', 26934, 26753),
('GOLDM', 'MCX', '2015-04-21', 26934, 26753),
('LEAD', 'MCX', '2015-04-21', 127.8, 126.15),
('NATURGAS', 'MCX', '2015-04-21', 163.7, 160.7),
('NICKEL', 'MCX', '2015-04-21', 808.2, 796),
('SILVER', 'MCX', '2015-04-21', 36515, 36031),
('SILVERM', 'MCX', '2015-04-21', 36515, 36031),
('ZINC', 'MCX', '2015-04-21', 138.25, 136.7),
('COPPER', 'MCX', '2015-04-22', 376.55, 371.25),
('CRUDEOIL', 'MCX', '2015-04-22', 3621, 3520),
('GOLD', 'MCX', '2015-04-22', 26960, 26606),
('GOLDM', 'MCX', '2015-04-22', 26960, 26606),
('LEAD', 'MCX', '2015-04-22', 130.3, 128.6),
('NATURGAS', 'MCX', '2015-04-22', 165.4, 160.8),
('NICKEL', 'MCX', '2015-04-22', 805.7, 793.7),
('SILVER', 'MCX', '2015-04-22', 36320, 35591),
('SILVERM', 'MCX', '2015-04-22', 36320, 35591),
('ZINC', 'MCX', '2015-04-22', 141.15, 138.95),
('COPPER', 'MCX', '2015-04-23', 378, 370.25),
('CRUDEOIL', 'MCX', '2015-04-23', 3710, 3545),
('GOLD', 'MCX', '2015-04-23', 26917, 26606),
('GOLDM', 'MCX', '2015-04-23', 26917, 26606),
('LEAD', 'MCX', '2015-04-23', 129.15, 127.55),
('NATURGAS', 'MCX', '2015-04-23', 164.85, 159.7),
('NICKEL', 'MCX', '2015-04-23', 810, 792.4),
('SILVER', 'MCX', '2015-04-23', 36194, 35805),
('SILVERM', 'MCX', '2015-04-23', 36194, 35805),
('ZINC', 'MCX', '2015-04-23', 141.35, 139.55),
('COPPER', 'MCX', '2015-04-24', 387.4, 376.3),
('CRUDEOIL', 'MCX', '2015-04-24', 3698, 3614),
('GOLD', 'MCX', '2015-04-24', 26959, 26630),
('GOLDM', 'MCX', '2015-04-24', 26959, 26630),
('LEAD', 'MCX', '2015-04-24', 132.35, 128.8),
('NATURGAS', 'MCX', '2015-04-24', 163.1, 160.3),
('NICKEL', 'MCX', '2015-04-24', 829.6, 805),
('SILVER', 'MCX', '2015-04-24', 36234, 35677),
('SILVERM', 'MCX', '2015-04-24', 36234, 35677),
('ZINC', 'MCX', '2015-04-24', 143.35, 140.45),
('COPPER', 'MCX', '2015-04-27', 389.6, 385.4),
('CRUDEOIL', 'MCX', '2015-04-27', 3693, 3601),
('GOLD', 'MCX', '2015-04-27', 27135, 26707),
('GOLDM', 'MCX', '2015-04-27', 27135, 26707),
('LEAD', 'MCX', '2015-04-27', 133.75, 132.3),
('NATURGAS', 'MCX', '2015-04-27', 160, 155.4),
('NICKEL', 'MCX', '2015-04-27', 867, 846),
('SILVER', 'MCX', '2015-04-27', 37323, 36016),
('SILVERM', 'MCX', '2015-04-27', 37323, 36016),
('ZINC', 'MCX', '2015-04-27', 146.65, 143.3),
('CXCOPPER', 'COMEX', '2015-04-27', 278.5, 273.2),
('CXCRUDE', 'COMEX', '2015-04-27', 57.89, 56.56),
('CXCRUDEMINI', 'COMEX', '2015-04-27', 57.89, 56.56),
('CXEURO', 'COMEX', '2015-04-27', 1.0934, 1.0826),
('CXEUROHALF', 'COMEX', '2015-04-27', 1.0934, 1.0826),
('CXGOLD', 'COMEX', '2015-04-27', 1206.7, 1177.6),
('CXGOLDMINI', 'COMEX', '2015-04-27', 1206.7, 1177.6),
('CXSILVER', 'COMEX', '2015-04-27', 1644.5, 1568),
('CXSILVERMINI', 'COMEX', '2015-04-27', 1644.5, 1568),
('CXCOPPER', 'COMEX', '2015-04-28', 278.7, 274.75),
('CXCRUDE', 'COMEX', '2015-04-28', 57.83, 56.07),
('CXCRUDEMINI', 'COMEX', '2015-04-28', 57.83, 56.07),
('CXEURO', 'COMEX', '2015-04-28', 1.0997, 1.0867),
('CXEUROHALF', 'COMEX', '2015-04-28', 1.0997, 1.0867),
('CXGOLD', 'COMEX', '2015-04-28', 1214.9, 1198.6),
('CXGOLDMINI', 'COMEX', '2015-04-28', 1214.9, 1198.6),
('CXSILVER', 'COMEX', '2015-04-28', 1666, 1627.5),
('CXSILVERMINI', 'COMEX', '2015-04-28', 1666, 1627.5),
('CXCOPPER', 'COMEX', '2015-04-29', 280.4, 275.45),
('CXCRUDE', 'COMEX', '2015-04-29', 59.24, 56.54),
('CXCRUDEMINI', 'COMEX', '2015-04-29', 59.24, 56.54),
('CXEURO', 'COMEX', '2015-04-29', 1.1195, 1.0965),
('CXEUROHALF', 'COMEX', '2015-04-29', 1.1195, 1.0965),
('CXGOLD', 'COMEX', '2015-04-29', 1213.5, 1203.9),
('CXGOLDMINI', 'COMEX', '2015-04-29', 1213.5, 1203.9),
('CXSILVER', 'COMEX', '2015-04-29', 1672.5, 1641.5),
('CXSILVERMINI', 'COMEX', '2015-04-29', 1672.5, 1641.5),
('COPPER', 'MCX', '2015-04-29', 390.85, 383.4),
('CRUDEOIL', 'MCX', '2015-04-29', 3771, 3590),
('GOLD', 'MCX', '2015-04-29', 27274, 27064),
('GOLDM', 'MCX', '2015-04-29', 27274, 27064),
('LEAD', 'MCX', '2015-04-29', 133.65, 131.15),
('NATURGAS', 'MCX', '2015-04-29', 166.6, 160.1),
('NICKEL', 'MCX', '2015-04-29', 855, 835.2),
('SILVER', 'MCX', '2015-04-29', 37790, 37150),
('SILVERM', 'MCX', '2015-04-29', 37790, 37150),
('ZINC', 'MCX', '2015-04-29', 147.1, 144.4),
('CXCOPPER', 'COMEX', '2015-04-30', 279.25, 278.6),
('CXCRUDE', 'COMEX', '2015-04-30', 5874, 5838),
('CXCRUDEMINI', 'COMEX', '2015-04-30', 5874, 5838),
('CXEURO', 'COMEX', '2015-04-30', 1.1132, 1.1108),
('CXEUROHALF', 'COMEX', '2015-04-30', 1.1132, 1.1108),
('CXGOLD', 'COMEX', '2015-04-30', 1205.7, 1200.3),
('CXGOLDMINI', 'COMEX', '2015-04-30', 1205.7, 1200.3),
('CXSILVER', 'COMEX', '2015-04-30', 1661, 1646.5),
('CXSILVERMINI', 'COMEX', '2015-04-30', 1661, 1646.5),
('COPPER', 'MCX', '2015-04-30', 402.8, 389.5),
('CRUDEOIL', 'MCX', '2015-04-30', 3797, 3730),
('GOLD', 'MCX', '2015-04-30', 27279, 26655),
('GOLDM', 'MCX', '2015-04-30', 27279, 26655),
('LEAD', 'MCX', '2015-04-30', 135.35, 133.3),
('NATURGAS', 'MCX', '2015-04-30', 173.9, 163.2),
('NICKEL', 'MCX', '2015-04-30', 881.4, 856),
('SILVER', 'MCX', '2015-04-30', 37730, 36060),
('SILVERM', 'MCX', '2015-04-30', 37730, 36060),
('ZINC', 'MCX', '2015-04-30', 150.25, 147.1),
('COPPER', 'MCX', '2015-05-04', 415.2, 412.9),
('CRUDEOIL', 'MCX', '2015-05-04', 3813, 3731),
('GOLD', 'MCX', '2015-05-04', 26959, 26697),
('GOLDM', 'MCX', '2015-05-04', 26959, 26697),
('LEAD', 'MCX', '2015-05-04', 136.35, 134.9),
('NATURGAS', 'MCX', '2015-05-04', 180.1, 175.9),
('NICKEL', 'MCX', '2015-05-04', 887.8, 880),
('SILVER', 'MCX', '2015-05-04', 38490, 37469),
('SILVERM', 'MCX', '2015-05-04', 38490, 37469),
('ZINC', 'MCX', '2015-05-04', 150.8, 149.75),
('COPPER', 'MCX', '2015-05-05', 417.4, 410.85),
('CRUDEOIL', 'MCX', '2015-05-05', 3886, 3738),
('GOLD', 'MCX', '2015-05-05', 27020, 26776),
('GOLDM', 'MCX', '2015-05-05', 27020, 26776),
('LEAD', 'MCX', '2015-05-05', 138.15, 134.4),
('NATURGAS', 'MCX', '2015-05-05', 179.8, 176.8),
('NICKEL', 'MCX', '2015-05-05', 914, 873.7),
('SILVER', 'MCX', '2015-05-05', 38295, 37583),
('SILVERM', 'MCX', '2015-05-05', 38295, 37583),
('ZINC', 'MCX', '2015-05-05', 153.55, 148.85),
('COPPER', 'MCX', '2015-05-06', 418, 411.55),
('CRUDEOIL', 'MCX', '2015-05-06', 3989, 3870),
('GOLD', 'MCX', '2015-05-06', 27054, 26813),
('GOLDM', 'MCX', '2015-05-06', 27054, 26813),
('LEAD', 'MCX', '2015-05-06', 138.4, 132.55),
('NATURGAS', 'MCX', '2015-05-06', 180.4, 175.5),
('NICKEL', 'MCX', '2015-05-06', 932.4, 886.3),
('SILVER', 'MCX', '2015-05-06', 38243, 37742),
('SILVERM', 'MCX', '2015-05-06', 38243, 37742),
('ZINC', 'MCX', '2015-05-06', 154.35, 150.85),
('COPPER', 'MCX', '2015-05-07', 420.5, 413),
('CRUDEOIL', 'MCX', '2015-05-07', 3946, 3867),
('GOLD', 'MCX', '2015-05-07', 27040, 26845),
('GOLDM', 'MCX', '2015-05-07', 27040, 26845),
('LEAD', 'MCX', '2015-05-07', 134.6, 131.4),
('NATURGAS', 'MCX', '2015-05-07', 180.4, 177.2),
('NICKEL', 'MCX', '2015-05-07', 909.9, 880.4),
('SILVER', 'MCX', '2015-05-07', 38070, 37686),
('SILVERM', 'MCX', '2015-05-07', 38070, 37686),
('ZINC', 'MCX', '2015-05-07', 153.65, 150.2),
('CXCOPPER', 'COMEX', '2015-05-07', 293.75, 290.5),
('CXCRUDE', 'COMEX', '2015-05-07', 61.31, 60.26),
('CXCRUDEMINI', 'COMEX', '2015-05-07', 61.31, 60.26),
('CXEURO', 'COMEX', '2015-05-07', 1.1398, 1.1306),
('CXEUROHALF', 'COMEX', '2015-05-07', 1.1398, 1.1306),
('CXGOLD', 'COMEX', '2015-05-07', 1192, 1179.8),
('CXGOLDMINI', 'COMEX', '2015-05-07', 1192, 1179.8),
('CXSILVER', 'COMEX', '2015-05-07', 1655.5, 1619.5),
('CXSILVERMINI', 'COMEX', '2015-05-07', 1655.5, 1619.5),
('COPPER', 'MCX', '2015-05-08', 418.35, 411.5),
('CRUDEOIL', 'MCX', '2015-05-08', 3830, 3744),
('GOLD', 'MCX', '2015-05-08', 27014, 26831),
('GOLDM', 'MCX', '2015-05-08', 27014, 26831),
('LEAD', 'MCX', '2015-05-08', 134.3, 132),
('NATURGAS', 'MCX', '2015-05-08', 184.5, 175.1),
('NICKEL', 'MCX', '2015-05-08', 922.3, 901),
('SILVER', 'MCX', '2015-05-08', 38150, 37620),
('SILVERM', 'MCX', '2015-05-08', 38150, 37620),
('ZINC', 'MCX', '2015-05-08', 153.5, 151.5),
('CXCOPPER', 'COMEX', '2015-05-11', 292.95, 289.7),
('CXCRUDE', 'COMEX', '2015-05-11', 59.45, 59.11),
('CXCRUDEMINI', 'COMEX', '2015-05-11', 59.45, 59.11),
('CXEURO', 'COMEX', '2015-05-11', 1.1211, 1.1142),
('CXEUROHALF', 'COMEX', '2015-05-11', 1.1211, 1.1142),
('CXGOLD', 'COMEX', '2015-05-11', 1189.6, 1187.1),
('CXGOLDMINI', 'COMEX', '2015-05-11', 1189.6, 1187.1),
('CXSILVER', 'COMEX', '2015-05-11', 1649.5, 1640.5),
('CXSILVERMINI', 'COMEX', '2015-05-11', 1649.5, 1640.5),
('COPPER', 'MCX', '2015-05-11', 415.95, 412),
('CRUDEOIL', 'MCX', '2015-05-11', 3827, 3763),
('GOLD', 'MCX', '2015-05-11', 26952, 26778),
('GOLDM', 'MCX', '2015-05-11', 26952, 26778),
('LEAD', 'MCX', '2015-05-11', 131.7, 128.25),
('NATURGAS', 'MCX', '2015-05-11', 186.3, 181.2),
('NICKEL', 'MCX', '2015-05-11', 929, 910.2),
('SILVER', 'MCX', '2015-05-11', 38137, 37549),
('SILVERM', 'MCX', '2015-05-11', 38137, 37549),
('ZINC', 'MCX', '2015-05-11', 151.9, 148),
('COPPER', 'MCX', '2015-05-12', 421.4, 414.8),
('CRUDEOIL', 'MCX', '2015-05-12', 3935, 3801),
('GOLD', 'MCX', '2015-05-12', 27241, 26860),
('GOLDM', 'MCX', '2015-05-12', 27241, 26860),
('LEAD', 'MCX', '2015-05-12', 132.65, 128.1),
('NATURGAS', 'MCX', '2015-05-12', 189.2, 179.6),
('NICKEL', 'MCX', '2015-05-12', 922.7, 902.1),
('SILVER', 'MCX', '2015-05-12', 38477, 37520),
('SILVERM', 'MCX', '2015-05-12', 38477, 37520),
('ZINC', 'MCX', '2015-05-12', 151.85, 147.3),
('COPPER', 'MCX', '2015-05-13', 420.7, 415.4),
('CRUDEOIL', 'MCX', '2015-05-13', 3965, 3893),
('GOLD', 'MCX', '2015-05-13', 27517, 27051),
('GOLDM', 'MCX', '2015-05-13', 27517, 27051),
('LEAD', 'MCX', '2015-05-13', 133.35, 128.7),
('NATURGAS', 'MCX', '2015-05-13', 189.6, 183.2),
('NICKEL', 'MCX', '2015-05-13', 919.7, 898.2),
('SILVER', 'MCX', '2015-05-13', 39575, 38253),
('SILVERM', 'MCX', '2015-05-13', 39575, 38253),
('ZINC', 'MCX', '2015-05-13', 153.2, 149.85),
('COPPER', 'MCX', '2015-05-14', 416.95, 413.05),
('CRUDEOIL', 'MCX', '2015-05-14', 3895, 3806),
('GOLD', 'MCX', '2015-05-14', 27595, 27370),
('GOLDM', 'MCX', '2015-05-14', 27595, 27370),
('LEAD', 'MCX', '2015-05-14', 129.85, 126.2),
('NATURGAS', 'MCX', '2015-05-14', 192.1, 183.3),
('NICKEL', 'MCX', '2015-05-14', 896.8, 879.3),
('SILVER', 'MCX', '2015-05-14', 40130, 39273),
('SILVERM', 'MCX', '2015-05-14', 40130, 39273),
('ZINC', 'MCX', '2015-05-14', 150.7, 147.5),
('COPPER', 'MCX', '2015-05-18', 414.95, 410.15),
('CRUDEOIL', 'MCX', '2015-05-18', 3868, 3766),
('GOLD', 'MCX', '2015-05-18', 27685, 27505),
('GOLDM', 'MCX', '2015-05-18', 27685, 27505),
('LEAD', 'MCX', '2015-05-18', 126.8, 124.5),
('NATURGAS', 'MCX', '2015-05-18', 194.5, 189.9),
('NICKEL', 'MCX', '2015-05-18', 889, 873.6),
('SILVER', 'MCX', '2015-05-18', 40574, 40050),
('SILVERM', 'MCX', '2015-05-18', 40574, 40050),
('ZINC', 'MCX', '2015-05-18', 147, 144.95),
('COPPER', 'MCX', '2015-05-19', 412.7, 401.55),
('CRUDEOIL', 'MCX', '2015-05-19', 3869, 3728),
('GOLD', 'MCX', '2015-05-19', 27580, 27258),
('GOLDM', 'MCX', '2015-05-19', 27580, 27258),
('LEAD', 'MCX', '2015-05-19', 125.35, 122.9),
('NATURGAS', 'MCX', '2015-05-19', 198.3, 187.8),
('NICKEL', 'MCX', '2015-05-19', 873.8, 826.3),
('SILVER', 'MCX', '2015-05-19', 40326, 38856),
('SILVERM', 'MCX', '2015-05-19', 40326, 38856),
('ZINC', 'MCX', '2015-05-19', 146, 142.35),
('CXCOPPER', 'COMEX', '2015-05-19', 292.3, 285),
('CXCRUDE', 'COMEX', '2015-05-19', 60.42, 58),
('CXCRUDEMINI', 'COMEX', '2015-05-19', 60.42, 58),
('CXEURO', 'COMEX', '2015-05-19', 1.1331, 1.1122),
('CXEUROHALF', 'COMEX', '2015-05-19', 1.1331, 1.1122),
('CXGOLD', 'COMEX', '2015-05-19', 1225.5, 1205.1),
('CXGOLDMINI', 'COMEX', '2015-05-19', 1225.5, 1205.1),
('CXSILVER', 'COMEX', '2015-05-19', 1773.5, 1687),
('CXSILVERMINI', 'COMEX', '2015-05-19', 1773.5, 1687),
('COPPER', 'MCX', '2015-05-20', 403.15, 399.7),
('CRUDEOIL', 'MCX', '2015-05-20', 3777, 3731),
('GOLD', 'MCX', '2015-05-20', 27364, 27186),
('GOLDM', 'MCX', '2015-05-20', 27364, 27186),
('LEAD', 'MCX', '2015-05-20', 123.5, 122.1),
('NATURGAS', 'MCX', '2015-05-20', 192.4, 187.7),
('NICKEL', 'MCX', '2015-05-20', 835, 817.1),
('SILVER', 'MCX', '2015-05-20', 39420, 38932),
('SILVERM', 'MCX', '2015-05-20', 39420, 38932),
('ZINC', 'MCX', '2015-05-20', 142.4, 140.8),
('CXCOPPER', 'COMEX', '2015-05-20', 285.65, 284.5),
('CXCRUDE', 'COMEX', '2015-05-20', 58.9, 58.1),
('CXCRUDEMINI', 'COMEX', '2015-05-20', 58.9, 58.1),
('CXEURO', 'COMEX', '2015-05-20', 1.1156, 1.1066),
('CXEUROHALF', 'COMEX', '2015-05-20', 1.1156, 1.1066),
('CXGOLD', 'COMEX', '2015-05-20', 1212.5, 1202.7),
('CXGOLDMINI', 'COMEX', '2015-05-20', 1212.5, 1202.7),
('CXSILVER', 'COMEX', '2015-05-20', 1719.5, 1693.5),
('CXSILVERMINI', 'COMEX', '2015-05-20', 1719.5, 1693.5),
('COPPER', 'MCX', '2015-05-21', 404.35, 401.25),
('CRUDEOIL', 'MCX', '2015-05-21', 3879, 3782),
('GOLD', 'MCX', '2015-05-21', 27318, 27089),
('GOLDM', 'MCX', '2015-05-21', 27318, 27089),
('LEAD', 'MCX', '2015-05-21', 125.75, 123.05),
('NATURGAS', 'MCX', '2015-05-21', 192.3, 186.1),
('NICKEL', 'MCX', '2015-05-21', 844.4, 823.1),
('SILVER', 'MCX', '2015-05-21', 39455, 38939),
('SILVERM', 'MCX', '2015-05-21', 39455, 38939),
('ZINC', 'MCX', '2015-05-21', 141.55, 138.55),
('COPPER', 'MCX', '2015-05-22', 403.6, 394.65),
('CRUDEOIL', 'MCX', '2015-05-22', 3883, 3793),
('GOLD', 'MCX', '2015-05-22', 27297, 27053),
('GOLDM', 'MCX', '2015-05-22', 27297, 27053),
('LEAD', 'MCX', '2015-05-22', 126.55, 123.1),
('NATURGAS', 'MCX', '2015-05-22', 188.4, 184.6),
('NICKEL', 'MCX', '2015-05-22', 830, 805.4),
('SILVER', 'MCX', '2015-05-22', 39538, 38773),
('SILVERM', 'MCX', '2015-05-22', 39538, 38773),
('ZINC', 'MCX', '2015-05-22', 140.65, 137.3),
('COPPER', 'MCX', '2015-05-26', 400.65, 394.2),
('CRUDEOIL', 'MCX', '2015-05-26', 3841, 3732),
('GOLD', 'MCX', '2015-05-26', 27195, 26881),
('GOLDM', 'MCX', '2015-05-26', 27195, 26881),
('LEAD', 'MCX', '2015-05-26', 124.65, 122.45),
('NATURGAS', 'MCX', '2015-05-26', 182.3, 177.8),
('NICKEL', 'MCX', '2015-05-26', 828, 803.6),
('SILVER', 'MCX', '2015-05-26', 39118, 38324),
('SILVERM', 'MCX', '2015-05-26', 39118, 38324),
('ZINC', 'MCX', '2015-05-26', 140.1, 138.35),
('CXCOPPER', 'COMEX', '2015-05-26', 283.45, 277.3),
('CXCRUDE', 'COMEX', '2015-05-26', 60.25, 58.08),
('CXCRUDEMINI', 'COMEX', '2015-05-26', 60.25, 58.08),
('CXEURO', 'COMEX', '2015-05-26', 1.1013, 1.0871),
('CXEUROHALF', 'COMEX', '2015-05-26', 1.1013, 1.0871),
('CXGOLD', 'COMEX', '2015-05-26', 1208.2, 1184.8),
('CXGOLDMINI', 'COMEX', '2015-05-26', 1208.2, 1184.8),
('CXSILVER', 'COMEX', '2015-05-26', 1718, 1664.5),
('CXSILVERMINI', 'COMEX', '2015-05-26', 1718, 1664.5),
('COPPER', 'MCX', '2015-05-25', 400.35, 394.85),
('CRUDEOIL', 'MCX', '2015-05-25', 3848, 3772),
('GOLD', 'MCX', '2015-05-25', 27175, 27082),
('GOLDM', 'MCX', '2015-05-25', 27175, 27082),
('LEAD', 'MCX', '2015-05-25', 123.25, 122.45),
('NATURGAS', 'MCX', '2015-05-25', 183.7, 178.3),
('NICKEL', 'MCX', '2015-05-25', 815, 806.6),
('SILVER', 'MCX', '2015-05-25', 39200, 38760),
('SILVERM', 'MCX', '2015-05-25', 39200, 38760),
('ZINC', 'MCX', '2015-05-25', 138.35, 137.4),
('CXCOPPER', 'COMEX', '2015-05-25', 283.45, 279.35),
('CXCRUDE', 'COMEX', '2015-05-25', 60.25, 59.1),
('CXCRUDEMINI', 'COMEX', '2015-05-25', 60.25, 59.1),
('CXEURO', 'COMEX', '2015-05-25', 1.1013, 1.0962),
('CXEUROHALF', 'COMEX', '2015-05-25', 1.1013, 1.0962),
('CXGOLD', 'COMEX', '2015-05-25', 1208.2, 1202.3),
('CXGOLDMINI', 'COMEX', '2015-05-25', 1208.2, 1202.3),
('CXSILVER', 'COMEX', '2015-05-25', 1718, 1700),
('CXSILVERMINI', 'COMEX', '2015-05-25', 1718, 1700),
('COPPER', 'MCX', '2015-05-27', 397, 393),
('CRUDEOIL', 'MCX', '2015-05-27', 3790, 3697),
('GOLD', 'MCX', '2015-05-27', 26975, 26835),
('GOLDM', 'MCX', '2015-05-27', 26975, 26835),
('LEAD', 'MCX', '2015-05-27', 124.45, 122.8),
('NATURGAS', 'MCX', '2015-05-27', 188, 181.8),
('NICKEL', 'MCX', '2015-05-27', 824.9, 807.6),
('SILVER', 'MCX', '2015-05-27', 38672, 38255),
('SILVERM', 'MCX', '2015-05-27', 38672, 38255),
('ZINC', 'MCX', '2015-05-27', 140.45, 138.4),
('COPPER', 'MCX', '2015-05-28', 394.5, 391.55),
('CRUDEOIL', 'MCX', '2015-05-28', 3721, 3625),
('GOLD', 'MCX', '2015-05-28', 26916, 26712),
('GOLDM', 'MCX', '2015-05-28', 26916, 26712),
('LEAD', 'MCX', '2015-05-28', 126.4, 123.65),
('NATURGAS', 'MCX', '2015-05-28', 183, 174.3),
('NICKEL', 'MCX', '2015-05-28', 829, 812.5),
('SILVER', 'MCX', '2015-05-28', 38450, 38026),
('SILVERM', 'MCX', '2015-05-28', 38450, 38026),
('ZINC', 'MCX', '2015-05-28', 142.5, 140);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `logId` int(11) NOT NULL,
  `tradeId` int(11) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logType` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL,
  `newExpName` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderscx`
--

CREATE TABLE `orderscx` (
  `orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL,
  `otherExpName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpMode` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL,
  `key` varchar(30) DEFAULT NULL,
  `value` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradecolor`
--

CREATE TABLE `tradecolor` (
  `id` int(11) NOT NULL,
  `tradetype` varchar(55) DEFAULT NULL,
  `sodatype` varchar(55) DEFAULT NULL,
  `color` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tradecolor`
--

INSERT INTO `tradecolor` (`id`, `tradetype`, `sodatype`, `color`) VALUES
(1, 'MCX', 'Buy', '231EFF'),
(2, 'MCX', 'Sell', 'FF071E'),
(3, 'COMEX', 'Buy', '47FFC6'),
(4, 'COMEX', 'Sell', 'F7FF4A');

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL,
  `pcname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtcx`
--

CREATE TABLE `tradetxtcx` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `vendor` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL,
  `pcname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` enum('SUPER_ADMIN','READ_ONLY','ENTRY_USER') NOT NULL DEFAULT 'READ_ONLY'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'SUPER_ADMIN'),
(2, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'READ_ONLY'),
(4, 'abc', '900150983cd24fb0d6963f7d28e17f72', 'SUPER_ADMIN'),
(5, 'trade', '21e119daf697467b14b076f2d5819aec', 'ENTRY_USER');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE `user_access` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `page` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE `user_type` (
  `id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `type`) VALUES
(1, 'ADMIN'),
(2, 'READ_ONLY'),
(3, 'ENTRY_USER');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortemp`
--

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL,
  `userId` varchar(10) DEFAULT NULL,
  `memberId` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxstanding`
--

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(10) DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankmaster`
--
ALTER TABLE `bankmaster`
  ADD PRIMARY KEY (`bankId`);

--
-- Indexes for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  ADD PRIMARY KEY (`bhavcopyid`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`cashFlowId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`exchangeId`);

--
-- Indexes for table `expensemaster`
--
ALTER TABLE `expensemaster`
  ADD PRIMARY KEY (`expensemasterId`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`generalId`);

--
-- Indexes for table `grouplist`
--
ALTER TABLE `grouplist`
  ADD PRIMARY KEY (`groupId`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  ADD PRIMARY KEY (`newExpMasterId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `orderscx`
--
ALTER TABLE `orderscx`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `otherexp`
--
ALTER TABLE `otherexp`
  ADD PRIMARY KEY (`otherexpId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settingsId`);

--
-- Indexes for table `standing`
--
ALTER TABLE `standing`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `tradecolor`
--
ALTER TABLE `tradecolor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tradetxt`
--
ALTER TABLE `tradetxt`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type` (`user_type`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);

--
-- Indexes for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `vendortemp`
--
ALTER TABLE `vendortemp`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendortrades`
--
ALTER TABLE `vendortrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `zcxitem`
--
ALTER TABLE `zcxitem`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `zcxmember`
--
ALTER TABLE `zcxmember`
  ADD PRIMARY KEY (`zCxMemberId`);

--
-- Indexes for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankmaster`
--
ALTER TABLE `bankmaster`
  MODIFY `bankId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  MODIFY `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `cashFlowId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `clientId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `exchangeId` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expensemaster`
--
ALTER TABLE `expensemaster`
  MODIFY `expensemasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `generalId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grouplist`
--
ALTER TABLE `grouplist`
  MODIFY `groupId` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  MODIFY `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderscx`
--
ALTER TABLE `orderscx`
  MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otherexp`
--
ALTER TABLE `otherexp`
  MODIFY `otherexpId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settingsId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `standing`
--
ALTER TABLE `standing`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradecolor`
--
ALTER TABLE `tradecolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tradetxt`
--
ALTER TABLE `tradetxt`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendorId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortemp`
--
ALTER TABLE `vendortemp`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortrades`
--
ALTER TABLE `vendortrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxmember`
--
ALTER TABLE `zcxmember`
  MODIFY `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
