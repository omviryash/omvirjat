-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 10:09 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `omvirjat`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL,
  `bankName` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone1` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone2` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL,
  `exchange` varchar(30) NOT NULL DEFAULT '',
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) NOT NULL DEFAULT '',
  `marketType` varchar(15) NOT NULL DEFAULT '',
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) NOT NULL DEFAULT '',
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) NOT NULL DEFAULT '',
  `scriptGroup` varchar(5) NOT NULL DEFAULT '',
  `scriptType` varchar(5) NOT NULL DEFAULT '',
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL DEFAULT '',
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) NOT NULL DEFAULT '',
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) NOT NULL DEFAULT '',
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL,
  `clientId` int(10) DEFAULT '0',
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `dwStatus` char(2) DEFAULT '',
  `dwAmount` float DEFAULT '0',
  `plStatus` char(2) DEFAULT '',
  `plAmount` float DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) DEFAULT NULL,
  `transMode` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tradeRefNo` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `clientBroker` int(2) DEFAULT '0',
  `groupName` varchar(100) DEFAULT '0',
  `profitBankRate` double DEFAULT NULL,
  `lossBankRate` double DEFAULT NULL,
  `commission` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(10) DEFAULT NULL,
  `exchange` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `exchangeId` int(6) UNSIGNED NOT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES
(1, 'MCX', 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL,
  `expenseName` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expensemaster`
--

INSERT INTO `expensemaster` (`expensemasterId`, `expenseName`) VALUES
(1, 'ggg');

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `exchange` varchar(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expiry`
--

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES
(1, 'GOLD', '05DEC2012', NULL),
(3, 'CXGOLD', '10DEC2012', NULL),
(4, 'CRUDEOIL', '15NOV2012', NULL),
(5, 'COPPER', '30NOV2012', NULL),
(6, 'GOLDM', '05DEC2012', NULL),
(7, 'LEAD', '31OCT2012', NULL),
(8, 'ZINC', '31OCT2012', NULL),
(9, 'NICKEL', '31OCT2012', NULL),
(10, 'NATURGAS', '26OCT2012', NULL),
(11, 'CXSILVER', '10DEC2012', NULL),
(12, 'CXCRUDE', '31NOV2012', NULL),
(13, 'CXEURO', '10DEC2012', NULL),
(14, 'CXGOLDMINI', '10DEC2012', NULL),
(15, 'CXCOPPER', '10DEC2012', NULL),
(20, 'SILVER', '05DEC2012', NULL),
(24, 'MINICRUDECX', '30NOV2012', NULL),
(21, 'SILVERM', '05DEC2012', NULL),
(25, 'SILVERMINICX', '10DEC2012', NULL),
(26, 'GOLD', '05DEC2012', 'MCX'),
(27, 'CXGOLD', '10DEC2012', 'COMEX'),
(28, 'MINICRUDECX', '30NOV2012', 'COMEX'),
(29, 'CXSILVER', '10DEC2012', 'COMEX'),
(30, 'CXCRUDE', '30NOV2012', 'COMEX'),
(31, 'CXEURO', '30NOV2012', 'COMEX'),
(32, 'CXGOLDMINI', '10DEC2012', 'COMEX'),
(33, 'CXCOPPER', '30NOV2012', 'COMEX'),
(34, 'SILVERMINICX', '10DEC2012', 'COMEX'),
(35, 'SILVER', '05DEC2012', 'MCX'),
(36, 'CRUDEOIL', '15NOV2012', 'MCX'),
(37, 'COPPER', '30NOV2012', 'MCX'),
(38, 'GOLDM', '05DEC2012', 'MCX'),
(39, 'LEAD', '30NOV2012', 'MCX'),
(40, 'ZINC', '30NOV2012', 'MCX'),
(41, 'NICKEL', '30NOV2012', 'MCX'),
(43, 'NATURGAS', '27NOV2012', 'MCX'),
(44, 'SILVERM', '30NOV2012', 'MCX');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grouplist`
--

CREATE TABLE `grouplist` (
  `groupId` int(6) UNSIGNED NOT NULL,
  `groupName` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grouplist`
--

INSERT INTO `grouplist` (`groupId`, `groupName`) VALUES
(2, '31'),
(3, '72 V');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `itemId` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `itemShort` varchar(50) DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchange` varchar(20) DEFAULT 'MCX',
  `high` double NOT NULL DEFAULT '0',
  `low` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchange`, `high`, `low`) VALUES
('GOLD', 'GOLD', 'GOLD', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('SILVER', 'SILVER', 'SILVER', NULL, NULL, 500, 30, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('CXGOLD', 'CXGOLD', 'CXGOLD', NULL, NULL, 40, 1, 1, 100, 1000, 2000, NULL, 'COMEX', 0, 0),
('CRUDEOIL', 'CRUDEOIL', 'CRUDEOIL', NULL, NULL, 500, 100, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('COPPER', 'COPPER', 'COPPER', NULL, NULL, 0, 1000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('GOLDM', 'GOLDM', 'GOLDM', NULL, NULL, 0, 10, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('MINICRUDECX', 'MINICRUDECX', 'MINICRUDECX', NULL, NULL, 40, 1, NULL, 500, 0, 0, NULL, 'COMEX', 0, 0),
('LEAD', 'LEAD', 'LEAD', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('ZINC', 'ZINC', 'ZINC', NULL, NULL, 500, 5000, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('NICKEL', 'NICKEL', 'NICKEL', NULL, NULL, 500, 250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('NATURGAS', 'NATURGAS', 'NATURGAS', NULL, NULL, 500, 1250, NULL, 1, 0, 0, NULL, 'MCX', 0, 0),
('CXSILVER', 'CXSILVER', 'CXSILVER', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0),
('CXCRUDE', 'CXCRUDE', 'CXCRUDE', NULL, NULL, 40, 1, 1, 1000, 0, 0, NULL, 'COMEX', 0, 0),
('CXEURO', 'CXEURO', 'CXEURO', NULL, NULL, 40, 1, 1, 125000, 0, 0, NULL, 'COMEX', 0, 0),
('CXGOLDMINI', 'CXGOLDMINI', 'CXGOLDMINI', NULL, NULL, 40, 1, 1, 50, 0, 0, NULL, 'COMEX', 0, 0),
('CXCOPPER', 'CXCOPPER', 'CXCOPPER', NULL, NULL, 30, 1, 1, 250, 0, 0, NULL, 'COMEX', 0, 0),
('SILVERMINICX', 'SILVERMINICX', 'SILVERMINICX', NULL, NULL, 40, 1, NULL, 25, 0, 0, NULL, 'COMEX', 0, 0),
('SILVERM', 'SILVERM', 'SILVERM', NULL, NULL, 100, 5, NULL, 1, 0, 0, NULL, 'MCX', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `itemdatewise`
--

CREATE TABLE `itemdatewise` (
  `itemId` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itemDate` date NOT NULL DEFAULT '0000-00-00',
  `high` double NOT NULL DEFAULT '0',
  `low` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `logId` int(11) NOT NULL,
  `tradeId` int(11) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logType` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL,
  `newExpName` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderscx`
--

CREATE TABLE `orderscx` (
  `orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL,
  `otherExpName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpMode` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL,
  `key` varchar(30) DEFAULT NULL,
  `value` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradecolor`
--

CREATE TABLE `tradecolor` (
  `id` int(11) NOT NULL,
  `tradetype` varchar(55) DEFAULT NULL,
  `sodatype` varchar(55) DEFAULT NULL,
  `color` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tradecolor`
--

INSERT INTO `tradecolor` (`id`, `tradetype`, `sodatype`, `color`) VALUES
(1, 'MCX', 'Buy', '5EBFFF'),
(2, 'MCX', 'Sell', 'FF2643'),
(3, 'COMEX', 'Buy', '1F26FF'),
(4, 'COMEX', 'Sell', 'FF2643');

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtcx`
--

CREATE TABLE `tradetxtcx` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `vendor` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'admin', '6ce4c58528a4f3cea94b4cf7b4e742a5');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortemp`
--

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL,
  `userId` varchar(10) DEFAULT NULL,
  `memberId` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxstanding`
--

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(10) DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankmaster`
--
ALTER TABLE `bankmaster`
  ADD PRIMARY KEY (`bankId`);

--
-- Indexes for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  ADD PRIMARY KEY (`bhavcopyid`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`cashFlowId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`exchangeId`);

--
-- Indexes for table `expensemaster`
--
ALTER TABLE `expensemaster`
  ADD PRIMARY KEY (`expensemasterId`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`generalId`);

--
-- Indexes for table `grouplist`
--
ALTER TABLE `grouplist`
  ADD PRIMARY KEY (`groupId`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  ADD PRIMARY KEY (`newExpMasterId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `orderscx`
--
ALTER TABLE `orderscx`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `otherexp`
--
ALTER TABLE `otherexp`
  ADD PRIMARY KEY (`otherexpId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settingsId`);

--
-- Indexes for table `standing`
--
ALTER TABLE `standing`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `tradecolor`
--
ALTER TABLE `tradecolor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tradetxt`
--
ALTER TABLE `tradetxt`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);

--
-- Indexes for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `vendortemp`
--
ALTER TABLE `vendortemp`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendortrades`
--
ALTER TABLE `vendortrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `zcxitem`
--
ALTER TABLE `zcxitem`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `zcxmember`
--
ALTER TABLE `zcxmember`
  ADD PRIMARY KEY (`zCxMemberId`);

--
-- Indexes for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankmaster`
--
ALTER TABLE `bankmaster`
  MODIFY `bankId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  MODIFY `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `cashFlowId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `clientId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=541;
--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `exchangeId` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expensemaster`
--
ALTER TABLE `expensemaster`
  MODIFY `expensemasterId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `generalId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `grouplist`
--
ALTER TABLE `grouplist`
  MODIFY `groupId` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  MODIFY `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderscx`
--
ALTER TABLE `orderscx`
  MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otherexp`
--
ALTER TABLE `otherexp`
  MODIFY `otherexpId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settingsId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `standing`
--
ALTER TABLE `standing`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradecolor`
--
ALTER TABLE `tradecolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tradetxt`
--
ALTER TABLE `tradetxt`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendorId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vendortemp`
--
ALTER TABLE `vendortemp`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vendortrades`
--
ALTER TABLE `vendortrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxmember`
--
ALTER TABLE `zcxmember`
  MODIFY `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
