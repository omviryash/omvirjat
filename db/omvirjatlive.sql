-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2015 at 02:43 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `omvirjatlive`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE IF NOT EXISTS `bankmaster` (
`bankId` int(6) NOT NULL,
  `bankName` varchar(60) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone1` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `phone2` varchar(12) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE IF NOT EXISTS `bhavcopy` (
`bhavcopyid` int(10) NOT NULL,
  `exchange` varchar(30) NOT NULL DEFAULT '',
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) NOT NULL DEFAULT '',
  `marketType` varchar(15) NOT NULL DEFAULT '',
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) NOT NULL DEFAULT '',
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) NOT NULL DEFAULT '',
  `scriptGroup` varchar(5) NOT NULL DEFAULT '',
  `scriptType` varchar(5) NOT NULL DEFAULT '',
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) NOT NULL DEFAULT '',
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) NOT NULL DEFAULT '',
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) NOT NULL DEFAULT '',
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE IF NOT EXISTS `cashflow` (
`cashFlowId` int(6) NOT NULL,
  `clientId` int(10) DEFAULT '0',
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `dwStatus` char(2) DEFAULT '',
  `dwAmount` float DEFAULT '0',
  `plStatus` char(2) DEFAULT '',
  `plAmount` float DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) DEFAULT NULL,
  `transMode` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `tradeRefNo` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`clientId` int(6) NOT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(10) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `clientBroker` int(2) DEFAULT '0',
  `groupName` varchar(100) DEFAULT '0',
  `profitBankRate` double DEFAULT NULL,
  `lossBankRate` double DEFAULT NULL,
  `commission` double DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE IF NOT EXISTS `clientbrok` (
`clientBrokId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(10) DEFAULT NULL,
  `exchange` varchar(30) CHARACTER SET utf8 DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE IF NOT EXISTS `exchange` (
`exchangeId` int(6) unsigned NOT NULL,
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE IF NOT EXISTS `expensemaster` (
`expensemasterId` int(6) NOT NULL,
  `expenseName` varchar(50) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE IF NOT EXISTS `expiry` (
`expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `exchange` varchar(10) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE IF NOT EXISTS `general` (
`generalId` int(6) NOT NULL,
  `filePath` varchar(250) DEFAULT NULL,
  `fileName` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grouplist`
--

CREATE TABLE IF NOT EXISTS `grouplist` (
`groupId` int(6) unsigned NOT NULL,
  `groupName` varchar(100) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
  `itemId` varchar(50) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `itemShort` varchar(50) DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchange` varchar(20) DEFAULT 'MCX',
  `high` double NOT NULL DEFAULT '0',
  `low` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `itemdatewise`
--

CREATE TABLE IF NOT EXISTS `itemdatewise` (
  `itemId` varchar(250) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `exchange` varchar(20) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `itemDate` date NOT NULL DEFAULT '0000-00-00',
  `high` double NOT NULL DEFAULT '0',
  `low` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
`logId` int(11) NOT NULL,
  `tradeId` int(11) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logType` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE IF NOT EXISTS `newexpmaster` (
`newExpMasterId` int(6) NOT NULL,
  `newExpName` varchar(30) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
`orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderscx`
--

CREATE TABLE IF NOT EXISTS `orderscx` (
`orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) DEFAULT NULL,
  `orderNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `orderType` varchar(30) NOT NULL DEFAULT '',
  `orderValidity` varchar(15) NOT NULL DEFAULT '',
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) NOT NULL DEFAULT '',
  `triggerPrice` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE IF NOT EXISTS `otherexp` (
`otherexpId` int(6) NOT NULL,
  `otherExpName` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) CHARACTER SET utf8 DEFAULT NULL,
  `otherExpMode` varchar(60) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`settingsId` int(6) NOT NULL,
  `key` varchar(30) DEFAULT NULL,
  `value` varchar(60) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE IF NOT EXISTS `standing` (
`standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE IF NOT EXISTS `tradetxt` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtcx`
--

CREATE TABLE IF NOT EXISTS `tradetxtcx` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) CHARACTER SET utf8 DEFAULT NULL,
  `vendor` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `highLowConf` char(1) CHARACTER SET utf8 DEFAULT '0',
  `ipaddress` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE IF NOT EXISTS `tradetxtv1` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `type` enum('SUPER_ADMIN','READ_ONLY','ENTRY_USER') NOT NULL DEFAULT 'READ_ONLY'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `type`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'SUPER_ADMIN'),
(2, 'test', 'e10adc3949ba59abbe56e057f20f883e', 'READ_ONLY'),
(4, 'abc', '900150983cd24fb0d6963f7d28e17f72', 'SUPER_ADMIN'),
(5, 'trade', '21e119daf697467b14b076f2d5819aec', 'ENTRY_USER');

-- --------------------------------------------------------

--
-- Table structure for table `user_access`
--

CREATE TABLE IF NOT EXISTS `user_access` (
`id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `page` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
`id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `type`) VALUES
(1, 'ADMIN'),
(2, 'READ_ONLY'),
(3, 'ENTRY_USER');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE IF NOT EXISTS `vendor` (
`vendorId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `address` text,
  `phone` varchar(30) DEFAULT NULL,
  `mobile` varchar(22) DEFAULT NULL,
  `fax` varchar(30) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE IF NOT EXISTS `vendorbrok` (
`clientBrokId` int(6) NOT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortemp`
--

CREATE TABLE IF NOT EXISTS `vendortemp` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE IF NOT EXISTS `vendortrades` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `userRemarks` varchar(50) DEFAULT NULL,
  `ownClient` varchar(50) DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE IF NOT EXISTS `zcxexpiry` (
`expiryId` int(6) NOT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE IF NOT EXISTS `zcxitem` (
  `itemId` varchar(10) NOT NULL DEFAULT '',
  `item` varchar(50) DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE IF NOT EXISTS `zcxmember` (
`zCxMemberId` int(6) NOT NULL,
  `userId` varchar(10) DEFAULT NULL,
  `memberId` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxstanding`
--

CREATE TABLE IF NOT EXISTS `zcxstanding` (
`standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE IF NOT EXISTS `zcxtrades` (
`tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) DEFAULT NULL,
  `middleName` varchar(35) DEFAULT NULL,
  `lastName` varchar(35) DEFAULT NULL,
  `buySell` varchar(10) DEFAULT NULL,
  `itemId` varchar(50) DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) DEFAULT NULL,
  `tradeNote` varchar(200) DEFAULT NULL,
  `expiryDate` varchar(20) DEFAULT NULL,
  `vendor` varchar(10) DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankmaster`
--
ALTER TABLE `bankmaster`
 ADD PRIMARY KEY (`bankId`);

--
-- Indexes for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
 ADD PRIMARY KEY (`bhavcopyid`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
 ADD PRIMARY KEY (`cashFlowId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
 ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
 ADD PRIMARY KEY (`exchangeId`);

--
-- Indexes for table `expensemaster`
--
ALTER TABLE `expensemaster`
 ADD PRIMARY KEY (`expensemasterId`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
 ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
 ADD PRIMARY KEY (`generalId`);

--
-- Indexes for table `grouplist`
--
ALTER TABLE `grouplist`
 ADD PRIMARY KEY (`groupId`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`logId`);

--
-- Indexes for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
 ADD PRIMARY KEY (`newExpMasterId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
 ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `orderscx`
--
ALTER TABLE `orderscx`
 ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `otherexp`
--
ALTER TABLE `otherexp`
 ADD PRIMARY KEY (`otherexpId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`settingsId`);

--
-- Indexes for table `standing`
--
ALTER TABLE `standing`
 ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `tradetxt`
--
ALTER TABLE `tradetxt`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access`
--
ALTER TABLE `user_access`
 ADD PRIMARY KEY (`id`), ADD KEY `user_type` (`user_type`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
 ADD PRIMARY KEY (`vendorId`);

--
-- Indexes for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
 ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `vendortemp`
--
ALTER TABLE `vendortemp`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendortrades`
--
ALTER TABLE `vendortrades`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
 ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `zcxitem`
--
ALTER TABLE `zcxitem`
 ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `zcxmember`
--
ALTER TABLE `zcxmember`
 ADD PRIMARY KEY (`zCxMemberId`);

--
-- Indexes for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
 ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
 ADD PRIMARY KEY (`tradeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankmaster`
--
ALTER TABLE `bankmaster`
MODIFY `bankId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
MODIFY `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
MODIFY `cashFlowId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
MODIFY `clientId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
MODIFY `exchangeId` int(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expensemaster`
--
ALTER TABLE `expensemaster`
MODIFY `expensemasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
MODIFY `generalId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `grouplist`
--
ALTER TABLE `grouplist`
MODIFY `groupId` int(6) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
MODIFY `logId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
MODIFY `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orderscx`
--
ALTER TABLE `orderscx`
MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otherexp`
--
ALTER TABLE `otherexp`
MODIFY `otherexpId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `settingsId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `standing`
--
ALTER TABLE `standing`
MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxt`
--
ALTER TABLE `tradetxt`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtcx`
--
ALTER TABLE `tradetxtcx`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_access`
--
ALTER TABLE `user_access`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
MODIFY `vendorId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortemp`
--
ALTER TABLE `vendortemp`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortrades`
--
ALTER TABLE `vendortrades`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxmember`
--
ALTER TABLE `zcxmember`
MODIFY `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
