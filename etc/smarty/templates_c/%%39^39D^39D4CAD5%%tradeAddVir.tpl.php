<?php /* Smarty version 2.6.10, created on 2016-03-14 16:33:26
         compiled from tradeAddVir.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', 'tradeAddVir.tpl', 315, false),array('function', 'html_options', 'tradeAddVir.tpl', 334, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<title>!! JAT !!</title>
<link rel="stylesheet" type="text/css" href="./css/main.css" />
<link rel="stylesheet" type="text/css" href="./css/style.css" />
<script type="text/javascript" src="./js/jquery.min.js"></script>
<link rel="stylesheet" href="css/style1.css" media="screen">
<?php echo '	
<style type="text/css">
html, body { margin: 0;	padding: 0; font-family:arial; font-size:14px; }
ul.menu { margin: 0px auto 0 auto; }

input:focus
{
	background-color:yellow;
}
select:focus
{
	background-color:yellow;
}
</style>
<script type="text/javascript">
  $(document).ready(function()
  {
  	selectExpiry();
  	$("#itemId").focus();
  	//selectExpiry($("#item").val());
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 109)
      {
		if($(\'#exch\').val() == \'mcx\'){
			$("body").css("background-color","red");
			$(".tbl td").css("background-color","red");
		}
		else{
			$("body").css("background-color","#FE79FE");
			$(".tbl td").css("background-color","#FE79FE");
		}
		
        $("#buySell").val("Sell");
        return false;
      }
       if(code == 107)
      {
		if($(\'#exch\').val() == \'mcx\'){
			$("body").css("background-color","blue");
			$(".tbl td").css("background-color","blue");
		}
		else{
			$("body").css("background-color","#419FFC");
			$(".tbl td").css("background-color","#419FFC");
		}
		 
        $("#buySell").val("Buy");
        return false;
      }
     /* if(code == 13)
      {
      	var didConfirm = confirm("Are you sure?");
        if (didConfirm == true) 
        {
         	//callAjax();
			//return false;
        }
		else
		return false;
      }*/
      
    });
	
	
	$("#price").keyup(function(e){
		
		var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// Uparrow
      if(code == 38)
      {
      	var price      = parseFloat($(this).val());
      	
      	$(this).val(price+1)
      } 
       if(code == 40)
      {
      	var price      = parseFloat($(this).val());
      	
      	$(this).val(price-1)
      }
       
		
	});
	
 
	
	
	
	$("#quantity").keyup(function(e){
		
		var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// Uparrow
      if(code == 38)
      {
      	var qty      = $(\'.min\').val();
      	var minQty   = $(\'#hiddenQty\').val();
      	var finalQty = parseFloat(qty) + parseFloat(minQty);
      	$(\'.min\').val(finalQty);
      } 
       if(code == 40)
      {
      	var qty      = $(\'.min\').val();
      	var minQty   = $(\'#hiddenQty\').val();
      	var finalQty = parseFloat(qty) - parseFloat(minQty);
      	$(\'.minMinus\').val(finalQty);
      }
       if(code == 33)
      {
      	var qty      = $(\'.min\').val();
      	var minQty   = $(\'#hiddenQty\').val();
      	if($(\'#hiddenQty\').val() == $(\'.min\').val())
      	{
      	  var finalQty = parseFloat(minQty) * 10;
      	}
      	else
      	{
      		var finalQty = parseFloat($(\'.min\').val()) + parseFloat(parseFloat(minQty) * 10);
      	}
      	$(\'.minTenPlus\').val(finalQty);
      }
       
      if(code == 34)
      {
      	var qty      = $(\'.min\').val();
      	var minQty   = $(\'#hiddenQty\').val();
      	if($(\'#hiddenQty\').val() == $(\'.min\').val())
      	{
      	  var finalQty = parseFloat(minQty) * 10;
      	}
      	else
      	{
      		var finalQty = parseFloat($(\'.min\').val()) - parseFloat(parseFloat(minQty) * 10);
      	}
      	$(\'.minTenMinus\').val(finalQty);
      }
		
	})
	
  });
  
  
 
 
  
	function selectExch(){ 
	//alert($(\'#exch\').val());
	  	if($(\'#exch\').val() == \'mcx\')
		{
			if($("#buySell").val() == \'Buy\'){
				$("body").css("background-color","blue");
				$(".tbl td").css("background-color","blue");
			}
			else{
				$("body").css("background-color","red");
				$(".tbl td").css("background-color","red");
			}
			
			$(\'#cmxitemId\').css(\'display\',\'none\');
			$(\'#itemId\').css(\'display\',\'block\');
			
			selectExpiry();
		}
		else
		{
			
			if($("#buySell").val() == \'Buy\'){
				$("body").css("background-color","#419FFC");
				$(".tbl td").css("background-color","#419FFC");
			}
			else{
				$("body").css("background-color","#FE79FE");
				$(".tbl td").css("background-color","#FE79FE");
			}
			
			$(\'#itemId\').css(\'display\',\'none\');
			$(\'#cmxitemId\').css(\'display\',\'block\');
			
			selectExpiry_cmx();
		}
	}
  
	function selectExpiry()
  {
  	var item = $(\'#itemId\').val();
  	//var exchange = $(\'#exchange\').val();
	var exchange = \'mcx\';

    $.ajax({
	    url:\'expirySelectAjax.php\',
	    data:{ itemVar: item ,exchange : exchange},
	    dataType:\'HTML\',
	    type:\'POST\',
	    success:function(msg)
	    {
	      $(\'#itemExpireDiv\').html(msg);
	      $(\'#quantity\').val($(\'#hiddenQty\').val());
	      $(\'#minHidden\').val($(\'#hiddenQty\').val());
	    }
    });
  }
  
  function selectExpiry_cmx()
  { 
  	var item = $(\'#cmxitemId\').val();
  	//var exchange = $(\'#exchange\').val();
	var exchange = \'comex\';

    $.ajax({
	    url:\'expirySelectAjax.php\',
	    data:{ itemVar: item ,exchange : exchange},
	    dataType:\'HTML\',
	    type:\'POST\',
	    success:function(msg)
	    { 
	      $(\'#itemExpireDiv\').html(msg);
	      $(\'#quantity\').val($(\'#hiddenQty\').val());
	      $(\'#minHidden\').val($(\'#hiddenQty\').val());
	    }
    });
  }
  
  function callAjax()
  {
    var date         = $(\'#edd\').val() +"-"+ $(\'#edm\').val() +"-"+ $(\'#edy\').val();
    var item         = $(\'#itemId\').val();
    var itemExpire   = $(\'#itemExpire\').val();
    var price        = $(\'#price\').val();
    var client1      = $(\'#client1\').val();
    var quantity     = $(\'#quantity\').val();
    var buySell      = $(\'#buySell\').val();
	'; ?>

    
    var datastring = 'date='+date +'&item='+item +'&itemExpire='+itemExpire +'&price='+price +'&client1='+client1
    +'&quantity='+quantity+'&buySell='+buySell+'&exchange='+<?php echo $this->_tpl_vars['exchange']; ?>
;
	<?php echo '

    $.ajax({
      type: "GET",
      url: "ajaxResponce.php",
      data: datastring,
      success:function(data)
      {
        $(\'#dataDisplay\').html(data);
      	$("#itemId").focus();
      }
    });
  }
  </script>
   <style type="text/css" rel="stylesheet">
  	body
  	{
	'; ?>

		<?php if ($this->_tpl_vars['exch'] == 'mcx' && $this->_tpl_vars['buySell'] == 'Buy'): ?>
			background-color:blue;
		<?php elseif ($this->_tpl_vars['exch'] == 'mcx' && $this->_tpl_vars['buySell'] == 'Sell'): ?>
			background-color:red;
		<?php elseif ($this->_tpl_vars['exch'] == 'cmx' && $this->_tpl_vars['buySell'] == 'Buy'): ?>
			background-color:#419FFC;	
		<?php elseif ($this->_tpl_vars['exch'] == 'cmx' && $this->_tpl_vars['buySell'] == 'Sell'): ?>
			background-color:#FE79FE;
		<?php else: ?>	
			background-color:blue;
		<?php endif; ?>
			
	<?php echo '
  	}
	.tbl td{
		'; ?>

		color:#fff;font-size:18px;font-weight:bold;
		<?php echo '
	}
	.tbl a{
		color:#fff;
	}
    select:focus,
    input:focus
    {
        border: solid 2px #F7847E;
    }
	input,select
	{
		font-size:18px;
	}
  </style>
	'; ?>

</head>
<body>
  <br/><br/><br/>
  <form name="dataForm" method="POST" onsubmit="return confirm('Are you sure?')">
  <input type="hidden" name="buySell" id="buySell" value="<?php echo $this->_tpl_vars['buySell']; ?>
"/>
  	<input type="hidden" name="exchange" id="exchange" value="<?php echo $this->_tpl_vars['exchange']; ?>
"/>
    <input type="hidden" name="hid_exch" id="hid_exch" value="<?php echo $this->_tpl_vars['exch']; ?>
"/>
  	<input type="hidden" name="tradeId" id="tradeId" value="<?php echo $this->_tpl_vars['tradeId']; ?>
" />
  	<input type="hidden" name="isedit"  value="<?php echo $this->_tpl_vars['isedit']; ?>
" />
    <table class="tbl" border="1" align="center" style="" width="100%" height="60">
      <tr>
        <td>
          Date
        </td>
        <td nowrap>
        	<lable id="dataDate" name="dataDate">
            <?php echo smarty_function_html_select_date(array('prefix' => 'curDate','day_id' => 'edd','month_id' => 'edm','year_id' => 'edy','start_year' => "-1",'end_year' => "+2",'field_order' => 'DMY','month_format' => "%m",'day_value_format' => "%02d"), $this);?>

          </lable>
        </td>
        <td>
          Exchange
        </td>
        <td>
          <select name="exch" id="exch" onchange="selectExch();">
            <option <?php if ($this->_tpl_vars['updateArray']['exch'] == 'mcx'): ?>selected='selected'<?php endif; ?> value="mcx">MCX</option>
            <option <?php if ($this->_tpl_vars['updateArray']['exch'] == 'cmx'): ?>selected='selected'<?php endif; ?> value="cmx">CMX</option>
          </select>
          
         
        </td>
        <td>
          Item
        </td>
        <td>
          <select name="itemId" class="itemddl" id="itemId" onchange="selectExpiry();">
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['item']['itemId'],'output' => $this->_tpl_vars['item']['itemId']), $this);?>

          </select>
          <select name="cmxitemId" class="itemddl" id="cmxitemId" onchange="selectExpiry_cmx();" style="display:none">
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['cmxitem']['itemId'],'output' => $this->_tpl_vars['cmxitem']['itemId']), $this);?>

          </select>
        </td>
        <td name="itemExpire">
          Expriy
        </td>
        <td>
        	<div id="itemExpireDiv">
	        </div>
        </td>
        <td>
          Qty
        </td>
        <td>
          <input type="text" size="5" name="quantity" id="quantity" class="min minMinus minTenPlus minTenMinus" value="<?php echo $this->_tpl_vars['updateArray']['qty']; ?>
">
        </td>
        <td>
          Price
        </td>
        <td>
          <input type="text"  size="5" name="price" id="price" value="<?php echo $this->_tpl_vars['updateArray']['price']; ?>
">
        </td>
        <td>
          Client1
        </td>
        <td>
          <input type="text" size="8" name="client1" id="client1" value="<?php echo $this->_tpl_vars['updateArray']['clientId']; ?>
" >
        </td>
        <td>
          <input type="Submit" value=" Trade " name="btn" >
        </td>
      </tr>
    </table>
    <script type="text/javascript">
		 selectExch();
		 	//alert($('#exch').val());
         </script> 
  </form>
  <br/>
  <div id="dataDisplay">
    <table  class="tbl1" align="center" style="width:100%;background:#fff;color:#000;font-size:16px;text-align:center;font-weight:bold;">
    	<tr>
    	  <td><strong>Client1</strong></td>
    	  <td><strong>Item</strong></td>
          <td><strong>Price</strong></td>
           <td><strong>Qty</strong></td>
           <td><strong>Date</strong></td>
           <td><strong>Time</strong></td>
    	  <td><strong>Expriy</strong></td>
    	 
    	  
    	  
    	  <td align="center">Action</td>
    	</tr>
    	<?php unset($this->_sections['silTrd']);
$this->_sections['silTrd']['name'] = 'silTrd';
$this->_sections['silTrd']['loop'] = is_array($_loop=$this->_tpl_vars['treadeTxtArray']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['silTrd']['show'] = true;
$this->_sections['silTrd']['max'] = $this->_sections['silTrd']['loop'];
$this->_sections['silTrd']['step'] = 1;
$this->_sections['silTrd']['start'] = $this->_sections['silTrd']['step'] > 0 ? 0 : $this->_sections['silTrd']['loop']-1;
if ($this->_sections['silTrd']['show']) {
    $this->_sections['silTrd']['total'] = $this->_sections['silTrd']['loop'];
    if ($this->_sections['silTrd']['total'] == 0)
        $this->_sections['silTrd']['show'] = false;
} else
    $this->_sections['silTrd']['total'] = 0;
if ($this->_sections['silTrd']['show']):

            for ($this->_sections['silTrd']['index'] = $this->_sections['silTrd']['start'], $this->_sections['silTrd']['iteration'] = 1;
                 $this->_sections['silTrd']['iteration'] <= $this->_sections['silTrd']['total'];
                 $this->_sections['silTrd']['index'] += $this->_sections['silTrd']['step'], $this->_sections['silTrd']['iteration']++):
$this->_sections['silTrd']['rownum'] = $this->_sections['silTrd']['iteration'];
$this->_sections['silTrd']['index_prev'] = $this->_sections['silTrd']['index'] - $this->_sections['silTrd']['step'];
$this->_sections['silTrd']['index_next'] = $this->_sections['silTrd']['index'] + $this->_sections['silTrd']['step'];
$this->_sections['silTrd']['first']      = ($this->_sections['silTrd']['iteration'] == 1);
$this->_sections['silTrd']['last']       = ($this->_sections['silTrd']['iteration'] == $this->_sections['silTrd']['total']);
?>
      <tr style="color:<?php if (( $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['exch'] == 'mcx' && $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['buySell'] == 'Buy' )): ?>blue<?php elseif (( $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['exch'] == 'mcx' && $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['buySell'] == 'Sell' )): ?>red<?php elseif (( $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['exch'] == 'cmx' && $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['buySell'] == 'Buy' )): ?>#419FFC<?php else: ?>#FE79FE<?php endif; ?>">
      	<td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['clientId']; ?>
</td>
      	<td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['itemId']; ?>
</td>
        <td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['price']; ?>
</td>
        <td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['qty']; ?>
</td>
      	
      	<td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['tradeDate']; ?>
</td>
        <td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['tradeTime']; ?>
</td>
      	<td><?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['expiryDate']; ?>
</td>
      	
      	<td><?php if ($this->_tpl_vars['usertype'] == 'SUPER_ADMIN'): ?><a href="tradeAddVir.php?tradeId=<?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['tradeId']; ?>
&exchange=<?php echo $this->_tpl_vars['exchange']; ?>
&ex=<?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['exch']; ?>
">Edit</a><?php endif; ?></td>
      	<!--<td><a href="deleteTradeTxt.php?tradeId=<?php echo $this->_tpl_vars['treadeTxtArray'][$this->_sections['silTrd']['index']]['tradeId']; ?>
&exchange=<?php echo $this->_tpl_vars['exchange']; ?>
">Delete</a></td>-->
      </tr>
      <?php endfor; endif; ?>
    </table>
  </div>
</body>
</html>