<?php /* Smarty version 2.6.10, created on 2016-04-18 12:11:37
         compiled from clientTrades-vijay.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'clientTrades-vijay.tpl', 245, false),array('function', 'math', 'clientTrades-vijay.tpl', 381, false),)), $this); ?>
<HTML>
<HEAD>
<TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
<?php echo '
td {
	font-weight: BOLD
}
.lossStyle {
	color: red
}
.profitStyle {
	color: blue
}
</STYLE>
<script src="./js/jquery.min.js"></script>
<script type="text/javascript">
function confirmedCheckbox(myObj, objValues)
{
  if($(myObj).is(\':checked\') == 1)
    $(".confirmed"+objValues).attr(\'checked\', true);
  else
    $(".confirmed"+objValues).attr(\'checked\', false);
}  
/*function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "./clientTrades.php";
  else if($("#exchange").val() == "Comex")
    window.location.href = "./clientTradesComex.php";
}*/

$(document).ready(function(){

  if($("#hide_show").val()==\'Show List\'){
		$("#list").hide();
		$("#hide_show").val("Show List");
	}else{
		$("#list").show();
		$("#hide_show").val("Hide List");
	}
	$("#hide_show").click(function()
	{
		if($("#hide_show").val() == "Show List")
		{
			$("#list").show();
			$("#hide_show").val("Hide List");
		}
		else
		{
			$("#list").hide();
			$("#hide_show").val("Show List");
		}
	});
  
  $("#storeLocally").click(function() {
    // alert("Hello");
    var calculations = [];
    $(".price").each(function(i, ele) {
      calculations.push({
        \'item\': $(ele).data(\'item\'),
        \'expiry\': $(ele).data(\'expiry\'),
        \'value\': $(ele).val()
      });
    }).promise().done(function() {
      window.sessionStorage.setItem(\'calculations\', JSON.stringify(calculations));
    });
  });


  $(".price").change(function(){
    var self = this;
    var item = $(self).data(\'item\');
    var expiry = $(self).data(\'expiry\');
    var list = $(".data-holder[data-item="+item+"][data-expiry="+expiry+"]");
    var clients = {};
    $(list).each(function(i, row) {
      var qty = $(row).data("qty");
      var price = $(row).data(\'price\');
      var client = $(row).data("client");
      var transection = $(row).data(\'transection-type\');

      qty = transection == "Sell" ? qty * (-1): qty;

      if(clients.hasOwnProperty(client)) {
        clients[client].landingCost += (qty * price);
        clients[client].totalQty += qty;
      } else {
        clients[client] = {
          \'landingCost\': (qty * price),
          \'totalQty\': qty
        }
      }
    }).promise().done(function(){
      var item = $(self).data(\'item\');
      var expiry = $(self).data(\'expiry\');
      var totalAmount = 0;
      for(clientId in clients) {
        var clientDetails = clients[clientId];
        var calculatedAmount = (clientDetails.totalQty * $(self).val()) - clientDetails.landingCost ;
        totalAmount += calculatedAmount;
        $(".calculation-holder[data-client="+clientId+"][data-item="+item+"][data-expiry="+expiry+"]")
          .text(calculatedAmount)
          .removeClass("lossStyle")
          .removeClass("profitStyle")
          .addClass((calculatedAmount < 0 ?"lossStyle": "profitStyle"));
      }
      $(".total-amount-holder[data-item="+item+"][data-expiry="+expiry+"]")
        .text(totalAmount)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((totalAmount < 0 ?"lossStyle": "profitStyle"));
      // total profit loss count
      var grandTotal = 0;
      $(".total-amount-holder").each(function(i, amtEle) {
        grandTotal += parseInt($(amtEle).text());
      }).promise().done(function() {
        $("#wholepl1")
        .text(grandTotal)
        .removeClass("lossStyle")
        .removeClass("profitStyle")
        .addClass((grandTotal < 0 ?"lossStyle": "profitStyle"));
      });
    });
    // OLD CODE COMMENTED BY HK
    // var rowids = $(this).closest(\'tr\').attr(\'id\');     
    // var expival = $("#"+rowids+"_exp").val();
    // var itemval = $("#"+rowids+"_item").val();
    // var totprice = 0;
    // $("#itemtable tr").each(function(){
    //  var rowid = $(this).attr(\'id\'); 
    //  var rowcls = $(this).attr(\'class\');       
    //  var expval = $("#"+rowid+"_exp").val();
    //  var ival = $("#"+rowid+"_item").val();
    //  if(expval == expival && itemval == ival){
    //    var buy_qty = $("#"+rowid+"_buy").val();
    //    var sell_qty = $("#"+rowid+"_sell").val();
    //    //var quanval = sell_qty-buy_qty;
    //    //$("#row_"+rowcls+"_quan").html(quanval);
    //    var buyprice = 0;               
    //    var clientIds = $("#"+rowid+"_client").val();
    //    $("."+clientIds+"_"+ival+"_"+expival+"_buyprice").each(function(){
    //      buyprice = parseInt(buyprice)+parseInt($(this).val());              
    //    });
    //    //var quanval = $("#row_"+rowcls+"_quan").html();     
    //    var priceval = $("#"+rowids+"_price").val();
    //    var price = (sell_qty*priceval)-buyprice;
    //    totprice = totprice+price;
        
    
    //    /*if(quanval < 0){
    //      $("#row_"+rowcls+"_quan1").html(quanval.replace(\'-\', \'\')+" qty, Each one sell at "+priceval);
    //    } else {
    //      $("#row_"+rowcls+"_quan1").html(quanval+" qty buy at "+priceval);
    //    }*/
        
    //    $("#row_"+rowcls+"_price").html(price);
    //    $("#whole_"+ival+"_"+expival).html(totprice);
    //    $("#whole1_"+ival+"_"+expival).html(totprice);
    //    $("#wholeval_"+ival+"_"+expival).val(totprice);           
    //    var clientTotal = 0;
    //    $("."+clientIds+"_pl").each(function(){
        
    //    if($(this).html()!=\'\'){
    //      clientTotal = parseInt(clientTotal) + parseInt($(this).html());
    //      $("#"+clientIds+"_itemtotal").html(clientTotal);
    //      }
    //    });
    //  }
                
    // });
    // var totalprice = 0;
    //    $(".wholeprice").each(function(){
    //      var wholeval = $(this).val();
    //      if(wholeval!=\'\'){
    //        totalprice = parseInt(totalprice)+parseInt(wholeval); 
    //      }
    //    });
    // $("#wholepl").html(totalprice);
    // $("#wholepl1").html(totalprice);
  });

  var calculations = eval("("+window.sessionStorage.getItem(\'calculations\')+")");

  if(calculations != undefined) {
    for(i in calculations) {
      var ele = calculations[i];
      var htmlEle = $(".price[data-item="+ele.item+"][data-expiry="+ele.expiry+"]");
      $(htmlEle).val(ele.value);
      $(htmlEle).change();
    }
  }

});
'; ?>

</script>
</HEAD>
<BODY bgColor="#FFFF80">
<INPUT type="button" id="hide_show" name="hide_show" value="Show List">

<div id="list">

<FORM name="form1" method="get" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
">
  <INPUT type="hidden" name="display" value="<?php echo $this->_tpl_vars['display']; ?>
">
  <INPUT type="hidden" name="itemIdChanged" value="0">
  <input type="hidden" name="ex" value="<?php echo $this->_tpl_vars['ex']; ?>
">  
  <table cellPadding="5" cellSpacing="5" border="1" style="margin-bottom:20px;">
    <tr>
      <th>Item</th>
      <th>Expiry</th>
      <th>Current Price</th>
    </tr>
    <?php $_from = $this->_tpl_vars['expiryDetails']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['eDetails']):
?>
    <tr id="val_<?php echo $this->_tpl_vars['eDetails']['id']; ?>
">
      <td><?php echo $this->_tpl_vars['eDetails']['itemId']; ?>

        <input type="hidden" id="val_<?php echo $this->_tpl_vars['eDetails']['id']; ?>
_item" value="<?php echo $this->_tpl_vars['eDetails']['itemId']; ?>
"></td>
      <td><?php echo $this->_tpl_vars['eDetails']['expiryDate']; ?>

        <input type="hidden" id="val_<?php echo $this->_tpl_vars['eDetails']['id']; ?>
_exp" value="<?php echo $this->_tpl_vars['eDetails']['expiryDate']; ?>
"></td>
      <td><input
    type="text"
    class="price"
    id="val_<?php echo $this->_tpl_vars['eDetails']['id']; ?>
_price"
    data-expiry="<?php echo $this->_tpl_vars['eDetails']['expiryDate']; ?>
"
    data-item="<?php echo $this->_tpl_vars['eDetails']['itemId']; ?>
"
  ></td>
    </tr>
    <?php endforeach; endif; unset($_from); ?>
    <tr>
      <td colspan="3" style="text-align: right"><button id="storeLocally">Save</button></td>
    </tr>
  </table>
</div>

  
  <br>
  <br>
  <br>
  <TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
    <TR>
      <TD>Client :
        <SELECT name="clientId" onChange="document.form1.submit();">
          
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['clientIdSelected']),'values' => ($this->_tpl_vars['clientIdValues']),'output' => ($this->_tpl_vars['clientIdOptions'])), $this);?>

    
        </SELECT></TD>
      <TD>Item :
        <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
          
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['itemIdSelected']),'values' => ($this->_tpl_vars['itemIdValues']),'output' => ($this->_tpl_vars['itemIdOptions'])), $this);?>

    
        </SELECT></TD>
      <TD> Buy Sell :
        <select name="buySellOnly" onChange="document.form1.submit();">
          <option value="All"  <?php echo $this->_tpl_vars['buySellOnlyAll']; ?>
>All</option>
          <option value="Buy"  <?php echo $this->_tpl_vars['buySellOnlyBuy']; ?>
>Buy</option>
          <option value="Sell" <?php echo $this->_tpl_vars['buySellOnlySell']; ?>
>Sell</option>
        </select></TD>
      <TD>Group :
        <select name="groupName" onChange="document.form1.submit();">
          
    <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['groupName'],'output' => $this->_tpl_vars['groupName'],'selected' => $this->_tpl_vars['groupIdSelected']), $this);?>

  
        </select>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Expiry :
        <SELECT name="expiryDate" onChange="document.form1.submit();">
          
    <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['expiryDateSelected']),'values' => ($this->_tpl_vars['expiryDateValues']),'output' => ($this->_tpl_vars['expiryDateOptions'])), $this);?>

    
        </SELECT>
        <!--Exchange :
        <SELECT name="exchange" id="exchange" onChange="changeExchange();">
          <option value="MCX" <?php echo $this->_tpl_vars['exchangeOnlyMCX']; ?>
>MCX</option>
          <option value="Comex" <?php echo $this->_tpl_vars['exchangeOnlyComex']; ?>
>Comex</option>
        </SELECT>--></TD>
    </TR>
    <TR>
      <TD colspan="3" align="center"><A href="selectDtSession.php?goTo=clientTrades">Date range</A> : <?php echo $this->_tpl_vars['fromDate']; ?>
 To : <?php echo $this->_tpl_vars['toDate']; ?>

        </CENTER></TD>
    </TR>
    <TR>
      <TD colspan="3" align="center"> <?php echo $this->_tpl_vars['message']; ?>
 </TD>
    </TR>
  </TABLE>
</FORM>
<FORM name="form2" method="post" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
">
  <INPUT type="hidden" name="display" value="<?php echo $this->_tpl_vars['display']; ?>
">
  <INPUT type="hidden" name="itemIdChanged" value="0">
  <input type="hidden" name="clientId" value="<?php echo $this->_tpl_vars['clientIdSelected']; ?>
">
  <input type="hidden" name="itemId" value="<?php echo $this->_tpl_vars['itemIdSelected']; ?>
">
  <input type="hidden" name="buySellOnly" value="<?php echo $this->_tpl_vars['buySellOnlySelected']; ?>
">
  <input type="hidden" name="groupName" value="<?php echo $this->_tpl_vars['groupIdSelected']; ?>
">
  <input type="hidden" name="expiryDate" value="<?php echo $this->_tpl_vars['expiryDateSelected']; ?>
">
  <input type="hidden" name="exchange" value="<?php echo $this->_tpl_vars['exchangeSelected']; ?>
">
  <TABLE border="1" cellPadding="2" cellSpacing="0" id="itemtable">
  <!-- Table header 1st row -->
  <TR>
    <TD>&nbsp;</TD>
    <TD colspan="2" align="center">Buy</TD>
    <TD>&nbsp;</TD>
    <TD colspan="2" align="center">Sell</TD>
    <TD colspan="6">&nbsp;</TD>
  </TR>
  <!-- Table header 2nd row -->
  <TR>
    <TD align="center">BuySell</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD>&nbsp;</TD>
    <TD align="center">Qty</TD>
    <TD align="center">Price</TD>
    <TD align="center">Date</TD>
    <TD align="center">Stand</TD>
    <TD align="center">Item</TD>
    <TD align="center">NetProfitLoss</TD>
    <TD align="center">Vendor</TD>
    <TD align="center">Delete</TD>
    <?php if ($this->_tpl_vars['display'] == 'detailed'): ?>
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
    <?php endif; ?> </TR>
  <!-- Table blank row after header -->
  <TR>
    <TD colspan="2">&nbsp;</TD>
    <TD colspan="3" align="center"></td>
    <TD colspan="7">&nbsp;</td>
  </TR>
  <!-- Loop --> 
  <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=($this->_tpl_vars['trades'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevClientId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevItemId'] || $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate'] != $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['prevExpiryDate']): ?>
  <TR >
    <TD colspan="2"><U><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>
</U>&nbsp;:&nbsp;(<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientDeposit']; ?>
)</TD>
    <TD colspan="3" align="center"><input type="submit" name="confirmBtn" value="! Done !" />
      <input type="checkbox" name="checkAll" class="checkAll<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'];  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
" onClick="confirmedCheckbox(this,'<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'];  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
')"></td>
    <TD colspan="3">&nbsp;</td>
    <TD colspan="4" align="left"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
 </TD>
  </TR>
  <?php endif; ?>
  <TR style="color:<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['fontColor']; ?>
"
    class="data-holder"
    data-client="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
"
    data-item="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId']; ?>
"
    data-expiry="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate']; ?>
"
    data-transection-type="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell']; ?>
"
    data-qty="<?php if (( $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell'] == 'Buy' )):  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty'];  else:  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty'];  endif; ?>"
    data-price="<?php if (( $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell'] == 'Buy' )):  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['price'];  else:  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellPrice'];  endif; ?>"

>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buySell']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['price']; ?>
</TD>
    <TD align="right"><input type="checkbox" name="confirmed[<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
]" class="confirmed<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId'];  echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
" 
           value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
" <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['confirmed'] == 1): ?>CHECKED<?php endif; ?>></TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellPrice']; ?>

      <input type="hidden" class="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate']; ?>
_buyprice" value=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyQty']*$this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['price']; ?>
></TD>
    <TD align="center" NOWRAP> <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeDate']; ?>

      <?php if ($this->_tpl_vars['display'] == 'detailed'): ?> <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeTime']; ?>
 <?php endif; ?> </TD>
    <TD align="center"> <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing'] != 'Open' && $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing'] != 'Close'): ?>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['highLowConf'] == 1): ?> <font style="bgColor: black;color:<?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['fontColor'] == 'red'): ?>black<?php else: ?>black<?php endif; ?>;"><b>@@@</b></font> <?php endif; ?>
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing']; ?>
 </TD>
    <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
</TD>
    <TD>&nbsp;</TD>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['vendor']; ?>
&nbsp;</TD>
    <TD><A onClick="return confirm('Are you sure?');" href="deleteTxt.php?goTo=<?php echo $this->_tpl_vars['goTo']; ?>
&tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
"> <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['standing'] != 'Close'): ?>
      Delete
      <?php else: ?>
      Delete Stand
      <?php endif; ?> </A> &nbsp;&nbsp; <A href="tradeAdd3Edit.php?tradeId=<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">Edit</a></TD>
    <?php if ($this->_tpl_vars['display'] == 'detailed'): ?>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['userRemarks']; ?>
</TD>
    <TD align="center"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['ownClient']; ?>
</TD>
    <TD align="center" NOWRAP><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeRefNo']; ?>
</TD>
    <?php endif; ?> </TR>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispGross'] != 0): ?>
  <TR id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
" class="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
">
    <TD align="right" NOWRAP> Net: <?php echo smarty_function_math(array('equation' => "totBuyQty-totSellQty",'totBuyQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'],'totSellQty' => $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']), $this);?>
 </TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty']; ?>

      <input type="hidden" value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty']; ?>
" id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_buy"></TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['buyRash']; ?>
</TD>
    <TD>&nbsp;</TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']; ?>

      <input type="hidden" value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty']; ?>
" id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_sell"></TD>
    <TD align="right"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['sellRash']; ?>
</TD>
    <input type="hidden" id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_exp" value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate']; ?>
">
    <input type="hidden" id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_item" value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId']; ?>
">
    <input type="hidden" id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_client" value="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
">
    
    <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totBuyQty'] == $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['totSellQty']): ?>
    <TD colspan="3" align="right" NOWRAP> <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss'] < 0): ?> <FONT class="lossStyle">Loss : 
      <?php else: ?> <FONT class="profitStyle">Profit : 
      <?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['profitLoss']; ?>
</FONT> 
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['oneSideBrok']; ?>

      <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['brok1']; ?>

      <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['brok2']; ?>

      <!--Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['oneSideBrok']; ?>
 <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['brok1']; ?>
 <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['brok2']; ?>
--> </TD>
    <!--<TD align="right" NOWRAP> <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss']; ?>
</FONT></TD>
    <TD colspan="2">&nbsp;</TD>-->
    </TD>
    <TD align="right" NOWRAP>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['netProfitLoss']; ?>
</FONT>
    </TD>
    <TD colspan="2">&nbsp;</TD>
    <?php else: ?>
    <TD colspan="6"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemIdExpiry']; ?>
 : Buy Sell Qty Not Same</TD>
    <?php endif; ?>
    
  </TR>  
  <TR id="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
" > 
    <!--td colspan="3">Quantity: <span id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_quan"></span></td-->
    <td colspan="4">Profit/Loss: <span 
      id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_price"
      class="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_pl calculation-holder"
      data-client="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
"
      data-item="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['itemId']; ?>
"
      data-expiry="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['expiryDate']; ?>
"> </span></td>
    <!--td colspan="5">Quan With Price: <span id="row_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['tradeId']; ?>
_quan1"></span></td--> 
  </TR>
  </TR>
  <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['dispClientWhole'] != 0): ?>
  <TR>
    <TD colspan="6" align="right"> : Total : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
 : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientName']; ?>
 </TD>
    <TD colspan="3" align="right"><U> <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss'] < 0): ?> <FONT class="lossStyle">Loss : 
      <?php else: ?> <FONT class="profitStyle">Profit : 
      <?php endif; ?> <span id="<?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientId']; ?>
_itemtotal"><?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotProfitLoss']; ?>
</span></FONT></U>
      Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok']; ?>

      <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok1']; ?>

      <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok2']; ?>

       <!--Brok       : <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok']; ?>
 <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok1']; ?>
 <br />
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotBrok2']; ?>
--></TD>
    <TD align="right"><U>
      <?php if ($this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?>
      <?php echo $this->_tpl_vars['trades'][$this->_sections['sec1']['index']]['clientTotNetProfitLoss']; ?>
</FONT></U></TD>
    <TD align="center" colspan="2">&nbsp;</TD>
  </TR>
  <?php endif; ?>
  <?php endif; ?>
  <?php endfor; endif; ?>
  <TR>
    <TD colspan="2">&nbsp;</TD>
    <TD colspan="3" align="center"><input type="submit" name="confirmBtn" value="! Done !" /></td>
    <TD colspan="7">&nbsp;</td>
  </TR>
  <TR>
    <TD align="center">Net</TD>
    <TD>Buy</TD>
    <TD>Rash</TD>
    <TD>&nbsp;</TD>
    <TD>Sell</TD>
    <TD>Rash</TD>
    <TD>Check</TD>
    <TD colspan="2" align="center">Item</TD>
    <TD align="center">ProfitLoss</TD>
    <TD colspan="2" align="center">Brok</TD>
    <TD align="center">NetProfitLoss</TD>
  </TR>
  <?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=($this->_tpl_vars['wholeItemArr'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
  <TR>
    <TD align="right" NOWRAP> <?php echo smarty_function_math(array('equation' => "buyQty-sellQty",'buyQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty'],'sellQty' => $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty']), $this);?>
 </TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['buyRash']; ?>
</TD>
    <TD>&nbsp;</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['sellRash']; ?>
</TD>
    <TD><input type="checkbox" /></TD>
    <TD align="right" colspan="2" NOWRAP><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemIdExpiry']; ?>
</TD>
    <TD align="right" NOWRAP> <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?> <span id="whole1_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemId']; ?>
_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['expiryDate']; ?>
"
    class="total-amount-holder"
    data-item="<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemId']; ?>
"
    data-expiry="<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['expiryDate']; ?>
"
    ><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['profitLoss']; ?>
</span></FONT></TD>
    <TD align="right" colspan="2"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['oneSideBrok']; ?>
</TD>
    <TD align="right" NOWRAP> <?php if ($this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?> <span id="whole_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemId']; ?>
_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['expiryDate']; ?>
"><?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['netProfitLoss']; ?>
</span>
      <input type="hidden" id="wholeval_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['itemId']; ?>
_<?php echo $this->_tpl_vars['wholeItemArr'][$this->_sections['sec2']['index']]['expiryDate']; ?>
" value="" class="wholeprice">
      </FONT></TD>
  </TR>
  <?php endfor; endif; ?>
  <TR>
    <TD align="right" NOWRAP> <?php echo smarty_function_math(array('equation' => "buyQty-sellQty",'buyQty' => $this->_tpl_vars['wholeBuyQty'],'sellQty' => $this->_tpl_vars['wholeSellQty']), $this);?>
 </TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeBuyQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeBuyRash']; ?>
</TD>
    <TD>&nbsp;</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeSellQty']; ?>
</TD>
    <TD align="right"><?php echo $this->_tpl_vars['wholeSellRash']; ?>
</TD>
    <TD align="right" colspan="2" NOWRAP> : Total : </TD>
    <TD align="right" colspan="2"  NOWRAP> <?php if ($this->_tpl_vars['wholeProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?> <span id="wholepl1"><?php echo $this->_tpl_vars['wholeNetProfitLoss']; ?>
</span></FONT></TD>
    <TD align="right" colspan="2"> <?php echo $this->_tpl_vars['wholeOneSideBrok']; ?>
<br />
      <?php echo $this->_tpl_vars['wholeBrok1']; ?>
<br />
      <?php echo $this->_tpl_vars['wholeBrok2']; ?>
 </TD>
    <TD align="right" NOWRAP> <?php if ($this->_tpl_vars['wholeNetProfitLoss'] < 0): ?><FONT class="lossStyle"><?php else: ?><FONT class="profitStyle"><?php endif; ?> <span id="wholepl"><?php echo $this->_tpl_vars['wholeNetProfitLoss']; ?>
</span></FONT></TD>
  </TR>
</form>
</TABLE>
</BODY>
</HTML>