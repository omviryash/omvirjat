<?php
class DMLcommon
{
		function __construct()
		{
		}
		public function DMLInsertWithoutKey($table,$data)
		{
			$ssKey='';
			$ssVal='';
			foreach($data as $key=>$val)
			{
				$ssKey.=$key.',';
				if($key=='password'){				
					$ssVal.="'".addslashes(md5($val))."',";
				}
				else{
					$ssVal.="'".addslashes($val)."',";
				}
			}
			$ssKey=substr($ssKey,0,-1);
			$ssVal=substr($ssVal,0,-1);
			$sql="insert into ".$table." (".$ssKey.") values (".$ssVal.")";
			
			mysql_query($sql);
			return mysql_insert_id();
		}
		
		public function selectWithNestedKey($table,$fieldArray,$where,$orderField,$order)
		{
			$getArray=array();
			$i=0;
			$sql="select * from ".$table;
			if($where!='')
			{
				$sql.=" where ".$where;
			}
			if($orderField!='' && $order!='')
			{
				$sql.=" order by ".$orderField." ".$order;
			}
			
			$sqlRes=mysql_query($sql);
			if(mysql_num_rows($sqlRes)>0)
			{
				while($sqlRow=mysql_fetch_array($sqlRes))
				{
					foreach($fieldArray as $key=>$val)
					{
						$getArray[$i][$val]=$sqlRow[$val];
					}
					$i++;
				}
			}
			return $getArray;
		}
		
		public function selectWithSingleKey($table,$fieldArray,$where,$orderField,$order)
		{
			
			$getArray=array();
			$i=0;
			$sql="select * from ".$table;
			if($where!='')
			{
				$sql.=" where ".$where;
			}
			if($orderField!='' && $order!='')
			{
				$sql.=" order by ".$orderField." ".$order;
			}		
			
			$sqlRes=mysql_query($sql);
			if(mysql_num_rows($sqlRes)>0)
			{
				while($sqlRow=mysql_fetch_array($sqlRes))
				{
					foreach($fieldArray as $key=>$val)
					{
						$getArray[$val]=$sqlRow[$val];
					}
				}
			}
			return $getArray;
		}
		
		public function selectWithoutKey($table,$fieldArray,$where,$orderField,$order)
		{
			
			$getArray=array();
			$i=0;
			$sql="select * from ".$table;
			if($where!='')
			{
				$sql.=" where ".$where;
			}
			if($orderField!='' && $order!='')
			{
				$sql.=" order by ".$orderField." ".$order;
			}			
			$sqlRes=mysql_query($sql);
			if(mysql_num_rows($sqlRes)>0)
			{
				while($sqlRow=mysql_fetch_array($sqlRes))
				{
					foreach($fieldArray as $key=>$val)
					{
						$getArray[]=$sqlRow[$val];
					}
				}
			}
			return $getArray;
		}
		
		public function singleUpdate($table,$data,$where='')
		{
			$ssKeyVal='';	
			$sql="update ".$table." set ";
			foreach($data as $key=>$val)
			{
				if($key=='password')
					$ssKeyVal.=$key." = '".addslashes(md5($val))."',";
				else
					$ssKeyVal.=$key." = '".addslashes($val)."',";
			}
			$ssKeyVal=substr($ssKeyVal,0,-1);
			if($where!='')
			{
				$ssMerge=" where ".$where;
				$sql.=$ssKeyVal.$ssMerge;
			}
			
			$data=mysql_query($sql);
			if(mysql_affected_rows()){
				return true;
			}	
			else{
				return false;
			}
		}
		
		public function DMLDelete($table,$where)
		{
			$ssMerge=" where ".$where;
			$sql="delete from ".$table.$ssMerge;			
			mysql_query($sql);
		}
}
$dml=new DMLCommon();
?>