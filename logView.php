<?php session_start(); ?><?php
include "./etc/om_config.inc";
//  include "./etc/config.inc";
    if(isset($_GET["logid"]) && $_GET["logid"]!=''){
        $deleteLog = "DELETE FROM log Where logId='".$_GET["logid"]."'";
        mysql_query($deleteLog);
        header("Location:logView.php");
    }

    $where = '';
    if(isset($_POST['date_range']) && $_POST['date_range'] != ''){
        $date_range = explode('-',$_POST['date_range']);
        $startDateTime = date('Y-m-d',strtotime(str_replace('/','-',$date_range[0])));
        $endDateTime = date('Y-m-d',strtotime(str_replace('/','-',$date_range[1])));
        $where .= " AND datetime >= '$startDateTime 00:00:00'";
        $where .= " AND datetime <= '$endDateTime 23:59:59'";
    }

    $smarty=new SmartyWWW();
    $selectLog = "SELECT * FROM log Where (logType = 'Edit Trade MCX' OR logType = 'Edit Trade CX' OR logType = 'Edit Trade F_O' OR logType = 'Delete Trade MCX' OR logType = 'Delete Trade CX' OR logType = 'Delete Trade F_O') $where order by `datetime` desc";
    $resultLog = mysql_query($selectLog);
    $i=0;

    if(isset($_POST['logIds']) && !empty($_POST['logIds'])){
        $logIds = '';
        $i = 0;
        foreach ($_POST['logIds'] as $IdRow) {
            if($i != 0){
                $logIds .= ",";
            }
            $logIds .= "'".$IdRow."'";
            $i++;
        }
        $delete_query = "DELETE FROM `log` WHERE logId IN (".$logIds.")";
        mysql_query($delete_query);
        unset($_POST['logIds']);
        echo "<script>alert('You have successfully deleted records.')</script>";
        header("Location:logView.php");
    }
?>
<html>
<head>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>
    <link href="css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" media="screen"/>
    <style>
        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            padding:2px;
            font-size: 14px;
        }
        input[type=checkbox] {
            content: "";
            display: inline-block;
            width: 15px !important;
            height: 15px;
            vertical-align:middle;
            margin-right: 8px;
        }
    </style>
</head>
<body>
<form id="frmDeleteLog" action="" method="post">
    <div class="row">
        <div class="col-md-12">
            <h1>Log View</h1>
            <div class="col-lg-12 text-center">
                <h4><?php  echo isset($_POST['date_range']) && $_POST['date_range'] != ''?'Filter date : '.$_POST['date_range']:'';?></h4>
            </div>
            <div class="col-lg-3">
            </div>
            <label class="text-semibold col-lg-1">Select Date</label>
            <div class="col-lg-2">
                <input type="text" name="date_range" class="form-control date_range" id="date_range" value="">
            </div>
            <div class="col-lg-4">
                <button type="submit" class="btn btn-primary">Load data</button>
            </div>
            <div class="clearfix"></div>
            <table class="table table-bordered" id="TblLogView">
                <thead>
                <tr>
                    <th data-orderable="false">
                        &nbsp;<input type="checkbox" class="ts_checkbox_all">&nbsp;<a href="javascript:void(0);" class="btnSubmit">Delete</a>
                    </th>
                    <th>Trade Id</th>
                    <th>Old Client Id</th>
                    <th>Client Id</th>
                    <th>Old BuySell</th>
                    <th>BuySell</th>
                    <th>Old Item Id</th>
                    <th>Item Id</th>
                    <th>Old Quantity</th>
                    <th>Quantity</th>
                    <th>Old Price</th>
                    <th>Price</th>
                    <th>Brok</th>
                    <th>Date Time</th>
                    <th>Log Message</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    while($rowLog = mysql_fetch_array($resultLog))
                    {
                        ?>
                        <tr style="color:#ffffff;" bgcolor="<?php if($rowLog['logType']=="Delete Trade CX" || $rowLog['logType']=="Delete Trade MCX" || $rowLog['logType']=="Delete Trade F_O") { echo "#FF8AC5";} else {echo "#649ADD";} ?>">
                            <td>
                                &nbsp;<input type="checkbox" class="form-control" name="logIds[]" value="<?php echo $rowLog['logId'];?>">&nbsp;<a href="logView.php?logid=<?php echo $rowLog['logId']; ?>" onclick="return askConfirm();" style="color:#ffffff;">Delete</a>
                            </td>
                            <td><?php echo $rowLog['tradeId']; ?></td>
                            <td><?php echo $rowLog['old_clientId']; ?></td>
                            <td><?php echo $rowLog['clientId']; ?></td>
                            <td><?php echo $rowLog['old_buySell']; ?></td>
                            <td><?php echo $rowLog['buySell']; ?></td>
                            <td><?php echo $rowLog['old_itemId']; ?></td>
                            <td><?php echo $rowLog['itemId']; ?></td>
                            <td><?php echo $rowLog['old_qty']; ?></td>
                            <td><?php echo $rowLog['qty']; ?></td>
                            <td><?php echo $rowLog['old_price']; ?></td>
                            <td><?php echo $rowLog['price']; ?></td>
                            <td><?php echo $rowLog['brok']; ?></td>
                            <td><?php echo $rowLog['datetime']; ?></td>
                            <td><?php if($rowLog['logType']=="") { echo "Trade Deleted";} else {echo $rowLog['logType'];} ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</form>
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>

<script>
    $(document).ready(function() {
        // Display date dropdowns
        $('#date_range').daterangepicker({
            showDropdowns: true,
            timePicker: false,
        },function(start, end, label) {
            $('#date_range').val(start.format('YYYY/MM/DD')+' - '+end.format('YYYY/MM/DD'));
        });

        $('#TblLogView').DataTable();

        $(document).on('change','.ts_checkbox_all',function(){
            $(':checkbox').prop('checked', this.checked);
        });

        $('.btnSubmit').on('click',function(){
            if(confirm("Are You Sure You want to Delete selected records ?"))
            {
                $('button[type="submit"]').trigger('click');
            }
            else{
                return true;
            }
        });
    } );
    function askConfirm()
    {
        if(confirm("Are You Sure You want to Delete log?"))
        {
            return true;
        }
        else{
            return false;
        }
    }
</script>
</body>
</html>