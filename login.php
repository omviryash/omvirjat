<?php
session_start();
//$host = "localhost"; //database location
//$user = "omvirco_om2"; //database username
//$pass = "omvir159951"; //database password
//$dbName = "omvirco_mhare"; //database name

include "./etc/om_config.inc";
$message = "";
if(isset($_SESSION['user'])) {
	header("Location:menu.php");
}

if(isset($_POST['submit'])) {
	$checkLogin = "SELECT * FROM user
				    WHERE username LIKE BINARY '".$_POST['userid']."'
					  AND password LIKE BINARY  '".md5($_POST['password'])."'";
	$checkLoginresult = mysql_query($checkLogin);
	$numRows = mysql_num_rows($checkLoginresult);

	if($numRows > 0) {
		$userLoginRow = mysql_fetch_assoc($checkLoginresult);
		$_SESSION['user'] = $userLoginRow;
		
		if(isset($_GET['goTo'])) {
			header("Location:".$_GET['goTo']);
		} else if(isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == "SUPER_ADMIN"){
			$_SESSION['super_user'] = $_SESSION['user']['username'];
            header("Location: index.php");
		} else if(isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == "ENTRY_USER"){
			$_SESSION['entry_user'] = $_SESSION['user']['username'];
            header("Location: menu2.php");}
		else {
			header("Location: tradeReport.php");
		}
	} else {
		$message = "Invalid UserName Or password"; 
	}
}
?>

<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>J & M Company | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/admin.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
        <div class="form-box" id="login-box">
            <div class="header">J & M Company | Sign In</div>
            <form action="login.php" method="post">
                <div class="body bg-gray" >
					<div class="message"><?php echo $message; ?></div>
                    <div class="form-group">
                        <input type="text" name="userid" class="form-control" placeholder="User ID"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" name="submit" class="btn bg-olive btn-block">Sign in</button>  
                </div>
            </form>
        </div>
        <script src="js/admin/jquery.min.js"></script>
        <script src="js/admin/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>