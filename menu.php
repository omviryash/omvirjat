<?php
session_start();
if(!isset($_SESSION['user'])) {
	header("Location:login.php");
	exit;
}
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Dashboard | Log in</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="css/admin.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
	
	<!-- Static navbar -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">J & M Company</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="menu.php">Home</a></li>
              <!--<li><a href="clientTrades.php">Mcx Trades</a></li>
              <li><a href="clientTradesComex.php">Comex Trades</a></li>
              <li><a href="clientTrades.php?display=gross">Mcx Gross</a></li>
              <li><a href="clientTradesComex.php?display=gross">Comex Gross</a></li>-->
              <li><a href="tradeReport.php">Trade Report</a></li>
            </ul>
			<ul class="nav navbar-nav navbar-right">
              <!--<li><a href="changePassword.php">Change Password</a></li>-->
              <li><a href="signout.php">Signout</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>
	<div class="form-box" id="login-box">
            <div class="header">J & M Company | Sign In</div>
        </div>
        
		
	<script src="js/admin/jquery.min.js"></script>
	<script src="js/admin/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>
