-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2016 at 10:08 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jatshare`
--

-- --------------------------------------------------------

--
-- Table structure for table `bankmaster`
--

CREATE TABLE `bankmaster` (
  `bankId` int(6) NOT NULL,
  `bankName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone1` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `phone2` varchar(12) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bankmaster`
--

INSERT INTO `bankmaster` (`bankId`, `bankName`, `phone1`, `phone2`) VALUES
(1, 'Bill', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `bhavcopy`
--

CREATE TABLE `bhavcopy` (
  `bhavcopyid` int(10) NOT NULL,
  `exchange` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `bhavcopyDate` date NOT NULL DEFAULT '0000-00-00',
  `sessionId` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `marketType` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `instrumentId` int(10) NOT NULL DEFAULT '0',
  `instrumentName` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `scriptCode` int(10) NOT NULL DEFAULT '0',
  `contractCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `scriptGroup` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `scriptType` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `expiryDate` date NOT NULL DEFAULT '0000-00-00',
  `expiryDateBc` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `strikePrice` float NOT NULL DEFAULT '0',
  `optionType` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `previousClosePrice` float NOT NULL DEFAULT '0',
  `openPrice` float NOT NULL DEFAULT '0',
  `highPrice` float NOT NULL DEFAULT '0',
  `lowPrice` float NOT NULL DEFAULT '0',
  `closePrice` float NOT NULL DEFAULT '0',
  `totalQtyTrade` int(10) NOT NULL DEFAULT '0',
  `totalValueTrade` double NOT NULL DEFAULT '0',
  `lifeHigh` float NOT NULL DEFAULT '0',
  `lifeLow` float NOT NULL DEFAULT '0',
  `quoteUnits` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `settlementPrice` float NOT NULL DEFAULT '0',
  `noOfTrades` int(6) NOT NULL DEFAULT '0',
  `openInterest` double NOT NULL DEFAULT '0',
  `avgTradePrice` float NOT NULL DEFAULT '0',
  `tdcl` float NOT NULL DEFAULT '0',
  `lstTradePrice` float NOT NULL DEFAULT '0',
  `remarks` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cashflow`
--

CREATE TABLE `cashflow` (
  `cashFlowId` int(6) NOT NULL,
  `clientId` int(10) NOT NULL DEFAULT '0',
  `itemIdExpiryDate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dwStatus` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `dwAmount` double NOT NULL DEFAULT '0',
  `plStatus` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `plAmount` double NOT NULL DEFAULT '0',
  `transactionDate` date DEFAULT NULL,
  `transType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transMode` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `clientId` int(6) NOT NULL,
  `passwd` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `openingDate` date DEFAULT NULL,
  `opening` float DEFAULT NULL,
  `deposit` int(6) DEFAULT NULL,
  `currentBal` float DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(22) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oneSide` tinyint(1) DEFAULT '0',
  `remiser` int(6) DEFAULT '0',
  `remiserBrok` float DEFAULT '0',
  `remiserBrokIn` tinyint(1) DEFAULT '1',
  `clientBroker` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE `clientbrok` (
  `clientBrokId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exchange` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `clientexchange`
--

CREATE TABLE `clientexchange` (
  `clientexchangeId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT NULL,
  `exchange` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brok` int(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `clientexchange`
--

INSERT INTO `clientexchange` (`clientexchangeId`, `clientId`, `exchange`, `brok`) VALUES
(180, 2, 'F_O', 100),
(183, 4, 'F_O', 100),
(185, 6, 'F_O', 100),
(187, 13, 'F_O', 90),
(189, 14, 'F_O', 50),
(191, 15, 'F_O', 100),
(192, 16, 'F_O', 100),
(193, 17, 'F_O', 100),
(195, 18, 'F_O', 30),
(196, 5, 'F_O', 100),
(198, 19, 'F_O', 100),
(202, 25, 'F_O', 100),
(204, 26, 'F_O', 30),
(206, 27, 'F_O', 100),
(208, 30, 'F_O', 50),
(210, 33, 'F_O', 60),
(213, 37, 'F_O', 20),
(215, 36, 'F_O', 30),
(217, 38, 'F_O', 20),
(220, 47, 'F_O', 70),
(222, 49, 'F_O', 60),
(224, 50, 'F_O', 60),
(226, 51, 'F_O', 100),
(228, 52, 'F_O', 30),
(230, 54, 'F_O', 60),
(232, 53, 'F_O', 30),
(234, 56, 'F_O', 80),
(236, 57, 'F_O', 30),
(238, 58, 'F_O', 30),
(240, 59, 'F_O', 20),
(242, 60, 'F_O', 20),
(244, 61, 'F_O', 120),
(246, 62, 'F_O', 20),
(248, 63, 'F_O', 150),
(250, 64, 'F_O', 100),
(252, 66, 'F_O', 20),
(255, 68, 'F_O', 80),
(256, 65, 'F_O', 100),
(258, 69, 'F_O', 30),
(260, 80, 'F_O', 30),
(262, 82, 'F_O', 100),
(263, 85, 'F_O', 70),
(264, 84, 'F_O', 30),
(265, 83, 'F_O', 30),
(266, 86, 'F_O', 30),
(268, 87, 'F_O', 0),
(270, 88, 'F_O', 100),
(272, 90, 'F_O', 30),
(274, 89, 'F_O', 30),
(276, 91, 'F_O', 60),
(277, 22, 'F_O', 50),
(279, 92, 'F_O', 100),
(282, 104, 'F_O', 100),
(286, 110, 'F_O', 100),
(289, 112, 'F_O', 70),
(291, 115, 'F_O', 20),
(293, 116, 'F_O', 20),
(294, 117, 'F_O', 60),
(296, 118, 'F_O', 100),
(298, 119, 'F_O', 30),
(300, 120, 'F_O', 100),
(301, 121, 'F_O', 60),
(302, 122, 'F_O', 0),
(304, 123, 'F_O', 100),
(306, 124, 'F_O', 140),
(308, 128, 'F_O', 70),
(309, 130, 'F_O', 70),
(311, 131, 'F_O', 100),
(314, 125, 'F_O', 20),
(316, 129, 'F_O', 20),
(317, 81, 'F_O', 100),
(321, 134, 'F_O', 100),
(322, 135, 'F_O', 150),
(323, 111, 'F_O', 50),
(325, 136, 'F_O', 100),
(327, 137, 'F_O', 100),
(330, 140, 'F_O', 30),
(332, 141, 'F_O', 70),
(334, 142, 'F_O', 40),
(335, 144, 'F_O', 100),
(337, 143, 'F_O', 100),
(339, 145, 'F_O', 30),
(341, 146, 'F_O', 20),
(343, 147, 'F_O', 70),
(345, 148, 'F_O', 70),
(347, 152, 'F_O', 20),
(349, 153, 'F_O', 100),
(351, 151, 'F_O', 80),
(356, 7, 'F_O', 50),
(357, 149, 'F_O', 100),
(358, 155, 'F_O', 100),
(360, 159, 'F_O', 100),
(362, 160, 'F_O', 150),
(364, 162, 'F_O', 20),
(366, 163, 'F_O', 70),
(368, 164, 'F_O', 150),
(371, 28, 'F_O', 20),
(373, 39, 'F_O', 100),
(375, 21, 'F_O', 100),
(377, 23, 'F_O', 60),
(379, 34, 'F_O', 50),
(381, 32, 'F_O', 100),
(384, 12, 'F_O', 100),
(386, 9, 'F_O', 100),
(389, 169, 'F_O', 100),
(391, 171, 'F_O', 100),
(394, 170, 'F_O', 100),
(398, 176, 'F_O', 20),
(399, 173, 'F_O', 40),
(401, 178, 'F_O', 50),
(403, 180, 'F_O', 80),
(405, 181, 'F_O', 50),
(407, 182, 'F_O', 60),
(409, 183, 'F_O', 100),
(411, 611, 'F_O', 50),
(413, 612, 'F_O', 20),
(414, 10, 'F_O', 150),
(416, 615, 'F_O', 80),
(418, 610, 'F_O', 20),
(420, 617, 'F_O', 20),
(422, 618, 'F_O', 50),
(424, 619, 'F_O', 20),
(426, 621, 'F_O', 100),
(428, 622, 'F_O', 100),
(430, 623, 'F_O', 100),
(432, 624, 'F_O', 100),
(434, 626, 'F_O', 20),
(436, 627, 'F_O', 10),
(438, 628, 'F_O', 100),
(440, 629, 'F_O', 40),
(442, 630, 'F_O', 20),
(444, 631, 'F_O', 100),
(446, 632, 'F_O', 40),
(448, 633, 'F_O', 20),
(450, 634, 'F_O', 20),
(452, 635, 'F_O', 100),
(453, 642, 'F_O', 40),
(454, 643, 'F_O', 31),
(456, 644, 'F_O', 100),
(458, 641, 'F_O', 40),
(459, 640, 'F_O', 30),
(461, 645, 'F_O', 50),
(463, 647, 'F_O', 30),
(465, 646, 'F_O', 30),
(468, 648, 'F_O', 100),
(470, 649, 'F_O', 30),
(472, 650, 'F_O', 100),
(474, 651, 'F_O', 90),
(476, 652, 'F_O', 70),
(478, 653, 'F_O', 100),
(480, 655, 'F_O', 100),
(482, 1003, 'F_O', 20),
(484, 1004, 'F_O', 100),
(486, 1005, 'F_O', 100),
(488, 1006, 'F_O', 100),
(490, 1007, 'F_O', 50),
(492, 1009, 'F_O', 100),
(494, 1008, 'F_O', 50),
(496, 1011, 'F_O', 150),
(498, 1014, 'F_O', 50),
(500, 1015, 'F_O', 50),
(504, 1018, 'F_O', 50),
(506, 1019, 'F_O', 20),
(508, 1020, 'F_O', 30),
(510, 1021, 'F_O', 30),
(512, 1022, 'F_O', 70),
(514, 1023, 'F_O', 30),
(516, 1024, 'F_O', 70),
(518, 1025, 'F_O', 40),
(520, 1026, 'F_O', 30),
(523, 1028, 'F_O', 30),
(525, 1030, 'F_O', 20),
(528, 1033, 'F_O', 30),
(530, 1034, 'F_O', 30),
(532, 1035, 'F_O', 70),
(533, 1037, 'F_O', 15),
(534, 1038, 'F_O', 30),
(535, 1043, 'F_O', 20),
(536, 1042, 'F_O', 25),
(537, 1044, 'F_O', 40),
(538, 1045, 'F_O', 50),
(539, 1046, 'F_O', 20),
(540, 1047, 'F_O', 20),
(541, 1048, 'F_O', 30),
(542, 1049, 'F_O', 30),
(543, 1052, 'F_O', 40),
(544, 1050, 'F_O', 40),
(545, 1053, 'F_O', 10),
(546, 1054, 'F_O', 30),
(547, 1055, 'F_O', 20),
(548, 1056, 'F_O', 10),
(549, 1057, 'F_O', 40),
(550, 1058, 'F_O', 20),
(551, 1059, 'F_O', 8),
(552, 1060, 'F_O', 10),
(553, 1061, 'F_O', 8);

-- --------------------------------------------------------

--
-- Table structure for table `exchange`
--

CREATE TABLE `exchange` (
  `exchangeId` int(6) UNSIGNED NOT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `multiply` tinyint(1) NOT NULL DEFAULT '0',
  `profitBankRate` float DEFAULT NULL,
  `lossBankRate` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `exchange`
--

INSERT INTO `exchange` (`exchangeId`, `exchange`, `multiply`, `profitBankRate`, `lossBankRate`) VALUES
(2, 'F_O', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `expensemaster`
--

CREATE TABLE `expensemaster` (
  `expensemasterId` int(6) NOT NULL,
  `expenseName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expensemaster`
--

INSERT INTO `expensemaster` (`expensemasterId`, `expenseName`) VALUES
(1, 'Light'),
(3, 'Petrol2');

-- --------------------------------------------------------

--
-- Table structure for table `expiry`
--

CREATE TABLE `expiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `expiry`
--

INSERT INTO `expiry` (`expiryId`, `itemId`, `expiryDate`, `exchange`) VALUES
(18406, 'BANKNIFTY', '31DEC2015', 'F_O'),
(18407, 'NIFTY', '31DEC2015', 'F_O'),
(18408, 'LUPIN', '31DEC2015', 'F_O'),
(18409, 'PIDILITIND', '31DEC2015', 'F_O'),
(18461, 'HDFC', '31DEC2015', 'F_O'),
(18412, 'SUNTV', '31DEC2015', 'F_O'),
(18413, 'BANKINDIA', '31DEC2015', 'F_O'),
(18442, 'INDUSINDBK', '31DEC2015', 'F_O'),
(18464, 'LICHSGFIN', '31DEC2015', 'F_O'),
(18416, 'HDFCBANK', '31DEC2015', 'F_O'),
(18417, 'ICICIBANK', '31DEC2015', 'F_O'),
(18466, 'POWERGRID', '31DEC2015', 'F_O'),
(18460, 'DRREDDY', '31DEC2015', 'F_O'),
(18422, 'BPCL', '31DEC2015', 'F_O'),
(18465, 'MMTC', '31DEC2015', 'F_O'),
(18456, 'ACC', '31DEC2015', 'F_O'),
(18457, 'ASIANPAINT', '31DEC2015', 'F_O'),
(18432, 'RELCAPITAL', '31DEC2015', 'F_O'),
(18437, 'BAJAJAUTO', '31DEC2015', 'F_O'),
(18467, 'TATACHEM', '31DEC2015', 'F_O'),
(18435, 'RELIANCE', '31DEC2015', 'F_O'),
(18441, 'HEROMOTOCO', '31DEC2015', 'F_O'),
(18458, 'AXISBANK', '31DEC2015', 'F_O'),
(18455, 'RELINFRA', '31DEC2015', 'F_O'),
(18463, 'KOTAKBANK', '31DEC2015', 'F_O'),
(18446, 'MARUTI', '31DEC2015', 'F_O'),
(18462, 'JINDALSTEL', '31DEC2015', 'F_O'),
(18448, 'ONGC', '31DEC2015', 'F_O'),
(18449, 'RCOM', '31DEC2015', 'F_O'),
(18450, 'TATASTEEL', '31DEC2015', 'F_O'),
(18459, 'BANKBARODA', '31DEC2015', 'F_O'),
(18452, 'YESBANK', '31DEC2015', 'F_O'),
(18453, 'SBIN', '31DEC2015', 'F_O'),
(18454, 'JSWSTEEL', '31DEC2015', 'F_O'),
(18405, 'F_O', '31DEC2015', 'F_O');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `generalId` int(6) NOT NULL,
  `filePath` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`generalId`, `filePath`, `fileName`) VALUES
(1, 'bhavcopies', 'MS20081227.csv');

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `itemId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemShort` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `brok` float DEFAULT NULL,
  `brok2` float DEFAULT NULL,
  `oneSideBrok` float DEFAULT NULL,
  `min` int(6) DEFAULT NULL,
  `priceOn` int(6) DEFAULT NULL,
  `mulAmount` float DEFAULT '1',
  `rangeStart` float DEFAULT NULL,
  `rangeEnd` float DEFAULT NULL,
  `qtyInLots` tinyint(1) DEFAULT NULL,
  `exchangeId` int(6) UNSIGNED NOT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `multiply` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `item`, `itemShort`, `brok`, `brok2`, `oneSideBrok`, `min`, `priceOn`, `mulAmount`, `rangeStart`, `rangeEnd`, `qtyInLots`, `exchangeId`, `exchange`, `multiply`) VALUES
('GOLD', 'GOLD', 'GOLD', 300, 200, 400, 100, 10, 1, 20000, 29999, NULL, 2, 'F_O', 1),
('NIFTY', 'NIFTY', 'NIFTY', 0, 0, 150, 1, 1, 1, 4000, 11000, NULL, 2, 'F_O', 1),
('BANKNIFTY', 'BANKNIFTY', 'BANKNIFTY', 0, 0, 150, 1, 1, 1, 11001, 25000, NULL, 2, 'F_O', 1),
('RELIANCE', 'RELIANCE', 'RELIANCE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SBIN', 'SBIN', 'SBIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BHEL', 'BHEL', 'BHEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RELCAPITAL', 'RELCAPITAL', 'RELCAPITAL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RCOM', 'RCOM', 'RCOM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ADLABSFILM', 'ADLABSFILM', 'ADLABSFILM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ABAN', 'ABAN', 'ABAN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CENTURYTEX', 'CENTURYTEX', 'CENTURYTEX', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MCDOWELL_N', 'MCDOWELL_N', 'MCDOWELL_N', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JPASSOCIAT', 'JPASSOCIAT', 'JPASSOCIAT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('WELGUJ', 'WELGUJ', 'WELGUJ', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RELINFRA', 'RELINFRA', 'RELINFRA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('WIPRO', 'WIPRO', 'WIPRO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ICICIBANK', 'ICICIBANK', 'ICICIBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('YESBANK', 'YESBANK', 'YESBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HDFC', 'HDFC', 'HDFC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('STER', 'STER', 'STER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BHARTIARTL', 'BHARTIARTL', 'BHARTIARTL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HDFCBANK', 'HDFCBANK', 'HDFCBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CANBK', 'CANBK', 'CANBK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('LT', 'LT', 'LT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RPL', 'RPL', 'RPL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RNRL', 'RNRL', 'RNRL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ESSAROIL', 'ESSAROIL', 'ESSAROIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JINDALSTEL', 'JINDALSTEL', 'JINDALSTEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DLF', 'DLF', 'DLF', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('EDUCOMP', 'EDUCOMP', 'EDUCOMP', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATASTEEL', 'TATASTEEL', 'TATASTEEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATAPOWER', 'TATAPOWER', 'TATAPOWER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NDTV', 'NDTV', 'NDTV', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATAMOTORS', 'TATAMOTORS', 'TATAMOTORS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('AXISBANK', 'AXISBANK', 'AXISBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('UNITECH', 'UNITECH', 'UNITECH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NTPC', 'NTPC', 'NTPC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IVRCLINFRA', 'IVRCLINFRA', 'IVRCLINFRA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TCS', 'TCS', 'TCS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PUNJLLOYD', 'PUNJLLOYD', 'PUNJLLOYD', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SRF', 'SRF', 'SRF', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CIPLA', 'CIPLA', 'CIPLA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ONGC', 'ONGC', 'ONGC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GSPL', 'GSPL', 'GSPL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JSWSTEEL', 'JSWSTEEL', 'JSWSTEEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MARUTI', 'MARUTI', 'MARUTI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MNM', 'MNM', 'MNM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ABB', 'ABB', 'ABB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RENUKA', 'RENUKA', 'RENUKA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('LITL', 'LITL', 'LITL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('WOCKPHARMA', 'WOCKPHARMA', 'WOCKPHARMA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DCB', 'DCB', 'DCB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('VOLTAS', 'VOLTAS', 'VOLTAS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('VIJAYABANK', 'VIJAYABANK', 'VIJAYABANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('UTVSOF', 'UTVSOF', 'UTVSOF', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('UNIPHOS', 'UNIPHOS', 'UNIPHOS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('UNIONBANK', 'UNIONBANK', 'UNIONBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ULTRACEMCO', 'ULTRACEMCO', 'ULTRACEMCO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('UCOBANK', 'UCOBANK', 'UCOBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TVSMOTOR', 'TVSMOTOR', 'TVSMOTOR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TV18', 'TV18', 'TV18', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TTML', 'TTML', 'TTML', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TRIVENI', 'TRIVENI', 'TRIVENI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TORNTPOWER', 'TORNTPOWER', 'TORNTPOWER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TITAN', 'TITAN', 'TITAN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TECHM', 'TECHM', 'TECHM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATATEA', 'TATATEA', 'TATATEA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATACOMM', 'TATACOMM', 'TATACOMM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATACHEM', 'TATACHEM', 'TATACHEM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SYNDIBANK', 'SYNDIBANK', 'SYNDIBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SUZLON', 'SUZLON', 'SUZLON', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SUNTV', 'SUNTV', 'SUNTV', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SUNPHARMA', 'SUNPHARMA', 'SUNPHARMA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('STERLINBIO', 'STERLINBIO', 'STERLINBIO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('STAR', 'STAR', 'STAR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SREINTFIN', 'SREINTFIN', 'SREINTFIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SKUMARSYNF', 'SKUMARSYNF', 'SKUMARSYNF', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SINTEX', 'SINTEX', 'SINTEX', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SIEMENS', 'SIEMENS', 'SIEMENS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SESAGOA', 'SESAGOA', 'SESAGOA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SCI', 'SCI', 'SCI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SAIL', 'SAIL', 'SAIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RPOWER', 'RPOWER', 'RPOWER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ROLTA', 'ROLTA', 'ROLTA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RIIL', 'RIIL', 'RIIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RECLTD', 'RECLTD', 'RECLTD', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RANBAXY', 'RANBAXY', 'RANBAXY', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('RAJESHEXPO', 'RAJESHEXPO', 'RAJESHEXPO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PTC', 'PTC', 'PTC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PRAJIND', 'PRAJIND', 'PRAJIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('POWERGRID', 'POWERGRID', 'POWERGRID', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('POLARIS', 'POLARIS', 'POLARIS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PNB', 'PNB', 'PNB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PIRHEALTH', 'PIRHEALTH', 'PIRHEALTH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PFC', 'PFC', 'PFC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PETRONET', 'PETRONET', 'PETRONET', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PENINLAND', 'PENINLAND', 'PENINLAND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PATNI', 'PATNI', 'PATNI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PATELENG', 'PATELENG', 'PATELENG', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('PANTALOONR', 'PANTALOONR', 'PANTALOONR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ORIENTBANK', 'ORIENTBANK', 'ORIENTBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ORCHIDCHEM', 'ORCHIDCHEM', 'ORCHIDCHEM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('OPTOCIRCUI', 'OPTOCIRCUI', 'OPTOCIRCUI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('OFSS', 'OFSS', 'OFSS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NIITLTD', 'NIITLTD', 'NIITLTD', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NEYVELILIG', 'NEYVELILIG', 'NEYVELILIG', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NETWORK18', 'NETWORK18', 'NETWORK18', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NBVENTURES', 'NBVENTURES', 'NBVENTURES', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NATIONALUM', 'NATIONALUM', 'NATIONALUM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NAGARFERT', 'NAGARFERT', 'NAGARFERT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NAGARCONST', 'NAGARCONST', 'NAGARCONST', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MTNL', 'MTNL', 'MTNL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MRPL', 'MRPL', 'MRPL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MRF', 'MRF', 'MRF', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MPHASIS', 'MPHASIS', 'MPHASIS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MOSERBAER', 'MOSERBAER', 'MOSERBAER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MONNETISPA', 'MONNETISPA', 'MONNETISPA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MAHSEAMLES', 'MAHSEAMLES', 'MAHSEAMLES', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MAHLIFE', 'MAHLIFE', 'MAHLIFE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('LUPIN', 'LUPIN', 'LUPIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('LICHSGFIN', 'LICHSGFIN', 'LICHSGFIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('LAXMIMACH', 'LAXMIMACH', 'LAXMIMACH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KTKBANK', 'KTKBANK', 'KTKBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KSOILS', 'KSOILS', 'KSOILS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KSK', 'KSK', 'KSK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KOTAKBANK', 'KOTAKBANK', 'KOTAKBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KFA', 'KFA', 'KFA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('KESORAMIND', 'KESORAMIND', 'KESORAMIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JPHYDRO', 'JPHYDRO', 'JPHYDRO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JINDALSAW', 'JINDALSAW', 'JINDALSAW', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JETAIRWAYS', 'JETAIRWAYS', 'JETAIRWAYS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ITC', 'ITC', 'ITC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ISPATIND', 'ISPATIND', 'ISPATIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IRB', 'IRB', 'IRB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IOC', 'IOC', 'IOC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IOB', 'IOB', 'IOB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INFOSYSTCH', 'INFOSYSTCH', 'INFOSYSTCH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INDUSINDBK', 'INDUSINDBK', 'INDUSINDBK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INDIANB', 'INDIANB', 'INDIANB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INDIAINFO', 'INDIAINFO', 'INDIAINFO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INDIACEM', 'INDIACEM', 'INDIACEM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INDHOTEL', 'INDHOTEL', 'INDHOTEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IFCI', 'IFCI', 'IFCI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IDFC', 'IDFC', 'IDFC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IDEA', 'IDEA', 'IDEA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IDBI', 'IDBI', 'IDBI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ICSA', 'ICSA', 'ICSA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IBREALEST', 'IBREALEST', 'IBREALEST', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HOTELEELA', 'HOTELEELA', 'HOTELEELA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HINDZINC', 'HINDZINC', 'HINDZINC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HINDUNILVR', 'HINDUNILVR', 'HINDUNILVR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HINDOILEXP', 'HINDOILEXP', 'HINDOILEXP', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HINDALCO', 'HINDALCO', 'HINDALCO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CEATTYRE', 'CEATTYRE', 'CEATTYRE', 0, 0, 35, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HDIL', 'HDIL', 'HDIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HCLTECH', 'HCLTECH', 'HCLTECH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HCLINSYS', 'HCLINSYS', 'HCLINSYS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HCC', 'HCC', 'HCC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HAVELLS', 'HAVELLS', 'HAVELLS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GVKPIL', 'GVKPIL', 'GVKPIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GUJALKALI', 'GUJALKALI', 'GUJALKALI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GTOFFSHORE', 'GTOFFSHORE', 'GTOFFSHORE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GTLINFRA', 'GTLINFRA', 'GTLINFRA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GTL', 'GTL', 'GTL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GRASIM', 'GRASIM', 'GRASIM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GNFC', 'GNFC', 'GNFC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GLAXO', 'GLAXO', 'GLAXO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GITANJALI', 'GITANJALI', 'GITANJALI', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GESHIP', 'GESHIP', 'GESHIP', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GDL', 'GDL', 'GDL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GAIL', 'GAIL', 'GAIL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('FSL', 'FSL', 'FSL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('FINANTECH', 'FINANTECH', 'FINANTECH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('FEDERALBNK', 'FEDERALBNK', 'FEDERALBNK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ESCORTS', 'ESCORTS', 'ESCORTS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('EKC', 'EKC', 'EKC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('EDELWEISS', 'EDELWEISS', 'EDELWEISS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DRREDDY', 'DRREDDY', 'DRREDDY', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DIVISLAB', 'DIVISLAB', 'DIVISLAB', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DISHTV', 'DISHTV', 'DISHTV', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DENABANK', 'DENABANK', 'DENABANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DCHL', 'DCHL', 'DCHL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DABUR', 'DABUR', 'DABUR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CUMMINSIND', 'CUMMINSIND', 'CUMMINSIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CROMPGREAV', 'CROMPGREAV', 'CROMPGREAV', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CORPBANK', 'CORPBANK', 'CORPBANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CONCOR', 'CONCOR', 'CONCOR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('COLPAL', 'COLPAL', 'COLPAL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CHENNPETRO', 'CHENNPETRO', 'CHENNPETRO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CHAMBLFERT', 'CHAMBLFERT', 'CHAMBLFERT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CESC', 'CESC', 'CESC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CENTRALBK', 'CENTRALBK', 'CENTRALBK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('CAIRN', 'CAIRN', 'CAIRN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BRFL', 'BRFL', 'BRFL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BPCL', 'BPCL', 'BPCL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BOSCHLTD', 'BOSCHLTD', 'BOSCHLTD', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BOMDYEING', 'BOMDYEING', 'BOMDYEING', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BIRLACORPN', 'BIRLACORPN', 'BIRLACORPN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BIOCON', 'BIOCON', 'BIOCON', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BHUSANSTL', 'BHUSANSTL', 'BHUSANSTL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BHARATFORG', 'BHARATFORG', 'BHARATFORG', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BEL', 'BEL', 'BEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BATAINDIA', 'BATAINDIA', 'BATAINDIA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BANKINDIA', 'BANKINDIA', 'BANKINDIA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BANKBARODA', 'BANKBARODA', 'BANKBARODA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BALRAMCHIN', 'BALRAMCHIN', 'BALRAMCHIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BALLARPUR', 'BALLARPUR', 'BALLARPUR', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BALAJITELE', 'BALAJITELE', 'BALAJITELE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BAJAJHLDNG', 'BAJAJHLDNG', 'BAJAJHLDNG', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BAJAJHIND', 'BAJAJHIND', 'BAJAJHIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('BAJAJAUTO', 'BAJAJAUTO', 'BAJAJAUTO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ASIANPAINT', 'ASIANPAINT', 'ASIANPAINT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ASHOKLEY', 'ASHOKLEY', 'ASHOKLEY', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ARVIND', 'ARVIND', 'ARVIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ANDHRABANK', 'ANDHRABANK', 'ANDHRABANK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('AMTEKAUTO', 'AMTEKAUTO', 'AMTEKAUTO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('AMBUJACEM', 'AMBUJACEM', 'AMBUJACEM', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ALBK', 'ALBK', 'ALBK', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ACC', 'ACC', 'ACC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ABIRLANUVO', 'ABIRLANUVO', 'ABIRLANUVO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MCRACAL', 'MCRACAL', 'MCRACAL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('F_O', 'F_O', 'F_O', 0, 0, 0, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GENUS', 'GENUS', 'GENUS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('FORTISHELTH', 'FORTISHELTH', 'FORTISHELTH', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JINDALSWHL', 'JINDALSWHL', 'JINDALSWHL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('EXIDE', 'EXIDE', 'EXIDE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NHPC', 'NHPC', 'NHPC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GODREJIND', 'GODREJIND', 'GODREJIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ONMOBILE', 'ONMOBILE', 'ONMOBILE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MUNDRAPORT', 'MUNDRAPORT', 'MUNDRAPORT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NIFTYPUT', 'NIFTYPUT', 'NIFTYPUT', 0, 0, 0, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('COREPROJECT', 'COREPROJECT', 'COREPROJECT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ADANIPOWER', 'ADANIPOWER', 'ADANIPOWER', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SOBHA', 'SOBHA', 'SOBHA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('VIDEOIND', 'VIDEOIND', 'VIDEOIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MAX', 'MAX', 'MAX', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('GMDC', 'GMDC', 'GMDC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NMDC', 'NMDC', 'NMDC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('NIFTYCALL', 'NIFTYCALL', 'NIFTYCALL', 0, 0, 0, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('VIPIND', 'VIPIND', 'VIPIND', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ESTERINDU', 'ESTERINDU', 'ESTERINDU', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MMTC', 'MMTC', 'MMTC', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('TATACOFEE', 'TATACOFEE', 'TATACOFEE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JUBLFOOD', 'JUBLFOOD', 'JUBLFOOD', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('COALINDIA', 'COALINDIA', 'COALINDIA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ZEEL', 'ZEEL', 'ZEEL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('APOLOTYRE', 'APOLOTYRE', 'APOLOTYRE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HEXAWARE', 'HEXAWARE', 'HEXAWARE', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('AUROPHARMA', 'AUROPHARMA', 'AUROPHARMA', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ADANIENT', 'ADANIENT', 'ADANIENT', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('MNMFIN', 'MNMFIN', 'MNMFIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HINDPETRO', 'HINDPETRO', 'HINDPETRO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('INFY', 'INFY', 'INFY', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JUSTDIAL', 'JUSTDIAL', 'JUSTDIAL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('HEROMOTOCO', 'HEROMOTOCO', 'HEROMOTOCO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('SKSMICRO', 'SKSMICRO', 'SKSMICRO', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('IGL', 'IGL', 'IGL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('APOLLOHOSPIT', 'APOLLOHOSPIT', 'APOLLOHOSPIT', 0, 0, 150, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('TATAMOTERDVR', 'TATAMOTERDVR', 'TATAMOTERDVR', 0, 0, 150, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('UPL', 'UPL', 'UPL', 0, 0, 150, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('GLENMARK', 'GLENMARK', 'GLENMARK', 0, 0, 150, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('MOTHERSUMI', 'MOTHERSUMI', 'MOTHERSUMI', 0, 0, 400, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('CADILAHC', 'CADILAHC', 'CADILAHC', 0, 0, 150, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('PIDILITIND', 'PIDILITIND', 'PIDILITIND', 0, 0, 400, 1, 1, 1, 1, 1, NULL, 2, 'F_O', 1),
('TATAGLOBAL', 'TATAGLOBAL', 'TATAGLOBAL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('DHFL', 'DHFL', 'DHFL', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('ENGINERSIN', 'ENGINERSIN', 'ENGINERSIN', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1),
('JISLJALEQS', 'JISLJALEQS', 'JISLJALEQS', 0, 0, 150, 1, 1, 1, 0, 0, NULL, 2, 'F_O', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menuId` int(10) UNSIGNED NOT NULL,
  `fileToOpen` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `displayToAdmin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `displayToOperator` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `displayToClient` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `newWindow` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `newWindowName` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `newWindowPerameter` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `newexpmaster`
--

CREATE TABLE `newexpmaster` (
  `newExpMasterId` int(6) NOT NULL,
  `newExpName` varchar(30) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderId` int(6) NOT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientId2` int(6) DEFAULT NULL,
  `firstName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderDate` date DEFAULT NULL,
  `orderTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `price2` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `orderRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRemarks` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownClient` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orderType` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `orderValidity` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `orderValidTillDate` date NOT NULL DEFAULT '0000-00-00',
  `orderStatus` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `triggerPrice` float DEFAULT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refOrderId` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `otherexp`
--

CREATE TABLE `otherexp` (
  `otherexpId` int(6) NOT NULL,
  `otherExpName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherExpDate` date DEFAULT NULL,
  `otherExpAmount` float DEFAULT NULL,
  `note` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `otherExpMode` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `partybrokerage`
--

CREATE TABLE `partybrokerage` (
  `partybrokerageId` int(11) NOT NULL,
  `partyId` int(11) DEFAULT NULL,
  `brokerageDate` date DEFAULT NULL,
  `brokerage` float NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `settingsId` int(6) NOT NULL,
  `settingsKey` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`settingsId`, `settingsKey`, `value`) VALUES
(1, 'uploadFileWorks', '1'),
(2, 'expiryDisplay', 'monthOnly'),
(3, 'profitBankRate', '44'),
(4, 'lossBankRate', '44.50'),
(5, 'clientFieldInTxt', 'ownClient'),
(6, 'clientFieldInTxt', 'userRemarks'),
(7, 'takeTimeEntry', '0'),
(8, 'takeTradeNote', '0'),
(9, 'qtyInLots', '0'),
(10, 'useItemPriceRange', '1'),
(11, 'odinTxtFilePath', 'C:\\ODIN\\DIET\\OnLineBackup\\MCX\\Trades'),
(12, 'billWithLedger', '1');

-- --------------------------------------------------------

--
-- Table structure for table `standing`
--

CREATE TABLE `standing` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standingPrice` float DEFAULT NULL,
  `exchange` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `storedbhav`
--

CREATE TABLE `storedbhav` (
  `stordId` int(11) NOT NULL,
  `storDate` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tradecolor`
--

CREATE TABLE `tradecolor` (
  `id` int(11) NOT NULL,
  `tradetype` varchar(55) DEFAULT NULL,
  `sodatype` varchar(55) DEFAULT NULL,
  `color` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tradecolor`
--

INSERT INTO `tradecolor` (`id`, `tradetype`, `sodatype`, `color`) VALUES
(1, 'SHARE', 'Buy', '4A2EFF'),
(2, 'SHARE', 'Sell', 'FF2643');

-- --------------------------------------------------------

--
-- Table structure for table `tradetxt`
--

CREATE TABLE `tradetxt` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clientId2` int(6) DEFAULT NULL,
  `firstName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName2` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `price2` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRemarks` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownClient` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `exchange` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refTradeId` int(10) DEFAULT NULL,
  `selfRefId` int(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tradetxtv1`
--

CREATE TABLE `tradetxtv1` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRemarks` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownClient` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `clientId` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deletepassword` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `userType` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`clientId`, `name`, `password`, `deletepassword`, `userType`) VALUES
('', 'shree', 'om', '', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `vendorId` int(6) NOT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `phone` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(22) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deposit` float DEFAULT NULL,
  `currentBal` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`vendorId`, `vendor`, `firstName`, `middleName`, `lastName`, `address`, `phone`, `mobile`, `fax`, `email`, `deposit`, `currentBal`) VALUES
(6, '_SELF', '_SELF', '', '', '', '', '', '', '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendorbrok`
--

CREATE TABLE `vendorbrok` (
  `clientBrokId` int(6) NOT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oneSideBrok` int(6) DEFAULT NULL,
  `brok1` float DEFAULT NULL,
  `brok2` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendortemp`
--

CREATE TABLE `vendortemp` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRemarks` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownClient` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendortrades`
--

CREATE TABLE `vendortrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT '0',
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `userRemarks` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ownClient` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zcxexpiry`
--

CREATE TABLE `zcxexpiry` (
  `expiryId` int(6) NOT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zcxitem`
--

CREATE TABLE `zcxitem` (
  `itemId` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oneSideBrok` float DEFAULT '0',
  `mulAmount` float DEFAULT '0',
  `minQty` float DEFAULT NULL,
  `brok` int(6) DEFAULT '1',
  `brok2` int(6) DEFAULT '1',
  `per` int(6) DEFAULT '1',
  `unit` int(6) DEFAULT '1',
  `min` int(6) DEFAULT '1',
  `priceOn` int(6) DEFAULT '1',
  `priceUnit` int(6) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zcxmember`
--

CREATE TABLE `zcxmember` (
  `zCxMemberId` int(6) NOT NULL,
  `userId` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `memberId` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zcxstanding`
--

CREATE TABLE `zcxstanding` (
  `standingId` int(6) NOT NULL,
  `standingDtCurrent` date DEFAULT NULL,
  `standingDtNext` date DEFAULT NULL,
  `itemIdExpiryDate` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standingPrice` float DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zcxtrades`
--

CREATE TABLE `zcxtrades` (
  `tradeId` int(6) NOT NULL,
  `standing` tinyint(1) DEFAULT NULL,
  `clientId` int(6) DEFAULT '0',
  `firstName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buySell` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `itemId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeDate` date DEFAULT NULL,
  `tradeTime` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` int(6) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `brok` int(6) DEFAULT NULL,
  `tradeRefNo` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tradeNote` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expiryDate` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendor` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `removeFromAccount` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bankmaster`
--
ALTER TABLE `bankmaster`
  ADD PRIMARY KEY (`bankId`);

--
-- Indexes for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  ADD PRIMARY KEY (`bhavcopyid`);

--
-- Indexes for table `cashflow`
--
ALTER TABLE `cashflow`
  ADD PRIMARY KEY (`cashFlowId`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`clientId`);

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `clientexchange`
--
ALTER TABLE `clientexchange`
  ADD PRIMARY KEY (`clientexchangeId`);

--
-- Indexes for table `exchange`
--
ALTER TABLE `exchange`
  ADD PRIMARY KEY (`exchangeId`);

--
-- Indexes for table `expensemaster`
--
ALTER TABLE `expensemaster`
  ADD PRIMARY KEY (`expensemasterId`);

--
-- Indexes for table `expiry`
--
ALTER TABLE `expiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`generalId`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menuId`);

--
-- Indexes for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  ADD PRIMARY KEY (`newExpMasterId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderId`);

--
-- Indexes for table `otherexp`
--
ALTER TABLE `otherexp`
  ADD PRIMARY KEY (`otherexpId`);

--
-- Indexes for table `partybrokerage`
--
ALTER TABLE `partybrokerage`
  ADD PRIMARY KEY (`partybrokerageId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`settingsId`);

--
-- Indexes for table `standing`
--
ALTER TABLE `standing`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `tradecolor`
--
ALTER TABLE `tradecolor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tradetxt`
--
ALTER TABLE `tradetxt`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`vendorId`);

--
-- Indexes for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  ADD PRIMARY KEY (`clientBrokId`);

--
-- Indexes for table `vendortemp`
--
ALTER TABLE `vendortemp`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `vendortrades`
--
ALTER TABLE `vendortrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  ADD PRIMARY KEY (`expiryId`);

--
-- Indexes for table `zcxitem`
--
ALTER TABLE `zcxitem`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `zcxmember`
--
ALTER TABLE `zcxmember`
  ADD PRIMARY KEY (`zCxMemberId`);

--
-- Indexes for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  ADD UNIQUE KEY `standingId` (`standingId`);

--
-- Indexes for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  ADD PRIMARY KEY (`tradeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bankmaster`
--
ALTER TABLE `bankmaster`
  MODIFY `bankId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bhavcopy`
--
ALTER TABLE `bhavcopy`
  MODIFY `bhavcopyid` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cashflow`
--
ALTER TABLE `cashflow`
  MODIFY `cashFlowId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `clientId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1063;
--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6958;
--
-- AUTO_INCREMENT for table `clientexchange`
--
ALTER TABLE `clientexchange`
  MODIFY `clientexchangeId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=554;
--
-- AUTO_INCREMENT for table `exchange`
--
ALTER TABLE `exchange`
  MODIFY `exchangeId` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `expensemaster`
--
ALTER TABLE `expensemaster`
  MODIFY `expensemasterId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `expiry`
--
ALTER TABLE `expiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18468;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `generalId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menuId` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `newexpmaster`
--
ALTER TABLE `newexpmaster`
  MODIFY `newExpMasterId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otherexp`
--
ALTER TABLE `otherexp`
  MODIFY `otherexpId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partybrokerage`
--
ALTER TABLE `partybrokerage`
  MODIFY `partybrokerageId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `settingsId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `standing`
--
ALTER TABLE `standing`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tradecolor`
--
ALTER TABLE `tradecolor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tradetxt`
--
ALTER TABLE `tradetxt`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8515;
--
-- AUTO_INCREMENT for table `tradetxtv1`
--
ALTER TABLE `tradetxtv1`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `vendorId` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `vendorbrok`
--
ALTER TABLE `vendorbrok`
  MODIFY `clientBrokId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortemp`
--
ALTER TABLE `vendortemp`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendortrades`
--
ALTER TABLE `vendortrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxexpiry`
--
ALTER TABLE `zcxexpiry`
  MODIFY `expiryId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxmember`
--
ALTER TABLE `zcxmember`
  MODIFY `zCxMemberId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxstanding`
--
ALTER TABLE `zcxstanding`
  MODIFY `standingId` int(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `zcxtrades`
--
ALTER TABLE `zcxtrades`
  MODIFY `tradeId` int(6) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
