<?php /* Smarty version 2.6.10, created on 2016-03-17 11:01:11
         compiled from ajxEditLimitTradeMcx2.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', 'ajxEditLimitTradeMcx2.tpl', 80, false),array('function', 'html_options', 'ajxEditLimitTradeMcx2.tpl', 91, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type='text/javascript' src="./js/jquery.autocomplete.js"></script>
  
  
  <script type="text/javascript">
  <?php echo '
  function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "../tradeAdd3.php?exchange1=MCX";
  else if($("#exchange").val() == "Comex")
    window.location.href = "../tradeAdd3cx.php?exchange1=Comex";
  else if($("#exchange").val() == "Share")
    window.location.href = "./addTrade.php?exchange=F_O&exchange1=Share"; 
}
function askConfirm()
{
  if(document.getElementById("itemId").value==\'\'){
	  alert("Please Enter Item");
	  document.getElementById("itemId").focus();
	  return false;
  }
  else{
	  if(confirm("Are You Sure You want to Save Record?"))
	  {
		document.form1.makeTrade.value=1;
		return true;
	  }
	  else
		return false;
  }
  
}
'; ?>

  </script>
  <link rel="stylesheet" type="text/css" href="./jquery.autocomplete.css" />
  <?php echo '
  <style type="text/css">
    span  {
      color:white;
    }
    /*a  {
      color:white;
    }*/
	input:focus{color:#F60;}
	select:focus{color:#F60;}
  </style>
  '; ?>

  <title>!! MCX !! Add Trade</title>
  <style>
<?php echo '
td { font-color="white";FONT-SIZE: 16px; font-family:arial }
th { FONT-SIZE: 16px; font-family:arial }
.bluetd{ font-weight:bold; }
.redtd{ font-weight:bold; }
input {FONT-SIZE: 15px; font-weight: bold;}
select {FONT-SIZE: 15Px; font-weight: bold; }
'; ?>

</style>
</head>
<body bgColor="#<?php echo $this->_tpl_vars['buycolor']; ?>
" id="body" >
  <span><a href="index.php" style="color:#fff;">Home</a> </span>
<form name="form1" action="ajxEditLimitTradeMcx2.php?exchange=F_O&showlist=1&tradeId=<?php  echo $_GET['tradeId'] ?>&type=<?php  echo $_GET['type'] ?>" method="post" >
<input type="hidden" name="tradeHidden" value="trade" />
  <SELECT name="exchange" id="exchange" onChange="changeExchange();">
      <option value="MCX" <?php echo $this->_tpl_vars['exchangeOnlyMCX']; ?>
>MCX</option>
      <option value="Comex" <?php echo $this->_tpl_vars['exchangeOnlyComex']; ?>
>Comex</option>
    <option value="Share" <?php echo $this->_tpl_vars['exchangeOnlyShare']; ?>
>Share</option>
    </SELECT>
  <span style="color:#fff;">Date :</span>
  <?php 
		session_start();
		if (isset($_SESSION['entry_user']))
		{ ?>
		
  
  <?php echo smarty_function_html_select_date(array('time' => ($this->_tpl_vars['edit_tradeDateDisplay']),'day_value_format' => "%02d",'month_value_format' => "%m",'day_format' => "%d",'month_format' => "%m",'field_order' => 'DMY','start_year' => "-1",'end_year' => "+1",'all_extra' => "disabled=\"disabled\""), $this);?>

  <?php 	}
		else
		{ ?>
		
  
  <?php echo smarty_function_html_select_date(array('time' => ($this->_tpl_vars['edit_tradeDateDisplay']),'day_value_format' => "%02d",'month_value_format' => "%m",'day_format' => "%d",'month_format' => "%m",'field_order' => 'DMY','start_year' => "-1",'end_year' => "+1"), $this);?>

  
   <?php 		} ?>
  <?php if ($this->_tpl_vars['exchange'] == 'MCX'): ?>
  <select name="itemId" onchange="itemChange(this);">
    <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['item']['itemId'],'output' => $this->_tpl_vars['item']['item']), $this);?>

  </select>
  <?php else: ?>
  <input type="text" name="itemId" id="itemId" onkeydown="itemChange(this);" value="<?php echo $this->_tpl_vars['edit_itemIdSelected']; ?>
"/>
  <?php endif; ?>
  <select name="expiryDate" id="expiryDate">
  </select> &nbsp;
  <span>Quantity : <input type="text" name="quantity" id="quantity" size="3" value="<?php echo $this->_tpl_vars['edit_qty']; ?>
"/></span>
  <input type="hidden" name="quantityHidden" id="quantityHidden"/>
  <input type="hidden" name="min" id="min" />
  <input type="hidden" name="exchange" id="exchange" value="<?php echo $this->_tpl_vars['exchange']; ?>
" />
  <input type="hidden" name="tradeId" id="tradeId" value="<?php echo $this->_tpl_vars['tradeId']; ?>
" />
  <input type="hidden" name="refTradeId" id="refTradeId" value="<?php echo $this->_tpl_vars['refTradeId']; ?>
" />
   <?php if ($this->_tpl_vars['forStand'] == 1): ?>
    <select name="standing">
      <option name="open" value="-1">Open Standing</option>
      <option name="close" value="1">Close Standing</option>
    </select>
  <?php else: ?>
    <input type="hidden" name="standing" value="0" />
  <?php endif; ?>
  <span>Price <input type="text" name="price1" id="price1" size="6" value="<?php echo $this->_tpl_vars['edit_price']; ?>
" onkeydown="changePrice(this,event);" /></span>
  <span>Client </span>
  <input type="text" name="clientId" id="clientId" size=4 value="<?php echo $this->_tpl_vars['edit_client']; ?>
" />
  <INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();">
  <input type="hidden" name="buySell" id="buySell" value="<?php echo $this->_tpl_vars['buyOrSell']; ?>
" disabled size="10"/>
  <input type="hidden" name="buySellHidden" id="buySellHidden" value="<?php echo $this->_tpl_vars['buyOrSell']; ?>
" />
  <INPUT type="hidden" name="makeTrade" value="0">
  <input name="buy_color" value="#<?php echo $this->_tpl_vars['buycolor']; ?>
" id="buy_color" type="hidden" />
  <input name="sell_color" value="#<?php echo $this->_tpl_vars['sellcolor']; ?>
" id="sell_color" type="hidden" />
  <!--<span  ><a href="javascript:void(0);" style="color:white;" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Enter => Trade)" >Help</a></span>-->
    <?php if ($this->_tpl_vars['currentBuySell'] == 'Sell'): ?>
    <script type = "text/javascript">
      window.document.bgColor=document.form1.sell_color.value;
      document.form1.buySellText.value = "Sell";
      document.form1.buySell.value = "Sell";
    </script>
  <?php endif; ?>
  <br/>
  <br/>

<script type="text/javascript">
  var item = new Array();
  <?php $_from = $this->_tpl_vars['item']['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['id'] => $this->_tpl_vars['itemName']):
?>
    item[<?php echo $this->_tpl_vars['id']; ?>
] = '<?php echo $this->_tpl_vars['itemName']; ?>
';
  <?php endforeach; endif; unset($_from); ?>
  <?php echo '
  $(document).ready(function()
  {
	if($("#showlist").val()==\'0\'){
		$("#list").hide();
		$("#hide_show").val("Show List");
	}else{
		$("#list").show();
		$("#hide_show").val("Hide List");
	}
    if($("#hide_show").val() == "Show List")
	{
		//$("#list").hide();
		//$("#showlist").val(\'0\');
	}
	$("#hide_show").click(function()
	{
		if($("#hide_show").val() == "Show List")
		{
			$("#list").show();
			$("#showlist").val(\'1\');
			$("#hide_show").val("Hide List");
		}
		else
		{
			$("#list").hide();
			$("#showlist").val(\'0\');
			$("#hide_show").val("Show List");
		}
	});
	
	$("#clientId").keyup(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
      $("#clientIdDiv").html("");
      <?php unset($this->_sections['secClient1']);
$this->_sections['secClient1']['name'] = 'secClient1';
$this->_sections['secClient1']['loop'] = is_array($_loop=$this->_tpl_vars['clientIdValues']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secClient1']['show'] = true;
$this->_sections['secClient1']['max'] = $this->_sections['secClient1']['loop'];
$this->_sections['secClient1']['step'] = 1;
$this->_sections['secClient1']['start'] = $this->_sections['secClient1']['step'] > 0 ? 0 : $this->_sections['secClient1']['loop']-1;
if ($this->_sections['secClient1']['show']) {
    $this->_sections['secClient1']['total'] = $this->_sections['secClient1']['loop'];
    if ($this->_sections['secClient1']['total'] == 0)
        $this->_sections['secClient1']['show'] = false;
} else
    $this->_sections['secClient1']['total'] = 0;
if ($this->_sections['secClient1']['show']):

            for ($this->_sections['secClient1']['index'] = $this->_sections['secClient1']['start'], $this->_sections['secClient1']['iteration'] = 1;
                 $this->_sections['secClient1']['iteration'] <= $this->_sections['secClient1']['total'];
                 $this->_sections['secClient1']['index'] += $this->_sections['secClient1']['step'], $this->_sections['secClient1']['iteration']++):
$this->_sections['secClient1']['rownum'] = $this->_sections['secClient1']['iteration'];
$this->_sections['secClient1']['index_prev'] = $this->_sections['secClient1']['index'] - $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['index_next'] = $this->_sections['secClient1']['index'] + $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['first']      = ($this->_sections['secClient1']['iteration'] == 1);
$this->_sections['secClient1']['last']       = ($this->_sections['secClient1']['iteration'] == $this->_sections['secClient1']['total']);
?>
      if(this.value == <?php echo $this->_tpl_vars['clientIdValues'][$this->_sections['secClient1']['index']]; ?>
)
        $("#clientIdDiv").html("<?php echo $this->_tpl_vars['clientIdOutput'][$this->_sections['secClient1']['index']]; ?>
");
      <?php endfor; endif; ?>
      if(document.getElementById("clientIdDiv").innerHTML == "")
        $("#clientIdDiv").html("Client not available");
      <?php endif; ?>
      <?php echo '
    });
    $("#clientId2").keyup(function()
    {
      '; ?>

      <?php if ($this->_tpl_vars['clientIdAskInTextBox'] == 1): ?>
      $("#clientIdDiv2").html("");
      <?php unset($this->_sections['secClient1']);
$this->_sections['secClient1']['name'] = 'secClient1';
$this->_sections['secClient1']['loop'] = is_array($_loop=$this->_tpl_vars['clientIdValues']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['secClient1']['show'] = true;
$this->_sections['secClient1']['max'] = $this->_sections['secClient1']['loop'];
$this->_sections['secClient1']['step'] = 1;
$this->_sections['secClient1']['start'] = $this->_sections['secClient1']['step'] > 0 ? 0 : $this->_sections['secClient1']['loop']-1;
if ($this->_sections['secClient1']['show']) {
    $this->_sections['secClient1']['total'] = $this->_sections['secClient1']['loop'];
    if ($this->_sections['secClient1']['total'] == 0)
        $this->_sections['secClient1']['show'] = false;
} else
    $this->_sections['secClient1']['total'] = 0;
if ($this->_sections['secClient1']['show']):

            for ($this->_sections['secClient1']['index'] = $this->_sections['secClient1']['start'], $this->_sections['secClient1']['iteration'] = 1;
                 $this->_sections['secClient1']['iteration'] <= $this->_sections['secClient1']['total'];
                 $this->_sections['secClient1']['index'] += $this->_sections['secClient1']['step'], $this->_sections['secClient1']['iteration']++):
$this->_sections['secClient1']['rownum'] = $this->_sections['secClient1']['iteration'];
$this->_sections['secClient1']['index_prev'] = $this->_sections['secClient1']['index'] - $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['index_next'] = $this->_sections['secClient1']['index'] + $this->_sections['secClient1']['step'];
$this->_sections['secClient1']['first']      = ($this->_sections['secClient1']['iteration'] == 1);
$this->_sections['secClient1']['last']       = ($this->_sections['secClient1']['iteration'] == $this->_sections['secClient1']['total']);
?>
      if(this.value == <?php echo $this->_tpl_vars['clientIdValues'][$this->_sections['secClient1']['index']]; ?>
)
        $("#clientIdDiv2").html("<?php echo $this->_tpl_vars['clientIdOutput'][$this->_sections['secClient1']['index']]; ?>
");
      <?php endfor; endif; ?>
      if(document.getElementById("clientIdDiv2").innerHTML == "")
        $("#clientIdDiv2").html("Client not available");
      <?php endif; ?>
      <?php echo '
    });
    $("#itemId").autocomplete(item,
    {
      minChars: 0,
      max: 30,
      autoFill: true,
      mustMatch: true,
      matchContains: false,
      scrollHeight: 220,
      formatItem: function(data, i, total)
      {
        return data[0];
      }
    });
    $("#lot").keydown(function()
    {
      minQuantityFunc(this.value);
    });
    $("#price1").blur(function()
    {
      //$("#price2").val(this.value);
      '; ?>

      <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['rangeStart']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
      if((document.form1.price1.value >= <?php echo $this->_tpl_vars['item']['rangeStart'][$this->_sections['sec']['index']]; ?>
) && (document.form1.price1.value <= <?php echo $this->_tpl_vars['item']['rangeEnd'][$this->_sections['sec']['index']]; ?>
))<?php echo '
      {
        '; ?>

        document.form1.min.value = <?php echo $this->_tpl_vars['item']['min'][$this->_sections['sec']['index']]; ?>
;
        <?php if ($this->_tpl_vars['exchange'] == 'MCX'): ?>
        selectOptionByValue(document.form1.itemId, "<?php echo $this->_tpl_vars['item']['itemId'][$this->_sections['sec']['index']]; ?>
");
        minQuantityFunc(document.form1.lot.value);
        <?php endif; ?>
        <?php echo '
      }
      '; ?>

      <?php endfor; endif; ?>
      <?php echo '
    });
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      '; ?>

      <?php if ($this->_tpl_vars['exchange'] == 'MCX'): ?>
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      <?php else: ?>
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      <?php endif; ?>
      <?php echo '

      if(code == 109)
      {
        $("#body").css("background-color",$("#sell_color").val());
        $("#buySell").val("Sell");
        $("#buySellHidden").val("Sell");
        return false;
      }
      if(code == 107)
      {
        $("#body").css("background-color",$("#buy_color").val());
        $("#buySell").val("Buy");
        $("#buySellHidden").val("Buy");
        return false;
      }
    });

  });
  function changePrice(theObject,event)
  {
    var changePriceCode = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    if(changePriceCode == 38)
      theObject.value = parseInt(theObject.value)+1;
    if(changePriceCode == 40)
      theObject.value = parseInt(theObject.value)-1;
    if(theObject.name == "lot")
      plusOrMinusValue = 5;
    else
      plusOrMinusValue = 10;
    if(changePriceCode == 33)
      theObject.value = parseInt(theObject.value)+plusOrMinusValue;
    if(changePriceCode == 34)
      theObject.value = parseInt(theObject.value)-plusOrMinusValue;
  }
  function minQuantityFunc(lot)
  {
    document.form1.quantity.value = lot*document.form1.min.value;
    document.form1.quantityHidden.value = lot*document.form1.min.value;
  }
  function itemChange(theObject)
  {
    var form   = theObject.form;
    itemId     = document.form1.itemId;
    expiryDate = document.form1.expiryDate;
    expiryDate.options.length = 0;
    '; ?>

    <?php if ($this->_tpl_vars['exchange'] == 'MCX'): ?>
    <?php unset($this->_sections['itemSec']);
$this->_sections['itemSec']['name'] = 'itemSec';
$this->_sections['itemSec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['itemId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['itemSec']['show'] = true;
$this->_sections['itemSec']['max'] = $this->_sections['itemSec']['loop'];
$this->_sections['itemSec']['step'] = 1;
$this->_sections['itemSec']['start'] = $this->_sections['itemSec']['step'] > 0 ? 0 : $this->_sections['itemSec']['loop']-1;
if ($this->_sections['itemSec']['show']) {
    $this->_sections['itemSec']['total'] = $this->_sections['itemSec']['loop'];
    if ($this->_sections['itemSec']['total'] == 0)
        $this->_sections['itemSec']['show'] = false;
} else
    $this->_sections['itemSec']['total'] = 0;
if ($this->_sections['itemSec']['show']):

            for ($this->_sections['itemSec']['index'] = $this->_sections['itemSec']['start'], $this->_sections['itemSec']['iteration'] = 1;
                 $this->_sections['itemSec']['iteration'] <= $this->_sections['itemSec']['total'];
                 $this->_sections['itemSec']['index'] += $this->_sections['itemSec']['step'], $this->_sections['itemSec']['iteration']++):
$this->_sections['itemSec']['rownum'] = $this->_sections['itemSec']['iteration'];
$this->_sections['itemSec']['index_prev'] = $this->_sections['itemSec']['index'] - $this->_sections['itemSec']['step'];
$this->_sections['itemSec']['index_next'] = $this->_sections['itemSec']['index'] + $this->_sections['itemSec']['step'];
$this->_sections['itemSec']['first']      = ($this->_sections['itemSec']['iteration'] == 1);
$this->_sections['itemSec']['last']       = ($this->_sections['itemSec']['iteration'] == $this->_sections['itemSec']['total']);
?>
    if(itemId.selectedIndex == <?php echo $this->_sections['itemSec']['index']; ?>
)
    <?php echo '
    {
      '; ?>

      <?php unset($this->_sections['expiryDateSec']);
$this->_sections['expiryDateSec']['name'] = 'expiryDateSec';
$this->_sections['expiryDateSec']['loop'] = is_array($_loop=$this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['expiryDateSec']['show'] = true;
$this->_sections['expiryDateSec']['max'] = $this->_sections['expiryDateSec']['loop'];
$this->_sections['expiryDateSec']['step'] = 1;
$this->_sections['expiryDateSec']['start'] = $this->_sections['expiryDateSec']['step'] > 0 ? 0 : $this->_sections['expiryDateSec']['loop']-1;
if ($this->_sections['expiryDateSec']['show']) {
    $this->_sections['expiryDateSec']['total'] = $this->_sections['expiryDateSec']['loop'];
    if ($this->_sections['expiryDateSec']['total'] == 0)
        $this->_sections['expiryDateSec']['show'] = false;
} else
    $this->_sections['expiryDateSec']['total'] = 0;
if ($this->_sections['expiryDateSec']['show']):

            for ($this->_sections['expiryDateSec']['index'] = $this->_sections['expiryDateSec']['start'], $this->_sections['expiryDateSec']['iteration'] = 1;
                 $this->_sections['expiryDateSec']['iteration'] <= $this->_sections['expiryDateSec']['total'];
                 $this->_sections['expiryDateSec']['index'] += $this->_sections['expiryDateSec']['step'], $this->_sections['expiryDateSec']['iteration']++):
$this->_sections['expiryDateSec']['rownum'] = $this->_sections['expiryDateSec']['iteration'];
$this->_sections['expiryDateSec']['index_prev'] = $this->_sections['expiryDateSec']['index'] - $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['index_next'] = $this->_sections['expiryDateSec']['index'] + $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['first']      = ($this->_sections['expiryDateSec']['iteration'] == 1);
$this->_sections['expiryDateSec']['last']       = ($this->_sections['expiryDateSec']['iteration'] == $this->_sections['expiryDateSec']['total']);
?>
      expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
] = new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']][$this->_sections['expiryDateSec']['index']]; ?>
","<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['itemSec']['index']][$this->_sections['expiryDateSec']['index']]; ?>
");
      expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
].selected = true;
      <?php endfor; endif; ?>
      <?php echo '
    }
    '; ?>

    <?php endfor; endif; ?>
    <?php else: ?>
    <?php unset($this->_sections['expiryDateSec']);
$this->_sections['expiryDateSec']['name'] = 'expiryDateSec';
$this->_sections['expiryDateSec']['loop'] = is_array($_loop=$this->_tpl_vars['expiryDate']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['expiryDateSec']['show'] = true;
$this->_sections['expiryDateSec']['max'] = $this->_sections['expiryDateSec']['loop'];
$this->_sections['expiryDateSec']['step'] = 1;
$this->_sections['expiryDateSec']['start'] = $this->_sections['expiryDateSec']['step'] > 0 ? 0 : $this->_sections['expiryDateSec']['loop']-1;
if ($this->_sections['expiryDateSec']['show']) {
    $this->_sections['expiryDateSec']['total'] = $this->_sections['expiryDateSec']['loop'];
    if ($this->_sections['expiryDateSec']['total'] == 0)
        $this->_sections['expiryDateSec']['show'] = false;
} else
    $this->_sections['expiryDateSec']['total'] = 0;
if ($this->_sections['expiryDateSec']['show']):

            for ($this->_sections['expiryDateSec']['index'] = $this->_sections['expiryDateSec']['start'], $this->_sections['expiryDateSec']['iteration'] = 1;
                 $this->_sections['expiryDateSec']['iteration'] <= $this->_sections['expiryDateSec']['total'];
                 $this->_sections['expiryDateSec']['index'] += $this->_sections['expiryDateSec']['step'], $this->_sections['expiryDateSec']['iteration']++):
$this->_sections['expiryDateSec']['rownum'] = $this->_sections['expiryDateSec']['iteration'];
$this->_sections['expiryDateSec']['index_prev'] = $this->_sections['expiryDateSec']['index'] - $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['index_next'] = $this->_sections['expiryDateSec']['index'] + $this->_sections['expiryDateSec']['step'];
$this->_sections['expiryDateSec']['first']      = ($this->_sections['expiryDateSec']['iteration'] == 1);
$this->_sections['expiryDateSec']['last']       = ($this->_sections['expiryDateSec']['iteration'] == $this->_sections['expiryDateSec']['total']);
?>
    expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
] = new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['expiryDateSec']['index']]; ?>
","<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['expiryDateSec']['index']]; ?>
");
    expiryDate.options[<?php echo $this->_sections['expiryDateSec']['index']; ?>
].selected = true;
    <?php endfor; endif; ?>
    <?php endif; ?>
    <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['item']['itemId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
    if((document.form1.itemId.value == "<?php echo $this->_tpl_vars['item']['itemId'][$this->_sections['sec']['index']]; ?>
"))<?php echo '
    {
      '; ?>

      document.form1.min.value = <?php echo $this->_tpl_vars['item']['min'][$this->_sections['sec']['index']]; ?>
;
      minQuantityFunc(document.form1.lot.value);
      <?php echo '
    }
    '; ?>

    <?php endfor; endif; ?>
    <?php echo '
  }
  function selectOptionByValue(selObj, val)
  {
    var A = selObj.options, L = A.length;
    while(L)
    {
      if (A[--L].value == val)
      {
        selObj.selectedIndex = L;
        L = 0;
        break;
      }
    }
    itemChange(document.form1.itemId);
  }
  '; ?>

  $('#clientId').focus();
  itemChange(document.form1.itemId);
</script>
  
  </form>
</body>
</html>