<?php /* Smarty version 2.6.10, created on 2016-02-18 13:24:10
         compiled from itemAdd.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'itemAdd.tpl', 32, false),)), $this); ?>
<HTML>
<HEAD><TITLE>Brokerage settings</TITLE>
<?php echo '
<script type="text/javascript">
function alphanumeric() 
{
  if( (event.keyCode >= 47 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) 
       || (event.keyCode >= 97 && event.keyCode <= 122) || (event.keyCode == 95) ) 
  {
    return true;
  }
  else
  {
    alert("Please Enter Only Alphanumeric");
    return false;
  }
    
}
</script>
'; ?>

</HEAD>
<BODY bgColor="#FFCEE7">
<CENTER>
  <FORM name="form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" METHOD="post">
  <B><A href="./index.php">Home</A></B><BR><BR>
  <B><A href="brokerage.php">Item List</A></B><BR><BR>
  <TABLE border="1" cellSpacing="0" cellPadding="2">
  <TR>
    <TD colspan="8" align="center">
      <select name="exchange">
      <?php unset($this->_sections['sec']);
$this->_sections['sec']['name'] = 'sec';
$this->_sections['sec']['loop'] = is_array($_loop=$this->_tpl_vars['exchangeId']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec']['show'] = true;
$this->_sections['sec']['max'] = $this->_sections['sec']['loop'];
$this->_sections['sec']['step'] = 1;
$this->_sections['sec']['start'] = $this->_sections['sec']['step'] > 0 ? 0 : $this->_sections['sec']['loop']-1;
if ($this->_sections['sec']['show']) {
    $this->_sections['sec']['total'] = $this->_sections['sec']['loop'];
    if ($this->_sections['sec']['total'] == 0)
        $this->_sections['sec']['show'] = false;
} else
    $this->_sections['sec']['total'] = 0;
if ($this->_sections['sec']['show']):

            for ($this->_sections['sec']['index'] = $this->_sections['sec']['start'], $this->_sections['sec']['iteration'] = 1;
                 $this->_sections['sec']['iteration'] <= $this->_sections['sec']['total'];
                 $this->_sections['sec']['index'] += $this->_sections['sec']['step'], $this->_sections['sec']['iteration']++):
$this->_sections['sec']['rownum'] = $this->_sections['sec']['iteration'];
$this->_sections['sec']['index_prev'] = $this->_sections['sec']['index'] - $this->_sections['sec']['step'];
$this->_sections['sec']['index_next'] = $this->_sections['sec']['index'] + $this->_sections['sec']['step'];
$this->_sections['sec']['first']      = ($this->_sections['sec']['iteration'] == 1);
$this->_sections['sec']['last']       = ($this->_sections['sec']['iteration'] == $this->_sections['sec']['total']);
?>
        <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['exchangeId'][$this->_sections['sec']['index']],'output' => $this->_tpl_vars['exchange'][$this->_sections['sec']['index']]), $this);?>

      <?php endfor; endif; ?>
      </select>
   </TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">Item</FONT></TD>
    <TD align="center"><INPUT type='text' name='item' size='15' onKeyPress="return alphanumeric();"></TD>
        <INPUT type='hidden' name='brok' value='0' size='15'>
        <INPUT type='hidden' name='brok2' value='0' size='15'>
        <INPUT type='hidden' name='priceOn'value='1' size='15'>
      
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">OneSideBrok </FONT></TD>
    <TD align="center"><INPUT type='text' name='oneSideBrok' size='15'></TD>
    <TD align="center"><B>For example, for Gold : </B></TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta">Minimum </FONT></TD>
    <TD align="center"><INPUT type='text' name='min' size='15'></TD>
    <TD align="center"><FONT color="red">100</FONT></TD>
  </TR>
  <TR>
    <TD align="center"><FONT color="DarkMagenta"> Price Range Start </FONT></TD>
    <TD align="center"><INPUT type='text' name='rangeStart' size='15' value="0"></TD>
    <TD align="center"><FONT color="red">5800</FONT></TD>
  </TR>
  <TR>
    <TD align="center"> <FONT color="DarkMagenta">Price Range End </FONT></TD>
    <TD align="center"><INPUT type='text' name='rangeEnd' size='15' value="0"></TD>
    <TD align="center"><FONT color="red">6999</FONT></TD>
  </TR>
   <TR>
    <TD align="center"> <FONT color="DarkMagenta">Multiply</FONT></TD>
    <TD align="center"><INPUT type='text' name='multiply' size='15' value='1'></TD>
    <TD align="center"><FONT color="red">&nbsp;</FONT></TD>
  </TR>
  <TR>
    <TD align="center"><INPUT type="submit" name="submitBtn" value="Submit!"></TD>
    <TD align="center"><INPUT type="reset" value="Reset"></TD>
    <TD align="center"></TD>
  </TR>
  </TABLE>
 <SCRIPT language="javascript">document.form1.item.focus();</SCRIPT>
  </FORM>
</CENTER>
</BODY>
</HTML>