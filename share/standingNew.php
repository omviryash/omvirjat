<?php
session_start();
if(!isset($_SESSION['toDate'])) 
  header("Location:selectDtSession.php?goTo=standing");
else
{
  //include om_config.inc
  include "etc/om_config.inc";
    //declare smarty class to object
  $smarty  = new SmartyWWW();
  $exchangeArray = array();
  $selectedExchange = 0;
  
  // if post :Start
  if(isset($_POST['expiryId']) && isset($_POST['submitBtn']))
  {
  	$closeDate = $_POST['standingDtCurrentYear']."-".$_POST['standingDtCurrentMonth']."-".$_POST['standingDtCurrentDay'];
  	$openDate  = $_POST['standingDtNextYear']."-".$_POST['standingDtNextMonth']."-".$_POST['standingDtNextDay'];
    while(list($key,$val) = each ($_POST['expiryId']) )
    {
      //if value greate than 0 than insert
      if($_POST['expiryId'][$key] > 0)
      {
        $standingPrice = $_POST['expiryId'][$key];
        
      	$selectQuery = "SELECT * FROM expiry WHERE expiryId = '".$key."'";
      	$selectQueryResult = mysql_query($selectQuery);
      	while($expiryRow = mysql_fetch_array($selectQueryResult))
      	{
      		$itemIdPost = $expiryRow['itemId'];
      		$expiryDatePost = $expiryRow['expiryDate'];
      		$exchangePost = $expiryRow['exchange'];
      	}
        $itemIdExpiryDate = $itemIdPost."".$expiryDatePost;
        
        $selectStandingQuery = "SELECT * FROM standing
                                 WHERE standingDtCurrent = '".$closeDate."'
                                   AND itemIdExpiryDate = '".$itemIdExpiryDate."'
                                   AND exchange = '".$exchangePost."'";
        $selectStandingResult = mysql_query($selectStandingQuery);
        if($selectStandingRow = mysql_fetch_assoc($selectStandingResult))
        {
          $updateQuery = "UPDATE standing SET standingDtCurrent = '".$closeDate."',standingDtNext = '".$openDate."',
                                 standingPrice = '".$standingPrice."',exchange = '".$exchangePost."'
                           WHERE standingDtCurrent = '".$closeDate."'
                             AND itemIdExpiryDate = '".$itemIdExpiryDate."'
                             AND exchange = '".$exchangePost."'";
          $updateQueryResult = mysql_query($updateQuery);
          if(!$updateQueryResult)
            echo "Error".mysql_error();
        }
        else
        {
          // inserting Record :Start
          $insertQuery = "INSERT INTO standing (standingDtCurrent,standingDtNext,itemIdExpiryDate,
                                                standingPrice,exchange)
                                        VALUES ('".$closeDate."','".$openDate."','".$itemIdExpiryDate."',
                                                '".$standingPrice."','".$exchangePost."')";
          $insertQueryResult = mysql_query($insertQuery);
          if(!$insertQueryResult)
            echo "Error".mysql_error();
          // inserting Record :End
        }
        $updateTradetxtQuery = "UPDATE tradetxt SET price = '".$standingPrice."'
                         WHERE (tradeDate      = '".$closeDate."'
                                AND itemId     = '".$itemIdPost."'
                                AND expiryDate = '".$expiryDatePost."'
                                AND exchange   = '".$exchangePost."'
                                AND standing   = 1)
                            OR (tradeDate      = '".$openDate."'
                                AND itemId     = '".$itemIdPost."'
                                AND expiryDate = '".$expiryDatePost."'
                                AND exchange   = '".$exchangePost."'
                                AND standing   = -1)";
        $updateTradetxtQueryResult = mysql_query($updateTradetxtQuery);
        if(!$updateTradetxtQueryResult)
          echo "Error".mysql_error();
      }
    }
    header("Location: ./standingNamesDirect.php");
  }
  // if post :End
  
//displayItems :Start
  $displayItems = array();
  $displayItemsSelected = isset($_POST['displayItems'])? $_POST['displayItems'] : "Pending";
  $displayItems[0] = "Pending";
  $displayItems[1] = "All";
//displayItems :End

  if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 7 )
    $selectedExchange = "F_O";
  else if(isset($_SESSION['ses_decidePackFor']) && $_SESSION['ses_decidePackFor'] == 6 )
    $selectedExchange = "MCX";
  else
    $selectedExchange = "All";
  if(isset($_POST['exchange']))
    $selectedExchange = $_POST['exchange'];
    
  include "./clientTradesIncludeMe.php";
  
  // Exchange Combo :Start
  $selectExchang = "SELECT * FROM exchange
                     ORDER BY exchange";
  $selectExchangResult = mysql_query($selectExchang);
  $a=0;
  while($row = mysql_fetch_array($selectExchangResult))
  {
    $exchangeArray['id'][$a]   = $row['exchangeId'];
    $exchangeArray['name'][$a] = $row['exchange'];
    $a++;
  }
  // Exchange Combo :End
  
  // select Query :Start
  $selectQuery = "SELECT * FROM expiry";
  
  if($selectedExchange != "All")
    $selectQuery .= " WHERE exchange = '".$selectedExchange."'";
                  
  $selectQuery .= " ORDER BY exchange, itemId, expiryDate";
  $selectQueryResult = mysql_query($selectQuery);
  $i = 0;
  $standingArray = array();
  while($row = mysql_fetch_array($selectQueryResult))
  {
    //create 2 dimentional array
    $displayItemsSelected;
    if($displayItemsSelected == "All" || isset($isPendingArray[$row['itemId'].$row['expiryDate']]))
    {
    	$standingArray[$i]['expiryId']   = $row['expiryId'];
    	$standingArray[$i]['exchange']   = $row['exchange'];
    	$standingArray[$i]['itemId']     = $row['itemId'];
    	$standingArray[$i]['expiryDate'] = $row['expiryDate'];
    	$i++;
    }
  }
  // select Query :End
  
  $standingDtNextSelected = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
  $smarty->assign("goTo","standing");
  $smarty->assign("displayItems",$displayItems);
  $smarty->assign("displayItemsSelected",$displayItemsSelected);
  $smarty->assign("standingDtNextSelected",$standingDtNextSelected);
  $smarty->assign("standingArray",$standingArray);
  $smarty->assign("PHP_SELF",$_SERVER['PHP_SELF']);
  $smarty->assign("exchange",$exchangeArray);
  $smarty->assign("selectedExchange",$selectedExchange);
  $smarty->display("standing.tpl");
include "./bottom.php";
}
?>