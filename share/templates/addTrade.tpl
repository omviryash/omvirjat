<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <script type="text/javascript" src="./js/jquery.js"></script>
  <script type='text/javascript' src="./js/jquery.autocomplete.js"></script>
  
  
  <script type="text/javascript">
  {literal}
  function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "../tradeAdd3.php?exchange1=MCX";
  else if($("#exchange").val() == "Comex")
    window.location.href = "../tradeAdd3cx.php?exchange1=Comex";
  else if($("#exchange").val() == "Share")
    window.location.href = "./addTrade.php?exchange=F_O&exchange1=Share"; 
}
function askConfirm()
{
  if(document.getElementById("itemId").value==''){
	  alert("Please Enter Item");
	  document.getElementById("itemId").focus();
	  return false;
  }
  else{
	  if(confirm("Are You Sure You want to Save Record?"))
	  {
		document.form1.makeTrade.value=1;
		return true;
	  }
	  else
		return false;
  }
  
}
{/literal}
  </script>
  <link rel="stylesheet" type="text/css" href="./jquery.autocomplete.css" />
  {literal}
  <style type="text/css">
    span  {
      color:white;
    }
    /*a  {
      color:white;
    }*/
	
	input:focus{color:#F0F;}
	select:focus{color:#F0F;}
  </style>
  {/literal}
  <title>!! SHARE !! Add Trade</title>
  <style>
{literal}
td { font-color="white";FONT-SIZE: 16px; font-family:arial }
th { FONT-SIZE: 16px; font-family:arial }
.bluetd{font-weight:bold;}
.redtd{ font-weight:bold; }
input {FONT-SIZE: 15px; font-weight: bold;}
select {FONT-SIZE: 15Px; font-weight: bold; }
{/literal}
</style>
</head>
<body bgColor="#{$buycolor}" id="body" >
  <span><a href="index.php" style="color:#fff;">Home</a> </span>
<form name="form1" action="addTrade.php?exchange=F_O" method="post" >
<input type="hidden" name="tradeHidden" value="trade" />
  <SELECT name="exchange" id="exchange" onChange="changeExchange();">
      <option value="MCX" {$exchangeOnlyMCX}>MCX</option>
      <option value="Comex" {$exchangeOnlyComex}>Comex</option>
    <option value="Share" {$exchangeOnlyShare}>Share</option>
    </SELECT>
  <span style="color:#fff;">Date :</span>
  {php}
		session_start();
		if (isset($_SESSION['entry_user']))
		{{/php}
		
  
  {html_select_date day_value_format="%02d" month_value_format="%m" day_format="%d" month_format="%m" field_order="DMY" start_year="-1" end_year="+1" all_extra="disabled=\"disabled\""}
  {php}	}
		else
		{{/php}
		
  
  {html_select_date day_value_format="%02d" month_value_format="%m" day_format="%d" month_format="%m" field_order="DMY" start_year="-1" end_year="+1"}
  
   {php}		}{/php}
  {if $exchange == "MCX"}
  <select name="itemId" onchange="itemChange(this);">
    {html_options values=$item.itemId output=$item.item}
  </select>
  {else}
  <input type="text" name="itemId" id="itemId" onkeydown="itemChange(this);"/>
  {/if}
  <select name="expiryDate" id="expiryDate">
  </select> &nbsp;
  <span>Quantity : <input type="text" name="quantity" id="quantity" size="3" /></span>
  <input type="hidden" name="quantityHidden" id="quantityHidden"/>
  <input type="hidden" name="min" id="min" />
  <input type="hidden" name="exchange" id="exchange" value="{$exchange}" />
  <input type="hidden" name="tradeId" id="tradeId" value="{$tradeId}" />
  <input type="hidden" name="refTradeId" id="refTradeId" value="{$refTradeId}" />
   {if $forStand == 1}
    <select name="standing">
      <option name="open" value="-1">Open Standing</option>
      <option name="close" value="1">Close Standing</option>
    </select>
  {else}
    <input type="hidden" name="standing" value="0" />
  {/if}
  <span>Price <input type="text" name="price1" id="price1" size="6" onkeydown="changePrice(this,event);" /></span>
  <span>Client </span>
  <input type="text" name="clientId" id="clientId" size=4 />
  <INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();">
  <INPUT type="button" id="hide_show" name="hide_show" value="Show List">
  <INPUT type="button" id="edit_delete" name="edit_delete" value="Delete" style="display:none;">
  <INPUT type="submit" id="btn_delete" onClick="return confirm('Are you sure?');" name="btn_delete" value="Delete"></br>
  <input type="hidden" name="buySell" id="buySell" value="{$buyOrSell}" disabled size="10"/>
  <input type="hidden" name="buySellHidden" id="buySellHidden" value="{$buyOrSell}" />
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="showlist" value="{$showlist}" id="showlist">
  <input name="buy_color" value="#{$buycolor}" id="buy_color" type="hidden" />
  <input name="sell_color" value="#{$sellcolor}" id="sell_color" type="hidden" />
  <!--<span  ><a href="javascript:void(0);" style="color:white;" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Enter => Trade)" >Help</a></span>-->
    {if $currentBuySell == "Sell"}
    <script type = "text/javascript">
      window.document.bgColor=document.form1.sell_color.value;
      document.form1.buySellText.value = "Sell";
      document.form1.buySell.value = "Sell";
    </script>
  {/if}
  <br/>
  <br/>

<script type="text/javascript">
  var item = new Array();
  {foreach from=$item.item item=itemName key=id}
    item[{$id}] = '{$itemName}';
  {/foreach}
  {literal}
  $(document).ready(function()
  {
	if($("#showlist").val()=='0'){
		$("#list").hide();
		$("#hide_show").val("Show List");
	}else{
		$("#list").show();
		$("#hide_show").val("Hide List");
	}
    if($("#hide_show").val() == "Show List")
	{
		//$("#list").hide();
		//$("#showlist").val('0');
	}
	$("#hide_show").click(function()
	{
		if($("#hide_show").val() == "Show List")
		{
			$("#list").show();
			$("#showlist").val('1');
			$("#hide_show").val("Hide List");
		}
		else
		{
			$("#list").hide();
			$("#showlist").val('0');
			$("#hide_show").val("Show List");
		}
	});
	
	$("#clientId").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv").innerHTML == "")
        $("#clientIdDiv").html("Client not available");
      {/if}
      {literal}
    });
    $("#clientId2").keyup(function()
    {
      {/literal}
      {if $clientIdAskInTextBox == 1}
      $("#clientIdDiv2").html("");
      {section name=secClient1 loop=$clientIdValues}
      if(this.value == {$clientIdValues[secClient1]})
        $("#clientIdDiv2").html("{$clientIdOutput[secClient1]}");
      {/section}
      if(document.getElementById("clientIdDiv2").innerHTML == "")
        $("#clientIdDiv2").html("Client not available");
      {/if}
      {literal}
    });
    $("#itemId").autocomplete(item,
    {
      minChars: 0,
      max: 30,
      autoFill: true,
      mustMatch: true,
      matchContains: false,
      scrollHeight: 220,
      formatItem: function(data, i, total)
      {
        return data[0];
      }
    });
    $("#lot").keydown(function()
    {
      minQuantityFunc(this.value);
    });
    $("#price1").blur(function()
    {
      //$("#price2").val(this.value);
      {/literal}
      {section name=sec loop=$item.rangeStart}
      if((document.form1.price1.value >= {$item.rangeStart[sec]}) && (document.form1.price1.value <= {$item.rangeEnd[sec]})){literal}
      {
        {/literal}
        document.form1.min.value = {$item.min[sec]};
        {if $exchange == "MCX"}
        selectOptionByValue(document.form1.itemId, "{$item.itemId[sec]}");
        minQuantityFunc(document.form1.lot.value);
        {/if}
        {literal}
      }
      {/literal}
      {/section}
      {literal}
    });
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      {/literal}
      {if $exchange == "MCX"}
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesMcx.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {else if $exchange == "F_O"}
      if(e.ctrlKey && e.shiftKey && code == 119)
        window.open("brokerTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      else if(e.ctrlKey && code == 119)
        window.open("clientTradesPer2side2fo.php","orderListWindow",'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=800, height=300, left=1, top=250');
      {/if}
      {literal}

      if(code == 109)
      {
        $("#body").css("background-color",$("#sell_color").val());
        $("#buySell").val("Sell");
        $("#buySellHidden").val("Sell");
        return false;
      }
      if(code == 107)
      {
        $("#body").css("background-color",$("#buy_color").val());
        $("#buySell").val("Buy");
        $("#buySellHidden").val("Buy");
        return false;
      }
    });

  });
  function changePrice(theObject,event)
  {
    var changePriceCode = event.charCode ? event.charCode : event.keyCode ? event.keyCode : 0;
    if(changePriceCode == 38)
      theObject.value = parseInt(theObject.value)+1;
    if(changePriceCode == 40)
      theObject.value = parseInt(theObject.value)-1;
    if(theObject.name == "lot")
      plusOrMinusValue = 5;
    else
      plusOrMinusValue = 10;
    if(changePriceCode == 33)
      theObject.value = parseInt(theObject.value)+plusOrMinusValue;
    if(changePriceCode == 34)
      theObject.value = parseInt(theObject.value)-plusOrMinusValue;
  }
  function minQuantityFunc(lot)
  {
    document.form1.quantity.value = lot*document.form1.min.value;
    document.form1.quantityHidden.value = lot*document.form1.min.value;
  }
  function itemChange(theObject)
  {
    var form   = theObject.form;
    itemId     = document.form1.itemId;
    expiryDate = document.form1.expiryDate;
    expiryDate.options.length = 0;
    {/literal}
    {if $exchange == "MCX"}
    {section name="itemSec" loop=$item.itemId}
    if(itemId.selectedIndex == {%itemSec.index%})
    {literal}
    {
      {/literal}
      {section name="expiryDateSec" loop=$expiryDate[itemSec]}
      expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[itemSec][expiryDateSec]}","{$expiryDate[itemSec][expiryDateSec]}");
      expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
      {/section}
      {literal}
    }
    {/literal}
    {/section}
    {else if $exchange == "F_O"}
    {section name="expiryDateSec" loop=$expiryDate}
    expiryDate.options[{%expiryDateSec.index%}] = new Option("{$expiryDate[expiryDateSec]}","{$expiryDate[expiryDateSec]}");
    expiryDate.options[{$smarty.section.expiryDateSec.index}].selected = true;
    {/section}
    {/if}
    {section name=sec loop=$item.itemId}
    if((document.form1.itemId.value == "{$item.itemId[sec]}")){literal}
    {
      {/literal}
      document.form1.min.value = {$item.min[sec]};
      minQuantityFunc(document.form1.lot.value);
      {literal}
    }
    {/literal}
    {/section}
    {literal}
  }
  function selectOptionByValue(selObj, val)
  {
    var A = selObj.options, L = A.length;
    while(L)
    {
      if (A[--L].value == val)
      {
        selObj.selectedIndex = L;
        L = 0;
        break;
      }
    }
    itemChange(document.form1.itemId);
  }
  {/literal}
  $('#clientId').focus();
  itemChange(document.form1.itemId);
</script>
  <div id="list">
  <table border="0" cellpadding="2" cellspacing="2" width="100%" style="background-color:#fff; text-align:center;">
  {$lastTradeInfoVar}
  </table>
  </div>
  </form>
</body>
</html>