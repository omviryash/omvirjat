<html>
<head><title>Om !!! Add Trade</title>
 <script type="text/javascript" src="js/jquery.js"></script>
<script type='text/javascript' src='js/jquery.autocomplete.js'></script>
<link rel="stylesheet" type="text/css" href="css/jquery.autocomplete.css" />
<script type="text/javascript">
var item = new Array();
{foreach from=$itemIdOutput item=itemName key=id}
  item['{$id}'] = '{$itemName}';
{/foreach}
{literal}
$().ready(function() 
{
	$("#itemId").autocomplete(item, 
	{
		minChars: 0,
		max: 30,
		autoFill: true,
		mustMatch: true,
		matchContains: false,
		scrollHeight: 220,
		formatItem: function(data, i, total) 
		{
			return data[0];
		}
	});
});
</script>  
{/literal}
{literal}
<style type="text/css">
.contentColor
{
  color:White;
}
</style>
<script type="text/javascript">
function blankCheck()
{
  if(!checkDate())
    return false;
  if(document.form1.clientId.value == '')
  {
    alert('Client Id could not be Blank');
    document.form1.clientId.value='';
    document.form1.clientId.focus();
    return false;
  }
  else if(document.form1.clientId2.value == '')
  {
    alert('Client Id-2 could not be Blank');
    document.form1.clientId2.value='';
    document.form1.clientId2.focus();
    return false;
  }
  else if(document.form1.price.value == '')
  {
    alert('Price could not be Blank');
    document.form1.price.value='';
    document.form1.price.focus();
    return false;
  }
  else if(document.form1.expiryDate.value == '')
  {
    alert('Expiry Date could not be Blank');
    document.form1.itemId.focus();
    return false;
  }
  else if(document.form1.qtyLot.value == '')
  {
    alert('Quantity Lot could not be Blank');
    document.form1.qtyLot.focus();
    return false;
  }
  else if(document.getElementById('clientName1').innerHTML == 'No Available Client')
  {
    alert('Client Id 1 Not Valid');
    document.form1.clientId.focus();
    return false;
  }
  else if {/literal}({$twoClientTextBox} == 1){literal}
  {
    if(document.getElementById('clientName2').innerHTML == 'No Available Client')
    {
      alert('Client Id 2 Not Valid');
      document.form1.clientId2.focus();
      return false;
    }
    else
    {
      saveRecord();
      return true;
    }
  }
  else
  {
    saveRecord();
    return true;
  }
} 
  
window.name = 'displayAll';
var minQtytoUse = 1;
function change()
{
  var select1value= document.form1.itemId.value.toUpperCase();
  {/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
    {literal}
      document.form1.qty.value={/literal}{$min[sec1]}{literal}
      minQtytoUse = document.form1.qty.value;
    }
      {/literal}
  {/section}
 }
{literal}

function changePrice(thePriceObject)
{
  var price;
  price = parseFloat(thePriceObject.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(thePriceObject.value != price)
      thePriceObject.value = price;
  }
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
document.form1.itemId.focus();
}
function changeQtyLot()
{
  {/literal}
  var mQty = minQtytoUse;
  {literal}
  var qLot;
  qLot = parseFloat(document.form1.qtyLot.value);
  var lotQty;
  lotQty = qLot * mQty;
    document.form1.qty.value=lotQty; 
}
function qtyLotChange(theQtyLotObject)
{
  var qtyLt;
  qtyLt = parseFloat(theQtyLotObject.value);
  if(parseFloat(qtyLt) || qtyLt==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qtyLt=qtyLt+1;
    if(event.keyCode==33)
      qtyLt=qtyLt+5;
    if(event.keyCode==40)
      qtyLt=qtyLt-1;
    if(event.keyCode==34)
      qtyLt=qtyLt-5;
    if(theQtyLotObject.value != qtyLt)
       theQtyLotObject.value = qtyLt;
  }
  changeQtyLot();
}

function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+minQty;
    if(event.keyCode==33)
      qty=qty+minQty*5;
    if(event.keyCode==40)
      qty=qty-minQty;
    if(event.keyCode==34)
      qty=qty-minQty*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
  }
}
function changeBuySale()
{
  if(event.keyCode == 107 || event.keyCode==120)
  {
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode == 109 || event.keyCode==123)
  {
    document.form1.buySell.value = "Sell";
    return false;
  }
  return true;
}
function checkBuySell()
{
  if(document.form1.buySell.value == "Buy" || document.form1.buySell.value == "Sell" )
  {}
  else
    document.form1.buySell.value = document.form1.beforeBuySell.value;
}

function bodyKeyPress()
{  
  if(event.keyCode==107 || event.keyCode==120)
  {
    window.document.bgColor="blue";
  }
  else if(event.keyCode==109 || event.keyCode==123)
  {
    window.document.bgColor="red";
  }
  if(document.form1.exchange.value == 'MCX')
  {
    if(event.keyCode==117)
    {
      orderListWindow = window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
    else if(event.ctrlKey && event.shiftKey && event.keyCode==119)
    {
      tradeListWindow = window.open('brokerTradesMcx.php', 'tradeListWindow',
                            'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
    else if(event.ctrlKey && event.keyCode==119)
    {
      tradeListWindow = window.open('clientTradesMcx.php', 'tradeListWindow',
                            'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
  }
  else
  {
    if(event.keyCode==117)
    {
      orderListWindow = window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
    else if(event.ctrlKey && event.shiftKey && event.keyCode==119)
    {
      tradeListWindow = window.open('brokerTradesPer2side2fo.php', 'tradeListWindow',
                            'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
    else if(event.ctrlKey && event.keyCode==119)
    {
      tradeListWindow = window.open('clientTradesPer2side2fo.php', 'tradeListWindow',
                            'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
    }
  }
  if(event.keyCode == 13) 
  {
    if(event.ctrlKey && event.shiftKey)
    {
      if(confirm("Save As SL?"))
      {
        document.form1.makeTrade.value=3;
        blankCheck();
        return true;
      }
    }
    else if(event.ctrlKey)
    {
      if(confirm("Save As Trade?"))
      {
        document.form1.makeTrade.value=1;
        blankCheck();
        return true;
      }
    }
    else if(event.shiftKey)
    {
      if(confirm("Save As RL?"))
      {
        document.form1.makeTrade.value=2;
        blankCheck();
        return true;
      }
    }
    else
    {
    	if(window.event.srcElement.name == "submitBtnRl" || window.event.srcElement.name == "submitBtnSl" 
    	         || window.event.srcElement.name == "submitBtn")
        document.form1.clientId.focus();
    	else
    	  window.event.keyCode = 9;
    }	
  }
  return changeBuySale();
}

function checkButton(a)
{
	if(a =='submitBtnRl')
	{
	  if(confirm('Save As RL?'))
	  {
  		document.form1.makeTrade.value=2;
  		blankCheck();
	  	return true;
	  }
	}
	else if(a == 'submitBtnSl')
	{
	  if(confirm('Save As SL?'))
	  {
  		document.form1.makeTrade.value=3;
  		blankCheck();
	  	return true;
	  }
	}
	else
	{
	  if(confirm('Save As Trade?'))
	  {
  		document.form1.makeTrade.value=1;
  		blankCheck();
	  	return true;
	  }
	}
}
function askConfirmOk()
{
  if(!document.form1.expiryDate)
  {
    alert("Please Select Item");
    document.form1.itemId.focus();
  }
  else
    return false;
}

function enterOrTabOnItem()
{
  if((event.keyCode == 9 && !event.shiftKey) || event.keyCode == 13 )
  { 
    var str = changeItem(document.form1.itemId.value);
    //document.form1.expiryDate.value
	    //document.form1.expiryDate.focus(); 
    changeQtyLot();
  }
}

function itemChangeOnBlur()
{
  var itemValueUpper = document.form1.itemId.value.toUpperCase();
  changeItem(itemValueUpper);
  changeQtyLot();
  document.form1.itemId.value = itemValueUpper;
}

function getClientName(clientNm,clientCode)
{
  var sIdClientNm = clientNm;
  var sIdClientCode = clientCode;
  var url="./ajxDispClientName.php";
  var params = "clientNm="+escape(sIdClientNm)
                          +"&clientCode="+escape(sIdClientCode);
  http2.open("POST",url,true);
  http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http2.setRequestHeader("Content-length", params.length);
	http2.setRequestHeader("Connection", "close");
	http2.send(params);
 	http2.onreadystatechange = handleHttpResponse2;
}
function getClientName1(clientNm,clientCode)
{
  var sIdClientNm = clientNm;
  var sIdClientCode = clientCode;
  var url="./ajxDispClientName.php";
  var params = "clientNm="+escape(sIdClientNm)
                          +"&clientCode="+escape(sIdClientCode);
  http3.open("POST",url,true);
  http3.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http3.setRequestHeader("Content-length", params.length);
	http3.setRequestHeader("Connection", "close");
	http3.send(params);
 	http3.onreadystatechange = handleHttpResponse3;
}
function changeItem(str)
{
  var url="ajxExpiryCombo.php";
  url=url+"?q="+str;
  http1.open("GET",url,true);
  http1.onreadystatechange = handleHttpResponse1;
  http1.send(null);
  change();
}

function saveRecord()
{
  var sIdmakeTrade = document.getElementById("makeTrade").value;
  var sIdday = document.getElementById("fromDateDay").value;
  var sIdmonth = document.getElementById("fromDateMonth").value;
  var sIdyear = document.getElementById("fromDateYear").value;
  var sIdclientId = document.getElementById("clientId").value;
  var sIdprice = document.getElementById("price").value;
  var sIdprice2 = document.getElementById("price2").value;
  var sIditemId = document.getElementById("itemId").value;
  var sIdexpiryDate = document.getElementById("expiryDate").value;
  var sIdbuySell = document.getElementById("buySell").value;
  var sIdqty = document.getElementById("qty").value;
  var sIdclientId2 = document.getElementById("clientId2").value;
  var sIdvendor = document.getElementById("vendor").value;
  var sIdStanding = document.getElementById("standing").value;
  var sIdExchange = document.getElementById("exchange").value;
  
  var now = new Date();
  var hour = now.getHours();
  if (hour < 10)
    hour = '0'+hour;
  var minute = now.getMinutes();
  if (minute < 10)
    minute = '0'+minute;
  var second = now.getSeconds();
  if (second < 10)
    second = '0'+second;
  var sTime = hour + ":" + minute + ":" + second;
	var url = "ajxStoreLimitTradeMcx1.php";
	var params = "makeTrade="+escape(sIdmakeTrade)
	            +"&tradeDay="+escape(sIdday)
              +"&tradeMonth="+escape(sIdmonth)
              +"&tradeYear="+escape(sIdyear)
              +"&clientId="+escape(sIdclientId)
              +"&price="+escape(sIdprice)
              +"&price2="+escape(sIdprice2)
              +"&itemId="+escape(sIditemId)
              +"&expiryDate="+escape(sIdexpiryDate)
              +"&buySell="+escape(sIdbuySell)
              +"&qty="+escape(sIdqty)
              +"&clientId2="+escape(sIdclientId2)
              +"&vendor="+escape(sIdvendor)
              +"&standing="+escape(sIdStanding)
              +"&exchange="+escape(sIdExchange)
              +"&sTime="+escape(sTime);
  http.open("POST",url,true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("Content-length", params.length);
	http.setRequestHeader("Connection", "close");
	http.send(params);
	http.onreadystatechange = handleHttpResponse;
	document.form1.clientId.focus();
}

	// End Using Post Method.
		function getHTTPObject()
		{
		  var xmlhttp;
			  if(window.XMLHttpRequest)
			  {	xmlhttp = new XMLHttpRequest(); }
  			  else if (window.ActiveXObject)
			  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    		   		if (!xmlhttp) {
        				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			  }
   		  return xmlhttp;
		}
	{/literal}
	var http = getHTTPObject(); // We create the HTTP Object

  {literal}
	function handleHttpResponse()
	{
		if (http.readyState == 4 || http.readyState == 'complete')
		{
			if(http.status==200)
			{
				var results=http.responseText;
				//alert(results);
				if(results == "")
				{
				  document.getElementById('msg').innerHTML = "Record Inserted Successfully";
				  document.form1.qtyLot.value = 1;
				  qtyLotChange(document.form1.qtyLot.value);
				}
				else
				  alert("Record Not Inserted");
			}
		}
	}
{/literal}
var http1 = getHTTPObject();
{literal}
	function handleHttpResponse1()
	{
		if (http1.readyState == 4 || http1.readyState == 'complete')
		{
			if(http1.status == 200)
			{
				var results1=http1.responseText;
				document.getElementById('expiryDate1').innerHTML = results1;
				if(results1)
				{
//				  if(document.form1.exchange.value == 'F_O')
//				    document.form1.expiryDate.focus(); 
				}
			}
		}
	}

{/literal}
var http2 = getHTTPObject();
{literal}
	function handleHttpResponse2()
	{
		if (http2.readyState == 4 || http2.readyState == 'complete')
		{
			if(http2.status==200)
			{
				var results2=http2.responseText;
				document.getElementById('clientName1').innerHTML = results2;
				document.getElementById('msg').innerHTML = "";
			}
		}
	}
	
{/literal}

var http3 = getHTTPObject();
{literal}
	function handleHttpResponse3()
	{
		if (http3.readyState == 4 || http3.readyState == 'complete')
		{
			if(http3.status==200)
			{
				var results3=http3.responseText;
				if {/literal}({$twoClientTextBox} == 1){literal}
				  document.getElementById('clientName2').innerHTML = results3;
			}
		}
	}
{/literal}
</script>
</head>
{literal}
<body bgColor="blue" onload="todayFun();" onKeyDown="return bodyKeyPress(); " class="body">
{/literal}
  <form name="form1" action="{$PHP_SELF}" METHOD="post">
  <input type="hidden" name="changedField" >
  <input type="hidden" name="makeTrade" value="0">
  <input type="hidden" name="forStand" value="{$forStand}">
  <input type="hidden" name="exchange" value="{$exchange}">
  <table BORDER=1 cellPadding="2" cellSpacing="0">
  <tr>
    <td class="contentColor">
    	<a href="./index.php"><font color="white">Home</font></a>&nbsp;&nbsp;&nbsp;<BR />
    	Date : 
    {html_select_date prefix="fromDate" start_year=-5 end_year=+2 day_value_format="%02d" month_value_format="%m" month_format="%b" field_order="DMY"}
    </td>
    <td class="contentColor" colspan="4">
        <span class="contentColor">[ Client-1 = <u><b><font style="color:yellow"><label id="clientName1"></label></font></b></u>&nbsp;&nbsp;]&nbsp;&nbsp;
          {if $twoClientTextBox == 1}
          [ Client-2 = <u><b><font style="color:yellow"><label id="clientName2"></label></font></b></u>&nbsp;&nbsp;]
					{/if}<div id="msg" style="position:absolute;">&nbsp;</div></span><br />
        {if $useTpl == 1}
				Client 1 : <input type="text" name="clientId" size=4 onkeyup="getClientName(this.name,this.value);" onFocus="javascript:this.select();" onBlur="document.getElementById('msg').innerHTML = ''";>
         {else}
        <SELECT name="clientId" id="clientId" onkeyup="getClientName(this.name,this.value);"  >
        {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOutput" }
      </SELECT>
        {/if}
      {if $useTpl == 1}
	      {if $twoClientTextBox == 1}
	    	Client 2 : <input type="text" name="clientId2" size=4 onkeyup="getClientName1(this.name,this.value);" onFocus="javascript:this.select();" value="{$defaultClientId2}">
	      {else}
	      <input type="hidden" name="clientId2" value="{$zSelfClientId}" size=4">
      	{/if}
      {else}
        {if $twoClientTextBox == 1}
	    	Client 2 :
	      <SELECT name="clientId2" id="clientId2" onkeyup="getClientName1(this.name,this.value);" >
	      {html_options selected="$clientId2Selected" values="$clientId2Values" output="$clientId2Output" }
	      </SELECT>
	      {else}
	      <input type="hidden" name="clientId2" value="{$zSelfClientId}" size=4">
      	{/if}
    {/if}
        {if $exchange eq 'F_O'}
          Price : <input size="10" type="text" name="price" value="{$lastPrice}" onKeydown="changePrice(this);" onBlur="document.form1.price2.value=this.value;">&nbsp;&nbsp;&nbsp;
        {else}
          Price : <input size="10" type="text" name="price" value="{$lastPrice}" onKeydown="changePrice(this);" onBlur="document.form1.price2.value=this.value; itemFromPrice(); changeItem(document.form1.itemId.value); qtyLotChange(this);">&nbsp;&nbsp;&nbsp;
        {/if}
    {if $twoClientTextBox == 1}
        Price2 : <input size="10" type="text" name="price2" value="{$lastPrice2}" onKeydown="changePrice(this);">&nbsp;&nbsp;&nbsp;
    {else}
        <input size="10" type="hidden" name="price2" value="{$lastPrice2}">&nbsp;&nbsp;&nbsp;
    {/if}
      </td>
  </tr>
  <tr>
    <td class="contentColor">
      <input type="hidden" name="beforeBuySell" value="Buy">
      <input type="text" name="buySell" value="Buy" size="8"
        onFocus="document.form1.beforeBuySell.value=document.form1.buySell.value;"
        onChange="checkBuySell();" DISABLED >
        {if $exchange eq 'F_O'}
          Item : <input type="text" id="itemId" onBlur="itemChangeOnBlur();" onKeydown="enterOrTabOnItem();"  />
        {else}
          <select name="itemId" onChange="changeItem(this.value); changeQtyLot();">
          {html_options values="$itemIdValues" output="$itemIdOutput"}
          </select>
        {/if}
   </td>
   <td>
      <div id="expiryDate1"> </div>
   </td>
   <td colspan="2" NOWRAP class="contentColor">
    Qty.by Lot : <input type="text" name="qtyLot" size=8 value="1" onKeyup="qtyLotChange(this);">&nbsp;&nbsp;
   </TD>
   <TD class="contentColor">
    Quantity : <input type="text" name="qty" value="{$minQty}" onKeyDown="changeQty();" size=8  DISABLED>&nbsp;&nbsp;&nbsp;
		{if $forStand == 1}
	    <select name="standing">
	      <option name="open" value="-1">Open Standing</option>
	      <option name="close" value="1">Close Standing</option>
	    </select>
		{else}
		  <input type="hidden" name="standing" value="0">
		{/if}
      <input type="hidden" name="vendor" value="_SELF">
      <input type="button" name="submitBtnRl" value="RL" onClick="checkButton(this.name);">
      <input type="button" name="submitBtnSl" value="SL" onClick="checkButton(this.name);">
      <input type="button" name="submitBtn" value="Trade" onClick="checkButton(this.name);">
      <a href="#" title="(Shift + Enter => RL) (Ctrl + Shift + Enter => SL) (Ctrl + Enter => Trade)"	><font color="white">Help</font></A>
    </TD>
  </TR>
  </table>
  </form>
  
<script language="javascript">
	{literal}
	function todayFun()
	{
		var today = new Date();
		var curDate = today.getDate();
		var curMonth = today.getMonth();
		var curYear = today.getYear();
		if(curDate < 10)
		  curDate = '0'+curDate;
		if(curMonth < 10)
		  curMonth = '0'+(parseInt(curMonth+1));
		else
		  curMonth = curMonth+1;
    document.form1.fromDateDay.value   = curDate;
    document.form1.fromDateMonth.value = curMonth;
    document.form1.fromDateYear.value  = curYear;
	}
	function checkDate()
	{
    var dateSubmited = document.form1.fromDateDay.value+'-'+document.form1.fromDateMonth.value+'-'+document.form1.fromDateYear.value;
    if(document.form1.fromDateMonth.value == 01 || document.form1.fromDateMonth.value == 03 || 
       document.form1.fromDateMonth.value == 05 || document.form1.fromDateMonth.value == 07 || 
       document.form1.fromDateMonth.value == 08 || document.form1.fromDateMonth.value == 10 || 
       document.form1.fromDateMonth.value == 12)
      return true;
    if(document.form1.fromDateMonth.value == 02)
    {
      if(document.form1.fromDateYear.value % 4 == 0)
      {
        if(document.form1.fromDateDay.value > '29')
        {
        	alert("Please enter valid date");
        	document.form1.fromDateDay.focus();
        	return false;
        }
        else
        {
         	return true;
        }
      }
      else if(document.form1.fromDateYear.value % 400 == 0)
      {
        if(document.form1.fromDateDay.value > '29')
        {
        	alert("Please enter valid date");
        	document.form1.fromDateDay.focus();
        	return false;
        }
        else
        {
        	return true;
        }
      }
      else if(document.form1.fromDateDay.value > '28')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 04)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
      	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 06)
    {
      if(document.form1.fromDateDay.value > '30')
    	{
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 09)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
    if(document.form1.fromDateMonth.value == 11)
    {
      if(document.form1.fromDateDay.value > '30')
      {
      	alert("Please enter valid date");
       	document.form1.fromDateDay.focus();
      	return false;
      }
      else
      {
      	return true;
      }
    }
  }
  change();
  {/literal}
  document.form1.clientId.focus();
</script>
</BODY>
</HTML>