<HTML>
<HEAD><TITLE>Order Entry</TITLE>
<STYLE>
{literal}
td {  font-color="white";FONT-SIZE: 12px;}
INPUT {FONT-SIZE: 9px; }
SELECT {FONT-SIZE: 9Px; }
{/literal}
</STYLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiry;
  select2value.options.length = 0;
{/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}","{$expiryDate[sec1][sec2]}"); 
        {/if}
      {/section}
    {literal}
      document.form1.qty.value={/literal}{$min[sec1]}{literal}
    }
      {/literal}
  {/section}
 }
{literal}
function askConfirmTrade()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    example1(); 
    return true;
  }
  else
    return false;
}
function askConfirmLimit()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    example(); 
    return true;
  }
  else
    return false;
}
function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}
//
var addingQty=0;
function changeQty()
{
  //alert(event.keyCode);
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(addingQty==0)
    addingQty=qty;
  
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
    {
      qty=qty+addingQty;
      document.form1.qty.value=qty;
    }
    if(event.keyCode==40)
    {
      qty=qty-addingQty;
      document.form1.qty.value=qty;
    }
  }
}
function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}
function objectKeyPress()
{
  if(event.keyCode==13)
  {
   if(confirm("Are You Sure You want to Save Record?"))
   {
     example1(); 
     return true;
   }
   else
     return false;
  }
}
function bodyKeyPress()
{
  if(event.keyCode==107)
  {
    window.document.bgColor="blue";
    document.form1.buySellText.value="Buy";
    document.form1.buySell.value="Buy";
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="red";
    document.form1.buySellText.value="Sell";
    document.form1.buySell.value="Sell";
    return false;
  }

  if(event.keyCode==117)
  {
    orderListWindow=window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==119)
  {
    tradeListWindow=window.open('clientTrades.php', 'tradeListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    alert(document.form1.price.value); 
  }
}
function orderTypeChanged()
{
  if(document.form1.orderType.value == "SL")
    document.form1.triggerPrice.disabled = 0;
  else
    document.form1.triggerPrice.disabled = 1;
}
function orderValidityChange() 
{
  if(document.form1.orderValidity.value == "GTD")
  {
    document.form1.eday.disabled = 0;
    document.form1.emonth.disabled = 0;
    document.form1.eyear.disabled = 0;
    
  }
  else
  {
    document.form1.eday.disabled = 1;
    document.form1.emonth.disabled = 1;
    document.form1.eyear.disabled = 1;
    
  }
}
{/literal}
  {literal}      
	function handleHttpResponse()
	{   
		if (http.readyState == 4 || http.readyState == 'complete') 
		{
			if(http.status==200) 
			{
				var results=http.responseText;
				document.getElementById('client').innerHTML = results;
			}
		}
	}
   
	// Start Using Post Method.
  	function example1()
		{
		  var curDateTime = new Date();   
      var curHour = curDateTime.getHours() 
      var curMin = curDateTime.getMinutes()
      var curSec = curDateTime.getSeconds() 
      if (curHour<10)
        curHour="0"+curHour
      document.form1.curHour.value = curHour;
      if (curMin<10)
        curMin="0"+curMin
      document.form1.curMin.value = curMin;
      if (curSec<10)
        curSec="0"+curSec
      document.form1.curSec.value = curSec;
        
			var sIdchangedField = document.getElementById("changedField").value;
			var sIdmakeTrade = document.getElementById("makeTrade").value;
			var sIdfirstName = document.getElementById("firstName").value;
			var sIdmiddleName = document.getElementById("middleName").value;
			var sIdlastName = document.getElementById("lastName").value;
			var sIdforStand = document.getElementById("forStand").value;
			var sIdprice = document.getElementById("price").value;
			var sIdday = document.getElementById("day").value;
			var sIdmonth = document.getElementById("month").value;   
			var sIdyear = document.getElementById("year").value;
			var sIdhours = document.getElementById("curHour").value;  
			var sIdminutes = document.getElementById("curMin").value;  
			var sIdseconds = document.getElementById("curSec").value;
			var sIdorderType = document.getElementById("orderType").value;
			var sIditemId = document.getElementById("itemId").value;
			var sIdexpiryDate = document.getElementById("expiry").value;
			var sIdqty = document.getElementById("qty").value;
			var sIdpriceValue = document.getElementById("priceValue").value;
			var sIdtriggerPrice = document.getElementById("triggerPrice").value;
			var sIdorderValidity = document.getElementById("orderValidity").value;
			   
			var sIdeday = document.getElementById("eday").value;
			var sIdemonth = document.getElementById("emonth").value;
			var sIdeyear = document.getElementById("eyear").value;
			var sIdclientId = document.getElementById("clientId").value;
			var sIdbuySell = document.getElementById("buySell").value;
			var sIdvendorId = document.getElementById("vendorId").value;
			var sIdtradeBtn = document.getElementById("tradeBtn").value;
			
			var url = "tradeAdd3ajax.php";
			var params = "changedFi="+escape(sIdchangedField)
			            +"&makeTrad="+escape(sIdmakeTrade)
			            +"&firstNam="+escape(sIdfirstName)
		              +"&middleNam="+escape(sIdmiddleName)
		              +"&lastNam="+escape(sIdlastName)
		              +"&forStan="+escape(sIdforStand)
		              +"&pric="+escape(sIdprice)
		              +"&tday="+escape(sIdday)
		              +"&tmonth="+escape(sIdmonth)
		              +"&tyear="+escape(sIdyear)
		              +"&curHour="+escape(sIdhours)   
		              +"&curMin="+escape(sIdminutes) 
		              +"&curSec="+escape(sIdseconds)
		              +"&orderTyp="+escape(sIdorderType)
		              +"&itemI="+escape(sIditemId)
		              +"&expiryDat="+escape(sIdexpiryDate)
		              +"&qt="+escape(sIdqty)
		              +"&priceValu="+escape(sIdpriceValue)
		              +"&triggerPrice="+escape(sIdtriggerPrice)
		              +"&orderValidit="+escape(sIdorderValidity)
		              +"&eday="+escape(sIdeday)
		              +"&emonth="+escape(sIdemonth)
		              +"&eyear="+escape(sIdeyear)
		              +"&client="+escape(sIdclientId)
		              +"&bySell="+escape(sIdbuySell)
		              +"&vendor="+escape(sIdvendorId)
		              +"&tradBtn="+escape(sIdtradeBtn);
			http.open("POST", url, true);
			
			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", params.length);
			http.setRequestHeader("Connection", "close");
			http.send(params);
			http.onreadystatechange = handleHttpResponse;
			
			var sIdqty = document.getElementById("qty").value = "";
			var sIdpriceValue = document.getElementById("priceValue").value = "";
			var sIdclientId = document.getElementById("clientId").value = "";

      document.form1.itemId.selectedIndex=0;
			document.form1.qty.value=1;
			document.form1.itemId.focus();
			document.form1.triggerPrice.value = "";
			document.form1.orderType.selectedIndex=0;
			document.form1.triggerPrice.disabled = true;
		}
	// End Using Post Method.	
		function getHTTPObject() 
		{	  var xmlhttp;
			  if(window.XMLHttpRequest)
			  {	xmlhttp = new XMLHttpRequest(); }
  			  else if (window.ActiveXObject)
			  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    		   		if (!xmlhttp) {
        				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			  }
			  
     		  return xmlhttp;
		}{/literal}
	var http = getHTTPObject(); // We create the HTTP Object
	
  {literal}      
	function handleHttpResponse()
	{   
		if (http.readyState == 4 || http.readyState == 'complete') 
		{
			if(http.status==200) 
			{
				var results=http.responseText;
				document.getElementById('client').innerHTML = results;
			}
		}
	}
   
	// Start Using Post Method.
  	function example()
		{
        var curDateTime = new Date();   
        var curHour = curDateTime.getHours() 
        var curMin = curDateTime.getMinutes()
        var curSec = curDateTime.getSeconds() 
        if (curHour<10)
          curHour="0"+curHour
        document.form1.curHour.value = curHour;
        if (curMin<10)
          curMin="0"+curMin
        document.form1.curMin.value = curMin;
        if (curSec<10)
          curSec="0"+curSec
        document.form1.curSec.value = curSec;

			var sIdchangedField = document.getElementById("changedField").value;
			var sIdmakeTrade = document.getElementById("makeTrade").value;
			var sIdfirstName = document.getElementById("firstName").value;
			var sIdmiddleName = document.getElementById("middleName").value;
			var sIdlastName = document.getElementById("lastName").value;
			var sIdforStand = document.getElementById("forStand").value;
			var sIdprice = document.getElementById("price").value;
			var sIdday = document.getElementById("day").value;
			var sIdmonth = document.getElementById("month").value;   
			var sIdyear = document.getElementById("year").value;
			var sIdhours = document.getElementById("curHour").value;  
			var sIdminutes = document.getElementById("curMin").value;  
			var sIdseconds = document.getElementById("curSec").value;
			var sIdorderType = document.getElementById("orderType").value;
			var sIditemId = document.getElementById("itemId").value;
			var sIdexpiryDate = document.getElementById("expiry").value;
			var sIdqty = document.getElementById("qty").value;
			var sIdpriceValue = document.getElementById("priceValue").value;
			var sIdtriggerPrice = document.getElementById("triggerPrice").value;
			var sIdorderValidity = document.getElementById("orderValidity").value;
			   
			var sIdeday = document.getElementById("eday").value;
			var sIdemonth = document.getElementById("emonth").value;
			var sIdeyear = document.getElementById("eyear").value;
			var sIdclientId = document.getElementById("clientId").value;
			var sIdbuySell = document.getElementById("buySell").value;
			var sIdvendorId = document.getElementById("vendorId").value;
			var sIdlimitBtn = document.getElementById("limitBtn").value;
			
			var url = "tradeAdd3ajax.php";
			var params = "changedFi="+escape(sIdchangedField)
			            +"&makeTrad="+escape(sIdmakeTrade)
			            +"&firstNam="+escape(sIdfirstName)
		              +"&middleNam="+escape(sIdmiddleName)
		              +"&lastNam="+escape(sIdlastName)
		              +"&forStan="+escape(sIdforStand)
		              +"&pric="+escape(sIdprice)
		              +"&tday="+escape(sIdday)
		              +"&tmonth="+escape(sIdmonth)
		              +"&tyear="+escape(sIdyear)
		              +"&curHour="+escape(sIdhours)     
		              +"&curMin="+escape(sIdminutes)
		              +"&curSec="+escape(sIdseconds)
		              +"&orderTyp="+escape(sIdorderType)
		              +"&itemI="+escape(sIditemId)
		              +"&expiryDat="+escape(sIdexpiryDate)
		              +"&qt="+escape(sIdqty)
		              +"&priceValu="+escape(sIdpriceValue)
		              +"&triggerPrice="+escape(sIdtriggerPrice)
		              +"&orderValidit="+escape(sIdorderValidity)
		              +"&eday="+escape(sIdeday)
		              +"&emonth="+escape(sIdemonth)
		              +"&eyear="+escape(sIdeyear)
		              +"&client="+escape(sIdclientId)
		              +"&bySell="+escape(sIdbuySell)
		              +"&vendor="+escape(sIdvendorId)
		              +"&limiBtn="+escape(sIdlimitBtn);
			http.open("POST", url, true);
			
			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", params.length);
			http.setRequestHeader("Connection", "close");
			http.send(params);
			http.onreadystatechange = handleHttpResponse;
			
			var sIdqty = document.getElementById("qty").value = "";
			var sIdpriceValue = document.getElementById("priceValue").value = "";
			var sIdclientId = document.getElementById("clientId").value = "";
			
      document.form1.itemId.selectedIndex=0;
			document.form1.qty.value=1;
			document.form1.itemId.focus();
			document.form1.triggerPrice.value = "";
			document.form1.orderType.selectedIndex=0;
			document.form1.triggerPrice.disabled = true;
			
		}
		
	// End Using Post Method.	
		function getHTTPObject() 
		{	  var xmlhttp;
			  if(window.XMLHttpRequest)
			  {	xmlhttp = new XMLHttpRequest(); }
  			  else if (window.ActiveXObject)
			  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    		   		if (!xmlhttp) {
        				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			  }
			  
     		  return xmlhttp;
		}{/literal}
	var http = getHTTPObject(); // We create the HTTP Object
</SCRIPT>
</HEAD>

<BODY bgColor="blue" onKeyDown="return bodyKeyPress();" onLoad="orderValidityChange();change();">
<FORM name="form1">
  <INPUT type="hidden" id="changedField" value="">
  <INPUT type="hidden" id="makeTrade" name="makeTrade" value="0">
  <INPUT type="hidden" id="firstName" name="firstName" value="{$firstName}">
  <INPUT type="hidden" id="middleName" name="middleName" value="{$middleName}">
  <INPUT type="hidden" id="lastName" name="lastName" value="{$lastName}">
  <INPUT type="hidden" id="forStand" name="forStand" value="{$forStand}">
  <INPUT type="hidden" id="price" name="price">
<FONT color="white" style="FONT-SIZE: 12px;">
      <SELECT id="orderType" name="orderType" onChange="orderTypeChanged();" onKeyDown="return objectKeyPress();">
        <option value="RL">RL</option>
        <option value="SL">SL</option>
      </SELECT>
      Date :<SELECT id="day" onKeyDown="return objectKeyPress();">
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
    </SELECT>
    <SELECT id="month" onKeyDown="return objectKeyPress();">
       <option value="01">01</option>
       <option value="02">02</option>
       <option value="03">03</option>
       <option value="04">04</option>
       <option value="05">05</option>
       <option value="06">06</option>
       <option value="07">07</option>
       <option value="08">08</option>
       <option value="09">09</option>
       <option value="10">10</option>
       <option value="11">11</option>
       <option value="12">12</option>
    </SELECT>
    <SELECT id="year" onKeyDown="return objectKeyPress();">
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
    </SELECT>
   
   <SCRIPT LANGUAGE="Javascript">
    var now = new Date();
    var month = now.getMonth() + 1
    var day = now.getDate() 
    var year = now.getFullYear()
    if (month<10)
      month="0"+month
    document.form1.month.value = month ;
    if (day<10)
      day="0"+day
    document.form1.day.value = day ;
    document.form1.year.value = year ;
   </SCRIPT>
    
      <INPUT type="hidden" size="1" id="curHour" onKeyDown="return objectKeyPress();">
      <INPUT type="hidden" size="1" id="curMin" onKeyDown="return objectKeyPress();"> 
      <INPUT type="hidden" size="1" id="curSec" onKeyDown="return objectKeyPress();">
     <!-- {html_select_date time="$tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}-->
      <SELECT id="itemId" name="itemId" onChange="change();" onKeyDown="return objectKeyPress();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      <SCRIPT language="javascript">
        document.form1.itemId.focus();
      </SCRIPT>
      &nbsp;&nbsp;
      <SELECT id="expiry" name="expiry" onKeyDown="return objectKeyPress();">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
       </SELECT>
      &nbsp;
<!-- onBlur="itemFromPrice();"-->
      Qty : <INPUT type="text" id="qty" name="qty" size="5" onKeyDown="changeQty();" onKeyDown="return objectKeyPress();">
      &nbsp;
      Price : <INPUT size="10" type="text" id="priceValue" name="priceValue" value="{$lastPrice}" onKeydown="changePrice();return objectKeyPress();">&nbsp;&nbsp;
      <BR>
      TrigPrice : <INPUT size="10" type="text" id="triggerPrice" name="triggerPrice" value="{$lastTriggerPrice}" DISABLED onKeyDown="return objectKeyPress();">&nbsp;&nbsp;
{if $forStand == 1}
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
{/if}
      <SELECT id="orderValidity" name="orderValidity" onChange="orderValidityChange();" onKeyDown="return objectKeyPress();">
        <option value="EOS">EOS</option>
        <option value="GTD">GTD</option>
        <option value="GTC">GTC</option>
      </SELECT>
    
     <SELECT id="eday" onKeyDown="return objectKeyPress();">
      
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
    </SELECT>
    <SELECT id="emonth" onKeyDown="return objectKeyPress();">
       <option value="01">01</option>
       <option value="02">02</option>
       <option value="03">03</option>
       <option value="04">04</option>
       <option value="05">05</option>
       <option value="06">06</option>
       <option value="07">07</option>
       <option value="08">08</option>
       <option value="09">09</option>
       <option value="10">10</option>
       <option value="11">11</option>
       <option value="12">12</option>
    </SELECT>
    <SELECT id="eyear" onKeyDown="return objectKeyPress();">
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
    </SELECT>
   
    <SCRIPT LANGUAGE="Javascript">
      var now = new Date();
      var month = now.getMonth() + 1
      var day = now.getDate() 
      var year = now.getFullYear()
      
      if (month<10)
      month="0"+month
      document.form1.emonth.value = month ;
      if (day<10)
      day="0"+day
      document.form1.eday.value = day ;
      document.form1.eyear.value = year ;
    // -->
    </SCRIPT>
    
      User Remarks : 
      <INPUT type="text" id="clientId" name="clientId" onKeyDown="return objectKeyPress();">
      <INPUT type="hidden" id="vendorId" name="vendorId" onKeyDown="return objectKeyPress();">
      
      <INPUT type="button" id="tradeBtn" name="tradeBtn" value="Trade" onClick="return askConfirmTrade();" onKeyDown="return objectKeyPress();">
      &nbsp;
      <INPUT type="button" id="limitBtn" name="limitBtn" value="Limit" onClick="return askConfirmLimit();">
      &nbsp;
      <BR>
      <INPUT DISABLED type="text" id="buySellText" name="buySellText" value="Buy" size="5">
      <INPUT type="hidden" id="buySell" name="buySell" value="Buy" onKeyDown="return objectKeyPress();">
<BR>
<!--<TABLE>
    <TR bgcolor="#D3D3D3">
      <TD>Last trade : </TD>
      <TH>{$lastTradeInfoVar}</TH>
    </TR>
</TABLE>
  {$focusScript} -->
</FORM>
</BODY>
<div id="client"></div>
</HTML>