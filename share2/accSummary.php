<?php
session_start();
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=accSummary");
}
else
{
  include "./etc/om_config.inc";
  
  $smarty=new SmartyWWW();
  
  $clientQuery = "SELECT * FROM client
                  ORDER BY firstName,middleName,lastName";
  $clientResult = mysql_query($clientQuery);
  
  $creditTotal      = 0;
  $debitTotal       = 0;
  $grandTotal       = 0;
  $selectedExchange = 0;
  $exchange         = array();
  $i = 0;
  if(isset($_POST['exchange']))
    $selectedExchange = $_POST['exchange'];
    
  while($clientRow = mysql_fetch_array($clientResult))
  {
    $clientArray[$i]['firstName']  = $clientRow['firstName'];
    $clientArray[$i]['middleName'] = $clientRow['middleName'];
    $clientArray[$i]['lastName']   = $clientRow['lastName'];
    $clientArray[$i]['balance']    = 0;
    
    $cashFlowQuery = "SELECT * FROM cashflow
                      WHERE clientId = ".$clientRow['clientId']."
                        AND transactionDate <= '".$_SESSION['toDate']."'";
    if($selectedExchange > 0)
      $cashFlowQuery .= " AND exchange = (SELECT exchange FROM exchange WHERE exchangeId = ".$selectedExchange.")";
    $cashFlowResult = mysql_query($cashFlowQuery);
    while($cashFlowRow = mysql_fetch_array($cashFlowResult))
    {
      if($cashFlowRow['dwStatus'] != 'd')
        $clientArray[$i]['balance'] -= $cashFlowRow['dwAmount'];
      else
        $clientArray[$i]['balance'] += $cashFlowRow['dwAmount'];
      if($cashFlowRow['plStatus'] != 'p')
        $clientArray[$i]['balance'] += $cashFlowRow['plAmount'];
      else
        $clientArray[$i]['balance'] -= $cashFlowRow['plAmount'];
    }
    if($clientArray[$i]['balance'] >= 0)
      $creditTotal                += $clientArray[$i]['balance'];
    else
      $debitTotal                 += $clientArray[$i]['balance'];
    $i++;
  }
  $grandTotal = $creditTotal + $debitTotal;
  
  $selectExchange = "SELECT exchangeId,exchange FROM exchange
                      ORDER BY exchange";
  $selectExchangeRes = mysql_query($selectExchange);
  $a = 0;
  while($exchangeRow = mysql_fetch_object($selectExchangeRes))
  {
  	$exchange['id'][$a]   = $exchangeRow->exchangeId;
  	$exchange['name'][$a] = $exchangeRow->exchange;
  	$a++;
  }

  $smarty->assign("toDate",$_SESSION['toDate']);
  $smarty->assign("clientArray",$clientArray);
  $smarty->assign("creditTotal",$creditTotal);
  $smarty->assign("debitTotal",$debitTotal);
  $smarty->assign("grandTotal",$grandTotal);
  $smarty->assign("exchange",$exchange);
  $smarty->assign("selectedExchange",$selectedExchange);
  $smarty->display("accSummary.tpl");
}
?>