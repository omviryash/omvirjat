<?php /* Smarty version 2.6.10, created on 2016-05-05 19:59:02
         compiled from deleteF_O.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', 'deleteF_O.tpl', 17, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "headerMain.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<FORM name="form1" action="deleteF_O.php" method="POST">
	<TABLE>
    <TR>
      <TD align="right">Client : </TD>
      <TD>
      	<select name="clientId">
      		<option value="0">ALL</option>
      		<?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['i']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
      			<option value="<?php echo $this->_tpl_vars['clientId'][$this->_sections['sec1']['index']]; ?>
"><?php echo $this->_tpl_vars['name'][$this->_sections['sec1']['index']]; ?>
</option>
      		<?php endfor; endif; ?>
      	</select>
      </TD>
    </TR>
    <TR>
      <TD align="right">From Date : </TD>
      <TD><?php echo smarty_function_html_select_date(array('prefix' => 'fromDate','start_year' => "+2",'end_year' => "-2",'field_order' => 'dmy'), $this);?>
</TD>
      <TD align="right">&nbsp;&nbsp;&nbsp;To Date : </TD>
      <TD><?php echo smarty_function_html_select_date(array('prefix' => 'toDate','start_year' => "+2",'end_year' => "-2",'field_order' => 'dmy'), $this);?>
</TD>
    </TR>
    <TR>
      <TD align="right">Item : </TD>
      <TD>
      	<select name="itemId">
      		<option value="All">ALL</option>
      		<?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=$this->_tpl_vars['j']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
      			<option value="<?php echo $this->_tpl_vars['itemId'][$this->_sections['sec2']['index']]; ?>
"><?php echo $this->_tpl_vars['itemName'][$this->_sections['sec2']['index']]; ?>
</option>
      		<?php endfor; endif; ?>
      	</select>
      </TD>
    </TR>
    <TR>
    	<TD align="right">Standing : </TD>
    	<TD>
    	  <INPUT type="checkbox" name="trades" value="0" CHECKED>Trades&nbsp;&nbsp;&nbsp;&nbsp;
    	  <INPUT type="checkbox" name="open" value="-1" CHECKED>Open&nbsp;&nbsp;&nbsp;&nbsp;
    	  <INPUT type="checkbox" name="close" value="1" CHECKED>Close
    	</TD>
    </TR>
    <TR>
    	<TD></TD>
    	<TD><INPUT type="submit" name="btnSubmit" value="Go !!"></TD>
    </TR>
    	
  </TABLE>
</FORM>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer1.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>