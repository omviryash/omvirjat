<?php /* Smarty version 2.6.10, created on 2016-03-12 14:25:27
         compiled from tradeAdd3.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', 'tradeAdd3.tpl', 200, false),array('function', 'html_options', 'tradeAdd3.tpl', 202, false),)), $this); ?>
<HTML>
<HEAD><TITLE>MCX Order Entry</TITLE>
<STYLE>
<?php echo '
td {  font-color="white";FONT-SIZE: 12px;}
INPUT {FONT-SIZE: 9px; }
SELECT {FONT-SIZE: 9Px; }
'; ?>

</STYLE>
<SCRIPT language="javascript">
<?php echo '
window.name = \'displayAll\';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiryDate;
  select2value.options.length=0;
'; ?>

   <?php unset($this->_sections['sec1']);
$this->_sections['sec1']['name'] = 'sec1';
$this->_sections['sec1']['loop'] = is_array($_loop=$this->_tpl_vars['k']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec1']['show'] = true;
$this->_sections['sec1']['max'] = $this->_sections['sec1']['loop'];
$this->_sections['sec1']['step'] = 1;
$this->_sections['sec1']['start'] = $this->_sections['sec1']['step'] > 0 ? 0 : $this->_sections['sec1']['loop']-1;
if ($this->_sections['sec1']['show']) {
    $this->_sections['sec1']['total'] = $this->_sections['sec1']['loop'];
    if ($this->_sections['sec1']['total'] == 0)
        $this->_sections['sec1']['show'] = false;
} else
    $this->_sections['sec1']['total'] = 0;
if ($this->_sections['sec1']['show']):

            for ($this->_sections['sec1']['index'] = $this->_sections['sec1']['start'], $this->_sections['sec1']['iteration'] = 1;
                 $this->_sections['sec1']['iteration'] <= $this->_sections['sec1']['total'];
                 $this->_sections['sec1']['index'] += $this->_sections['sec1']['step'], $this->_sections['sec1']['iteration']++):
$this->_sections['sec1']['rownum'] = $this->_sections['sec1']['iteration'];
$this->_sections['sec1']['index_prev'] = $this->_sections['sec1']['index'] - $this->_sections['sec1']['step'];
$this->_sections['sec1']['index_next'] = $this->_sections['sec1']['index'] + $this->_sections['sec1']['step'];
$this->_sections['sec1']['first']      = ($this->_sections['sec1']['iteration'] == 1);
$this->_sections['sec1']['last']       = ($this->_sections['sec1']['iteration'] == $this->_sections['sec1']['total']);
?>
    if( select1value=="<?php echo $this->_tpl_vars['itemId'][$this->_sections['sec1']['index']]; ?>
")
  	<?php echo '{'; ?>

  		<?php unset($this->_sections['sec2']);
$this->_sections['sec2']['name'] = 'sec2';
$this->_sections['sec2']['loop'] = is_array($_loop=$this->_tpl_vars['l']+10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sec2']['show'] = true;
$this->_sections['sec2']['max'] = $this->_sections['sec2']['loop'];
$this->_sections['sec2']['step'] = 1;
$this->_sections['sec2']['start'] = $this->_sections['sec2']['step'] > 0 ? 0 : $this->_sections['sec2']['loop']-1;
if ($this->_sections['sec2']['show']) {
    $this->_sections['sec2']['total'] = $this->_sections['sec2']['loop'];
    if ($this->_sections['sec2']['total'] == 0)
        $this->_sections['sec2']['show'] = false;
} else
    $this->_sections['sec2']['total'] = 0;
if ($this->_sections['sec2']['show']):

            for ($this->_sections['sec2']['index'] = $this->_sections['sec2']['start'], $this->_sections['sec2']['iteration'] = 1;
                 $this->_sections['sec2']['iteration'] <= $this->_sections['sec2']['total'];
                 $this->_sections['sec2']['index'] += $this->_sections['sec2']['step'], $this->_sections['sec2']['iteration']++):
$this->_sections['sec2']['rownum'] = $this->_sections['sec2']['iteration'];
$this->_sections['sec2']['index_prev'] = $this->_sections['sec2']['index'] - $this->_sections['sec2']['step'];
$this->_sections['sec2']['index_next'] = $this->_sections['sec2']['index'] + $this->_sections['sec2']['step'];
$this->_sections['sec2']['first']      = ($this->_sections['sec2']['iteration'] == 1);
$this->_sections['sec2']['last']       = ($this->_sections['sec2']['iteration'] == $this->_sections['sec2']['total']);
?>
        <?php if ($this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']] != ""): ?>
          select2value.options[<?php echo $this->_sections['sec2']['index']; ?>
]=new Option("<?php echo $this->_tpl_vars['expiryDate'][$this->_sections['sec1']['index']][$this->_sections['sec2']['index']]; ?>
"); 
        <?php endif; ?>
      <?php endfor; endif; ?>
    <?php echo '
      document.form1.qty.value=';  echo $this->_tpl_vars['min'][$this->_sections['sec1']['index']];  echo '
    }
      '; ?>

  <?php endfor; endif; ?>
 }
<?php echo '
function askConfirm()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    return true;
  }
  else
    return false;
}
function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

var addingQty=0;
function changeQty()
{
  //alert(event.keyCode);
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(addingQty==0)
    addingQty=qty;
  
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
    {
      qty=qty+addingQty;
      document.form1.qty.value=qty;
    }
    if(event.keyCode==40)
    {
      qty=qty-addingQty;
      document.form1.qty.value=qty;
    }
  }
}
function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
'; ?>

<?php echo $this->_tpl_vars['itemFromPriceJS']; ?>

<?php echo '
}

function bodyKeyPress()
{
  var price=(document.form1.priceValue.value);
  if(event.keyCode==107)
  {
    window.document.bgColor="blue";
    document.form1.buySellText.value = "Buy";
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="red";
    document.form1.buySellText.value = "Sell";
    document.form1.buySell.value = "Sell";
    return false;
  }
  if(event.keyCode==117)
  {
    orderListWindow=window.open(\'orderList.php?listOnly=1\', \'orderListWindow\',
                          \'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250\');
  }
  if(event.keyCode==119)
  {
    tradeListWindow=window.open(\'clientTrades.php\', \'tradeListWindow\',
                          \'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250\');
  }
  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value==\'Buy\')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value==\'Buy\')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    alert(document.form1.price.value); 
  }
}
function orderTypeChanged()
{
  if(document.form1.orderType.value == "SL")
    document.form1.triggerPrice.disabled = 0;
  else
    document.form1.triggerPrice.disabled = 1;
}
function orderValidityChange()
{
  if(document.form1.orderValidity.value == "GTD")
  {
    document.form1.gtdDateDay.disabled = 0;
    document.form1.gtdDateMonth.disabled = 0;
    document.form1.gtdDateYear.disabled = 0;
    
  }
  else
  {
    document.form1.gtdDateDay.disabled = 1;
    document.form1.gtdDateMonth.disabled = 1;
    document.form1.gtdDateYear.disabled = 1;
    
  }
}
'; ?>

</SCRIPT>
</HEAD>
<BODY bgColor="blue" onKeyDown="return bodyKeyPress();" onLoad="orderValidityChange();change();">
  <FORM name="form1" action="<?php echo $this->_tpl_vars['PHP_SELF']; ?>
" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="<?php echo $this->_tpl_vars['firstName']; ?>
">
  <INPUT type="hidden" name="middleName" value="<?php echo $this->_tpl_vars['middleName']; ?>
">
  <INPUT type="hidden" name="lastName" value="<?php echo $this->_tpl_vars['lastName']; ?>
">
  <INPUT type="hidden" name="forStand" value="<?php echo $this->_tpl_vars['forStand']; ?>
">
  <INPUT type="hidden" name="price">
<FONT color="white" style="FONT-SIZE: 12px;">
      <SELECT name="orderType" onChange="orderTypeChanged();">
        <option value="RL">RL</option>
        <option value="SL">SL</option>
      </SELECT>
    Date : 
      <?php echo smarty_function_html_select_date(array('time' => ($this->_tpl_vars['tradeDateDisplay']),'prefix' => 'trade','start_year' => "-1",'end_year' => "+1",'month_format' => "%m",'field_order' => 'DMY','day_value_format' => "%02d"), $this);?>

      <SELECT name="itemId" onChange="change();">
      <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['itemIdSelected']),'values' => ($this->_tpl_vars['itemIdValues']),'output' => ($this->_tpl_vars['itemIdOutput'])), $this);?>

      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      <?php echo smarty_function_html_options(array('selected' => ($this->_tpl_vars['expiryDateSelected']),'values' => ($this->_tpl_vars['expiryDateValues']),'output' => ($this->_tpl_vars['expiryDateOutput'])), $this);?>

      </SELECT>
      &nbsp;
<!-- onBlur="itemFromPrice();"-->
      Qty : <INPUT type="text" name="qty" size="5" onKeyDown="changeQty();">
      &nbsp;
      Price : <INPUT size="10" type="text" name="priceValue" value="<?php echo $this->_tpl_vars['lastPrice']; ?>
" onKeydown="changePrice();">&nbsp;&nbsp;
      <BR>
      TrigPrice : <INPUT size="10" type="text" name="triggerPrice" value="<?php echo $this->_tpl_vars['lastTriggerPrice']; ?>
" DISABLED>&nbsp;&nbsp;
<?php if ($this->_tpl_vars['forStand'] == 1): ?>
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
<?php endif; ?>
      <SELECT name="orderValidity" onChange="orderValidityChange();">
        <option value="EOS">EOS</option>
        <option value="GTD">GTD</option>
        <option value="GTC">GTC</option>
      </SELECT>

      <?php echo smarty_function_html_select_date(array('prefix' => 'gtdDate','field_order' => 'DMY'), $this);?>

      User Remarks : 
      <INPUT type="text" name="clientId">
      <INPUT type="text" name="vendorId" size="3">
      <INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();">
      &nbsp;
      <INPUT type="submit" name="limitBtn" value="Limit" onClick="return askConfirm();">
      &nbsp;
      <BR>
      <INPUT DISABLED type="text" name="buySellText" value="Buy" size="5">
      <INPUT type="hidden" name="buySell" value="Buy">
<!--       <B><?php echo $this->_tpl_vars['clientWholeName']; ?>
 : </B>
       Deposit : <?php echo $this->_tpl_vars['deposit']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : <?php echo $this->_tpl_vars['currentBal']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       Total : <?php echo $this->_tpl_vars['total']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;  
       Phone   : <?php echo $this->_tpl_vars['phone']; ?>
&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : <?php echo $this->_tpl_vars['mobile']; ?>
&nbsp;
      <BR>-->
<TABLE>
    <TR bgcolor="#D3D3D3">
      <TD>Last trade : </TD>
      <TH><?php echo $this->_tpl_vars['lastTradeInfoVar']; ?>
</TH>
    </TR>
</TABLE>
  <?php echo $this->_tpl_vars['focusScript']; ?>

  </FORM>
</BODY>
</HTML>