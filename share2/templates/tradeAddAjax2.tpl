 <HTML>
<HEAD><TITLE>Order Entry</TITLE>
<STYLE>
{literal}
td {  font-color="white";FONT-SIZE: 12px;}
INPUT {FONT-SIZE: 9px; }
SELECT {FONT-SIZE: 9Px; }
{/literal}
</STYLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiryDate;
  select2value.options.length = 0;
{/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}","{$expiryDate[sec1][sec2]}"); 
        {/if}
      {/section}
    {literal}
      document.form1.qty.value={/literal}{$min[sec1]}{literal}
      document.form1.qty2.value={/literal}{$min[sec1]}{literal}
    }
      {/literal}
  {/section}
 }
{literal}
function askConfirmTrade()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    example1(); 
    return true;
  }
  else
    return false;
}

var addingQty=0;
function changeQty()
{
  {/literal}
  var minQty = {$minQty};
  {literal}
  var qty;
  var qty2;
  qty = parseFloat(document.form1.qty.value);
  qty2 = parseFloat(document.form1.qty2.value);
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      qty=qty+qty2;
    if(event.keyCode==33)
      qty=qty+qty2*5;
    if(event.keyCode==40)
      qty=qty-qty2;
    if(event.keyCode==34)
      qty=qty-qty2*5;
    if(document.form1.qty.value != qty)
      document.form1.qty.value = qty;
   }
}
function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}
function objectKeyPress()
{
  if(event.keyCode==13)
  {
   if(confirm("Are You Sure You want to Save Record?"))
   {
     example1(); 
     return true;
   }
   else
     return false;
  }
}
function bodyKeyPress()
{
  if(event.keyCode==107)
  {
    window.document.bgColor="blue";
    document.form1.buySellText.value="Buy";
    document.form1.buySell.value="Buy";
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="red";
    document.form1.buySellText.value="Sell";
    document.form1.buySell.value="Sell";
    return false;
  }


  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    alert(document.form1.price.value); 
  }
}

{/literal}
  {literal}      
	function handleHttpResponse()
	{   
		if (http.readyState == 4 || http.readyState == 'complete') 
		{
			if(http.status==200) 
			{
				var results=http.responseText;
				document.getElementById('client').innerHTML = results;
			}
		}
	}
   
	// Start Using Post Method.
  	function example1()
		{
		  var curDateTime = new Date();   
      var curHour = curDateTime.getHours() 
      var curMin = curDateTime.getMinutes()
      var curSec = curDateTime.getSeconds() 
      if (curHour<10)
        curHour="0"+curHour
      document.form1.curHour.value = curHour;
      if (curMin<10)
        curMin="0"+curMin
      document.form1.curMin.value = curMin;
      if (curSec<10)
        curSec="0"+curSec
      document.form1.curSec.value = curSec;
        
			var sIdchangedField = document.getElementById("changedField").value;
			var sIdmakeTrade = document.getElementById("makeTrade").value;
			var sIdfirstName = document.getElementById("firstName").value;
			var sIdmiddleName = document.getElementById("middleName").value;
			var sIdlastName = document.getElementById("lastName").value;
			var sIdday = document.getElementById("day").value;
			var sIdmonth = document.getElementById("month").value;   
			var sIdyear = document.getElementById("year").value;
			var sIdhours = document.getElementById("curHour").value;  
			var sIdminutes = document.getElementById("curMin").value;  
			var sIdseconds = document.getElementById("curSec").value;
			var sIditemId = document.getElementById("itemId").value;
			var sIdexpiryDate = document.getElementById("expiryDate").value;
			var sIdqty = document.getElementById("qty").value;
			var sIdpriceValue = document.getElementById("priceValue").value;
			var sIdclientId = document.getElementById("clientId").value;
			var sIdbuySell = document.getElementById("buySell").value;
			var sIdvendorId = document.getElementById("vendorId").value;
			var sIdtradeBtn = document.getElementById("ok").value;
			
			var url = "tradeAddAjax2insert.php";
			var params = "changedFi="+escape(sIdchangedField)
			            +"&makeTrad="+escape(sIdmakeTrade)
			            +"&firstNam="+escape(sIdfirstName)
		              +"&middleNam="+escape(sIdmiddleName)
		              +"&lastNam="+escape(sIdlastName)
		              +"&tday="+escape(sIdday)
		              +"&tmonth="+escape(sIdmonth)
		              +"&tyear="+escape(sIdyear)
		              +"&curHour="+escape(sIdhours)   
		              +"&curMin="+escape(sIdminutes) 
		              +"&curSec="+escape(sIdseconds)
		              +"&itemI="+escape(sIditemId)
		              +"&expiryDat="+escape(sIdexpiryDate)
		              +"&qt="+escape(sIdqty)
		              +"&priceValu="+escape(sIdpriceValue)
		              +"&client="+escape(sIdclientId)
		              +"&bySell="+escape(sIdbuySell)
		              +"&vendor="+escape(sIdvendorId)
		              +"&tradBtn="+escape(sIdtradeBtn);
			http.open("POST", url, true);
			
			//Send the proper header information along with the request
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.setRequestHeader("Content-length", params.length);
			http.setRequestHeader("Connection", "close");
			http.send(params);
			http.onreadystatechange = handleHttpResponse;
			
			var sIdpriceValue = document.getElementById("priceValue").value = "";
			document.form1.clientId.focus();
		}
	// End Using Post Method.	
		function getHTTPObject() 
		{	  var xmlhttp;
			  if(window.XMLHttpRequest)
			  {	xmlhttp = new XMLHttpRequest(); }
  			  else if (window.ActiveXObject)
			  {		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    		   		if (!xmlhttp) {
        				xmlhttp=new ActiveXObject("Msxml2.XMLHTTP"); }
			  }
			  
     		  return xmlhttp;
		}{/literal}
	var http = getHTTPObject(); // We create the HTTP Object

</SCRIPT>
</HEAD>

<BODY bgColor="blue" onKeyDown="return bodyKeyPress();" onLoad="change();">
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post" >
  <INPUT type="hidden" id="changedField" value="">
  <INPUT type="hidden" id="makeTrade" name="makeTrade" value="0">
  <INPUT type="hidden" id="firstName" name="firstName" value="{$firstName}">
  <INPUT type="hidden" id="middleName" name="middleName" value="{$middleName}">
  <INPUT type="hidden" id="lastName" name="lastName" value="{$lastName}">
  <INPUT type="hidden" id="forStand" name="forStand" value="{$forStand}">
  <INPUT type="hidden" id="price" name="price">
  <FONT color="white" style="FONT-SIZE: 12px;">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <SELECT name="clientId" id="clientId" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
      {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOutput" }
      </SELECT>&nbsp;&nbsp;&nbsp;&nbsp;
      Date :<SELECT id="day" onKeyDown="if(event.keyCode==13) event.keyCode=9; return objectKeyPress(); ">
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
      <option value="13">13</option>
      <option value="14">14</option>
      <option value="15">15</option>
      <option value="16">16</option>
      <option value="17">17</option>
      <option value="18">18</option>
      <option value="19">19</option>
      <option value="20">20</option>
      <option value="21">21</option>
      <option value="22">22</option>
      <option value="23">23</option>
      <option value="24">24</option>
      <option value="25">25</option>
      <option value="26">26</option>
      <option value="27">27</option>
      <option value="28">28</option>
      <option value="29">29</option>
      <option value="30">30</option>
      <option value="31">31</option>
    </SELECT>
    <SELECT id="month" onKeyDown="if(event.keyCode==13) event.keyCode=9; return objectKeyPress();">
       <option value="01">01</option>
       <option value="02">02</option>
       <option value="03">03</option>
       <option value="04">04</option>
       <option value="05">05</option>
       <option value="06">06</option>
       <option value="07">07</option>
       <option value="08">08</option>
       <option value="09">09</option>
       <option value="10">10</option>
       <option value="11">11</option>
       <option value="12">12</option>
    </SELECT>
    <SELECT id="year" onKeyDown="if(event.keyCode==13) event.keyCode=9; return objectKeyPress();">
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
        <option value="2011">2011</option>
        <option value="2012">2012</option>
    </SELECT>
   
   <SCRIPT LANGUAGE="Javascript">
    var now = new Date();
    var month = now.getMonth() + 1
    var day = now.getDate() 
    var year = now.getFullYear()
    if (month<10)
      month="0"+month
    document.form1.month.value = month ;
    if (day<10)
      day="0"+day
    document.form1.day.value = day ;
    document.form1.year.value = year ;
   </SCRIPT>
      <INPUT type="hidden" size="1" id="curHour" onKeyDown="return objectKeyPress();">
      <INPUT type="hidden" size="1" id="curMin" onKeyDown="return objectKeyPress();"> 
      <INPUT type="hidden" size="1" id="curSec" onKeyDown="return objectKeyPress();">
      Price : <INPUT size="10" type="text" id="priceValue" name="priceValue" value="{$lastPrice}" onKeydown="if(event.keyCode==13) event.keyCode=9; changePrice(); return objectKeyPress();">&nbsp;&nbsp;
      <BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <SELECT id="itemId" name="itemId" onChange="change();" onKeyDown="if(event.keyCode==13) event.keyCode=9; return objectKeyPress();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;&nbsp;&nbsp;
      <SELECT id="expiryDate" name="expiryDate" onKeyDown="if(event.keyCode==13) event.keyCode=9;return objectKeyPress();">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
       </SELECT>
      &nbsp;
      <INPUT DISABLED type="text" id="buySellText" name="buySellText" value="Buy" size="5">
      <INPUT type="hidden" id="buySell" name="buySell" value="Buy" onKeyDown="if(event.keyCode==13) event.keyCode=9;return objectKeyPress();">
      Qty : <INPUT type="text" id="qty" name="qty" size="5" onKeyDown="changeQty(); if(event.keyCode==13) event.keyCode=9;   return objectKeyPress();"><INPUT type="hidden" name="qty2" id="qty2" value="{$minQty}" >&nbsp;&nbsp;&nbsp;
      &nbsp;
      <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Vendor : 
      <SELECT name="vendorId" id="vendorId" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
      {html_options selected="$vendorSelected" values="$vendorValues" output="$vendorOutput"}
      </SELECT>

      <INPUT type="button" id="ok" name="ok" value="ok" onClick="return askConfirmTrade();" onKeyDown="return objectKeyPress();">
      &nbsp;
      <INPUT type="reset" value="Reset">&nbsp;&nbsp;&nbsp;&nbsp;

</FORM>
</BODY>
<div id="client"></div>
</HTML>