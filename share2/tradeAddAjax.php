<?php
  include "etc/om_config.inc";
  
  $smarty = new SmartyWWW();
  if (isset($_POST['Ok']))
  {
    $tradeDate  = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay'];

    if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
      $expiryDate = $_POST['expiryDate'];
    else
      $expiryDate = "";
    $fullTime = $_POST['curHour'].":".$_POST['curMin'].":".$_POST['curSec'];
    $clientQuery = "SELECT * FROM client WHERE clientId = '".$_POST['clientId']."' ORDER BY firstName, middleName, lastName";
    $clientQueryResult = mysql_query($clientQuery);
    while($clientRow = mysql_fetch_array($clientQueryResult))
    {
      $firstNameP = $clientRow['firstName'];
      $middleNameP = $clientRow['middleName'];
      $lastNameP = $clientRow['lastName'];
    }
    $standing = isset($_POST['standing'])?$_POST['standing']:"0";
    $insertQuery  = "INSERT INTO tradetxtf_o (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,tradeTime,qty,price,expiryDate,vendor)
                      VALUES ('".$standing."', '".$_POST['clientId']."', 
                              '".$firstNameP."',
                              '".$middleNameP."',
                              '".$lastNameP."',
                              '".$_POST['buySell']."',
                              '".$_POST['itemId']."', '".$tradeDate."','".$fullTime."',
                              '".$_POST['qty']."', '".$_POST['price']."',
                              '".$expiryDate."', '".$_POST['vendor']."'
                             )";
    $result = mysql_query($insertQuery);
    if(!$result)
     echo mysql_error()."<BR>".$insertQuery;
  exit(); 
  }
  
///////////////////////////////////////////////////////
  $firstName  = '';
  $middleName = '';
  $lastName   = '';
  $selectQuery = "SELECT * FROM client
                  ORDER BY firstName, middleName, lastName
                 ";
  if(isset($_POST['clientId']))
    $currentClientId = $_POST['clientId'];
  else
    $currentClientId = 0;
  
  $result = mysql_query($selectQuery);
  
  $clientIdValues = array();
  $clientIdOutput = array();
  $i = 0;
  while($row = mysql_fetch_array($result))
  {
    if($currentClientId == 0)
    {
      $currentClientId = $row['clientId'];
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    }
    
    if($row['clientId'] == $currentClientId)
    {
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
      //////////////////////////////////////////////
      $currentBal = $row['currentBal'];
      $deposit = $row['deposit'];
      $phone   = $row['phone'];
      $mobile  = $row['mobile'];
      //////////////////////////////////////////////
    }
    
    $clientIdValues[$i] = $row['clientId'];
    $clientIdOutput[$i] = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    $i++;
  }
///////////////////////////////////////////////////////

  if(count($clientIdValues) > 0)
  {
    if(isset($_POST['price']))
      $lastPrice = $_POST['price'];
    else
      $lastPrice = '';
  
    if(isset($_POST['buySell']))
      $buySellSelected  = $_POST['buySell'];
    else
      $buySellSelected  = "Buy";
    $buySellValues = array("Buy", "Sell");
    $buySellOutput = array("Buy", "Sell");
  ////    
    if(isset($_POST['tradeDay']))
      $tradeDateDisplay = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."-";
    else
      $tradeDateDisplay = date("Y-m-d");
  
  ///////////////////////////////////////////////////////
    $minQty = 0;
    $selectItemQuery = "SELECT * FROM item ORDER BY itemId";
    if(isset($_POST['itemId']))
      $currentItemId = $_POST['itemId'];
    else
      $currentItemId = '';
    
    $itemResult = mysql_query($selectItemQuery);
    
    $i = 0;
    $itemIdSelected = '';
    $itemIdValues = array();
    $itemIdOutput = array();
    $itemFromPriceJS = '';
    while($itemRow = mysql_fetch_array($itemResult))
    {
      if($currentItemId == '')
        $currentItemId = $itemRow['itemId'];
      
      if($itemRow['itemId'] == $currentItemId)
      {
        $itemIdSelected = $itemRow['itemId'];
        $minQty = $itemRow['min'];
      }
      
      $itemIdValues[$i] = $itemRow['itemId'];
      $itemIdOutput[$i] = $itemRow['itemId'];
      
      //For JavaScript of itemFromPrice :Start
      if(!is_null($itemRow['rangeStart']) && !is_null($itemRow['rangeEnd']))
      {
        $itemFromPriceJS .= "  if(price >= ".$itemRow['rangeStart']." && price <= ".$itemRow['rangeEnd'].")\n";
        $itemFromPriceJS .= "  {\n";
        $itemFromPriceJS .= "    document.form1.itemId.selectedIndex = ".$i.";\n";
        $itemFromPriceJS .= "    changeItem();\n";
        $itemFromPriceJS .= "  }\n";
      }
      //For JavaScript of itemFromPrice :End
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $expiryQuery = "SELECT * FROM expiry
                    WHERE itemId = '".$currentItemId."'";
    $expiryResult = mysql_query($expiryQuery);
    
    $i = 0;
    $expiryDateSelected = (isset($_POST['expiryDate']))?$_POST['expiryDate']:"";
    $expiryDateValues  = array();
    $expiryDateOutput  = array();
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      $expiryDateValues[$i] = $expiryRow['expiryDate'];
      $expiryDateOutput[$i] = $expiryRow['expiryDate'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $vendorQuery = "SELECT * FROM vendor ORDER BY vendor";
    $vendorResult = mysql_query($vendorQuery);
    
    $i = 0;
    $vendorSelected = '';
    $vendorValues  = array();
    $vendorOutput  = array();
    while($vendorRow = mysql_fetch_array($vendorResult))
    {
      if(isset($_POST['vendor']) && $_POST['vendor'] == $vendorRow['vendor'])
        $vendorSelected = $vendorRow['vendor'];
      $vendorValues[$i] = $vendorRow['vendor'];
      $vendorOutput[$i] = $vendorRow['vendor'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $lastTradeInfoVar = '';
    if(isset($_POST['submitBtn']))
    {
      $clientInfoQuery  = "SELECT * FROM client
                                WHERE clientId = ".$_POST['clientId'];
      $clientInfoResult = mysql_query($clientInfoQuery);
      
      while($clientInfoRow = mysql_fetch_array($clientInfoResult))
      {
        $nameToDisplay = $clientInfoRow['firstName']." ".$clientInfoRow['middleName']." ".$clientInfoRow['lastName'];
      }
      
      if(isset($_POST['standing']) && $_POST['standing']==1)
        $standDisplay = " * Close Standing";
      elseif(isset($_POST['standing']) && $_POST['standing']==2)
        $standDisplay = " * Open Standing";
      else
        $standDisplay = "";
      if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
        $expiryDate = $_POST['expiryDate'];
      else
        $expiryDate = "";
      //$lastTradeInfoVar = $nameToDisplay." * Date : ".$_POST['tradeDay']."-".$_POST['tradeMonth']."-".$_POST['tradeYear']." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty'].$standDisplay." * Vendor : ".$_POST['vendor'];
    }
    else
    {
      $lastTradeIdQuery  = "SELECT max(tradeId) AS maxTradeId FROM tradetxtf_o";
      $lastTradeIdResult = mysql_query($lastTradeIdQuery);
      
      while($lastTradeIdRow = mysql_fetch_array($lastTradeIdResult))
      {
        if($lastTradeIdRow['maxTradeId'] > 0)
        {
          $lastTradeInfoQuery  = "SELECT * FROM tradetxtf_o
                                    WHERE tradeId = ".$lastTradeIdRow['maxTradeId'];
          $lastTradeInfoResult = mysql_query($lastTradeInfoQuery);
          
          while($lastTradeInfoRow = mysql_fetch_array($lastTradeInfoResult))
          {
            $clientInfoQuery  = "SELECT * FROM client
                                      WHERE clientId = ".$lastTradeInfoRow['clientId'];
            $clientInfoResult = mysql_query($clientInfoQuery);
            
            while($clientInfoRow = mysql_fetch_array($clientInfoResult))
            {
              $nameToDisplay = $clientInfoRow['firstName']." ".$clientInfoRow['middleName']." ".$clientInfoRow['lastName'];
            }
            
            if($lastTradeInfoRow['standing']==1)
              $standDisplay = " * Close Standing";
            elseif($lastTradeInfoRow['standing']==2)
              $standDisplay = " * Open Standing";
            else
              $standDisplay = "";
    
            //$lastTradeInfoVar = $nameToDisplay." * Date : ".substr($lastTradeInfoRow['tradeDate'],8,2)."-".substr($lastTradeInfoRow['tradeDate'],5,2)."-".substr($lastTradeInfoRow['tradeDate'],2,2)." * Price : ".$lastTradeInfoRow['price']." * ".$lastTradeInfoRow['itemId']." ".$lastTradeInfoRow['expiryDate']." * ".$lastTradeInfoRow['buySell']." * Qty : ".$lastTradeInfoRow['qty'].$standDisplay." * Vendor : ".$lastTradeInfoRow['vendor'];
          }
        }
      }
    }
//////////////////////////////////////////////////
   $selectItem="SELECT * FROM item ORDER BY itemId";
  
  $resultItem=mysql_query($selectItem);
  if(!$resultItem)
  {
    echo "No Item Selected";
  }
  else
  {
    $k=0;
    while($row=mysql_fetch_array($resultItem))
    {
      $itemId[$k]=$row['itemId'];
      $min[$k]=$row['min'];
      
      //SELECTION OF EXPIRYDATE:START
      
      $selectExpiry="SELECT * FROM expiry WHERE itemId='".$itemId[$k]."'";
      
      $resultExpiry=mysql_query($selectExpiry);
      if(!$resultExpiry)
      {
        echo "No Expiry Selected";
      }
      else
      {
        $l=0;
        while($expiryRow=mysql_fetch_array($resultExpiry))
        {
          $expiryDateToAssign[$k][$l]=$expiryRow['expiryDate'];
          $l++;
          $smarty->assign("expiryDate",$expiryDateToAssign);
          //$smarty->assign("expiryDateSelected",$expiryDateSelected);
        }
      }
      //SELECTION OF EXPIRYDATE:END
  
      $k++;
      $smarty->assign("itemId",$itemId);
      $smarty->assign("min",$min);
      $smarty->assign("l",$l);
      $smarty->assign("k",$k);
      
    }
  }
//////////////////////////////////////////////////
    if(isset($_POST['changedField']) && $_POST['changedField'] == "itemId")
      $focusScript = '<SCRIPT language="javascript">document.form1.clientId.focus();</SCRIPT>';
    elseif(isset($_POST['changedField']) && $_POST['changedField'] == "clientId")
      $focusScript = '<SCRIPT language="javascript">document.form1.clientId.focus();</SCRIPT>';
    else
      $focusScript = '<SCRIPT language="javascript">document.form1.clientId.focus();</SCRIPT>';
  //////////////////////////////////////////////////
    if(isset($_GET['forStand']))
      $forStand = $_GET['forStand'];
    elseif(isset($_POST['forStand']))
      $forStand = $_POST['forStand'];
    else
      $forStand = 0;
  //////////////////////////////////////////////////
    $smarty->assign("itemFromPriceJS",  $itemFromPriceJS);
    $smarty->assign("PHP_SELF",         $_SERVER['PHP_SELF']);
    $smarty->assign("firstName",        $firstName);
    $smarty->assign("middleName",       $middleName);
    $smarty->assign("lastName",         $lastName);
    $smarty->assign("clientIdSelected", $clientIdSelected);
    $smarty->assign("clientIdValues",   $clientIdValues);
    $smarty->assign("clientIdOutput",   $clientIdOutput);
    $smarty->assign("tradeDateDisplay", $tradeDateDisplay);
    $smarty->assign("buySellSelected",  $buySellSelected);
    $smarty->assign("buySellValues",    $buySellValues);
    $smarty->assign("buySellOutput",    $buySellOutput);
    $smarty->assign("itemIdSelected",   $itemIdSelected);
    $smarty->assign("itemIdValues",     $itemIdValues);
    $smarty->assign("itemIdOutput",     $itemIdOutput);
    $smarty->assign("expiryDateSelected", $expiryDateSelected);
    $smarty->assign("expiryDateValues"  , $expiryDateValues);
    $smarty->assign("expiryDateOutput"  , $expiryDateOutput);
    $smarty->assign("lastPrice"         , $lastPrice);
    $smarty->assign("vendorSelected"    , $vendorSelected);
    $smarty->assign("vendorValues"      , $vendorValues);
    $smarty->assign("vendorOutput"      , $vendorOutput);
    $smarty->assign("clientWholeName"   , $clientWholeName);
    $smarty->assign("currentBal"        , $currentBal);
    $smarty->assign("total"             , ($deposit + $currentBal));
    $smarty->assign("deposit"           , $deposit);
    $smarty->assign("phone"             , $phone);
    $smarty->assign("mobile"            , $mobile);
    $smarty->assign("minQty"            , $minQty);
    $smarty->assign("lastTradeInfoVar"  , $lastTradeInfoVar);
    $smarty->assign("focusScript"  ,      $focusScript);
    $smarty->assign("forStand"          , $forStand);
    $smarty->display("tradeAddAjax.tpl");
  }
  else
    echo "No clients added !";
?>