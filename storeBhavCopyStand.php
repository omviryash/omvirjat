<?php session_start(); ?><?php

  include "./etc/om_config.inc";
// session_start();

if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=storeBhavCopyStand");
}
else
{
?>
<HTML>
<HEAD><TITLE>Om !!! Your Trades</TITLE>
<SCRIPT language="javascript">
window.name = 'displayAll';
function changeStandingDt()
{
  document.dateForm.submit();
}
</SCRIPT>
</HEAD>
<?php
  $buyRowColor        = '#80FFFF';//80BFFF//FFCEE7';
  $sellRowColor       = '#FFFEE7';
?>
<BODY>
<?php
  //include "header.php";
?>
<?php
  $dayToAdd = 1;
  $openDate = date("Y-m-d", mktime(0, 0, 0, 
                                   substr($_SESSION['toDate'],5,2), 
                                   substr($_SESSION['toDate'],8,2)+$dayToAdd, 
                                   substr($_SESSION['toDate'],0,4)));
  $colSpan = 14;
  $totBuyQty = 0;
  $totSellQty = 0;
  $totBuyAmount = 0;
  $totSellAmount = 0;
  $profitLoss = 0;
  $profLossToDisplay = 0;
	$buyQtyPending = 0;
	$sellQtyPending = 0;
	$oldTradeDate      = '';
  $currentDt         = '';
  $currentDtBuyQty    = 0;
  $currentDtSellQty   = 0;
  $currentDtBuyQtyForBrok = 0;
  $currentDtSellQtyForBrok = 0;
  $grossBuyQty        = 0;
  $grossSellQty       = 0;
  $grossBuyAmt        = 0;
  $grossSellAmt       = 0;
  $grossProfitLoss    = 0;
  $grossQtyForBrok = 0;
  
  $query = "SELECT * FROM tradetxt";
  $whereGiven = false;
  if(isset($_GET['clientId']))
  {
    $query .= "   WHERE clientId = ".$_GET['clientId'];
    $whereGiven = true;
  }
///////////////////////////////
   import_request_variables('p', 'p_');                              
   if(isset($_SESSION['fromDate']) && isset($_SESSION['toDate']))
   {//WHERE tradeDate >=  '2004-08-03' AND tradeDate <=  '2004-08-04'
     if($whereGiven)
       $query .= " AND tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
     else
     {
       $query .= " WHERE tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'" ;
       $whereGiven = true;
     }
   }
///////////////////////////////
  $query .= "   ORDER BY firstName, middleName, lastName, itemId ASC, expiryDate, tradeDate ASC, tradeId";
  $result = mysql_query($query);
  if(!$result)
    echo "<BR>".mysql_error()." Om query : ".$query;
  $oldClientId = 0;
  $oldItemId = '';
  $oldExpiryDate = '';
  while($row = mysql_fetch_array($result))
  {
    $tradeId   = $row['tradeId'];
    $clientId  = $row['clientId'];
    $itemId    = $row['itemId'];
    $tradeDate = substr($row['tradeDate'],8,2)."-".substr($row['tradeDate'],5,2)."-".substr($row['tradeDate'],0,4);
    $buySell   = $row['buySell'];
    $expiryDate = $row['expiryDate'];
    $tradeBrok = $row['brok'];
    
    if($oldClientId != 0 && ($oldClientId != $clientId || $oldItemId != $itemId || $oldExpiryDate != $expiryDate))
    {
    	if($grossBuyQty != 0)
    	  $dispBuyRash = round($grossBuyAmt/$grossBuyQty);
    	else
    	  $dispBuyRash = '&nbsp;';
    	  
    	if($grossSellQty != 0)
    	  $dispSellRash = round($grossSellAmt/$grossSellQty);
    	else
    	  $dispSellRash = '&nbsp;';
    	  
	    if($currentDtBuyQty >= $currentDtSellQty)
	      $grossQtyForBrok = $grossQtyForBrok + $currentDtBuyQtyForBrok;
	    else
  	    $grossQtyForBrok = $grossQtyForBrok + $currentDtSellQtyForBrok;
	    
	  $brokToDisplay = $grossQtyForBrok*$brok;
    
	  if($grossBuyQty != $grossSellQty)
	  {
      $settlementPrice = getSettlementPrice($oldItemId, $oldExpiryDate);
      
  	  if($grossBuyQty > $grossSellQty)
  	  {
        $insertCloseQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Sell', '".$oldItemId."', '".$_SESSION['toDate']."', '".($grossBuyQty - $grossSellQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertCloseResult = mysql_query($insertCloseQuery);
			  if(!$insertCloseResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertCloseQuery;
      }
      else
      {
        $insertCloseQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Buy', '".$oldItemId."', '".$_SESSION['toDate']."', '".($grossSellQty - $grossBuyQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertCloseResult = mysql_query($insertCloseQuery);
			  if(!$insertCloseResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertCloseQuery;
      }
      
  	  if($grossBuyQty > $grossSellQty)
  	  {
        $insertOpenQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (-1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Buy', '".$oldItemId."', '".$openDate."', '".($grossBuyQty - $grossSellQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertOpenResult = mysql_query($insertOpenQuery);
			  if(!$insertOpenResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertOpenQuery;
      }
      else
      {
        $insertOpenQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (-1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Sell', '".$oldItemId."', '".$openDate."', '".($grossSellQty - $grossBuyQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertOpenResult = mysql_query($insertOpenQuery);
			  if(!$insertOpenResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertOpenQuery;
      }
    }
    }
    
    if($oldClientId != $clientId)
    {
      $oldItemId = '';
      $oldExpiryDate = '';
    }
    
    if($oldItemId != $itemId || $oldExpiryDate != $expiryDate)
    {
		  $totBuyQty = 0;
		  $totSellQty = 0;
		  $totBuyAmount = 0;
		  $totSellAmount = 0;
		  $profitLoss = 0;
		  $profLossToDisplay = 0;
			$buyQtyPending = 0;
			$sellQtyPending = 0;
			$oldTradeDate      = '';
		  $currentDt         = '';
		  $currentDtBuyQty    = 0;
		  $currentDtSellQty   = 0;
		  $currentDtBuyQtyForBrok    = 0;
		  $currentDtSellQtyForBrok   = 0;
		  $grossBuyQty        = 0;
		  $grossSellQty       = 0;
		  $grossBuyAmt        = 0;
		  $grossSellAmt       = 0;
		  $grossProfitLoss    = 0;
      $grossQtyForBrok = 0;
		  
      $selectClientQuery = "SELECT * FROM client
                              WHERE clientId = ".$clientId;
      $ClientResult = mysql_query($selectClientQuery);
      
      $oldClientId = 0;
      while($selectClientRow = mysql_fetch_array($ClientResult))
      {
        $firstName  = $selectClientRow['firstName'];
        $middleName = $selectClientRow['middleName'];
        $lastName   = $selectClientRow['lastName'];
      }
    }
    
    if($buySell == 'Buy')
    {
      $rowColor  = $buyRowColor;
      $buyColor  = $sellRowColor;
      $sellColor = $buyRowColor;
      $buyQty = $row['qty'];
      $sellQty = 0;
    }
    else
    {
      $rowColor  = $sellRowColor;
      $buyColor  = $buyRowColor;
      $sellColor = $sellRowColor;
      $buyQty = 0;
      $sellQty = $row['qty'];
    }
    
    $totBuyQty  = $totBuyQty  + $buyQty;
    $totSellQty = $totSellQty + $sellQty;
	  $grossBuyQty        = $grossBuyQty  + $buyQty;
	  $grossSellQty       = $grossSellQty + $sellQty;
  	$buyQtyPending = $buyQtyPending + $buyQty;
  	$sellQtyPending = $sellQtyPending + $sellQty;
    
    if($oldTradeDate != $tradeDate)
    {
		  $currentDt          = $tradeDate;
		  
	    if($currentDtBuyQty >= $currentDtSellQty)
	      $grossQtyForBrok = $grossQtyForBrok + $currentDtBuyQtyForBrok;
	    else
  	    $grossQtyForBrok = $grossQtyForBrok + $currentDtSellQtyForBrok;
	    
		  $currentDtBuyQty    = $buyQty;
		  $currentDtSellQty   = $sellQty;
    	if($tradeBrok != 0)
    	{
			  $currentDtBuyQtyForBrok    = $buyQty;
			  $currentDtSellQtyForBrok   = $sellQty;
		  }
		  else
		  {
			  $currentDtBuyQtyForBrok    = 0;
			  $currentDtSellQtyForBrok   = 0;
			}
    }
    else
    {
    	$currentDtBuyQty = $currentDtBuyQty + $buyQty;
    	$currentDtSellQty = $currentDtSellQty + $sellQty;
    	
    	if($tradeBrok != 0)
    	{
	    	$currentDtBuyQtyForBrok = $currentDtBuyQtyForBrok + $buyQty;
	    	$currentDtSellQtyForBrok = $currentDtSellQtyForBrok + $sellQty;
	    }
    }
    
    $itemSelectQuery = "SELECT * FROM item
                          WHERE itemId = '".$itemId."'";
    $itemResult = mysql_query($itemSelectQuery);
    
    while($itemRow = mysql_fetch_array($itemResult))
    {
      $brok    = $itemRow['brok'];
      $priceOn = $itemRow['priceOn'];
    }
    
    $amount        = $row['qty']*$row['price'];
    
    if($buyQty > 0)
    {
      $totBuyAmount = $totBuyAmount + $amount;
      $grossBuyAmt  = $grossBuyAmt  + $amount;
    }
    else
    {
      $totSellAmount = $totSellAmount + $amount;
      $grossSellAmt  = $grossSellAmt  + $amount;
    }
    
    if($totBuyQty > 0 && $totSellQty > 0)
    {
      $prevProfitLoss = $profitLoss;
	      if($totBuyQty >= $totSellQty)
	      {
	        $qtyToCount = $sellQtyPending;
	        $buyAmtToCount = $totBuyAmount/$totBuyQty*$qtyToCount;
	        $profitLoss = $totSellAmount - $buyAmtToCount;
	      }
	      else
	      {
	        $qtyToCount = $buyQtyPending;
	        $sellAmtToCount = $totSellAmount/$totSellQty*$qtyToCount;
	        $profitLoss = $sellAmtToCount - $totBuyAmount;
	      }
      $profLossToDisplay = $profitLoss - $prevProfitLoss;

      //if($totBuyQty == $totSellQty)
      if($profLossToDisplay > 0)
      {
      	$profitLoss = 0;
      	$prevProfitLoss = 0;
      	$buyQtyPending = 0;
      	$sellQtyPending = 0;
      	$totBuyAmount   = 0;
      	$totSellAmount  = 0;
      	$totBuyQty = 0;
      	$totSellQty = 0;
      }
    }
    else
    {
      $profitLoss = 0;
      $profLossToDisplay = 0;
    }
    
    if($buySell == "Buy")
    {
      $buyQtyToDisplay = $buyQty;
      $sellQtyToDisplay = "&nbsp;";
      $buyPrice   = $row['price'];
      $sellPrice  = "&nbsp;";
      $buyAmount  = round($amount);
      $sellAmount = "&nbsp;";
    }
    else
    {
      $sellQtyToDisplay = $sellQty;
      $buyQtyToDisplay = "&nbsp;";
      $buyPrice   = "&nbsp;";
      $sellPrice  = $row['price'];
      $buyAmount  = "&nbsp;";
      $sellAmount = round($amount);
    }
    $qtyForBrok    = $currentDtBuyQty - $currentDtSellQty;
    $totalBrok     = '&nbsp;';
    $netProfitLoss = '&nbsp;';
    
    //$grossProfitLoss    = $grossProfitLoss + $profLossToDisplay;
    if($grossBuyQty == $grossSellQty)
      $grossProfitLoss      = $grossSellAmt - $grossBuyAmt;
    elseif($grossBuyQty > $grossSellQty)
    {
    	$grossBuyQtyToCount = $grossSellQty;
    	$grossBuyAmtToCount = $grossBuyAmt/$grossBuyQty*$grossBuyQtyToCount;
      $grossProfitLoss    = $grossSellAmt - $grossBuyAmtToCount;
    }
    else
    {
    	$grossSellQtyToCount = $grossBuyQty;
    	$grossSellAmtToCount = $grossSellAmt/$grossSellQty*$grossSellQtyToCount;
      $grossProfitLoss     = $grossSellAmtToCount - $grossBuyAmt;
    }
    
    if($tradeBrok == 0)
      $stand = "*Stand";
    else
      $stand = "&nbsp;";
      
    $oldClientId = $clientId;
    $oldItemId   = $itemId;
    $oldExpiryDate   = $expiryDate;
    $oldTradeDate   = $tradeDate;
  }

    	if($grossBuyQty != 0)
    	  $dispBuyRash = round($grossBuyAmt/$grossBuyQty);
    	else
    	  $dispBuyRash = '&nbsp;';
    	  
    	if($grossSellQty != 0)
    	  $dispSellRash = round($grossSellAmt/$grossSellQty);
    	else
    	  $dispSellRash = '&nbsp;';
    	  
	    if($currentDtBuyQty >= $currentDtSellQty)
	      $grossQtyForBrok = $grossQtyForBrok + $currentDtBuyQtyForBrok;
	    else
  	    $grossQtyForBrok = $grossQtyForBrok + $currentDtSellQtyForBrok;

////////////////////////////////////////////////
	  if($grossBuyQty != $grossSellQty)
	  {
      $settlementPrice = getSettlementPrice($oldItemId, $oldExpiryDate);
      
  	  if($grossBuyQty > $grossSellQty)
  	  {
        $insertCloseQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Sell', '".$oldItemId."', '".$_SESSION['toDate']."', '".($grossBuyQty - $grossSellQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
        $insertCloseResult = mysql_query($insertCloseQuery);
        
        if(!$insertCloseResult)
          echo mysql_error()."<BR>".$insertCloseQuery;
      }
      else
      {
        $insertCloseQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Sell', '".$oldItemId."', '".$_SESSION['toDate']."', '".($grossSellQty - $grossBuyQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
        $insertCloseResult = mysql_query($insertCloseQuery);
        
        if(!$insertCloseResult)
          echo mysql_error()."<BR>".$insertCloseQuery;
      }
      
  	  if($grossBuyQty > $grossSellQty)
  	  {
        $insertOpenQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (-1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Buy', '".$oldItemId."', '".$openDate."', '".($grossBuyQty - $grossSellQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertOpenResult = mysql_query($insertOpenQuery);
			  if(!$insertOpenResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertOpenQuery;
      }
      else
      {
        $insertOpenQuery = "INSERT INTO tradetxt (standing,clientId,firstName,middleName,lastName,
                                           buySell,itemId,tradeDate,qty,price,expiryDate,vendor)
                            VALUES (-1, ".$oldClientId.", '".$firstName."', '".$middleName."', '".$lastName."',
                              'Sell', '".$oldItemId."', '".$openDate."', '".($grossSellQty - $grossBuyQty)."',
                              '".$settlementPrice."', '".$oldExpiryDate."', '_SELF')
                           "; 
			  $insertOpenResult = mysql_query($insertOpenQuery);
			  if(!$insertOpenResult)
			    echo "<BR>".mysql_error()." Om query : ".$insertOpenQuery;
      }
    }
///////////////////////////////////////////////
?>
</TABLE>
</BODY>
</HTML>
<?php
}

function getSettlementPrice($itemId, $expiryDate)
{
	$settlementPrice = 0;
	$settlementQuery = "SELECT * FROM bhavcopy
	                    WHERE bhavcopyDate = '".$_SESSION['toDate']."'
	                      AND contractCode LIKE '".$itemId."'
	                      AND expiryDateBc LIKE '".$expiryDate."'";
  $settlementResult = mysql_query($settlementQuery);
  if(!$settlementResult)
    echo "<BR>".mysql_error()." Om query : ".$settlementQuery;
  while($settlementRow = mysql_fetch_array($settlementResult))
  {
    $settlementPrice = $settlementRow['settlementPrice'];
  }
  return $settlementPrice;
}
?>