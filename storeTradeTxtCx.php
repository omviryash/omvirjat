<?php
session_start();
include "etc/om_config.inc";
if(!isset($_SESSION['toDate'])) 
{
  header("Location: selectDtSession.php?goTo=writeTradesFile");
}
else
{
  $row = 1;
  $filePath = 'D:\Om\OnlineBackup\comex';
  $fileName = 'Om_COMEX_'.$_SESSION['toDate'].'.txt';
  
  $row = 0;
  $handle = fopen($filePath."/".$fileName, "r");
  while (($data = fgetcsv($handle, 1000, ",")) !== FALSE)
  {
    $num = count($data);
    $row++;
    $expiryDate = trim($data[5]);
    $tradeRefNo = trim($data[0])." # ".trim($data[6]);
  
    if ($data[14]==1)
      $buySell = "Buy";
    else
      $buySell = "Sell";
  
    $monthStr = substr($data[23],3,3);
    switch ($monthStr)
    {
      case "Jan":
        $monthInt = '01';
        break;
      case "Feb":
        $monthInt = '02';
        break;
      case "Mar":
        $monthInt = '03';
        break;
      case "Apr":
        $monthInt = '04';
        break;
      case "May":
        $monthInt = '05';
        break;
      case "Jun":
        $monthInt = '06';
        break;
      case "Jul":
        $monthInt = '07';
        break;
      case "Aug":
        $monthInt = '08';
        break;
      case "Sep":
        $monthInt = '09';
        break;        
      case "Oct":     
        $monthInt = '10';
        break;
      case "Nov":
        $monthInt = '11';
        break;
      case "Dec":
        $monthInt = '12';
        break;
    }
    
    $tradeDate = substr($data[23],7,4);
    $tradeDate .= '-';
    $tradeDate .= $monthInt;
    $tradeDate .= '-';
    //$date .= substr($data[23],3,3); 
    $tradeDate .= substr($data[23],0,2);
    $tradeTime = substr($data[23],12,8);
  
    //Checking of tradeRefNo in table start
    $selectTradeRefNoQuery = "SELECT * FROM tradetxtcx
                                WHERE tradeRefNo = '".$tradeRefNo."'";
    $selectTradeRefNoResult = mysql_query($selectTradeRefNoQuery);
  
    $storeThisRow = true;
    while($selectTradeRefNoRow = mysql_fetch_array($selectTradeRefNoResult))
    {
      $storeThisRow = false;
    }
    //Checking of tradeRefNo in table over
  
    if($storeThisRow)
    {
      $qty = $data[15];
      $itemID = trim($data[4]);
      $brok = "0";
      
      $firstName = '';
      $middleName = '';
      $lastName = '';
      //////////
      $clientQuery = "SELECT * FROM client WHERE clientId = '".trim($data[28])."'";
      $clientResult = mysql_query($clientQuery);
      if($clientRow = mysql_fetch_array($clientResult))
      {
        $firstName  = $clientRow['firstName'];
        $middleName = $clientRow['middleName'];
        $lastName   = $clientRow['lastName'];
      }
      //////////
      /////////////////
      $actualQty = $qty;
      $itemQuery = "SELECT * FROM item
                    WHERE itemId LIKE '".trim($data[4])."'";
      $itemResult = mysql_query($itemQuery);
      if($itemRow = mysql_fetch_array($itemResult))
      {
        $actualQty = $qty * $itemRow['min'];
      }
      /////////////////
      $insertQuery = "INSERT INTO tradetxtcx (buySell,itemId,qty,price,expiryDate,tradeDate,tradeTime,brok,
                                              tradeRefNo,clientId,firstName,middleName,lastName,userRemarks,confirmed)
                      VALUES ('".$buySell."','".$itemID."','".$qty."','".$data[16]."','".$data[5]."','".$tradeDate."',
                              '".$tradeTime."','".$brok."','".$tradeRefNo."','".trim($data[28])."','".$firstName."',
                               '".$middleName."','".$lastName."','".$data[28]."','".$data[29]."')";
      $result = mysql_query($insertQuery)or die("Insert Query Failed".mysql_error());
    }
  }
  fclose($handle);
  
  echo "The database has been updated !!! Go to <A href='index.php'>Index</A> page.";
}
?>
