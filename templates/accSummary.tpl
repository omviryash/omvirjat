<BODY>
<center>  
<A href="./mnuAccount.php">Menu</A><BR><BR>
<FORM name=Form1 action="{$PHP_SELF}" method=POST>

  <TABLE BORDER=1 cellspacing="0" cellpadding="3">
  <TR>
    <TD vAlign="top" colspan="4">
      <A href="selectDtSession.php?goTo=accSummary">Date range</A> : {$toDate}
    </TD>
  <TR>
    <TH align="center">Name</TH>
    <TH align="center">No</TH>
    <TH align="center">Credit</TH>
    <TH align="center">Debit</TH>
  </TR>
   {section name=sec1 loop=$clientArray}
 
 {if !($clientArray[sec1].balance == 0)}
 
  <TR>
    <TD align="center" bgcolor="#FFFFC4">
      {$clientArray[sec1].firstName} {$clientArray[sec1].middleName} {$clientArray[sec1].lastName} 
    </TD>
    <TD align="center">
      {$clientArray[sec1].clientId}&nbsp;&nbsp;&nbsp;
    </TD>
    <TD align="right">
      {if $clientArray[sec1].balance >= 0}
	    <span style="color:#0000FF"> {$clientArray[sec1].balance} </span>
      {else} 
         &nbsp;  
      {/if}
    </TD>
    <TD align="right">
      {if $clientArray[sec1].balance < 0}
	   <span style="color:#ff0000">{$clientArray[sec1].balance} </span>
      {else}                               &nbsp;
      {/if}
    </TD>
  </TR>
  {/if}
    {/section}
   <TR>
    <TD Align="right" >
      <b>Total :     </b>
    </TD>
    <TD Align="right" >
    &nbsp;
    </TD>
    <TD Align="right">
      <b style="color:#0000FF">{$creditTotal}</b>
    </TD>
    <TD Align="right">
      <b style="color:#ff0000">{$debitTotal}</b>
    </TD>
   </TR>
   <TR>
    <TD Align="right" >
      <b>Total Balance :     
    </TD>
    <TD Align="center" colspan="3">
	{if $grandTotal >= 0}
      <b style="color:#0000FF">{$grandTotal}</b>
	  {else} 
      <b style="color:#ff0000">{$grandTotal}</b>
    {/if}	  
    </TD>
   </TR>
</TABLE>
   <!--Balance as on date {$toDate} = <B>{$totalBalOnDate}</B>-->
</BODY>
</center>