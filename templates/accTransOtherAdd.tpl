<HTML>
<HEAD><TITLE>Om !!!</TITLE>
  <STYLE src="./templates/styles.css"></STYLE>
<SCRIPT language="javascript">
{literal}  
function bodyKeyPress()
{
  if(event.keyCode==107)
  {
    window.document.bgColor="skyblue";
    document.getElementById('cr').checked = true;
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="pink";
    document.getElementById('dr').checked = true;
    return false;
  }
}  
{/literal}
</SCRIPT>
</HEAD>
<BODY bgColor="skyblue" onKeyDown="return bodyKeyPress();">
<FORM name="form1" action="{$PHP_SELF}" method=POST>
<A href="./index.php"><font color="black">Home</font></A>&nbsp;&nbsp;<A href="./accTransList.php"><font color="black">List</font></A>&nbsp;&nbsp;<A href="./mnuAccount.php"><font color="black">Menu</font></A><BR><BR>
<TABLE>
    <TR>
      <TD>Name</TD>
      <TD><SELECT name = "cboName">
            {section name=sec1 loop=$fName}
              <OPTION value={$clId[sec1]}>
                {$fName[sec1]}
              </OPTION>
            {/section}
        </SELECT></TD>
    </TR>
    <TR>
      <TD>Date</TD>
      <TD> {html_select_date prefix="entryDate" start_year=1990 end_year=2025 day_format="%02d" month_format="%b" field_order="DMY"}</TD>
    </TR>
   <TR style="display:none;">
   	<TD>Transaction In</TD>
   	<TD><select name="transMode">
   				<option value="Cash">- Cash -</option>
   				{section name=secBank loop=$bank}
   				    <OPTION value="{$bank[secBank]}">
                {$bank[secBank]}
              </OPTION>
   				{/section}
   		</select></Td>
   </TR>
   <TR>
     <TD><INPUT type="radio" name="r1" id="cr" value="d" checked>Credit
         &nbsp;&nbsp;
         <INPUT type="radio" name="r1" id="dr" value="w">Debit</TD>
     <TD><INPUT type="text" name="txtdwAmount"></TD>
   </TR>
   <TR>
     <TD>Transaction Type : </TD>
     <TD>
         <INPUT type="radio" name="transType" value="Other" checked>Other
         &nbsp;&nbsp;&nbsp;
         <INPUT type="radio" name="transType" value="Margin">Margin
     </TD>
   </TR>
   <TR>
     <TD>Note : </TD>
     <TD><!--<INPUT type="text" name="itemIdExpiryDate" size="50">--><TEXTAREA style="text-transform:uppercase;" name="itemIdExpiryDate" rows=3 cols=50></TEXTAREA></TD>
   </TR>
   <TR>
      <TD><INPUT type=submit value="Submit Me">
         <INPUT type=reset value="Reset"></TD>
   </TR>
</TABLE>
</FORM>
{literal}
<style>
td,th
{
border:1px solid #999;
}
tr
{

}
</style>
{/literal}

<table style="width:600px;border:1px solid #999;border-collapse:collapse;" bgcolor="white" cellpadding="5" cellspacing="0">
<tr>
	<th align="center">Client ID</th>
    <th align="center">Date</th>
    <!--<th align="center">Transaction In</th>-->
    <th align="center">Credit/Debit</th>
    <th align="right">Amount</th>
        <th align="center">Note</th>
</tr>
 {section name=seccf loop=$cf}
<tr>
<td align="center">{$cf[seccf].clientId}</td>
<td align="center">{$cf[seccf].transactionDate}</td>
<!--<td align="center">{$cf[seccf].transMode}</td>-->
<td align="center">{if $cf[seccf].dwStatus == 'd'} Jama {else} Udhar {/if}</td>
<td align="right" style="color:{if $cf[seccf].dwStatus == 'd'} blue {else} red {/if}">{$cf[seccf].dwAmount}</td>
<td align="center">{$cf[seccf].itemIdExpiryDate}</td>
</tr>
{/section}
</table>
</BODY>
</HTML>