<HTML>
<HEAD><TITLE>MCX Order Entry</TITLE>
<style>
{literal}
td { font-color="white";FONT-SIZE: 16px; font-family:arial }
th { FONT-SIZE: 16px; font-family:arial }
.bluetd{ font-weight:bold; }
.redtd{ font-weight:bold; }
input {FONT-SIZE: 15px; font-weight: bold;}
select {FONT-SIZE: 15Px; font-weight: bold; }
input:focus{color:#F60;}
select:focus{color:#F60;}
{/literal}
</style>
<script src="./js/jquery.min.js"></script>
<SCRIPT language="javascript">
{literal}


  $(document).ready(function()
  {
    if($("#hide_show").val() == "Show List")
	{
		$("#list").hide();
	}
	else if($("#hide_show").val() == "Hide List")
	{
		$("#list").show();
	}
	$("#hide_show").click(function()
	{
		if($("#hide_show").val() == "Show List")
		{
			$("#list").show();
			$("#hide_show").val("Hide List");
		}
		else
		{
			$("#list").hide();
			$("#hide_show").val("Show List");
		}
	});
});
window.name = 'displayAll';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiryDate;
  select2value.options.length=0;
{/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}"); 
        {/if}
      {/section}
    {literal}
      
    }
      {/literal}
  {/section}
 }
{literal}
function askConfirm()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    return true;
  }
  else
    return false;
}
function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

var addingQty=0;
function changeQty(event)
{
  //alert(event.keyCode);
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(addingQty==0)
    addingQty=qty;
  
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
    {
      qty=qty+addingQty;
      document.form1.qty.value=qty;
    }
    if(event.keyCode==40)
    {
      qty=qty-addingQty;
      document.form1.qty.value=qty;
    }
  }
}
function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}

function bodyKeyPress()
{
  var price=(document.form1.priceValue.value);
  /*if(event.keyCode==107)
  {
    window.document.bgColor="blue";
    document.form1.buySellText.value = "Buy";
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="red";
    document.form1.buySellText.value = "Sell";
    document.form1.buySell.value = "Sell";
    return false;
  }
  if(event.keyCode==117)
  {
    orderListWindow=window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==119)
  {
    tradeListWindow=window.open('clientTrades.php', 'tradeListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    alert(document.form1.price.value); 
  }*/
}
function orderTypeChanged()
{
  if(document.form1.orderType.value == "SL")
    document.form1.triggerPrice.disabled = 0;
  else
    document.form1.triggerPrice.disabled = 1;
}
function orderValidityChange()
{
  if(document.form1.orderValidity.value == "GTD")
  {
    document.form1.gtdDateDay.disabled = 0;
    document.form1.gtdDateMonth.disabled = 0;
    document.form1.gtdDateYear.disabled = 0;
    
  }
  else
  {
    document.form1.gtdDateDay.disabled = 1;
    document.form1.gtdDateMonth.disabled = 1;
    document.form1.gtdDateYear.disabled = 1;
    
  }
}
function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "./tradeAdd3.php?exchange1=MCX";
  else if($("#exchange").val() == "Comex")
    window.location.href = "./tradeAdd3cx.php?exchange1=Comex";
  else if($("#exchange").val() == "Share")
    window.location.href = "./share/addTrade.php?exchange=F_O&exchange1=Share";	
}
$(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 109)
      {
        $("#body").css("background-color",$("#sell_color").val());
        $("#buySell").val("Sell");
		$("#buySellText").val("Sell");
        return false;
      }
      if(code == 107)
      {
        $("#body").css("background-color",$("#buy_color").val());
        $("#buySell").val("Buy");
        $("#buySellText").val("Buy");
        return false;
      }
    });
{/literal}

</SCRIPT>
</HEAD>
<BODY bgColor="#{$buycolor}" id="body" onKeyDown="return bodyKeyPress();" onLoad="orderValidityChange();change();">
 <span><a href="index.php">Home</a> </span>
  <FORM name="form1" action="{$PHP_SELF}?exchange=F_O&showlist=1&tradeId={php} echo $_GET['tradeId']{/php}&forStand={php} echo $_GET['forStand']{/php}" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
<FONT color="white" style="FONT-SIZE: 12px;">
      <SELECT name="orderType" onChange="orderTypeChanged();" style="display:none">
        <option value="RL">RL</option>
        <option value="SL">SL</option>
      </SELECT>
	  <SELECT name="exchange" id="exchange" onChange="changeExchange();">
    	<option value="MCX" {$exchangeOnlyMCX}>MCX</option>
    	<option value="Comex" {$exchangeOnlyComex}>Comex</option>
		<option value="Share" {$exchangeOnlyShare}>Share</option>
    </SELECT>	
    <strong style="font-size:16px;">Date :</strong> 
      
	{php}
		session_start();
		if (isset($_SESSION['entry_user']))
		{{/php}
			{html_select_date time="$edit_tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d" }
	{php}	}
		else
		{{/php}
			{html_select_date time="$edit_tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d" }
{php}		}{/php}
	  <SELECT name="itemId" onChange="change();">
      {html_options selected="$edit_itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      {html_options selected="$edit_expiryDate" values="$expiryDateValues" output="$expiryDateOutput"}
      </SELECT>
      &nbsp;
<!-- onBlur="itemFromPrice();"-->
      <strong style="font-size:16px;">Qty :</strong> <INPUT type="text" name="qty" size="5" value="{$edit_qty}">
      &nbsp;
      <strong style="font-size:16px;">Price :</strong> <INPUT size="8" type="text" name="priceValue" value="{$lastPrice}" onKeydown="changePrice();">&nbsp;&nbsp;
    
      <span style="display:none">
      TrigPrice : <INPUT size="10" type="text" name="triggerPrice" value="{$lastTriggerPrice}" DISABLED>&nbsp;&nbsp;</span>
	  {if $forStand == 1}
        <INPUT type="radio" name="standing" value="-1" {if $edit_standing == -1} checked="checked" {/if}  /> <span>Open Standing</span>
        <INPUT type="radio" name="standing" value="1" {if $edit_standing == 1} checked="checked" {/if} /> <span>Close Standing</span>
      {else}
        <input type="hidden" name="standing" value="0" />
      {/if}
      <SELECT name="orderValidity" onChange="orderValidityChange();" style="display:none">
        <option value="EOS">EOS</option>
        <option value="GTD">GTD</option>
        <option value="GTC">GTC</option>
      </SELECT>

      <span style="display:none">{html_select_date prefix="gtdDate" field_order="DMY"}</span>
      <strong style="font-size:16px;">Client :</strong> 
      <INPUT type="text" name="clientId" value="{$edit_client}" style="width:50px">
      <INPUT type="text" name="vendorId" size="3" style="display:none">
      <INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();">
  
      <BR>
      <INPUT  type="text" name="buySellText" id="buySellText" value="Buy" size="5" style="display:none">
      <INPUT type="hidden" name="buySell" value="Buy" id="buySell">
      <input name="buy_color" value="#{$buycolor}" id="buy_color" type="hidden" />
       <input name="sell_color" value="#{$sellcolor}" id="sell_color" type="hidden" />
      <br>
<!--       <B>{$clientWholeName} : </B>
       Deposit : {$deposit}&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : {$currentBal}&nbsp;&nbsp;&nbsp;&nbsp;
       Total : {$total}&nbsp;&nbsp;&nbsp;&nbsp;  
       Phone   : {$phone}&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : {$mobile}&nbsp;
      <BR>-->

  {$focusScript}
   {if $currentBuySell == "Sell"}
    <script type = "text/javascript">
      window.document.bgColor=document.form1.sell_color.value;
      document.form1.buySellText.value = "Sell";
      document.form1.buySell.value = "Sell";
    </script>
  {/if}
  </FORM>
</BODY>
</HTML>