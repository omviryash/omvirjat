<HTML>
<HEAD><TITLE>Add Client</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
<FORM name="form1" action="{$PHP_SELF}" METHOD="POST" accept-charset="utf-8">
<A href="./index.php">Home</A>
<A href="./itemAdd.php">Add Item</A>
<TABLE BORDER=1 align="center" cellPadding="2" cellSpacing="0">
<TR>
  <TD align="center" colspan="5">
    <select name="mcxCombo" id="mcxCombo" onchange="document.form1.submit();">
      {html_options values=$mcxValuesArr output=$mcxOutputArr selected=$mcxComboSelected}
    </select>  
    Date :
    {html_select_date prefix="itemDate" start_year="-2" end_year="+1" month_format="%m"  field_order="DMY" day_value_format="%02d" time=$itemDate}   
    <INPUT type="submit" name="dateBtn" value="Go!">   
  </TD>
</TR>
<TR>
  <th align="center">Item</th>
  <th align="center">Exchange</th>
  <th align="center">High</th>
  <th align="center">Low</th>
  <th align="center">&nbsp;</th>
</TR>

{* <pre>
{$highArr|@var_dump}
</pre>  
  {debug} *}
{foreach key=itemKey item=itemId from=$itemName}

<TR>
<TD>{$itemId}</TD>
<TD>
  {$exchange[$itemId]}
  <INPUT type="hidden" size="6" name="exchange['{$itemId}']" value="{$exchange[$itemId]}" >  
</TD>
<TD>
  <INPUT type="text" size="6" name="high['{$itemId}']" value="{$highArr[$itemId]}" >
</TD>
<TD>
  <INPUT type="text" size="6" name="low['{$itemId}']" value="{$lowArr[$itemId]}" >
</TD>
<TD>
  <A href="editBrokerage.php?item={$itemId}">Edit</A>
  <A href="deleteBrokerage.php?item={$itemId}" onclick='return confirm("Are You Sure?");'>Delete</A>
</TD>
</TR>
{/foreach}   
<TR>
  <TD colspan='4' align="center"><INPUT type="submit" name="submitBtn" value="Submit me!"></TD>
</TR>
</TABLE>
  <SCRIPT language="javascript">
    document.form1.elements[0].focus();
  </SCRIPT>
</FORM>
</BODY>
</HTML>