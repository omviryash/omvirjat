<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
</STYLE>  
<script src="./js/jquery.min.js"></script>
<script type="text/javascript">
function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "./clientTrades.php?display=gross";
  else if($("#exchange").val() == "Comex")
    window.location.href = "./clientTradesComex.php?display=gross";
  else if($("#exchange").val() == "Share")
    window.location.href = "./share2/clientTradesPer2side2fo.php?display=gross";	
}
{/literal}
</script>
</HEAD>
<BODY>
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>
  </TD>
  <TD>
  Exchange : 
    <SELECT name="exchange" id="exchange" onChange="changeExchange();">
    	<option value="MCX" selected="selected">MCX</option>
    	<option value="Comex" >Comex</option>
		<option value="Share" >Share</option>
    </SELECT>
  </TD>
</TR>
</FORM>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
</TABLE>
<FORM name="form2" method="post" action="./acStorePl.php">
<TABLE border="1" cellPadding="2" cellSpacing="0">
  <INPUT type="hidden" name="isFrom" value="{$isFrom}" />
  
<TR>
  <TD colspan="5" align="center">ClientId</TD>
  <TD colspan="2" align="center">&nbsp;</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD colspan="2" align="center">NetProfitLoss</TD>
  <TD align="center"><INPUT type="submit" name="btnSubmit" value="Store to Money Transactions"></TD>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].dispGross != 0}
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="5" align="center">
      {$trades[sec1].clientId}
    </TD>
    <TD colspan="2">&nbsp;</TD>
    <TD align="right">
      {$trades[sec1].clientTotProfitLoss}
    </TD>
    <TD colspan="2" align="right">
      {$trades[sec1].clientTotBrok}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss < 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
    <TD align="right">
      {if $trades[sec1].clientTotNetProfitLoss >= 0}
        {$trades[sec1].clientTotNetProfitLoss}
      {else}
        &nbsp;
      {/if}
    </TD>
    <TD align="center" colspan="2">
      <INPUT type="checkbox" name="plFor[{$trades[sec1].clientId}]" value="{$trades[sec1].clientTotNetProfitLossNoFormat}">
	 <!-- <INPUT type="hidden" name="plclient[{$trades[sec1].clientId}]" value="{$trades[sec1].clientId}">-->
    </TD>
  </TR>
  {/if}
{/if}
{/section}
<TR><TD colspan="12">&nbsp;</TD>
  <TD align="center"><INPUT type="submit" name="btnSubmit" value="Store to Money Transactions"></TD>
</TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD colspan="2" align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>{$wholeItemArr[sec2].profitLoss}</TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}
      {$wholeItemArr[sec2].netProfitLoss}
    {else}
      &nbsp;
    {/if}
  </TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss >= 0}
      {$wholeItemArr[sec2].netProfitLoss}
    {else}
      &nbsp;
    {/if}
  </TD>
</TR>
{/section}
<TR>
  <TD align="right" colspan="10">&nbsp;</TD>
  <TD align="center" NOWRAP>{$wholeNetLossOnly}</TD>
  <TD align="center" NOWRAP>{$wholeNetProfitOnly}</TD>
</TR>
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" NOWRAP>{$wholeProfitLoss}</TD>
  <TD align="right" colspan="2">{$wholeOneSideBrok}</TD>
  <TD colspan="2" align="center" NOWRAP>{$wholeNetProfitLoss}</TD>
</TR>
</TABLE>
</FORM>
</BODY>
</HTML>
