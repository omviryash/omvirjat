<HTML>
<HEAD><TITLE>Om : Client Trades !!!</TITLE>
<STYLE>
{literal}
td{font-weight: BOLD}
.lossStyle   {color: red}
.profitStyle {color: blue}
</STYLE>  
<script src="./js/jquery.min.js"></script>
<script type="text/javascript">
function confirmedCheckbox(myObj, objValues)
{
  if($(myObj).is(':checked') == 1)
    $(".confirmed"+objValues).attr('checked', true);
  else
    $(".confirmed"+objValues).attr('checked', false);
}  
function changeExchange()
{
  if($("#exchange").val() == "MCX")
    window.location.href = "./clientTrades.php?exchange=MCX";
  else if($("#exchange").val() == "Comex")
    window.location.href = "./clientTradesComex.php?exchange=Comex";
  else if($("#exchange").val() == "Share")
    window.location.href = "./share2/clientTradesPer2side2fo.php?exchange=Share";	
}
function checkAllOptions(myObj)
{
  if($(myObj).is(':checked') == 1)
    $(".confirmcheck").attr('checked', true);
  else
    $(".confirmcheck").attr('checked', false);
}  

{/literal}
</script>
</HEAD>
<BODY bgColor="#FFFF80">
<FORM name="form1" method="get" action="{$PHP_SELF}">
<INPUT type="hidden" name="display" value="{$display}">
<INPUT type="hidden" name="itemIdChanged" value="0">
<TABLE width="100%" cellPadding="0" cellSpacing="0" border="0">
<TR>
  <TD>Client : 
    <SELECT name="clientId" onChange="document.form1.submit();">
    {html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}
    </SELECT>
  </TD>
  <TD>Item : 
    <SELECT name="itemId" onChange="document.form1.itemIdChanged.value=1;document.form1.submit();">
    {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}
    </SELECT>
  </TD>
  <TD>
  	Buy Sell : 
  	<select name="buySellOnly" onChange="document.form1.submit();">
      <option value="All"  {$buySellOnlyAll}>All</option>
      <option value="Buy"  {$buySellOnlyBuy}>Buy</option>
      <option value="Sell" {$buySellOnlySell}>Sell</option>
    </select>
  </TD>
	<TD>Group :
  <select name="groupName" onChange="document.form1.submit();">
    {html_options values=$groupName output=$groupName selected=$groupIdSelected}
  </select>	
  &nbsp;&nbsp;&nbsp;&nbsp;
  Expiry : 
    <SELECT name="expiryDate" onChange="document.form1.submit();">
    {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOptions"}
    </SELECT>	
  Exchange : 
    <SELECT name="exchange" id="exchange" onChange="changeExchange();">
    	<option value="MCX" {$exchangeOnlyMCX}>MCX</option>
    	<option value="Comex" {$exchangeOnlyComex}>Comex</option>
		<option value="Share" {$exchangeOnlyShare}>Share</option>
    </SELECT>	
  </TD>
</TR>
<TR>
  <TD colspan="3" align="center">
    <A href="selectDtSession.php?goTo=clientTrades">Date range</A> : {$fromDate} To : {$toDate}</CENTER>
  </TD>
</TR>
<TR><TD>&nbsp;</TD></TR>
<TR>
  <TD colspan="3" align="center">
    {$message}
  </TD>
</TR>
<TR><TD colspan="3" align="center"><input type="checkbox" class="confirmcheck" onClick="checkAllOptions(this);" />Check All</TD></TR>
</TABLE>
</FORM>
<FORM name="form2" method="post" action="{$PHP_SELF}">
  <INPUT type="hidden" name="display" value="{$display}">
  <INPUT type="hidden" name="itemIdChanged" value="0">
	<input type="hidden" name="clientId" value="{$clientIdSelected}">
	<input type="hidden" name="itemId" value="{$itemIdSelected}">
	<input type="hidden" name="buySellOnly" value="{$buySellOnlySelected}">
	<input type="hidden" name="groupName" value="{$groupIdSelected}">
	<input type="hidden" name="expiryDate" value="{$expiryDateSelected}">
	<input type="hidden" name="exchange" value="{$exchangeSelected}">
<TABLE border="1" cellPadding="2" cellSpacing="0">
<TR>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Buy</TD>
  <TD>&nbsp;</TD>
  <TD colspan="2" align="center">Sell</TD>
  <TD colspan="6">&nbsp;</TD>
</TR>
<TR>
  <TD align="center">BuySell</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD>&nbsp;</TD>
  <TD align="center">Qty</TD>
  <TD align="center">Price</TD>
  <TD align="center">Date</TD>
  <TD align="center">Stand</TD>
  <TD align="center">Item</TD>
  <TD align="center">NetProfitLoss</TD>
  <TD align="center">Vendor</TD>
  <TD align="center">Delete</TD>
  {if $display == "detailed"} 
    <TD align="center">UserRemarks</TD>
    <TD align="center">OwnClient</TD>
    <TD align="center" NOWRAP>TradeRefNo</TD>
  {/if}
</TR>
<TR>
  <TD colspan="2">&nbsp;</TD>
  <TD colspan="3" align="center">
  </td>
  <TD colspan="7">&nbsp;</td>
</TR>
{section name="sec1" loop="$trades"}
{if $trades[sec1].clientId != $trades[sec1].prevClientId or $trades[sec1].itemId != $trades[sec1].prevItemId or $trades[sec1].expiryDate != $trades[sec1].prevExpiryDate}
  <TR>
    <TD colspan="2"><U>{$trades[sec1].clientId}</U>&nbsp;:&nbsp;({$trades[sec1].clientDeposit})</TD>
    <TD colspan="3" align="center">
  	  <input type="submit" name="confirmBtn" value="! Done !" />
  	  <input type="checkbox" name="checkAll" class="checkAll{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry} confirmcheck" onClick="confirmedCheckbox(this,'{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry}')">
    </td>
    <TD colspan="3">&nbsp;</td>
    <TD colspan="4" align="left">{$trades[sec1].itemIdExpiry}</TD>
  </TR>
{/if}
<TR style="color:{$trades[sec1].fontColor}">
  <TD align="center">{$trades[sec1].buySell}</TD>
  <TD align="right">{$trades[sec1].buyQty}</TD>
  <TD align="right">{$trades[sec1].price}</TD>
  <TD align="right">
  	<input type="checkbox" name="confirmed[{$trades[sec1].tradeId}]" class="confirmed{$trades[sec1].clientId}{$trades[sec1].itemIdExpiry} confirmcheck" 
  	       value="{$trades[sec1].tradeId}" {if $trades[sec1].confirmed == 1}CHECKED{/if}>
  </TD>
  <TD align="right">{$trades[sec1].sellQty}</TD>
  <TD align="right">{$trades[sec1].sellPrice}</TD>
  <TD align="center" NOWRAP>
  	{$trades[sec1].tradeDate}
    {if $display == "detailed"} {$trades[sec1].tradeTime} {/if}
  </TD>
  <TD align="center">
  	{if $trades[sec1].standing != "Open" && $trades[sec1].standing != "Close"}
  	  {if $trades[sec1].highLowConf == 1}
  	  <font style="bgColor: black;color:{if $trades[sec1].fontColor == "red"}black{else}black{/if};"><b>@@@</b></font>
  	  {/if}
  	{/if}
  	{$trades[sec1].standing}
  </TD>
  <TD align="center" NOWRAP>{$trades[sec1].itemIdExpiry}</TD>
  <TD>&nbsp;</TD>
  <TD align="center">{$trades[sec1].vendor}&nbsp;</TD>
  <TD>
    <A onClick="return confirm('Are you sure?');" href="deleteTxt.php?goTo={$goTo}&tradeId={$trades[sec1].tradeId}">
    {if $trades[sec1].standing != "Close"}
      Delete
    {else}
      Delete Stand
    {/if}
    </A>
    &nbsp;&nbsp;
    <A href="tradeAdd3Edit.php?tradeId={$trades[sec1].tradeId}">Edit</a>
  </TD>
  {if $display == "detailed"} 
    <TD align="center">{$trades[sec1].userRemarks}</TD>
    <TD align="center">{$trades[sec1].ownClient}</TD>
    <TD align="center" NOWRAP>{$trades[sec1].tradeRefNo}</TD>
  {/if}
</TR>
{if $trades[sec1].dispGross != 0}
  <TR>
    <TD align="right" NOWRAP>
      Net: {math equation="totBuyQty-totSellQty" totBuyQty=$trades[sec1].totBuyQty totSellQty=$trades[sec1].totSellQty}
    </TD>
    <TD align="right">{$trades[sec1].totBuyQty}</TD>
    <TD align="right">{$trades[sec1].buyRash}</TD>
    <TD>&nbsp;</TD>
    <TD align="right">{$trades[sec1].totSellQty}</TD>
    <TD align="right">{$trades[sec1].sellRash}</TD>
  {if $trades[sec1].totBuyQty == $trades[sec1].totSellQty} 
    <TD colspan="3" align="right" NOWRAP>
      {if $trades[sec1].profitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].profitLoss}</FONT>
      Brok       : {$trades[sec1].oneSideBrok}
      <br />
      {$trades[sec1].brok1}
      <br />
      {$trades[sec1].brok2}
      </TD>
    <TD align="right" NOWRAP>
      {if $trades[sec1].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].netProfitLoss}</FONT>
    </TD>
    <TD colspan="2">&nbsp;</TD>
  {else}
    <TD colspan="6">{$trades[sec1].itemIdExpiry} : Buy Sell Qty Not Same</TD>
  {/if}
  </TR>
  {if $trades[sec1].dispClientWhole != 0}
  <TR>
    <TD colspan="6" align="right">
      : Total : {$trades[sec1].clientId}
    </TD>
    <TD colspan="3" align="right"><U>
      {if $trades[sec1].clientTotProfitLoss < 0}
        <FONT class="lossStyle">Loss : 
      {else}
        <FONT class="profitStyle">Profit : 
      {/if}
      {$trades[sec1].clientTotProfitLoss}</FONT></U>
      Brok       : {$trades[sec1].clientTotBrok}
      <br />
      {$trades[sec1].clientTotBrok1}
      <br />
      {$trades[sec1].clientTotBrok2}
      </U>
    </TD>
    <TD align="right"><U>
      {if $trades[sec1].clientTotNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
      {$trades[sec1].clientTotNetProfitLoss}</FONT></U></TD>
    <TD align="center" colspan="2">&nbsp;</TD>
  </TR>
  {/if}
{/if}
{/section}
<TR>
  <TD colspan="2">&nbsp;</TD>
  <TD colspan="3" align="center">
  	<input type="submit" name="confirmBtn" value="! Done !" />
  </td>
  <TD colspan="7">&nbsp;</td>
</TR>
<TR>
  <TD align="center">Net</TD>
  <TD>Buy</TD>
  <TD>Rash</TD>
  <TD>&nbsp;</TD>
  <TD>Sell</TD>
  <TD>Rash</TD>
  <TD>Check</TD>
  <TD colspan="2" align="center">Item</TD>
  <TD align="center">ProfitLoss</TD>
  <TD colspan="2" align="center">Brok</TD>
  <TD align="center">NetProfitLoss</TD>
</TR>
{section name="sec2" loop="$wholeItemArr"}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeItemArr[sec2].buyQty sellQty=$wholeItemArr[sec2].sellQty}
    </TD>
  <TD align="right">{$wholeItemArr[sec2].buyQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].buyRash}</TD>
  <TD>&nbsp;</TD>
  <TD align="right">{$wholeItemArr[sec2].sellQty}</TD>
  <TD align="right">{$wholeItemArr[sec2].sellRash}</TD>
  <TD><input type="checkbox" /></TD>
  <TD align="right" colspan="2" NOWRAP>{$wholeItemArr[sec2].itemIdExpiry}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].profitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].profitLoss}</FONT></TD>
  <TD align="right" colspan="2">{$wholeItemArr[sec2].oneSideBrok}</TD>
  <TD align="right" NOWRAP>
    {if $wholeItemArr[sec2].netProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeItemArr[sec2].netProfitLoss}</FONT></TD>
</TR>
{/section}
<TR>
    <TD align="right" NOWRAP>
      {math equation="buyQty-sellQty" buyQty=$wholeBuyQty sellQty=$wholeSellQty}
    </TD>
  <TD align="right">{$wholeBuyQty}</TD>
  <TD align="right">{$wholeBuyRash}</TD>
  <TD>&nbsp;</TD>
  <TD align="right">{$wholeSellQty}</TD>
  <TD align="right">{$wholeSellRash}</TD>
  <TD align="right" colspan="2" NOWRAP> : Total : </TD>
  <TD align="right" colspan="2"  NOWRAP>
    {if $wholeProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeProfitLoss}</FONT></TD>
  <TD align="right" colspan="2">
  	{$wholeOneSideBrok}<br />
  	{$wholeBrok1}<br />
  	{$wholeBrok2}
  </TD>
  <TD align="right" NOWRAP>
    {if $wholeNetProfitLoss < 0}<FONT class="lossStyle">{else}<FONT class="profitStyle">{/if}
    {$wholeNetProfitLoss}</FONT></TD>
</TR>
</form>
</TABLE>
</BODY>
</HTML>
