<html>
<HEAD><TITLE>Expiry Date Entry Form</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
<FORM name=form1 action="{$PHP_SELF}" method = POST>
<A href="index.php">Home</A>
<A href="expiryList.php">List</A>
<BR><BR>
<B>Set Expiry Date : </B>
<TABLE border="1">
 <TR>   
 	<TD>Exchange : </TD>
   <TD>
   	<select name="mcxCombo" id="mcxCombo" onchange="document.form1.submit();">
 		  {html_options values=$mcxValuesArr output=$mcxOutputArr selected=$mcxComboSelected}
 		</select>     	
   </TD>
 </TR>	
 <TR>
 <TD>Item Name</TD>
 <TD><SELECT name = cboItemName >
   {section name=sec1 loop=$j}
        <OPTION value={$resI[sec1]}>
          {$resI[sec1]}
        </OPTION>
      {/section}
 </TD>
 </TR>
 <TR><TD>Expiry Date</TD>
    <TD> {html_select_date prefix="expiryDate" start_year=1990 end_year=2025 day_value_format="%02d" month_value_format="%b" month_format="%b" field_order="DMY"}</TD>
 </TR>
</TABLE>
<BR>
<INPUT type="submit" name="submitBtn" Value="SAVE">
</FORM>
</BODY>
</HTML>