<HTML>
<HEAD><TITLE>Brokerage settings</TITLE></HEAD>
<BODY bgColor="#FFCEE7">
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <A href="./index.php">Home</A><BR><BR>
<A href="brokerage.php">Item List</A><BR>
  <TABLE border="1" cellSpacing="0" cellPadding="2">
  <TR>
    <TD colspan="8"><B>Add Item Settings : </B></TD>
  </TR>
  <TR>
    <TD>Exchange</TD>
    <TD>
      <select name="exchange" id="exchange">
        {html_options values=$mcxValuesArr output=$mcxOutputArr selected=$mcxComboSelected}
      </select> 
    </TD>
  </TR>
  <TR>
    <TD>Item</TD>
    <TD><INPUT type='text' name='item' size='15'></TD>
  </TR>  
  <TR>
    <TD>Multiply?</TD>
    <TD><INPUT type='text' name='mulAmount' size='15' value='1'></TD>
  </TR>
  <TR>
    <TD>OneSideBrok</TD>
    <TD><INPUT type='text' name='oneSideBrok' size='15'></TD>
    <TD><B>For example, for Gold : </B></TD>
  </TR>
  <TR>
    <TD>Minimum</TD>
    <TD><INPUT type='text' name='min' size='15'></TD>
    <TD>100</TD>
  </TR>
  <TR>
    <TD>Price Range Start</TD>
    <TD><INPUT type='text' name='rangeStart' size='15'></TD>
    <TD>5800</TD>
  </TR>
  <TR>
    <TD>Price Range End</TD>
    <TD><INPUT type='text' name='rangeEnd' size='15'></TD>
    <TD>6999</TD>
  </TR>
  <TR>
    <TD><INPUT type="submit" name="submitBtn" value="Submit!"></TD>
    <TD><INPUT type="reset" value="Reset"></TD>
  </TR>
  </TABLE>
</TABLE>
 <SCRIPT language="javascript">document.form1.item.focus();</SCRIPT>
  </FORM>
</BODY>
</HTML>