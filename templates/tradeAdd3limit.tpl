<HTML>
<HEAD><TITLE>MCX Order Entry</TITLE>
<STYLE>
{literal}
td {  font-color="white";FONT-SIZE: 12px;}
INPUT {FONT-SIZE: 15px; font-weight: bold;}
SELECT {FONT-SIZE: 15Px; font-weight: bold; }
{/literal}
</STYLE>
<SCRIPT language="javascript">
{literal}
window.name = 'displayAll';
function change()
 {
  var select1value= document.form1.itemId.value;
  var select2value=document.form1.expiryDate;
  select2value.options.length=0;
{/literal}
   {section name=sec1 loop=$k}
    if( select1value=="{$itemId[sec1]}")
  	{literal}{{/literal}
  		{section name=sec2 loop=$l+10}
        {if $expiryDate[sec1][sec2] neq ""}
          select2value.options[{$smarty.section.sec2.index}]=new Option("{$expiryDate[sec1][sec2]}"); 
        {/if}
      {/section}
    {literal}
      document.form1.qty.value={/literal}{$min[sec1]}{literal}
    }
      {/literal}
  {/section}
 }
{literal}
function askConfirm()
{
  if(confirm("Are You Sure You want to Save Record?"))
  {
    document.form1.makeTrade.value=1;
    return true;
  }
  else
    return false;
}
function changeName()
{
  document.form1.changedField.value = "clientId";
  document.form1.submit();
}
function changeItem()
{
  document.form1.changedField.value = "itemId";
  document.form1.submit();
}

var addingQty=0;
function changeQty()
{
  //alert(event.keyCode);
  var qty;
  qty = parseFloat(document.form1.qty.value);
  if(addingQty==0)
    addingQty=qty;
  
  if(parseFloat(qty) || qty==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
    {
      qty=qty+addingQty;
      document.form1.qty.value=qty;
    }
    if(event.keyCode==40)
    {
      qty=qty-addingQty;
      document.form1.qty.value=qty;
    }
  }
}
function changePrice()
{
  var price;
  price = parseFloat(document.form1.priceValue.value);
  if(parseFloat(price) || price==0)   //parseFloat does not consider 0, so we have put 0 in or condition
  {
    if(event.keyCode==38)
      price=price+1;
    if(event.keyCode==33)
      price=price+10;
    if(event.keyCode==40)
      price=price-1;
    if(event.keyCode==34)
      price=price-10;
    if(document.form1.priceValue.value != price)
    {
      document.form1.priceValue.value = price;
      
    }
  }
  document.form1.price.value=price;
  //alert(document.form1.price.value);
}
function itemFromPrice()
{
  var price;
  price = parseFloat(document.form1.price.value);
{/literal}
{$itemFromPriceJS}
{literal}
}

function bodyKeyPress()
{
  var price=(document.form1.priceValue.value);
  if(event.keyCode==107)
  {
    window.document.bgColor="green";
    document.form1.buySellText.value = "Buy";
    document.form1.buySell.value = "Buy";
    return false;
  }
  if(event.keyCode==109)
  {
    window.document.bgColor="pink";
    document.form1.buySellText.value = "Sell";
    document.form1.buySell.value = "Sell";
    return false;
  }
  if(event.keyCode==117)
  {
    orderListWindow=window.open('orderList.php?listOnly=1', 'orderListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==119)
  {
    tradeListWindow=window.open('clientTrades.php', 'tradeListWindow',
                          'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=yes, width=775, height=260, left=1, top=250');
  }
  if(event.keyCode==120)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-1;
  	else
  		price=price+1;
    document.form1.price.value=price;  
    alert(document.form1.price.value);
  }
  if(event.keyCode==121)
  {
  	var price=parseFloat(document.form1.price.value);
  	if(document.form1.buySell.value=='Buy')
  	  price=price-2;
  	else
  		price=price+2;
    document.form1.price.value=price; 
    alert(document.form1.price.value); 
  }
}
function orderTypeChanged()
{
  if(document.form1.orderType.value == "SL")
    document.form1.triggerPrice.disabled = 0;
  else
    document.form1.triggerPrice.disabled = 1;
}
function orderValidityChange()
{
  if(document.form1.orderValidity.value == "GTD")
  {
    document.form1.gtdDateDay.disabled = 0;
    document.form1.gtdDateMonth.disabled = 0;
    document.form1.gtdDateYear.disabled = 0;
    
  }
  else
  {
    document.form1.gtdDateDay.disabled = 1;
    document.form1.gtdDateMonth.disabled = 1;
    document.form1.gtdDateYear.disabled = 1;
    
  }
}
{/literal}
</SCRIPT>
</HEAD>
<BODY bgColor="green" onKeyDown="return bodyKeyPress();" onLoad="orderValidityChange();change();">
  <FORM name="form1" action="{$PHP_SELF}" METHOD="post">
  <INPUT type="hidden" name="changedField" value="">
  <INPUT type="hidden" name="makeTrade" value="0">
  <INPUT type="hidden" name="firstName" value="{$firstName}">
  <INPUT type="hidden" name="middleName" value="{$middleName}">
  <INPUT type="hidden" name="lastName" value="{$lastName}">
  <INPUT type="hidden" name="forStand" value="{$forStand}">
  <INPUT type="hidden" name="price">
<FONT color="white" style="FONT-SIZE: 12px;">
      <SELECT name="orderType" onChange="orderTypeChanged();">
        <option value="RL">RL</option>
        <option value="SL">SL</option>
      </SELECT>
    Date : 
      {html_select_date time="$tradeDateDisplay" prefix="trade" start_year="-1" end_year="+1" month_format="%m" field_order="DMY" day_value_format="%02d"}
      <SELECT name="itemId" onChange="change();">
      {html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOutput"}
      </SELECT>
      &nbsp;&nbsp;
      <SELECT name="expiryDate">
      {html_options selected="$expiryDateSelected" values="$expiryDateValues" output="$expiryDateOutput"}
      </SELECT>
      &nbsp;
<!-- onBlur="itemFromPrice();"-->
      Qty : <INPUT type="text" name="qty" size="5" onKeyDown="changeQty();">
      &nbsp;
      Price : <INPUT size="10" type="text" name="priceValue" value="{$lastPrice}" onKeydown="changePrice();">&nbsp;&nbsp;
      <BR>
      TrigPrice : <INPUT size="10" type="text" name="triggerPrice" value="{$lastTriggerPrice}" DISABLED>&nbsp;&nbsp;
{if $forStand == 1}
      <INPUT type="radio" name="standing" value="-1"> Open Standing
      <INPUT type="radio" name="standing" value="1"> Close Standing
{/if}
      <SELECT name="orderValidity" onChange="orderValidityChange();">
        <option value="EOS">EOS</option>
        <option value="GTD">GTD</option>
        <option value="GTC">GTC</option>
      </SELECT>

      {html_select_date prefix="gtdDate" field_order="DMY"}
      User Remarks : 
      <INPUT type="text" name="clientId">
      <INPUT type="text" name="vendorId" size="3">
      <!--INPUT type="submit" name="tradeBtn" value="Trade" onClick="return askConfirm();"-->
      &nbsp;
      <INPUT type="submit" name="limitBtn" value="Limit" onClick="return askConfirm();">
      &nbsp;
      <BR>
      <INPUT DISABLED type="text" name="buySellText" value="Buy" size="5">
      <INPUT type="hidden" name="buySell" value="Buy">
<!--       <B>{$clientWholeName} : </B>
       Deposit : {$deposit}&nbsp;&nbsp;&nbsp;&nbsp;
       CurrentBal : {$currentBal}&nbsp;&nbsp;&nbsp;&nbsp;
       Total : {$total}&nbsp;&nbsp;&nbsp;&nbsp;  
       Phone   : {$phone}&nbsp;&nbsp;&nbsp;&nbsp;
       Mobile  : {$mobile}&nbsp;
      <BR>-->
<TABLE>
    <TR bgcolor="#D3D3D3">
      <TD>Last trade : </TD>
      <TH>{$lastTradeInfoVar}</TH>
    </TR>
</TABLE>
  {$focusScript}
  {if $currentBuySell == "Sell"}
    <script type = "text/javascript">
	    window.document.bgColor="pink";
	    document.form1.buySellText.value = "Sell";
	    document.form1.buySell.value = "Sell";
    </script>
  {/if}
  </FORM>
</BODY>
</HTML>