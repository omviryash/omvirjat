<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<title>!! JAT !!</title>
<link rel="stylesheet" type="text/css" href="./css/main.css" />
<link rel="stylesheet" type="text/css" href="./css/style.css" />
<script type="text/javascript" src="./js/jquery.min.js"></script>
<link rel="stylesheet" href="css/style1.css" media="screen">
{literal}	
<style type="text/css">
html, body { margin: 0;	padding: 0; font-family:arial; font-size:14px; }
ul.menu { margin: 0px auto 0 auto; }

input:focus
{
	background-color:yellow;
}
select:focus
{
	background-color:yellow;
}
</style>
<script type="text/javascript">
  $(document).ready(function()
  {
  	selectExpiry();
  	$("#itemId").focus();
  	//selectExpiry($("#item").val());
    $(document).keydown(function(e)
    {
      var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
      if(code == 109)
      {
		if($('#exch').val() == 'mcx'){
			$("body").css("background-color","red");
			$(".tbl td").css("background-color","red");
		}
		else{
			$("body").css("background-color","#FE79FE");
			$(".tbl td").css("background-color","#FE79FE");
		}
		
        $("#buySell").val("Sell");
        return false;
      }
       if(code == 107)
      {
		if($('#exch').val() == 'mcx'){
			$("body").css("background-color","blue");
			$(".tbl td").css("background-color","blue");
		}
		else{
			$("body").css("background-color","#419FFC");
			$(".tbl td").css("background-color","#419FFC");
		}
		 
        $("#buySell").val("Buy");
        return false;
      }
     /* if(code == 13)
      {
      	var didConfirm = confirm("Are you sure?");
        if (didConfirm == true) 
        {
         	//callAjax();
			//return false;
        }
		else
		return false;
      }*/
      
    });
	
	
	$("#price").keyup(function(e){
		
		var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// Uparrow
      if(code == 38)
      {
      	var price      = parseFloat($(this).val());
      	
      	$(this).val(price+1)
      } 
       if(code == 40)
      {
      	var price      = parseFloat($(this).val());
      	
      	$(this).val(price-1)
      }
       
		
	});
	
 
	
	
	
	$("#quantity").keyup(function(e){
		
		var code = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
		
		// Uparrow
      if(code == 38)
      {
      	var qty      = $('.min').val();
      	var minQty   = $('#hiddenQty').val();
      	var finalQty = parseFloat(qty) + parseFloat(minQty);
      	$('.min').val(finalQty);
      } 
       if(code == 40)
      {
      	var qty      = $('.min').val();
      	var minQty   = $('#hiddenQty').val();
      	var finalQty = parseFloat(qty) - parseFloat(minQty);
      	$('.minMinus').val(finalQty);
      }
       if(code == 33)
      {
      	var qty      = $('.min').val();
      	var minQty   = $('#hiddenQty').val();
      	if($('#hiddenQty').val() == $('.min').val())
      	{
      	  var finalQty = parseFloat(minQty) * 10;
      	}
      	else
      	{
      		var finalQty = parseFloat($('.min').val()) + parseFloat(parseFloat(minQty) * 10);
      	}
      	$('.minTenPlus').val(finalQty);
      }
       
      if(code == 34)
      {
      	var qty      = $('.min').val();
      	var minQty   = $('#hiddenQty').val();
      	if($('#hiddenQty').val() == $('.min').val())
      	{
      	  var finalQty = parseFloat(minQty) * 10;
      	}
      	else
      	{
      		var finalQty = parseFloat($('.min').val()) - parseFloat(parseFloat(minQty) * 10);
      	}
      	$('.minTenMinus').val(finalQty);
      }
		
	})
	
  });
  
  
 
 
  
	function selectExch(){ 
	//alert($('#exch').val());
	  	if($('#exch').val() == 'mcx')
		{
			if($("#buySell").val() == 'Buy'){
				$("body").css("background-color","blue");
				$(".tbl td").css("background-color","blue");
			}
			else{
				$("body").css("background-color","red");
				$(".tbl td").css("background-color","red");
			}
			
			$('#cmxitemId').css('display','none');
			$('#itemId').css('display','block');
			
			selectExpiry();
		}
		else
		{
			
			if($("#buySell").val() == 'Buy'){
				$("body").css("background-color","#419FFC");
				$(".tbl td").css("background-color","#419FFC");
			}
			else{
				$("body").css("background-color","#FE79FE");
				$(".tbl td").css("background-color","#FE79FE");
			}
			
			$('#itemId').css('display','none');
			$('#cmxitemId').css('display','block');
			
			selectExpiry_cmx();
		}
	}
  
	function selectExpiry()
  {
  	var item = $('#itemId').val();
  	//var exchange = $('#exchange').val();
	var exchange = 'mcx';

    $.ajax({
	    url:'expirySelectAjax.php',
	    data:{ itemVar: item ,exchange : exchange},
	    dataType:'HTML',
	    type:'POST',
	    success:function(msg)
	    {
	      $('#itemExpireDiv').html(msg);
	      $('#quantity').val($('#hiddenQty').val());
	      $('#minHidden').val($('#hiddenQty').val());
	    }
    });
  }
  
  function selectExpiry_cmx()
  { 
  	var item = $('#cmxitemId').val();
  	//var exchange = $('#exchange').val();
	var exchange = 'comex';

    $.ajax({
	    url:'expirySelectAjax.php',
	    data:{ itemVar: item ,exchange : exchange},
	    dataType:'HTML',
	    type:'POST',
	    success:function(msg)
	    { 
	      $('#itemExpireDiv').html(msg);
	      $('#quantity').val($('#hiddenQty').val());
	      $('#minHidden').val($('#hiddenQty').val());
	    }
    });
  }
  
  function callAjax()
  {
    var date         = $('#edd').val() +"-"+ $('#edm').val() +"-"+ $('#edy').val();
    var item         = $('#itemId').val();
    var itemExpire   = $('#itemExpire').val();
    var price        = $('#price').val();
    var client1      = $('#client1').val();
    var quantity     = $('#quantity').val();
    var buySell      = $('#buySell').val();
	{/literal}
    
    var datastring = 'date='+date +'&item='+item +'&itemExpire='+itemExpire +'&price='+price +'&client1='+client1
    +'&quantity='+quantity+'&buySell='+buySell+'&exchange='+{$exchange};
	{literal}

    $.ajax({
      type: "GET",
      url: "ajaxResponce.php",
      data: datastring,
      success:function(data)
      {
        $('#dataDisplay').html(data);
      	$("#itemId").focus();
      }
    });
  }
  </script>
   <style type="text/css" rel="stylesheet">
  	body
  	{
	{/literal}
		{if $exch == 'mcx' && $buySell == 'Buy'}
			background-color:blue;
		{elseif $exch == 'mcx' && $buySell == 'Sell'}
			background-color:red;
		{elseif $exch == 'cmx' && $buySell == 'Buy'}
			background-color:#419FFC;	
		{elseif $exch == 'cmx' && $buySell == 'Sell'}
			background-color:#FE79FE;
		{else}	
			background-color:blue;
		{/if}
			
	{literal}
  	}
	.tbl td{
		{/literal}
		color:#fff;font-size:18px;font-weight:bold;
		{literal}
	}
	.tbl a{
		color:#fff;
	}
    select:focus,
    input:focus
    {
        border: solid 2px #F7847E;
    }
	input,select
	{
		font-size:18px;
	}
  </style>
	{/literal}
</head>
<body>
  <br/><br/><br/>
  <form name="dataForm" method="POST" onsubmit="return confirm('Are you sure?')">
  <input type="hidden" name="buySell" id="buySell" value="{$buySell}"/>
  	<input type="hidden" name="exchange" id="exchange" value="{$exchange}"/>
    <input type="hidden" name="hid_exch" id="hid_exch" value="{$exch}"/>
  	<input type="hidden" name="tradeId" id="tradeId" value="{$tradeId}" />
  	<input type="hidden" name="isedit"  value="{$isedit}" />
    <table class="tbl" border="1" align="center" style="" width="100%" height="60">
      <tr>
        <td>
          Date
        </td>
        <td nowrap>
        	<lable id="dataDate" name="dataDate">
            {html_select_date prefix="curDate" day_id="edd" month_id="edm" year_id="edy" start_year ="-1" end_year="+2" field_order="DMY" month_format="%m" day_value_format="%02d"}
          </lable>
        </td>
        <td>
          Exchange
        </td>
        <td>
          <select name="exch" id="exch" onchange="selectExch();">
            <option {if $updateArray.exch == 'mcx'}selected='selected'{/if} value="mcx">MCX</option>
            <option {if $updateArray.exch == 'cmx'}selected='selected'{/if} value="cmx">CMX</option>
          </select>
          
         
        </td>
        <td>
          Item
        </td>
        <td>
          <select name="itemId" class="itemddl" id="itemId" onchange="selectExpiry();">
            {html_options values=$item.itemId output=$item.itemId}
          </select>
          <select name="cmxitemId" class="itemddl" id="cmxitemId" onchange="selectExpiry_cmx();" style="display:none">
            {html_options values=$cmxitem.itemId output=$cmxitem.itemId}
          </select>
        </td>
        <td name="itemExpire">
          Expriy
        </td>
        <td>
        	<div id="itemExpireDiv">
	        </div>
        </td>
        <td>
          Qty
        </td>
        <td>
          <input type="text" size="5" name="quantity" id="quantity" class="min minMinus minTenPlus minTenMinus" value="{$updateArray.qty}">
        </td>
        <td>
          Price
        </td>
        <td>
          <input type="text"  size="5" name="price" id="price" value="{$updateArray.price}">
        </td>
        <td>
          Client1
        </td>
        <td>
          <input type="text" size="8" name="client1" id="client1" value="{$updateArray.clientId}" >
        </td>
        <td>
          <input type="Submit" value=" Trade " name="btn" >
        </td>
      </tr>
    </table>
    <script type="text/javascript">
		 selectExch();
		 	//alert($('#exch').val());
         </script> 
  </form>
  <br/>
  <div id="dataDisplay">
    <table  class="tbl1" align="center" style="width:100%;background:#fff;color:#000;font-size:16px;text-align:center;font-weight:bold;">
    	<tr>
    	  <td><strong>Client1</strong></td>
    	  <td><strong>Item</strong></td>
          <td><strong>Price</strong></td>
           <td><strong>Qty</strong></td>
           <td><strong>Date</strong></td>
           <td><strong>Time</strong></td>
    	  <td><strong>Expriy</strong></td>
    	 
    	  
    	  
    	  <td align="center">Action</td>
    	</tr>
    	{section name=silTrd loop=$treadeTxtArray}
      <tr style="color:{if ($treadeTxtArray[silTrd].exch=='mcx'&& $treadeTxtArray[silTrd].buySell=='Buy')}blue{elseif ($treadeTxtArray[silTrd].exch=='mcx'&& $treadeTxtArray[silTrd].buySell=='Sell')}red{elseif ($treadeTxtArray[silTrd].exch=='cmx'&&  $treadeTxtArray[silTrd].buySell=='Buy')}#419FFC{else ($treadeTxtArray[silTrd].exch=='cmx'&& $treadeTxtArray[silTrd].buySell=='Sell')}#FE79FE{/if}">
      	<td>{$treadeTxtArray[silTrd].clientId}</td>
      	<td>{$treadeTxtArray[silTrd].itemId}</td>
        <td>{$treadeTxtArray[silTrd].price}</td>
        <td>{$treadeTxtArray[silTrd].qty}</td>
      	
      	<td>{$treadeTxtArray[silTrd].tradeDate}</td>
        <td>{$treadeTxtArray[silTrd].tradeTime}</td>
      	<td>{$treadeTxtArray[silTrd].expiryDate}</td>
      	
      	<td>{if $usertype=='SUPER_ADMIN'}<a href="tradeAddVir.php?tradeId={$treadeTxtArray[silTrd].tradeId}&exchange={$exchange}&ex={$treadeTxtArray[silTrd].exch}">Edit</a>{/if}</td>
      	<!--<td><a href="deleteTradeTxt.php?tradeId={$treadeTxtArray[silTrd].tradeId}&exchange={$exchange}">Delete</a></td>-->
      </tr>
      {/section}
    </table>
  </div>
</body>
</html>