<html>
<head><title>Om : Client Trades !!!</title>
<style>
{literal}
td{font-weight: BOLD}
.lossStyle   {color: red}
.profitStyle {color: blue}
{/literal}
</style>  
<script src="./js/jquery.min.js"></script>
{literal}
<script>
$(document).ready(function(){
	$('#trade_type').change(function(){ 
		$('#form1').submit();
	});
	
    function update_row(){
        var last_mcxid = $('#last_mcxid').val();
		var last_comid = $('#last_comid').val();
		var tradetype = $('#tradetype').val();
        
        $.post('aj-report-rows.php',{tradetype:tradetype, last_mcxid:last_mcxid, last_comid:last_comid},function(data, success){
        	if(success == 'success'){
            	if(data.hasrow == 1){
                	$('#tbl_report').append(data.rows);
					
					if(data.last_mcxid > 0){
                    	$('#last_mcxid').val(data.last_mcxid);
					}
					
					if(data.last_comid > 0){
						$('#last_comid').val(data.last_comid);
					}
               	}
           	}
       	},'json');
    }
    
    setInterval(function(){update_row()}, 10000);
});
</script>
{/literal}
</head>
<body bgColor="#FFFF80">
<form name="form1" id="form1" method="post">
<input type="hidden" id="tradetype" value="{$tradetype}">
<input type="hidden" id="last_comid" value="{if $last_comid==""}-1{else}{$last_com}{/if}">
<input type="hidden" id="last_mcxid" value="{if $last_mcxid==""}-1{else}{$last_mcxid}{/if}">
<select id="trade_type" name="trade_type" style="width:150px;">
<option value="all" {if $tradetype=='all'}selected='selected'{/if}>All</option>
<option value="mcx" {if $tradetype=='mcx'}selected='selected'{/if}>MCX</option>
<option value="com" {if $tradetype=='com'}selected='selected'{/if}>Commex</option>
</select>
<br>
<br>
<table id="tbl_report" border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse">
<tr>
<th></th>
<th></th>
<th colspan="2">Buy</th>
<th colspan="2">Sell</th>
<th colspan="5"></th>
</tr>
<tr>
<th>Trade Type</th>
<th>Buy/Sell</th>
<th>Qty</th>
<th>Price</th>
<th>Qty</th>
<th>Price</th>

<th>Date</th>
<th>Stand</th>
<th>Item</th>
<th>Net Profit Loss</th>
<th>Vendor</th>
</tr>
{foreach from=$trade_ar item=trade}
<tr>
<td>{$trade.tradetype}</td>
<td>{$trade.buysell}</td>
<td>{$trade.b_qty}</td>
<td>{$trade.b_price}</td>
<td>{$trade.s_qty}</td>
<td>{$trade.s_price}</td>
<td>{$trade.tradedate}</td>
<td>{$trade.standing}</td>
<td>{$trade.itemid}</td>
<td>{$trade.netprofileloss}</td>
<td>{$trade.vendor}</td>
</tr>
{/foreach}
</table>
</form>
</body>
</html>
