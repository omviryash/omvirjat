<html>
<head>
<title>Om : Client Trades !!!</title>
<style>
{
literal
}
td {
	font-weight: BOLD
}
.mcx_loss {
	color: red
}
.mcx_profit {
	color: blue
}
.com_loss {
	color: #fe79fe
}
.com_profit {
	color: #419ffc
}
{
/literal
}
</style>
<script src="./js/jquery.min.js"></script>
{literal}
<script>
$(document).ready(function(){
	$('#trade_type').change(function(){ 
		$('#form1').submit();
	});
	
	$('#clientId').change(function(){ 
		$('#form1').submit();
	});
	
	$('#itemId').change(function(){ 
		$('#form1').submit();
	});
	
    function update_row(){
        var last_mcxid = $('#last_mcxid').val();
		var last_comid = $('#last_comid').val();
		var tradetype = $('#tradetype').val();
        
        $.post('aj-report-rows.php',{tradetype:tradetype, last_mcxid:last_mcxid, last_comid:last_comid},function(data, success){
        	if(success == 'success'){
            	if(data.hasrow == 1){
					//$('#tbl_report').find('#tot_row').remove();
                	//$('#tbl_report').append(data.rows);
					$('#tbl_report').prepend(data.rows);
					
					if(data.last_mcxid > 0){
                    	$('#last_mcxid').val(data.last_mcxid);
					}
					
					if(data.last_comid > 0){
						$('#last_comid').val(data.last_comid);
					}
               	}
           	}
       	},'json');
		
		$.post('aj-mcx-item-report.php',function(data, success){
        	if(success == 'success'){
            	if(data.hasrow > 0){
                	$('#tblmcx').html(data.rows);
               	}
           	}
       	},'json');
		
		$.post('aj-com-item-report.php',function(data, success){
        	if(success == 'success'){
            	if(data.hasrow > 0){
                	$('#tblcom').html(data.rows);
               	}
           	}
       	},'json');
    }
    
    setInterval(function(){update_row()}, 10000);
});
</script>
{/literal}
</head>
<body bgColor="#FFFF80">

<div style="width:100%;float:left;margin-bottom:10px;padding-left:20px;"><a href="{$homeurl}">Home</a></div>
<form name="form1" id="form1" method="post">
  <input type="hidden" id="tradetype" value="{$tradetype}">
  <input type="hidden" id="last_comid" value="{if $last_comid == ''}-1{else}{$last_comid}{/if}">
  <input type="hidden" id="last_mcxid" value="{if $last_mcxid == ''}-1{else}{$last_mcxid}{/if}">
  <table border="0" cellpadding="0" cellspacing="0" width="100%" style="width:100%;float:left;margin-bottom:10px;" > 
    <tr>
      <td width="25%"><strong>Exchange:</strong>
        <select id="trade_type" name="trade_type" style="width:150px;">
          <option value="all" {if $tradetype=='all'}selected='selected'{/if}>All</option>
          <option value="mcx" {if $tradetype=='mcx'}selected='selected'{/if}>MCX</option>
          <option value="com" {if $tradetype=='com'}selected='selected'{/if}>Commex</option>
          <option value="F_O" {if $tradetype=='F_O'}selected='selected'{/if}>Share</option>
        </select></td>
      <td width="20%"><strong>Client</strong>
        <SELECT name="clientId" id="clientId">
          
{html_options selected="$clientIdSelected" values="$clientIdValues" output="$clientIdOptions"}

        </SELECT></td>
      <td><strong>Item</strong>
        <SELECT name="itemId" id="itemId">
          
{html_options selected="$itemIdSelected" values="$itemIdValues" output="$itemIdOptions"}

        </SELECT></td>
    </tr>
  </table>
  <br>
  <br>
  <div style="float:left; width:60%">
    <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse; width:100%;float:left">
      <tr>
        <th>Trade Type</th>
        <th>Client</th>
        <th>Item</th>
        <th>Qty</th>
        <th>Price</th>
        <th>Date</th>
        <th>Time</th>
        <th>Expiry Date</th>
        <th>IP</th>
      </tr>
      <tbody id="tbl_report">
      {foreach from=$trade_ar item=trade}
      {if $trade.tradetype == 'MCX'}
      <tr class="{if $trade.buysell == 'Buy'}mcx_profit{else}mcx_loss{/if}"> {else}
      <tr class="{if $trade.buysell == 'Buy'}com_profit{else}com_loss{/if}"> {/if}
        <td>{$trade.tradetype}</td>
        <td>{$trade.clientid}</td>
        <td>{$trade.itemid}</td>
        {if $trade.buysell == 'Buy'}
        <td align="right">{$trade.b_qty}</td>
        <td align="right">{$trade.b_price}</td>
        {else}
        <td align="right">{$trade.s_qty}</td>
        <td align="right">{$trade.s_price}</td>
        {/if}
        <td>{$trade.tradedate}</td>
        <td>{$trade.tradetime}</td>
        <td>{$trade.expirydate}</td>
        <td>{$trade.ipaddress}</td>
      </tr>
      {/foreach}
      </tbody>
      <!--<tr id="tot_row">
        <td><strong>TOTAL</strong></td>
        <td></td>
        <td></td>
        <td align="right">{$tot_buy}</td>
        <td align="right"></td>
        <td align="right">{$tot_sell}</td>
        <td align="right"></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>-->
    </table>
  </div>
  
  <!--BUY SELL TABLE-->
  <div id="div_item_report" style="float:right; width:35%">
	<strong>MCX</strong>
    <div id="tblmcx">
      <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse; width:100%;">
        <tr>
          <th>Item</th>
          <th>Buy</th>
          <th>Sell</th>
          <th>Balance</th>
        </tr>
        {foreach from=$item_mcx_ar item=it}
        <tr>
          <td>{$it.itemname}</td>
          <td align="right" class="mcx_profit">{$it.buy}</td>
          <td align="right" class="mcx_loss">{$it.sell}</td>
          <td align="right" class="{if $it.difference >= 0}mcx_profit{else}mcx_loss{/if}">{$it.difference}</td>
        </tr>
        {/foreach}
      </table>
    </div>
    <br>
    <strong>COMEX</strong>
    <div id="tblcom">
      <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse; width:100%;">
        <tr>
          <th>Item</th>
          <th>Buy</th>
          <th>Sell</th>
          <th>Balance</th>
        </tr>
        {foreach from=$item_com_ar item=it}
        <tr>
          <td>{$it.itemname}</td>
          <td align="right" class="com_profit">{$it.buy}</td>
          <td align="right" class="com_loss">{$it.sell}</td>
          <td align="right" class="{if $it.difference >= 0}com_profit{else}com_loss{/if}">{$it.difference}</td>
        </tr>
        {/foreach}
      </table>
    </div>
    <br>
    <strong>Share</strong>
    <div id="tblcom">
      <table border="1" cellpadding="3" cellspacing="3" style="border-collapse:collapse; width:100%;">
        <tr>
          <th>Item</th>
          <th>Buy</th>
          <th>Sell</th>
          <th>Balance</th>
        </tr>
        {foreach from=$item_share_ar item=it}
        <tr>
          <td>{$it.itemname}</td>
          <td align="right" class="com_profit">{$it.buy}</td>
          <td align="right" class="com_loss">{$it.sell}</td>
          <td align="right" class="{if $it.difference >= 0}com_profit{else}com_loss{/if}">{$it.difference}</td>
        </tr>
        {/foreach}
      </table>
    </div>
  </div>
</form>
</body>
</html>
