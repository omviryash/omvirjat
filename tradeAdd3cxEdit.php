<?php session_start(); ?><?php
include "etc/om_config.inc";

$currentBuySell  = isset($_REQUEST['buySell']) ? $_REQUEST['buySell'] : "Buy";
$smarty = new SmartyWWW();

///////////////////////////  For Edit :Start///////////////  
$tradeId   = isset($_REQUEST['tradeId']) ? $_REQUEST['tradeId'] : 0;
$tradeUpdateData = array();
$tradeUpdateData['tradeId']    = 0;
$tradeUpdateData['tradeDate']  = date("Y-m-d");
$tradeUpdateData['itemId']     = "";
$tradeUpdateData['expiryDate']     = "";
$tradeUpdateData['qty']        = 0;
$tradeUpdateData['price']      = 0;
$tradeUpdateData['userRemarks']= 0;
$selectTradeForEdit = "SELECT tradeId,tradeDate,itemId,expiryDate,qty,price,userRemarks,buySell
                         FROM tradetxtcx
                        WHERE tradeId = ".$_REQUEST['tradeId'];
$selectTradeForEditRes = mysql_query($selectTradeForEdit);
if($tradeDataRow = mysql_fetch_assoc($selectTradeForEditRes))
{
  $tradeUpdateData['tradeId']     = $tradeDataRow['tradeId'];
  $tradeUpdateData['tradeDate']   = $tradeDataRow['tradeDate'];
  $tradeUpdateData['itemId']      = $tradeDataRow['itemId'];
  $tradeUpdateData['expiryDate']  = $tradeDataRow['expiryDate'];
  $tradeUpdateData['qty']         = $tradeDataRow['qty'];
  $tradeUpdateData['price']       = $tradeDataRow['price'];
  $tradeUpdateData['userRemarks'] = $tradeDataRow['userRemarks'];
  $tradeUpdateData['buySell']     = $tradeDataRow['buySell'];
}
if(isset($_POST['tradeId']) && strlen($_POST['tradeId']) > 0)
{
  $standing    = isset($_POST['standing']) ? $_POST['standing'] : 0;
  $userRemarks = isset($_POST['clientId']) ? $_POST['clientId'] : "";
  $expiryDate  = isset($_POST['expiryDate']) ? $_POST['expiryDate'] : "";
  
  if(isset($_POST['clientId']) && $_POST['clientId'] > 0)
  {
    $selectClientQuery = "SELECT * FROM client
                          WHERE clientId = ".$_POST['clientId'];
    $selectClientResult = mysql_query($selectClientQuery);
    if($selectClientRow = mysql_fetch_array($selectClientResult))
    {
      $clientId   = $_POST['clientId'];
      $firstName  = $selectClientRow['firstName'];
      $middleName = $selectClientRow['middleName'];
      $lastName   = $selectClientRow['lastName'];
      $clientWholeName = $selectClientRow['firstName']." ".$selectClientRow['middleName']." ".$selectClientRow['lastName'];
    }
    else
    {
      $clientId   = 0;
      $firstName  = '';
      $middleName = '';
      $lastName   = '';
      $clientWholeName = '';      
    }
  }
/////////////////////////////////////
  $vendor = "_SELF";
  if($_POST['vendorId'] > 0)
  {
    $vendorIdQuery = "SELECT * FROM vendor
                      WHERE vendorId = ".$_POST['vendorId'];
    $vendorResult = mysql_query($vendorIdQuery);
    while($vendorRow = mysql_fetch_array($vendorResult))
      $vendor = $vendorRow['vendor'];
  } 
  
  $edit_tradeQuery = "SELECT tradeId,clientId,firstName,
                          buySell,itemId,qty,price FROM tradetxtcx Where tradeId='".$_REQUEST['tradeId']."'";
	  $edit_tradeResult = mysql_query($edit_tradeQuery);
	  while($tradeRow = mysql_fetch_array($edit_tradeResult))
	  {
		$insertLogQuery  = "INSERT INTO log 
                         (tradeId,standing,old_clientId,clientId,firstName,
                          old_buySell,buySell,old_itemId,itemId,tradeDate,tradeTime,old_qty,qty,old_price,price,expiryDate,vendor,userRemarks,logType)
                      VALUES ('".$_REQUEST['tradeId']."','".$standing."','".$tradeRow["clientId"]."' , '".$clientId."', 
                              '".$firstName."',
							  '".$tradeRow["buySell"]."' ,
                              '".$_POST['buySell']."',
							  '".$tradeRow["itemId"]."' ,
                              '".$_POST['itemId']."', ''".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."'','".date("H:i:s",time())."',
							  '".$tradeRow["qty"]."' ,
                              '".$_POST['qty']."','".$tradeRow["price"]."' , '".$_POST['priceValue']."',
                              '".$expiryDate."', '".$vendor."','".$userRemarks."','Edit Trade CX'
                             )";
      $result = mysql_query($insertLogQuery);	
	   }
	   
  $updateTradeTxt = "UPDATE tradetxtcx 
		                    SET standing = '".$standing."',clientId = '".$clientId."',firstName = '".$firstName."',
		                        middleName = '".$middleName."',lastName = '".$lastName."',buySell = '".$_POST['buySell']."',
		                        itemId = '".$_POST['itemId']."',
		                        tradeDate = '".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."',
		                        qty = '".$_POST['qty']."', price = '".$_POST['priceValue']."',
		                        expiryDate = '".$expiryDate."', vendor = '".$vendor."',userRemarks = '".$userRemarks."'
		                  WHERE tradeId = ".$_REQUEST['tradeId'];
  $updateTradeTxtResult = mysql_query($updateTradeTxt);
  if(!$updateTradeTxtResult)
  {
    die("Record Not Updated : ". mysql_error()); 
  }
  else
  {
	  
	/*$insertLogQuery  = "INSERT INTO log 
                         (tradeId,standing,clientId,firstName,
                          buySell,itemId,tradeDate,tradeTime,qty,price,expiryDate,vendor,userRemarks,logType)
                      VALUES ('".$_REQUEST['tradeId']."','".$standing."', '".$clientId."', 
                              '".$firstName."',
                              '".$_POST['buySell']."',
                              '".$_POST['itemId']."', '".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."','".date("H:i:s",time())."',
                              '".$_POST['qty']."', '".$_POST['priceValue']."',
                              '".$expiryDate."', '".$vendor."','".$userRemarks."','Edit Trade CX'
                             )";
     mysql_query($insertLogQuery);*/
	    
    ////////////////////////////// High low Setting : Start ///
    $selectQuery1="SELECT high,low,itemId
                     FROM itemdatewise
                    WHERE itemDate = '".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."'";
    $result=mysql_query($selectQuery1);
    while($row=mysql_fetch_array($result))
    {
			$updateCxQuery = "UPDATE tradetxtcx 
			                     SET highLowConf = 1
			  	               WHERE tradeDate = '".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."'
                        AND itemId = '".$row['itemId']."'
                        AND (price < ".$row['low']." 
                         OR price > ".$row['high'].")"; 
			$updateCxQueryRes = mysql_query($updateCxQuery);   
			if(!$updateCxQueryRes)
			{
			  die("Update Query Error COMEX 1 : ". mysql_error());          
			}          

			$updateCxQuery = "UPDATE tradetxtcx 
			                     SET highLowConf = 0
			  	               WHERE tradeDate = '".$_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."'
                        AND itemId = '".$row['itemId']."'
                        AND price >= ".$row['low']." 
                        AND price <= ".$row['high'];
			$updateCxQueryRes = mysql_query($updateCxQuery);   
			if(!$updateCxQueryRes)
			{
			  die("Update Query Error COMEX 2 : ". mysql_error());          
			}          
    }        
    ////////////////////////////// High low Setting : End  ///      	
  }
  header("Location: clientTradesComex.php"); 
}
///////////////////////////  For Edit :End  ///////////////  
/////////////////////////
  $firstName  = '';
  $middleName = '';
  $lastName   = '';
  $selectQuery = "SELECT * FROM client
                  ORDER BY firstName, middleName, lastName
                 ";
  if(isset($_POST['clientId']))
    $currentClientId = $_POST['clientId'];
  else
    $currentClientId = 0;
  
  $result = mysql_query($selectQuery);
  
  $clientIdValues = array();
  $clientIdOutput = array();
  $i = 0;
  while($row = mysql_fetch_array($result))
  {
    if($currentClientId == 0)
    {
      $currentClientId = $row['clientId'];
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    }
    
    if($row['clientId'] == $currentClientId)
    {
      $clientIdSelected = $row['clientId'];
      $firstName  = $row['firstName'];
      $middleName = $row['middleName'];
      $lastName   = $row['lastName'];
      $clientWholeName = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
      //////////////////////////////////////////////
      $currentBal = $row['currentBal'];
      $deposit = $row['deposit'];
      $phone   = $row['phone'];
      $mobile  = $row['mobile'];
      //////////////////////////////////////////////
    }
    
    $clientIdValues[$i] = $row['clientId'];
    $clientIdOutput[$i] = $row['firstName']." ".$row['middleName']." ".$row['lastName'];
    $i++;
  }
///////////////////////////////////////////////////////

  if(isset($_POST['tradeDay']))
  {
    $tradeDateDisplay = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."-";
    
    $_SESSION['tradeYear']  = $_POST['tradeYear'];
    $_SESSION['tradeMonth'] = $_POST['tradeMonth'];
    $_SESSION['tradeDay']   = $_POST['tradeDay'];
  }
    
  if(count($clientIdValues) > 0)
  {
    if(isset($_POST['price']))
      $lastPrice = $_POST['price'];
    else
      $lastPrice = '';
  
    if(isset($_POST['buySell']))
      $buySellSelected  = $_POST['buySell'];
    else
      $buySellSelected  = "Buy";
    $buySellValues = array("Buy", "Sell");
    $buySellOutput = array("Buy", "Sell");
  ////    
    if(isset($_POST['tradeDay']))
      $tradeDateDisplay = $_POST['tradeYear']."-".$_POST['tradeMonth']."-".$_POST['tradeDay']."-";
    else
      $tradeDateDisplay = date("Y-m-d");
  
  ///////////////////////////////////////////////////////
    $minQty = 0;
    $selectItemQuery = "SELECT * FROM item 
                         WHERE exchange = 'COMEX'
                      ORDER BY itemId";
    if(isset($_POST['itemId']))
      $currentItemId = $_POST['itemId'];
    else
      $currentItemId = '';
    
    $itemResult = mysql_query($selectItemQuery);
    
    $i = 0;
    $itemIdSelected = '';
    $itemIdValues = array();
    $itemIdOutput = array();
    $itemFromPriceJS = '';
    while($itemRow = mysql_fetch_array($itemResult))
    {
      if($currentItemId == '')
        $currentItemId = $itemRow['itemId'];
      
      if($itemRow['itemId'] == $currentItemId)
      {
        $itemIdSelected = $itemRow['itemId'];
        $minQty = $itemRow['min'];
      }
      
      $itemIdValues[$i] = $itemRow['itemId'];
      $itemIdOutput[$i] = $itemRow['itemId'];
      
      //For JavaScript of itemFromPrice :Start
      if(!is_null($itemRow['rangeStart']) && !is_null($itemRow['rangeEnd']))
      {
        $itemFromPriceJS .= "  if(price >= ".$itemRow['rangeStart']." && price <= ".$itemRow['rangeEnd'].")\n";
        $itemFromPriceJS .= "  {\n";
        $itemFromPriceJS .= "    document.form1.itemId.selectedIndex = ".$i.";\n";
        $itemFromPriceJS .= "    changeItem();\n";
        $itemFromPriceJS .= "  }\n";
      }
      //For JavaScript of itemFromPrice :End
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $expiryQuery = "SELECT * FROM expiry
                      WHERE itemId = '".$currentItemId."'";
    $expiryResult = mysql_query($expiryQuery);
    
    $i = 0;
    $expiryDateSelected = '';
    $expiryDateValues  = array();
    $expiryDateOutput  = array();
    while($expiryRow = mysql_fetch_array($expiryResult))
    {
      $expiryDateValues[$i] = $expiryRow['expiryDate'];
      $expiryDateOutput[$i] = $expiryRow['expiryDate'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $vendorQuery = "SELECT * FROM vendor ORDER BY vendor";
    $vendorResult = mysql_query($vendorQuery);
    
    $i = 0;
    $vendorSelected = '';
    $vendorValues  = array();
    $vendorOutput  = array();
    while($vendorRow = mysql_fetch_array($vendorResult))
    {
      if(isset($_POST['vendor']) && $_POST['vendor'] == $vendorRow['vendor'])
        $vendorSelected = $vendorRow['vendor'];
      $vendorValues[$i] = $vendorRow['vendor'];
      $vendorOutput[$i] = $vendorRow['vendor'];
      $i++;
    }
  /////////////////////////////////////////////
  /////////////////////////////////////////////
    $lastTradeInfoVar = '';
    if(isset($_POST['submitBtn']))
    {
      $clientInfoQuery  = "SELECT * FROM client
                                WHERE clientId = ".$_POST['clientId'];
      $clientInfoResult = mysql_query($clientInfoQuery);
      
      while($clientInfoRow = mysql_fetch_array($clientInfoResult))
      {
        $nameToDisplay = $clientInfoRow['firstName']." ".$clientInfoRow['middleName']." ".$clientInfoRow['lastName'];
      }
      
      if(isset($_POST['standing']) && $_POST['standing']==1)
        $standDisplay = " * Close Standing";
      elseif(isset($_POST['standing']) && $_POST['standing']==2)
        $standDisplay = " * Open Standing";
      else
        $standDisplay = "";
      if(isset($_POST['expiryDate']) && strlen($_POST['expiryDate']) > 0)
        $expiryDate = $_POST['expiryDate'];
      else
        $expiryDate = "";
//      $lastTradeInfoVar = $nameToDisplay." * Date : ".$_POST['tradeDay']."-".$_POST['tradeMonth']."-".$_POST['tradeYear']." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty'].$standDisplay." * Vendor : ".$_POST['vendor'];
    }
//////////////////////////////////////////////////
    if(isset($_POST['changedField']) && $_POST['changedField'] == "itemId")
      $focusScript = '<SCRIPT language="javascript">document.form1.itemId.focus();</SCRIPT>';
    elseif(isset($_POST['changedField']) && $_POST['changedField'] == "clientId")
      $focusScript = '<SCRIPT language="javascript">document.form1.clientId.focus();</SCRIPT>';
    else
      $focusScript = '<SCRIPT language="javascript">document.form1.itemId.focus();</SCRIPT>';
  //////////////////////////////////////////////////
    if(isset($_GET['forStand']))
      $forStand = $_GET['forStand'];
    elseif(isset($_POST['forStand']))
      $forStand = $_POST['forStand'];
    else
      $forStand = 0;
  //////////////////////////////////////////////////
  
///////
  if(isset($_POST['limitBtn']))
    $limitTrade = $_POST['limitBtn'];
  elseif(isset($_POST['tradeBtn']))
    $limitTrade = $_POST['tradeBtn'];
  else
    $limitTrade = 0;
    
  if (isset($_POST['makeTrade']) && $_POST['makeTrade'] == 1)
    $lastTradeInfoVar = $_POST['clientId']." * ".$limitTrade." * Price : ".$_POST['price']." * ".$_POST['itemId']." ".$expiryDate." * ".$_POST['buySell']." * Qty : ".$_POST['qty']." * Vendor : ".$_POST['vendor'];
  else
    $lastTradeInfoVar = "";
///////

   $selectItem="SELECT * FROM item ORDER BY itemId";
  
  $resultItem=mysql_query($selectItem);
  if(!$resultItem)
  {
    echo "No Item Selected";
  }
  else
  {
    $k=0;
    while($row=mysql_fetch_array($resultItem))
    {
      $itemId[$k]=$row['itemId'];
      $min[$k]=$row['min'];
      
      //SELECTION OF EXPIRYDATE:START
      
      $selectExpiry="SELECT * FROM expiry WHERE itemId='".$itemId[$k]."'";
      
      $resultExpiry=mysql_query($selectExpiry);
      if(!$resultExpiry)
      {
        echo "No Expiry Selected";
      }
      else
      {
        $l=0;
        while($row=mysql_fetch_array($resultExpiry))
        {
          $expiryDate[$k][$l]=$row['expiryDate'];
          $expiryDateSelected[$l]=$row['expiryDate'];
          $l++;
          $smarty->assign("expiryDate",$expiryDate);
          //$smarty->assign("expiryDateSelected",$expiryDateSelected);
        }
      }
      //SELECTION OF EXPIRYDATE:END
  
      $k++;
      $smarty->assign("itemId",$itemId);
      $smarty->assign("min",$min);
      $smarty->assign("l",$l);
      $smarty->assign("k",$k);
      
    }
  }
  //SELECTION OF ITEM:END
    
    if(isset($_SESSION['tradeDay']))
      $tradeDateDisplay = $_SESSION['tradeYear']."-".$_SESSION['tradeMonth']."-".$_SESSION['tradeDay']."-";
	  
	$tradeQuery = "SELECT color,sodatype FROM tradecolor where tradetype='COMEX'";
	$tradeResult = mysql_query($tradeQuery);
	while($tradeRow = mysql_fetch_array($tradeResult))
	{
		if($tradeRow['sodatype']=='Buy')
		{
			$buycolor=$tradeRow['color'];
		}
		if($tradeRow['sodatype']=='Sell')
		{
			$sellcolor=$tradeRow['color'];
		}
	}   

    $smarty->assign("tradeId",  $tradeId);
    $smarty->assign("tradeUpdateData",  $tradeUpdateData);
    $smarty->assign("itemFromPriceJS",  $itemFromPriceJS);
    $smarty->assign("PHP_SELF",         $_SERVER['PHP_SELF']);
    $smarty->assign("firstName",        $firstName);
    $smarty->assign("middleName",       $middleName);
    $smarty->assign("lastName",         $lastName);
    $smarty->assign("tradeDateDisplay", $tradeDateDisplay);
    $smarty->assign("buySellSelected",  $buySellSelected);
    $smarty->assign("buySellValues",    $buySellValues);
    $smarty->assign("buySellOutput",    $buySellOutput);
    $smarty->assign("itemIdSelected",   $itemIdSelected);
    $smarty->assign("itemIdValues",     $itemIdValues);
    $smarty->assign("itemIdOutput",     $itemIdOutput);
    $smarty->assign("expiryDateSelected", $expiryDateSelected);
    $smarty->assign("expiryDateValues"  , $expiryDateValues);
    $smarty->assign("expiryDateOutput"  , $expiryDateOutput);
    $smarty->assign("lastPrice"         , $lastPrice);
    $smarty->assign("vendorSelected"    , $vendorSelected);
    $smarty->assign("vendorValues"      , $vendorValues);
    $smarty->assign("vendorOutput"      , $vendorOutput);
    $smarty->assign("clientWholeName"   , $clientWholeName);
    $smarty->assign("minQty"            , $minQty);
    $smarty->assign("lastTradeInfoVar"  , $lastTradeInfoVar);
    $smarty->assign("focusScript"  ,      $focusScript);
    $smarty->assign("forStand"          , $forStand);
    $smarty->assign("currentBuySell"          , $currentBuySell);
	$smarty->assign("buycolor",         $buycolor);
    $smarty->assign("sellcolor",         $sellcolor);
    $smarty->display("tradeAdd3cxEdit.tpl");
  }
  else
    echo "No clients added !";
?>