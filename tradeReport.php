<?php
session_start();
if(!isset($_SESSION['user'])) {
    header("Location:login.php?goTo=clientTrades");
	exit;
}

if(!isset($_SESSION['toDate'])) {
	header("Location: selectDtSession.php?goTo=tradeReport");
	exit;
} else {
	include "./etc/om_config.inc";
	include "./etc/functions.inc";
	$smarty = new SmartyWWW();
	
	$message = "";
	
	$lastMCX_rs = mysql_query("select max(tradeid) as lastid from tradetxt");
	$lastMCX_row = mysql_fetch_array($lastMCX_rs);
	
	$lastCom_rs = mysql_query("select max(tradeid) as lastid from tradetxtcx");
	$lastCom_row = mysql_fetch_array($lastCom_rs);
	
	$i = 0;
	
	$where = '';
	
	if(trim($_POST['clientId']) !="" && strtoupper($_POST['clientId']) != 'ALL'){
		$where .= " and clientId='".$_POST['clientId']."'";
	}
	
	if(trim($_POST['itemId']) !="" && strtoupper($_POST['itemId']) != 'ALL'){
		$where .= " and itemId='".$_POST['itemId']."'";
	}
	  
	$sql_mcx = "SELECT tradeId, buySell, qty, price, tradeDate, tradeTime, clientId, itemId, expiryDate, vendor, 'MCX' as tradetype, ipaddress  FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'".$where;
	$sql_com = "SELECT tradeId, buySell, qty, price, tradeDate, tradeTime, clientId, itemId, expiryDate, vendor, 'Commex' as tradetype, ipaddress  FROM tradetxtcx where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'".$where;
	$sql_share = "SELECT tradeId, buySell, qty, price, tradeDate, tradeTime, clientId, itemId, expiryDate, vendor, 'F_O' as tradetype, ipaddress  FROM tradetxtf_o where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'".$where;


	/*ADDED ON 27-MAY-2015*/  
	$data_mcx_tot_buy = mysql_query("SELECT SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) AS buy FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_mcx_tot_buy = mysql_fetch_array($data_mcx_tot_buy);
	$mcx_tot_buy = $row_mcx_tot_buy['buy'];
	
	/*ADDED ON 27-MAY-2015*/   
	$data_mcx_tot_sell = mysql_query("SELECT SUM(CASE buySell WHEN 'Sell' THEN qty ELSE 0 END) AS sell FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_mcx_tot_sell = mysql_fetch_array($data_mcx_tot_sell);
	$mcx_tot_sell = $row_mcx_tot_sell['sell'];
	
	/*ADDED ON 27-MAY-2015*/  
	$data_com_tot_buy = mysql_query("SELECT SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) AS buy FROM tradetxtcx where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_com_tot_buy = mysql_fetch_array($data_com_tot_buy);
	$com_tot_buy = $row_com_tot_buy['buy'];
		  
	/*ADDED ON 27-MAY-2015*/  		  
	$data_com_tot_sell = mysql_query("SELECT SUM(CASE buySell WHEN 'Sell' THEN qty ELSE 0 END) AS sell FROM tradetxtcx where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_com_tot_sell = mysql_fetch_array($data_com_tot_sell);
	$com_tot_sell = $row_com_tot_sell['sell'];
	
	/*ADDED ON 12-APR-2016*/  
	$data_share_tot_buy = mysql_query("SELECT SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) AS buy FROM tradetxtf_o where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_share_tot_buy = mysql_fetch_array($data_share_tot_buy);
	$share_tot_buy = $row_share_tot_buy['buy'];
		  
	/*ADDED ON 12-APR-2016*/  		  
	$data_share_tot_sell = mysql_query("SELECT SUM(CASE buySell WHEN 'Sell' THEN qty ELSE 0 END) AS sell FROM tradetxtf_o where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."'");
	$row_share_tot_sell = mysql_fetch_array($data_share_tot_sell);
	$share_tot_sell = $row_share_tot_sell['sell']; 
	
	
		  
	if(isset($_POST['trade_type']) && trim($_POST['trade_type'])!=""){
	  if($_POST['trade_type'] == 'mcx'){
		  $tradesQuery = $sql_mcx;
		  $tot_buy = $mcx_tot_buy;
		  $tot_sell = $mcx_tot_sell;
	  }
	  elseif($_POST['trade_type'] == 'com'){
		  $tradesQuery = $sql_com;
		  $tot_buy = $com_tot_buy;
		  $tot_sell = $com_tot_sell;
	  }
	  elseif($_POST['trade_type'] == 'F_O'){
		  $tradesQuery = $sql_share;
		  $tot_buy = $share_tot_buy;
		  $tot_sell = $share_tot_sell;
	  }
	  elseif($_POST['trade_type'] == 'all'){
		  $tradesQuery = $sql_mcx.' UNION '.$sql_com.' UNION '.$sql_share;
		  
		  $tot_buy = $mcx_tot_buy + $com_tot_buy + $share_tot_buy;
		  $tot_sell = $mcx_tot_sell + $com_tot_sell + $share_tot_sell;
	  }
	}
	else{
	  $tradesQuery = $sql_mcx.' UNION '.$sql_com.' UNION '.$sql_share;
	  
	  $tot_buy = $mcx_tot_buy + $com_tot_buy + $share_tot_buy;
	  $tot_sell = $mcx_tot_sell + $com_tot_sell + $share_tot_sell;
	}
	$tradesQuery." ORDER BY tradeId DESC";
	$trade_rs = mysql_query($tradesQuery." ORDER BY tradeId DESC");
	
	$trade_ar = array();
	while($trade_row = mysql_fetch_array($trade_rs)){
	$trade_ar[$i]['tradetype'] = $trade_row['tradetype'];
	$trade_ar[$i]['buysell'] = $trade_row['buySell'];
	
	if(strtoupper($trade_row['buySell']) == "BUY"){
		$trade_ar[$i]['b_qty'] = $trade_row['qty'];
		$trade_ar[$i]['b_price'] = $trade_row['price'];
		$trade_ar[$i]['s_qty'] = "";
		$trade_ar[$i]['s_price'] = "";
	}
	elseif(strtoupper($trade_row['buySell']) == "SELL"){
		$trade_ar[$i]['s_qty'] = $trade_row['qty'];
		$trade_ar[$i]['s_price'] = $trade_row['price'];
		$trade_ar[$i]['b_qty'] = "";
		$trade_ar[$i]['b_price'] = "";
	}
	
	$trade_ar[$i]['tradedate'] = date('d-m-y', strtotime($trade_row['tradeDate']));
	$trade_ar[$i]['tradetime'] = $trade_row['tradeTime']; 
	$trade_ar[$i]['itemid'] = $trade_row['itemId'];
	$trade_ar[$i]['expirydate'] = $trade_row['expiryDate'];
	$trade_ar[$i]['netprofileloss'] = "";
	$trade_ar[$i]['clientid'] = $trade_row['clientId']!="0"?$trade_row['clientId']:"";
	$trade_ar[$i]['ipaddress'] = $trade_row['ipaddress'];
	
	$i++;
	}
	
	if(isset($_POST['trade_type']) && trim($_POST['trade_type']) != ""){
	$tradetype = $_POST['trade_type'];
	}
	else{
	 $tradetype = "all"; 
	}
	
	/*********BUY/SELL SUMMARY - START***********/
	$sql_item_mcx = "SELECT CONCAT(itemid,' ',expirydate) AS item, SUM(CASE buySell WHEN  'Buy' THEN qty ELSE 0 END) AS buy, SUM(CASE `buySell`
	WHEN 'Sell' THEN `qty` ELSE 0 END) AS sell, SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) - SUM(CASE `buySell` WHEN 'Sell' THEN `qty`
	ELSE 0 END) AS difference FROM tradetxt where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."' GROUP BY  itemid, expirydate";
	
	$sql_item_com = "SELECT CONCAT(itemid,' ',expirydate) AS item, SUM(CASE buySell WHEN  'Buy' THEN qty ELSE 0 END) AS buy, SUM(CASE `buySell`
	WHEN 'Sell' THEN `qty` ELSE 0 END) AS sell, SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) - SUM(CASE `buySell` WHEN 'Sell' THEN `qty`
	ELSE 0 END) AS difference FROM tradetxtcx  where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."' GROUP BY itemid, expirydate";		
	
	$sql_item_share = "SELECT CONCAT(itemid,' ',expirydate) AS item, SUM(CASE buySell WHEN  'Buy' THEN qty ELSE 0 END) AS buy, SUM(CASE `buySell`
	WHEN 'Sell' THEN `qty` ELSE 0 END) AS sell, SUM(CASE buySell WHEN 'Buy' THEN qty ELSE 0 END) - SUM(CASE `buySell` WHEN 'Sell' THEN `qty`
	ELSE 0 END) AS difference FROM tradetxtf_o  where tradeDate >= '".$_SESSION['fromDate']."' AND tradeDate <= '".$_SESSION['toDate']."' GROUP BY itemid, expirydate";		
	
	/*if(isset($_POST['trade_type']) && trim($_POST['trade_type'])!=""){
	  if($_POST['trade_type'] == 'mcx'){
		  $sql_item = $sql_item_mcx;
	  }
	  elseif($_POST['trade_type'] == 'com'){
		  $sql_item = $sql_item_com;
	  }
	  elseif($_POST['trade_type'] == 'all'){
		  $sql_item = $sql_item_mcx.' UNION ALL '.$sql_item_com;
	  }
	}
	else{
	  $sql_item = $sql_item_mcx.' UNION ALL '.$sql_item_com;
	}*/
	
	//MCX
	$item_mcx_rs = mysql_query($sql_item_mcx);
	
	$i = 0;
	$item_mcx_ar = array();
	while($item_row = mysql_fetch_array($item_mcx_rs)){
		$item_mcx_ar[$i]['itemname'] = $item_row['item'];
		$item_mcx_ar[$i]['tradetype'] = $item_row['tradetype'];
		$item_mcx_ar[$i]['buy'] = $item_row['buy'];
		$item_mcx_ar[$i]['sell'] = $item_row['sell'];
		$item_mcx_ar[$i]['difference'] = $item_row['difference'];
		
		$i++;
	}

	//COM
	$item_com_rs = mysql_query($sql_item_com);
	
	$i = 0;
	$item_com_ar = array();
	while($item_row = mysql_fetch_array($item_com_rs)){
		$item_com_ar[$i]['itemname'] = $item_row['item'];
		$item_com_ar[$i]['tradetype'] = $item_row['tradetype'];
		$item_com_ar[$i]['buy'] = $item_row['buy'];
		$item_com_ar[$i]['sell'] = $item_row['sell'];
		$item_com_ar[$i]['difference'] = $item_row['difference'];
		
		$i++;
	}
	
	//SHARE
	
	$item_share_rs = mysql_query($sql_item_share);
	
	$i = 0;
	$item_share_ar = array();
	while($item_row = mysql_fetch_array($item_share_rs)){
		$item_share_ar[$i]['itemname'] = $item_row['item'];
		$item_share_ar[$i]['tradetype'] = $item_row['tradetype'];
		$item_share_ar[$i]['buy'] = $item_row['buy'];
		$item_share_ar[$i]['sell'] = $item_row['sell'];
		$item_share_ar[$i]['difference'] = $item_row['difference'];
		
		$i++;
	}
	/*********BUY/SELL SUMMARY - END***********/
	
	/*********CLIENT INFO FOR DDL - START***********/
	$clientIdSelected = isset($_REQUEST['clientId'])? $_REQUEST['clientId']:0;
	$clientIdValues = array();
	$clientIdOptions = array();
	$clientInfo      = array();
	$i = 0;
	$clientIdValues[0]  = 'All';
	$clientIdOptions[0] = 'All';
	$i++;
	
	$clientQuery = "SELECT * FROM client WHERE 1 = 1 ";
	
	/*if(isset($_REQUEST['groupName']) && $_REQUEST['groupName'] != 'All')
	{
	  $clientQuery    .= " AND groupName = '".$groupIdSelected."'";
	} */
	 
	$clientQuery .= " ORDER BY firstName, middleName, lastName";
	$clientResult = mysql_query($clientQuery);
	
	while($clientRow = mysql_fetch_array($clientResult)) {
		$clientIdValues[$i] = $clientRow['clientId'];
		$clientIdOptions[$i] = $clientRow['firstName']." ".$clientRow['middleName']." ".$clientRow['lastName'];
	
		$clientInfo[$clientRow['clientId']]['deposit'] = $clientRow['deposit'];
		$i++;
	}
	/*********CLIENT INFO FOR DDL - END***********/
	
	/*********ITEM INFO FOR DDL - START***********/
	if(isset($_REQUEST['itemId'])){
		$currentItemId = $_REQUEST['itemId'];
	}else{
		$currentItemId = "All";
	}
	
	$itemIdSelected = $currentItemId;
	$itemIdValues = array();
	$itemIdOptions = array();
	$itemCount = 0;
	$itemIdValues[0]  = "All";
	$itemIdOptions[0] = "All";
	$itemCount++;
	
	//$itemRecords = array();
	//$clientBrok  = array();
	//$clientBrok1 = array();
	//$clientBrok2 = array();
	$itemQuery = "SELECT * FROM item ";
	
	if(isset($_REQUEST['trade_type']) && $_REQUEST['trade_type'] != "all"){
		$itemQuery .= " WHERE exchange LIKE '".$_REQUEST['trade_type']."'";
	}
	
	$itemQuery .= " ORDER BY itemId";
	$itemResult = mysql_query($itemQuery);
	
	while($itemRow = mysql_fetch_array($itemResult)){
		//$itemRecords[$itemRow['itemId']]['priceOn'] = $itemRow['priceOn'];
		//$itemRecords[$itemRow['itemId']]['min']     = $itemRow['min'];
		//$itemRecords[$itemRow['itemId']]['high']    = $itemRow['high'];
		//$itemRecords[$itemRow['itemId']]['low']     = $itemRow['low'];
		
		//ClientBrok :Start
		/*for($i=0;$i<count($clientIdValues);$i++){
		  $clientBrok[$clientIdValues[$i]][$itemRow['itemId']]  = $itemRow['oneSideBrok'];
		  $clientBrok1[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
		  $clientBrok2[$clientIdValues[$i]][$itemRow['itemId']] = $itemRow['oneSideBrok'];
		}*/
		//ClientBrok :End
		
		$itemIdValues[$itemCount]  = $itemRow['itemId'];
		$itemIdOptions[$itemCount] = $itemRow['itemId'];
		$itemCount++;
	}
	
	if(isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == "SUPER_ADMIN"){
		$homeurl="index.php";
	}
	elseif(isset($_SESSION['user']['type']) && $_SESSION['user']['type'] == "ENTRY_USER"){
		$homeurl="menu2.php";
	}
	else{
		$homeurl="menu.php";
	}
	/*********ITEM INFO FOR DDL - END***********/
	
	$smarty->assign("clientIdSelected", $clientIdSelected);
	$smarty->assign("clientIdValues",   $clientIdValues);
	$smarty->assign("clientIdOptions",  $clientIdOptions);
	$smarty->assign("itemIdSelected",   $itemIdSelected);
	$smarty->assign("itemIdValues",     $itemIdValues);
	$smarty->assign("itemIdOptions",    $itemIdOptions);
	$smarty->assign("tot_buy", $tot_buy);
	$smarty->assign("tot_sell", $tot_sell);
	$smarty->assign("tradetype", $tradetype);
	$smarty->assign("trade_ar", $trade_ar);
	$smarty->assign("item_mcx_ar", $item_mcx_ar);
	$smarty->assign("item_com_ar", $item_com_ar);
	$smarty->assign("item_share_ar", $item_share_ar);
	$smarty->assign("last_mcxid", $lastMCX_row['lastid']);
	$smarty->assign("last_comid", $lastCom_row['lastid']);
	$smarty->assign("homeurl", $homeurl);
	$smarty->display("tradeReport.tpl");
}
mysql_close();
?>