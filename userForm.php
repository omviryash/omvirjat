<?php
  session_start();
  include "etc/om_config.inc";

  if($_SERVER['REQUEST_METHOD'] == "POST") {
    $sql_qry = '';
    if(isset($_GET['id'])) {
      if(isset($_POST['password']) && $_POST['password'] != "") {
        $_POST['password'] = md5($_POST['password']);
      } else {
        unset($_POST['password']);
      }
      $params = array();
      foreach ($_POST as $key => $value) {
        # code...
        array_push($params, "`" . $key . "`='" . $value . "'");
      }
      $sql_qry = "UPDATE `user` SET " . implode(", ", $params) . " WHERE `id`='" . $_GET['id'] . "';";
    } else {
      $_POST['password'] = md5($_POST['password']);
      $sql_qry = "INSERT INTO `user` (`" . implode("`, `", array_keys($_POST)) . "`) VALUES ('" . implode("', '", array_values($_POST)) . "');";
    }
    if($sql_qry == "") {
      echo "Unexpected query";
    } else {
      mysql_query($sql_qry);
      echo "Inserted successfully";
    }
    ?> <a href="users.php">Back to list</a> <?php
    exit;
  }

  $user = array();

  if(isset($_GET['id'])) {
    $sql_qry = "SELECT `username`, `type` FROM `user` WHERE `id`=".$_GET['id'].";";
    $result = mysql_query($sql_qry);
    $user = mysql_fetch_array($result);
  }

?>
<html>
  <head>
    <title>User form</title>
  </head>
  <body>
    <strong>User details</strong>
    <form method="POST">
      <table>
        <tr>
          <td>UserName:</td>
          <td><input type="text" name="username" value="<?=isset($user['username'])?$user['username']:''?>" ></td>
        </tr>
        <tr>
          <td>Password:</td>
          <td><input type="password" name="password"></td>
        </tr>
        <tr>
          <td>Type</td>
          <td>
            <select name="type">
              <option value="SUPER_ADMIN"  <?=isset($user['type'])&&$user['type']=="SUPER_ADMIN"?"selected='selected'":''?> >Super Admin</option>
              <option value="READ_ONLY" <?=isset($user['type'])&&$user['type']=="READ_ONLY"?"selected='selected'":''?>>Read only</option>
            </select>
          </td>
        </tr>
        <tr>
          <td> &nbsp; </td>
          <td> <input type="submit" value="Save" /> </td>
        </tr>
      </table>
    </form>
  </body>
</html>